﻿Public Class afSupportSubmission2
    Inherits System.Web.UI.Page

    Public bShowValues As Boolean = False
    Public sReturnURLToCaller As String = ""
    Public sHTMLPassthroughForm As String = ""

    Public oFormSupCode As clsFormSupportCore

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ProcessPostedData()

        End If

    End Sub

    Protected Sub ProcessPostedData()
        Dim oRow As TableRow, oCell As TableCell

        Try

            oFormSupCode = New clsFormSupportCore

            oFormSupCode.tblPostStringValues = Me.tblPostStringValues
            oFormSupCode.tblQueryStringValues = Me.tblQueryStringValues
            oFormSupCode.tblRecoveredStringValues = Me.tblRecoveredStringValues

            oFormSupCode.ProcessHTTPRequest(My.Request, Page, clsFormSupportCore.StagesToDo.AttachmentsDocsAndEmails)

            For Each oFile In oFormSupCode.AssociatedFiles.SubmissionFiles.Values
                oRow = New TableRow
                oCell = New TableCell()
                oCell.Text = oFile.FileName
                oRow.Cells.Add(oCell)
                oCell = New TableCell()
                oCell.Text = oFile.FileData.Length.ToString
                oRow.Cells.Add(oCell)
                oCell = New TableCell()
                oCell.Text = oFile.oSavedFileInfo.Extension
                oRow.Cells.Add(oCell)
                Me.tblFilesPosted.Rows.Add(oRow)

            Next

        Catch ex As Exception

        End Try

    End Sub

End Class