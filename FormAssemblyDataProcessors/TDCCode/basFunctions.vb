﻿Imports System.IO
Imports System.Management
Imports System.Xml



Module basFunctions

    Public Function DeriveURLParentPath(ByVal oURL As Uri) As String
        Dim sThisPath As String = oURL.Scheme & Uri.SchemeDelimiter & oURL.Authority & Right(Uri.SchemeDelimiter, 1)
        Dim sDelim As String = Right(Uri.SchemeDelimiter, 1), sPiece As String = ""

        For iPos = 0 To UBound(oURL.Segments) - 1
            If oURL.Segments(iPos) <> sDelim And oURL.Segments(iPos) <> "" Then
                sPiece = oURL.Segments(iPos)
                sThisPath &= sPiece & IIf(Right(sPiece, 1) <> sDelim, sDelim, "").ToString
            End If
        Next
        If Right(sThisPath, 1) = sDelim & sDelim Then sThisPath = Left(sThisPath, Len(sThisPath) - 1)

        Return sThisPath

    End Function



    Public Sub UpdateApplicationSetting(ByVal SettingID As String, ByVal sValue As String)
        Dim oCfg As System.Configuration.Configuration, oAppSettings As System.Configuration.AppSettingsSection
        Dim oSetting As KeyValueConfigurationElement

        Try

            oCfg = Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~")
            oAppSettings = oCfg.AppSettings
            oSetting = CType(oAppSettings.Settings(SettingID), KeyValueConfigurationElement)

            If oSetting Is Nothing Then
                oAppSettings.Settings.Add(SettingID, sValue)
            Else
                If Not oSetting.IsReadOnly Then
                    oSetting.Value = sValue
                    oCfg.Save()
                End If
            End If

        Catch ex As Exception
            Dim oErr As wRuntimeError = goErrs.NewError(ex)
            gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")

        End Try
    End Sub



    Public Function DoRegexReplacements(ByVal sStringIn As String, ByVal sRegex As String, ByVal sGroupName As String, ByVal sReplacement As String) As String
        Dim oMatch As Match, oGroup As Group
        Dim sReturn As String = sStringIn, sTemp As String, iLastPos As Integer
        ' Important note: the match need to be redone after each replacement as the string source changes, but the match remembers the pre replacement string.

        Try

            iLastPos = 0
            oMatch = Regex.Match(sReturn, sRegex, RegexOptions.IgnoreCase)
            Do While oMatch.Success
                oGroup = oMatch.Groups.Item(sGroupName)
                Do While oGroup.Index + oGroup.Length <= iLastPos
                    oMatch.NextMatch()
                    If Not oMatch.Success Then Exit Do ' This is now trying to do a match that has happened before
                Loop
                If Not oMatch.Success Then Exit Do
                iLastPos = oGroup.Index + Len(sReplacement)
                If oGroup.Index > 0 Then sTemp = Left(sReturn, oGroup.Index) Else sTemp = ""
                sTemp &= sReplacement & Mid(sReturn, oGroup.Index + oGroup.Length + 1)
                sReturn = sTemp
                oMatch = Regex.Match(sReturn, sRegex, RegexOptions.IgnoreCase)
            Loop

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            gsErr = goErrs.ToString

        End Try

        Return sReturn

    End Function



    Public Function DoRegexBlockRemoval(ByVal sStringIn As String, ByVal sRegexStart As String, ByVal sRegexEnd As String) As String
        Dim sReturn As String = sStringIn, sTemp As String, sTempStart As String = "", sTempEnd As String = ""
        Dim oMatchStart As Match, oMatchEnd As Match, iStart As Integer

        Try
            oMatchStart = Regex.Match(sReturn, sRegexStart, RegexOptions.IgnoreCase)
            Do While oMatchStart.Success
                iStart = oMatchStart.Index
                sTempStart = ""

                If iStart > 0 Then sTempStart = Left(sReturn, iStart)
                sTemp = Mid(sReturn, iStart + oMatchStart.Length - 1)

                oMatchEnd = Regex.Match(sTemp, sRegexEnd, RegexOptions.IgnoreCase)
                If Not oMatchEnd.Success Then Exit Do

                sTempEnd = ""
                If oMatchEnd.Index + oMatchEnd.Length < Len(sTemp) Then sTempEnd = Mid(sTemp, oMatchEnd.Index + oMatchEnd.Length + 1)

                sReturn = sTempStart & sTempEnd

                oMatchStart = Regex.Match(sReturn, sRegexStart, RegexOptions.IgnoreCase)

            Loop

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            gsErr = goErrs.ToString

        End Try
        Return sReturn
    End Function



    Public Function StringIsTrue(StrVal As String) As Boolean
        Dim bReturn As Boolean = False
        Try

            If IsNumeric(StrVal) And StrVal <> "" Then
                bReturn = Val(StrVal) <> 0
            Else
                bReturn = (StrVal = "True" Or StrVal = "T" Or StrVal = "Yes" Or StrVal = "Y")
            End If
        Catch ex As Exception
            Dim oErr As wRuntimeError = goErrs.NewError(ex)
        End Try
        Return bReturn
    End Function



    Public Function LoadIfFile(sData As String, aFilePathSpecific As SortedList(Of String, String), aHTMLSpecific As SortedList(Of String, String)) As String
        Dim oSubstitutes As clsValueSubstitutions, sResult As String = ""
        Dim oFileInfo As FileInfo, oReader As StreamReader, sTemp As String

        Try

            If sData.Length < 255 And (sData.LastIndexOf("\") > 0 Or sData.LastIndexOf("/") > 0) Then
                oSubstitutes = New clsValueSubstitutions(sData, aFilePathSpecific, clsValueSubstitutions.TargetModifier.ForFilePath)
            Else
                oSubstitutes = New clsValueSubstitutions(sData, aHTMLSpecific)
            End If
            sResult = oSubstitutes.Result
            If Regex.IsMatch(sResult, GetMySetting("FilePathRegex").ToString) Then
                oFileInfo = New FileInfo(sResult)
                If oFileInfo.Exists Then
                    oReader = oFileInfo.OpenText
                    sTemp = oReader.ReadToEnd
                    oReader.Close()
                    oSubstitutes = New clsValueSubstitutions(sData, aHTMLSpecific)
                    sResult = oSubstitutes.GetSubstitutionForString(sTemp)
                End If
            End If

        Catch ex As Exception

        End Try

        Return sResult
    End Function

    Public Function LoadIfFile(sData As String, aFilePathSpecific As SortedList, aHTMLSpecific As SortedList) As String
        Dim sReturn As String = "", sKey As String, sValue As String
        Dim aFilePathSpecificNew As SortedList(Of String, String), aHTMLSpecificNew As SortedList(Of String, String)
        Try

            aFilePathSpecificNew = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
            For Each sKey In aFilePathSpecific.Keys
                sValue = aFilePathSpecific.Item(sKey).ToString
                aFilePathSpecificNew.Add(sKey, sValue)
            Next
            aHTMLSpecificNew = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
            For Each sKey In aHTMLSpecific.Keys
                sValue = aHTMLSpecific.Item(sKey).ToString
                aHTMLSpecificNew.Add(sKey, sValue)
            Next

            sReturn = LoadIfFile(sData, aFilePathSpecificNew, aHTMLSpecificNew)

        Catch ex As Exception

        End Try

        Return sReturn
    End Function

    Public Function LoadFile(sPath As String) As String
        Dim sReturn As String = sPath, sTemp As String = ""
        Dim oFileInfo As FileInfo
        Try
            sTemp = GetMySetting("FilePathRegex").ToString
            If Regex.IsMatch(sPath, sTemp) Then
                'If File.Exists(sPath) Then sReturn = File.ReadAllText(sPath)
                oFileInfo = New FileInfo(sPath)
                If oFileInfo.Exists Then sReturn = File.ReadAllText(sPath)
            End If


        Catch ex As Exception
            Dim oErr As wRuntimeError = goErrs.NewError(ex)
        End Try
        Return sReturn
    End Function



    Public Function ObjectSerializedToString(oObj As Object) As String
        Dim oMemStream As MemoryStream = Nothing, oXMLSerializer As Serialization.XmlSerializer = Nothing, oXMLWriter As XmlTextWriter = Nothing
        Dim sReturn As String = "", aVals() As Byte
        Try

            oMemStream = New MemoryStream
            oXMLSerializer = New Serialization.XmlSerializer(oObj.GetType())
            oXMLWriter = New XmlTextWriter(oMemStream, Encoding.Unicode)
            oXMLSerializer.Serialize(oXMLWriter, oObj)
            oMemStream = DirectCast(oXMLWriter.BaseStream, IO.MemoryStream)
            aVals = oMemStream.ToArray()
            sReturn = Encoding.Unicode.GetString(aVals)

        Catch ex As Exception
            Dim oErr As wRuntimeError = goErrs.NewError(ex)
        Finally
            If oXMLWriter IsNot Nothing Then oXMLWriter.Close()
            oXMLWriter = Nothing
            oXMLSerializer = Nothing
            If oMemStream IsNot Nothing Then oMemStream.Close() : oMemStream.Dispose()
            oMemStream = Nothing
        End Try

        Return sReturn
    End Function



    Public Function ConvertSQLMetaToRegex(ByVal sSQLMeta As String) As String
        Dim sReturn As String = "", sTemp As String = "", aTemp() As String, iPos As Integer, iMax As Integer
        Dim sMetaChars As String = "\.+*?$^{}[]()", sChar As String = ""
        Try

            sTemp = Replace(Replace(sSQLMeta, "*", "%"), "?", "_")
            For iPos = 1 To sMetaChars.Length
                sChar = Mid(sMetaChars, iPos, 1)
                sTemp = Replace(sTemp, sChar, "\" & sChar)
            Next
            aTemp = Split(sTemp, "|")
            iMax = UBound(aTemp)

            For iPos = 0 To iMax
                sTemp = aTemp(iPos)
                If sTemp <> "" Then
                    sTemp = Replace(Replace(Replace(Replace(Replace(Replace(sTemp, "\", "\\"), ".", "\."), "?", "\?"), "*", "\*"), "*", "%"), "?", "_")
                    If Left(sTemp, 1) <> "%" Then sTemp = "^" & sTemp
                    If Right(sTemp, 1) <> "%" Then sTemp = sTemp & "$"
                    sTemp = Replace(sTemp, "%", ".*")
                    sTemp = Replace(sTemp, "_", ".")
                    sTemp = "(" & sTemp & ")"
                    aTemp(iPos) = sTemp

                End If
            Next

            sReturn = Join(aTemp, "|")

        Catch ex As Exception
            Dim oErr As wRuntimeError = goErrs.NewError(ex)

        End Try
        Return sReturn
    End Function



    Public Function GetMySetting(SettingName As String) As Object
        Dim oReturn As Object = Nothing
        Try
            oReturn = ConfigurationManager.AppSettings(SettingName)
            Try
                If oReturn Is Nothing Then oReturn = My.Settings.Item(SettingName) : 
            Catch e As Exception : End Try
            If oReturn Is Nothing Then oReturn = ""


        Catch ex As Exception
            Dim oErr As wRuntimeError = goErrs.NewError(ex)

        End Try
        Return oReturn
    End Function


    Public Function SanitizeFilePath(FilePath As String, Optional HasDirectory As Boolean = True) As String
        Dim sReturn = FilePath, sKey As String
        Dim aSubst As New Hashtable From {
                {"\", "_"},
                {"/", "_"},
                {"<", "["},
                {">", "]"},
                {":", ";"},
                {"""", "'"},
                {"|", "!"},
                {"?", "~"},
                {"*", "+"}}

        Try

            If HasDirectory Then
                If aSubst.ContainsKey("/") Then aSubst.Remove("/")
                If aSubst.ContainsKey("\") Then aSubst.Remove("\")
            End If

            For Each sKey In aSubst.Keys
                If FilePath.IndexOf(sKey) >= 0 Then FilePath = FilePath.Replace(sKey, aSubst.Item(sKey).ToString)
            Next

        Catch ex As Exception

            Dim oErr As wRuntimeError = goErrs.NewError(ex)
        End Try

        Return sReturn
    End Function


End Module
