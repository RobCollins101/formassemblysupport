﻿Imports System.IO

Public Class colAssociatedFiles
    Inherits SortedList(Of String, clsAttachedFileData)

    Protected Friend oParentCore As clsFormSupportCore

    Public Sub New()
        MyBase.New(StringComparer.OrdinalIgnoreCase)
    End Sub

    Public Overloads Function Add(sFilename As String, oAttachedFileData As clsAttachedFileData) As clsAttachedFileData
        Try
            Me.Add(oAttachedFileData)

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            oError.AddDetail("Filename : " & sFilename)
            gsErr = goErrs.ToString

        End Try
        Return oAttachedFileData
    End Function

    Public Overloads Function Add(oAttachedFileData As clsAttachedFileData) As clsAttachedFileData
        Try
            If Me.ContainsKey(oAttachedFileData.FileName) Then
                Me.Item(oAttachedFileData.FileName) = oAttachedFileData
            Else
                Me.Add(oAttachedFileData.FileName, oAttachedFileData)
            End If
            oAttachedFileData.oParentCollection = Me

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            gsErr = goErrs.ToString

        End Try

        Return oAttachedFileData
    End Function


    Public Function NewAttachment(LoadedFilePath As String, aFileData As Byte()) As clsAttachedFileData
        Dim oAttachedFileData As clsAttachedFileData = Nothing
        Try

            oAttachedFileData = New clsAttachedFileData(LoadedFilePath, aFileData)
            oAttachedFileData.oParentCollection = Me

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            oError.AddDetail("File path : " & LoadedFilePath)
            gsErr = goErrs.ToString

        End Try
        Return oAttachedFileData
    End Function


    Public Function NewAttachment(LoadedFilePath As String, aFileData As Stream) As clsAttachedFileData
        Dim oAttachedFileData As clsAttachedFileData = Nothing
        Try

            oAttachedFileData = New clsAttachedFileData(LoadedFilePath, aFileData)
            oAttachedFileData.oParentCollection = Me

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            oError.AddDetail("File Path : " & LoadedFilePath)
            gsErr = goErrs.ToString

        End Try
        Return oAttachedFileData
    End Function


    Public Function SubmissionFiles() As SortedList(Of String, clsAttachedFileData)
        Dim oList As New SortedList(Of String, clsAttachedFileData)(StringComparer.OrdinalIgnoreCase)
        Dim sKey As String
        Try

            For Each sKey In Me.Keys
                If Me.Item(sKey).IsFromSubmission Then oList.Add(sKey, Me.Item(sKey))
            Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            gsErr = goErrs.ToString

        End Try
        Return oList
    End Function

    Public Function SubmissionFilesAsList() As ArrayList
        Dim oReturn As New ArrayList, oAtt As clsAttachedFileData
        Try
            For Each oAtt In SubmissionFiles.Values
                oReturn.Add(oAtt.SavedFilePath)
            Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If oAtt IsNot Nothing Then oError.AddDetail("Attachment Path : " & oAtt.SavedFilePath)
            gsErr = goErrs.ToString

        End Try
        Return oReturn
    End Function



    Public Function AttachToEmailFiles() As SortedList(Of String, clsAttachedFileData)
        Dim oList As New SortedList(Of String, clsAttachedFileData)(StringComparer.OrdinalIgnoreCase)
        Dim sKey As String = ""
        Try

            For Each sKey In Me.Keys
                If Me.Item(sKey).CanAttachToEmail Then oList.Add(sKey, Me.Item(sKey))
            Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKey <> "" Then oError.AddDetail("Key : " & sKey)
            gsErr = goErrs.ToString

        End Try
        Return oList
    End Function
    Public Function AttachToEmailFilesAsList() As ArrayList
        Dim oReturn As New ArrayList, oAtt As clsAttachedFileData
        Try
            For Each oAtt In AttachToEmailFiles.Values
                oReturn.Add(oAtt.SavedFilePath)
            Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If oAtt IsNot Nothing Then oError.AddDetail("Attachment Path : " & oAtt.SavedFilePath)
            gsErr = goErrs.ToString

        End Try
        Return oReturn
    End Function



    Public Function ListOnEmail() As SortedList(Of String, clsAttachedFileData)
        Dim oList As New SortedList(Of String, clsAttachedFileData)(StringComparer.OrdinalIgnoreCase)
        Dim sKey As String
        Try

            For Each sKey In Me.Keys
                If Me.Item(sKey).CanListOnEmail Then oList.Add(sKey, Me.Item(sKey))
            Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKey <> "" Then oError.AddDetail("Key : " & sKey)
            gsErr = goErrs.ToString

        End Try
        Return oList
    End Function


    Public Function ListOnEmailFilesAsList() As ArrayList
        Dim oReturn As New ArrayList, oAtt As clsAttachedFileData
        Try
            For Each oAtt In ListOnEmail.Values
                oReturn.Add(oAtt.SavedFilePath)
            Next
        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If oAtt IsNot Nothing Then oError.AddDetail("Attachment Path : " & oAtt.SavedFilePath)
            gsErr = goErrs.ToString

        End Try
        Return oReturn
    End Function


    Public Sub UpdateStoredValues()
        Dim sLoadedFileListInternal As String, sLoadedFileListExternal As String
        Dim aTemp As New ArrayList, aAtt As clsAttachedFileData, sTemp As String
        Try

            oParentCore.ApplyValueIfExists("CountOfLoadedFilesValLabels", SubmissionFiles.Count.ToString)
            For Each aAtt In SubmissionFiles.Values
                sTemp = "<a href=""" & aAtt.DirectoryName & """ target=""_new"">" & aAtt.DirectoryName & "</a> " & aAtt.FileName
                aTemp.Add(sTemp)
            Next
            sLoadedFileListInternal = Strings.Join(aTemp.ToArray, vbCrLf)
            oParentCore.ApplyValueIfExists("EmailInternalListOfLoadedFilesValLabels", sLoadedFileListInternal)

            For Each aAtt In SubmissionFiles.Values
                sTemp = aAtt.FileName
                aTemp.Add(sTemp)
            Next
            sLoadedFileListExternal = Strings.Join(aTemp.ToArray, vbCrLf)
            oParentCore.ApplyValueIfExists("EMailExternalListOfLoadedFilesValLabels", sLoadedFileListExternal)


        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If aAtt IsNot Nothing Then oError.AddDetail("Attached File : " & aAtt.FileName)
            gsErr = goErrs.ToString

        End Try

    End Sub

End Class



Public Class clsAttachedFileData
    Friend oParentCollection As colAssociatedFiles

    Private sFileName As String = ""
    Public OriginalFilePath As String = ""
    Public FileData As Byte()

    Public LogRecordLineTemplate As String = ""
    Public iDocFileNo As Integer = 0

    Public IsFromSubmission As Boolean = False
    Public CanAttachToEmail As Boolean = False
    Public CanListOnEmail As Boolean = False

    Public oSavedFileInfo As FileInfo
    Public sSavedFilePath As String = ""
    Public sSavedFolder As String = ""



    Public Sub New()

    End Sub


    Public Sub New(SavedFilePathIn As String)

        Me.SavedFilePath = SavedFilePathIn


    End Sub



    Public Property FileName As String
        Get
            If sFileName = "" And oSavedFileInfo IsNot Nothing Then
                sFileName = oSavedFileInfo.Name
            ElseIf sFileName = "" And SavedFilePath <> "" Then
                sFileName = IO.Path.GetFileName(SavedFilePath)
            End If
            Return sFileName
        End Get
        Set(value As String)
            If value <> sFileName Then
                sFileName = value
                oSavedFileInfo = Nothing
            End If

        End Set
    End Property



    Public Property SavedFilePath As String
        Get
            If sSavedFilePath = "" And oSavedFileInfo IsNot Nothing Then
                sSavedFilePath = oSavedFileInfo.FullName

            ElseIf sSavedFilePath = "" And sSavedFolder <> "" Then
                sSavedFilePath = sSavedFolder & IIf(sSavedFolder.Substring(sSavedFolder.Length - 1) <> "\", "\", "").ToString & sFileName
                oSavedFileInfo = New FileInfo(sSavedFilePath)

            ElseIf sSavedFilePath = "" Then
                UpdateStoredValues()
                sSavedFolder = ""
                sSavedFilePath = oParentCollection.oParentCore.GetValueIfExists("SaveAttachmentsPathValLabels", True)
                oSavedFileInfo = New FileInfo(sSavedFilePath)
            End If

            Return sSavedFilePath
        End Get
        Set(value As String)
            If value <> sSavedFilePath Then
                oSavedFileInfo = New FileInfo(SavedFilePath)
                sSavedFilePath = value
            End If
        End Set
    End Property



    Public Property DirectoryName As String
        Get
            Dim sReturn As String = ""
            If sSavedFolder <> "" Then
                sReturn = sSavedFolder
            ElseIf SavedFilePath <> "" And oSavedFileInfo IsNot Nothing Then
                sReturn = oSavedFileInfo.DirectoryName
            ElseIf SavedFilePath <> "" Then
                sReturn = IO.Path.GetDirectoryName(SavedFilePath)
            End If
            Return sReturn
        End Get
        Set(value As String)
            If value <> sSavedFolder Then
                sSavedFolder = value
                sSavedFilePath = ""
                oSavedFileInfo = Nothing
            End If
        End Set
    End Property



    Public Sub New(LoadedFilePath As String, aFileData As Byte())

        FileName = LoadedFilePath
        FileData = aFileData

    End Sub



    Public Sub New(LoadedFilePath As String, oStream As Stream)
        Dim oMemoryStream As MemoryStream

        Try
            FileName = LoadedFilePath
            oMemoryStream = New IO.MemoryStream
            oStream.CopyTo(oMemoryStream)

            FileData = oMemoryStream.ToArray
            oMemoryStream.Close()
            oMemoryStream.Dispose()
            oMemoryStream = Nothing

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            oError.AddDetail("File Path : " & LoadedFilePath)
            gsErr = goErrs.ToString

        End Try
    End Sub



    Public Sub UpdateStoredValues()
        Dim oTemp As Object, sTemp As String
        Try

            oParentCollection.oParentCore.ApplyValueIfExists("AssociatedFileOriginalPathValLabels", OriginalFilePath, True)

            If sSavedFilePath <> "" Then
                oSavedFileInfo = New FileInfo(sSavedFilePath)
                oParentCollection.oParentCore.ApplyValueIfExists("AssociatedFileFullPathValLabels", oSavedFileInfo.FullName, True)
                oParentCollection.oParentCore.ApplyValueIfExists("AssociatedFileFolderValLabels", oSavedFileInfo.DirectoryName, True)
            Else
                oTemp = GetMySetting("DefaultStoreFolderDest")
                sTemp = FileName
                If TypeOf (oTemp) Is String Then sTemp = oTemp.ToString : sTemp &= IIf(sTemp.Substring(sTemp.Length - 1) <> "\", "\", "").ToString & FileName
                oSavedFileInfo = New FileInfo(FileName)
                oParentCollection.oParentCore.ApplyValueIfExists("AssociatedFileFullPathValLabels", "", True)
                oParentCollection.oParentCore.ApplyValueIfExists("AssociatedFileFolderValLabels", "", True)
            End If
            oParentCollection.oParentCore.ApplyValueIfExists("AssociatedFileNameValLabels", oSavedFileInfo.Name, True)
            oParentCollection.oParentCore.ApplyValueIfExists("AssociatedFileExtensionValLabels", oSavedFileInfo.Extension.Replace(".", ""), True)

            oParentCollection.oParentCore.ApplyValueIfExists("AssociatedFilePageNumberValLabels", iDocFileNo.ToString, True)

        Catch ex As Exception

        End Try
    End Sub



    Public Sub SaveFileData()
        Dim oStream As FileStream
        Try
            If oSavedFileInfo Is Nothing And SavedFilePath <> "" Then oSavedFileInfo = New FileInfo(SavedFilePath)
            UpdateStoredValues()
            If oSavedFileInfo IsNot Nothing Then 'AndAlso oSavedFileInfo.Exists Then
                oStream = oSavedFileInfo.OpenWrite
                oStream.Write(FileData, 0, FileData.Length)
                oStream.Flush()
                oStream.Close()
            End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If oSavedFileInfo IsNot Nothing Then oError.AddDetail("File : " & oSavedFileInfo.FullName)
            gsErr = goErrs.ToString


        End Try
    End Sub



    Public Sub LoadFileData()
        Dim oStream As FileStream
        Try
            If oSavedFileInfo Is Nothing And SavedFilePath <> "" Then oSavedFileInfo = New FileInfo(SavedFilePath)
            UpdateStoredValues()
            If oSavedFileInfo IsNot Nothing Then
                oStream = oSavedFileInfo.OpenRead
                oStream.Read(FileData, 0, CInt(oSavedFileInfo.Length))
                oStream.Close()
            End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If oSavedFileInfo IsNot Nothing Then oError.AddDetail("File : " & oSavedFileInfo.FullName)
            gsErr = goErrs.ToString


        End Try
    End Sub



    Public Property DataAsString As String
        Get
            Dim sReturn As String

            sReturn = System.Text.Encoding.Unicode.GetString(FileData)

            Return sReturn
        End Get
        Set(value As String)
            FileData = System.Text.Encoding.Unicode.GetBytes(value)
        End Set
    End Property



    Public Sub WriteToFile()
        Try

            'If SavedFilePath <> "" Then
            UpdateStoredValues()

            SaveFileData()

            'File.WriteAllBytes(SavedFilePath, FileData)
            'oSavedFileInfo = New FileInfo(SavedFilePath)

            'End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            gsErr = goErrs.ToString


        End Try
    End Sub



    Public Sub WriteToLogFile()
        Try

            UpdateStoredValues()
            oParentCollection.oParentCore.LogFileUpd.SaveLogLine("")

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            gsErr = goErrs.ToString


        End Try
    End Sub

End Class