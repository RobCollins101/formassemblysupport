﻿Option Compare Text

Public Class clsFormSupportCore
    Friend oFieldDataUsed As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

    Friend oRequest As HttpRequest
    Private sSessionID As String
    Friend sSubmissionTypeCode As String

    Friend oCurrentData As clsCurrentData

    Friend tblRecoveredStringValues As Table
    Friend tblQueryStringValues As Table
    Friend tblPostStringValues As Table
    Friend sReturnURLToCaller As String = ""

    Friend oPage As Page
    Friend bShowValues As Boolean

    Private oValuesFromHTTPRequest As clsValuesFromHTTPRequest
    Private oValuesFromXML As clsValuesFromStoredXML
    Private oRequestFiles As clsAttachmentsOfHTTPRequest
    Private oLogFileUPd As clsLogFileUpdater
    Private oRequestEmails As clsGeneratedEmailsForRequest
    Private oRequestDoc As colGeneratedDocumentsForRequest
    Private oAssociatedFiles As colAssociatedFiles
    Private oConfigDetails As clsBaseConfigFromXML

    Public Enum StagesToDo
        AttachmentsDocsAndEmails
        AttachmentsDocs
        AttachmentsEmails
        AttachmentsOnly
        FieldsOnly
    End Enum


    Friend Property ValuesFromHTTPRequest As clsValuesFromHTTPRequest
        Get
            If oValuesFromHTTPRequest Is Nothing Then
                oValuesFromHTTPRequest = New clsValuesFromHTTPRequest
                oValuesFromHTTPRequest.oParentCore = Me
                oValuesFromHTTPRequest.ProcessFieldValues()
            End If
            oValuesFromHTTPRequest.oParentCore = Me
            Return oValuesFromHTTPRequest
        End Get
        Set(value As clsValuesFromHTTPRequest)
            oValuesFromHTTPRequest = value
            oValuesFromHTTPRequest.oParentCore = Me
        End Set
    End Property


    Friend Property ValuesFromXML As clsValuesFromStoredXML
        Get
            If oValuesFromXML Is Nothing Then
                oValuesFromXML = New clsValuesFromStoredXML
                oValuesFromXML.oParentCore = Me
                oValuesFromXML.LoadFromXML()
            End If
            oValuesFromXML.oParentCore = Me
            Return oValuesFromXML
        End Get
        Set(value As clsValuesFromStoredXML)
            oValuesFromXML = value
            oValuesFromXML.oParentCore = Me
        End Set
    End Property


    Friend Property ConfigDetailsFromXML As clsBaseConfigFromXML
        Get
            If oConfigDetails Is Nothing Then
                oConfigDetails = New clsBaseConfigFromXML
                oConfigDetails.oParentCore = Me
                oValuesFromXML.LoadFromXML()
            End If
            oConfigDetails.oParentCore = Me
            Return oConfigDetails
        End Get
        Set(value As clsBaseConfigFromXML)
            oConfigDetails = value
            oConfigDetails.oParentCore = Me
        End Set
    End Property


    Friend Property RequestAttachments As clsAttachmentsOfHTTPRequest
        Get
            If oRequestFiles Is Nothing Then
                oRequestFiles = New clsAttachmentsOfHTTPRequest
                oRequestFiles.oParentCore = Me
                'oSaveRequestAttachments.SaveFilesOfSubmission()
            End If
            oRequestFiles.oParentCore = Me
            Return oRequestFiles
        End Get
        Set(value As clsAttachmentsOfHTTPRequest)
            oRequestFiles = oRequestFiles
            oRequestFiles.oParentCore = Me
        End Set
    End Property

    Friend Property LogFileUpd As clsLogFileUpdater
        Get
            If oLogFileUPd Is Nothing Then
                oLogFileUPd = New clsLogFileUpdater
                oLogFileUPd.oParentCore = Me
            End If
            oLogFileUPd.oParentCore = Me
            Return oLogFileUPd
        End Get
        Set(value As clsLogFileUpdater)
            oLogFileUPd = value
            oLogFileUPd.oParentCore = Me
        End Set
    End Property


    Friend Property RequestEmails As clsGeneratedEmailsForRequest
        Get
            If oRequestEmails Is Nothing Then
                oRequestEmails = New clsGeneratedEmailsForRequest
                oRequestEmails.oParentCore = Me
            End If
            oRequestEmails.oParentCore = Me
            Return oRequestEmails
        End Get
        Set(value As clsGeneratedEmailsForRequest)
            oRequestEmails = value
            oRequestEmails.oParentCore = Me
        End Set
    End Property


    Friend Property RequestDoc As colGeneratedDocumentsForRequest
        Get
            If oRequestDoc Is Nothing Then
                oRequestDoc = New colGeneratedDocumentsForRequest
                oRequestDoc.oParentCore = Me
            End If
            oRequestDoc.oParentCore = Me
            Return oRequestDoc
        End Get
        Set(value As colGeneratedDocumentsForRequest)
            oRequestDoc.oParentCore = Me
            oRequestDoc = value
        End Set
    End Property


    Friend Property AssociatedFiles As colAssociatedFiles
        Get
            If oAssociatedFiles Is Nothing Then
                oAssociatedFiles = New colAssociatedFiles
                oAssociatedFiles.oParentCore = Me
            End If
            oAssociatedFiles.oParentCore = Me
            Return oAssociatedFiles
        End Get
        Set(value As colAssociatedFiles)
            oAssociatedFiles.oParentCore = Me
            oAssociatedFiles = value
        End Set
    End Property


    Public Property SessionID As String
        Get
            If sSessionID = "" And oFieldDataUsed.Count > 0 Then
                sSessionID = GetValueIfExists("SessionIDValLabels")
            End If
            Return sSessionID
        End Get
        Set(value As String)
            sSessionID = value
            ApplyValueIfExists("SessionIDValLabels", sSessionID, True)
        End Set
    End Property




    Public Function ApplySubstitution(ValueToUpdate As String, Optional SubstModifider As clsValueSubstitutions.TargetModifier = clsValueSubstitutions.TargetModifier.NoModify) As String
        Dim sReturn As String = "", oSubst As clsValueSubstitutions
        Try
            sReturn = ValueToUpdate
            If sReturn.Length > 4 Then
                oSubst = New clsValueSubstitutions(sReturn, oFieldDataUsed, SubstModifider)
                sReturn = oSubst.Result
            End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If ValueToUpdate <> "" Then oError.AddDetail("Value to apply substitution : " & ValueToUpdate)
            gsErr = goErrs.ToString

        End Try
        Return sReturn
    End Function


    Public Function GetValueIfExists(sKeyListID As String,
                                     Optional bUseSubst As Boolean = False,
                                     Optional SubstModifider As clsValueSubstitutions.TargetModifier = clsValueSubstitutions.TargetModifier.NoModify,
                                     Optional DefaultValueKey As String = "") As String

        Dim sReturn As String = "", sKey As String, oTemp As Object
        Dim aVals As New ArrayList
        Try

            oTemp = GetMySetting(sKeyListID)
            If oTemp Is Nothing Then
                aVals = SplitToArray(sKeyListID)
                'aVals.Add(sKeyListID)
            Else
                If TypeOf (oTemp) Is Specialized.StringCollection Then
                    aVals = New ArrayList(TryCast(oTemp, Specialized.StringCollection))
                ElseIf TypeOf (oTemp) Is String Then
                    aVals = SplitToArray(oTemp.ToString)
                Else
                    aVals.Add(oTemp.ToString)
                End If

            End If

            sReturn = GetValueIfExists(aVals, bUseSubst, SubstModifider, DefaultValueKey)


            'For Each sKey In aVals
            '    If oFieldDataUsed.ContainsKey(sKey) Then
            '        sReturn = oFieldDataUsed.Item(sKey)
            '        If sReturn <> "" Then Exit For
            '    End If
            'Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKeyListID <> "" Then oError.AddDetail("Key List : " & sKeyListID)
            gsErr = goErrs.ToString

        End Try
        Return sReturn
    End Function

    Public Function GetValueIfExists(sKeyList As ArrayList,
                                     Optional bUseSubst As Boolean = False,
                                     Optional SubstModifider As clsValueSubstitutions.TargetModifier = clsValueSubstitutions.TargetModifier.NoModify,
                                     Optional DefaultValueKey As String = "") As String

        Dim sReturn As String = "", sKey As String, aVals As ArrayList, bFound As Boolean = False
        Dim oTemp As Object, sTemp As String
        Try

            aVals = New ArrayList(sKeyList)
            For Each sKey In sKeyList
                If oFieldDataUsed.ContainsKey(sKey) Then
                    sReturn = oFieldDataUsed.Item(sKey)
                    bFound = True
                    If sReturn <> "" Then
                        Exit For
                    End If
                End If
            Next

            If Not bFound And DefaultValueKey <> "" Then
                oTemp = GetMySetting(DefaultValueKey)
                If oTemp IsNot Nothing Then
                    sTemp = oTemp.ToString
                    sReturn = ApplySubstitution(sTemp, SubstModifider)
                End If
            Else
                If sReturn <> "" And bUseSubst Then
                    sReturn = ApplySubstitution(sReturn, SubstModifider)
                End If
            End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKeyList IsNot Nothing Then oError.AddDetail("Keys : " & Join(sKeyList.ToArray, ","))
            If sKey <> "" Then oError.AddDetail("sKey : " & sKey)
            gsErr = goErrs.ToString


        End Try
        Return sReturn
    End Function



    Protected Friend Function SplitToArray(ValString As String) As ArrayList
        Dim oReturn As New ArrayList
        Dim oTemp As Object, sTemp As String, aDelims As ArrayList = Nothing, sVal As String
        Dim iMax As Integer, iPos As Integer, sFirstDelim As String
        Try
            oTemp = GetMySetting("ValueDividersToArray")
            If TypeOf (oTemp) Is String Then
                aDelims = New ArrayList(oTemp.ToString.ToCharArray)
            ElseIf TypeOf (oTemp) Is Specialized.StringCollection Then
                aDelims = New ArrayList(TryCast(oTemp, Specialized.StringCollection))
            End If
            If aDelims IsNot Nothing Then

                iMax = aDelims.Count - 1

                If iMax > 0 Then
                    sFirstDelim = aDelims(0).ToString
                    For iPos = 1 To iMax
                        sVal = aDelims(iPos).ToString
                        If sVal <> "" Then ValString = ValString.Replace(sVal, sFirstDelim)
                    Next
                    oReturn = New ArrayList(Split(ValString, sFirstDelim))
                End If
            Else
                oReturn.Add(ValString)
            End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If ValString <> "" Then oError.AddDetail("ValString : " & ValString)
            If iPos <> 0 Then oError.AddDetail("iPos : " & iPos.ToString)
            gsErr = goErrs.ToString


        End Try
        Return oReturn
    End Function



    Public Sub ApplyValueIfExists(sKeyListID As String, ValueToApply As String, Optional CreateAll As Boolean = False)
        Dim oTemp As Object, aVals As New ArrayList
        Try

            oTemp = GetMySetting(sKeyListID)
            If oTemp Is Nothing Then
                aVals = SplitToArray(sKeyListID)
                'aVals.Add(sKeyListID)
            Else
                If TypeOf (oTemp) Is Specialized.StringCollection Then
                    aVals = New ArrayList(TryCast(oTemp, Specialized.StringCollection))
                ElseIf TypeOf (oTemp) Is String Then
                    aVals = SplitToArray(oTemp.ToString)
                Else
                    aVals.Add(oTemp.ToString)
                End If

            End If

            ApplyValueIfExists(aVals, ValueToApply, CreateAll)

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKeyListID <> "" Then oError.AddDetail("List Key : " & sKeyListID)
            If ValueToApply <> "" Then oError.AddDetail("Value : " & ValueToApply)
            gsErr = goErrs.ToString


        End Try
    End Sub

    Public Sub ApplyValueIfExists(sKeyList As ArrayList, ValueToApply As String, Optional CreateAll As Boolean = False)
        Dim sKey As String, bFound As Boolean
        Try

            For Each sKey In sKeyList
                If sKey <> "" Then
                    If oFieldDataUsed.ContainsKey(sKey) Then
                        oFieldDataUsed.Item(sKey) = ValueToApply
                        bFound = True
                    ElseIf CreateAll Then
                        oFieldDataUsed.Add(sKey, ValueToApply)
                    End If
                End If
            Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKey <> "" Then oError.AddDetail("Field Key : " & sKey)
            gsErr = goErrs.ToString


        End Try

    End Sub

    Public Sub AddValuesToFieldData(aNewValues As SortedList(Of String, String))
        Dim sKey As String, sValue As String
        Try
            For Each sKey In aNewValues.Keys
                sValue = aNewValues.Item(sKey)
                If oFieldDataUsed.ContainsKey(sKey) Then
                    oFieldDataUsed.Item(sKey) = sValue
                Else
                    oFieldDataUsed.Add(sKey, sValue)
                End If
            Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKey <> "" Then oError.AddDetail("Field Key : " & sKey)
            gsErr = goErrs.ToString


        End Try
    End Sub


    Public Sub ProcessHTTPRequest(oRequest As HttpRequest, oPage As Page, Optional eStages As StagesToDo = StagesToDo.AttachmentsDocsAndEmails)
        Try

            Me.oRequest = oRequest
            Me.oPage = oPage

            If ValuesFromHTTPRequest.SubmittedDataValues.Count = 0 Then ValuesFromHTTPRequest.ProcessFieldValues()

            sSubmissionTypeCode = GetValueIfExists("SessionIDValLabels")
            If sSubmissionTypeCode <> "" Then
                ConfigDetailsFromXML.XMLConfigKey = sSubmissionTypeCode
            End If

            If ValuesFromXML.aXMLDataValues.Count = 0 Then ValuesFromXML.LoadFromXML()

            If eStages = StagesToDo.AttachmentsDocsAndEmails Or eStages = StagesToDo.AttachmentsDocs Or eStages = StagesToDo.AttachmentsOnly Then RequestAttachments.ProcessFilesOfSubmission()
            If eStages = StagesToDo.AttachmentsDocsAndEmails Or eStages = StagesToDo.AttachmentsDocs Then RequestDoc.GenerateDocuments()
            If eStages = StagesToDo.AttachmentsDocsAndEmails Or eStages = StagesToDo.AttachmentsDocsAndEmails Then RequestEmails.SendEmails()

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            gsErr = goErrs.ToString

        End Try
    End Sub

End Class
