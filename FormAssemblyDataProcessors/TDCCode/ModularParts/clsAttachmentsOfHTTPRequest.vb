﻿Option Compare Text
Imports System.IO
Imports System.Net

Public Class clsAttachmentsOfHTTPRequest

    Protected Friend oParentCore As clsFormSupportCore

    Friend aFileValuesOfSubmission As SortedList(Of String, String)
    'Friend aAttachments As SortedList(Of String, clsAttachedFileData)

    Public aFilesDetected As ArrayList
    Public aFileLinksToLoadLabels As ArrayList

    Public sFilesDetected As String = ""



    Friend ReadOnly Property ValuesOfSubmission As SortedList(Of String, String)
        Get
            If aFileValuesOfSubmission Is Nothing Then
                aFileValuesOfSubmission = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

            End If
            Return aFileValuesOfSubmission
        End Get
    End Property


    'Friend ReadOnly Property Attachments As SortedList(Of String, clsAttachedFileData)
    '    Get
    '        If aAttachments Is Nothing Then ProcessFilesOfSubmission()

    '        Return aAttachments
    '    End Get
    'End Property


    Protected Friend Sub ProcessFilesOfSubmission()
        Dim sReturn As String = "", aFileURLs As New ArrayList
        Dim sFileName As String, oFilePosted As HttpPostedFile
        Dim iCharPos As Integer, oStream As IO.Stream, aBytes As Byte()
        Dim sValue As String, sKey As String, sTemp As String
        Dim sRegexURL As String, oMatches As MatchCollection, oMatch As Match, oGrp As Group
        Dim aPathParts As String(), sURL As String
        Dim oResponse As WebResponse, oHTTPRequester As WebRequest, aLocalURLs As New ArrayList
        Dim oMemoryStream As IO.MemoryStream, sFieldKey As String
        Dim oNewAttachment As clsAttachedFileData

        Try

            If aFilesDetected.Count = 0 Then
                'aAttachments = New SortedList(Of String, clsAttachedFileData)
                sRegexURL = GetMySetting("RegexURLInText").ToString

                For Each oFilePosted In oParentCore.oRequest.Files
                    sFileName = oFilePosted.FileName.Replace("\", "/")
                    aFilesDetected.Add(sFileName)
                    iCharPos = sFileName.LastIndexOf("/")
                    If iCharPos > 0 Then sFileName = sFileName.Substring(iCharPos + 1)

                    oStream = New IO.MemoryStream()
                    ReDim aBytes(CInt(oFilePosted.InputStream.Length))
                    oFilePosted.InputStream.Read(aBytes, 0, CInt(oFilePosted.InputStream.Length))

                    'If oParentCore.oCurrentData.oDataFromPost.ContainsKey(sFileName) Then
                    oParentCore.AssociatedFiles.NewAttachment(sFileName, aBytes)

                    'If aAttachments.ContainsKey(sFileName) Then
                    '    'oParentCore.oCurrentData.oDataFromPost.Item(sFileName) = New clsLoadedFileData(sFileName, aBytes)
                    '    aAttachments.Item(sFileName) = New clsAttachedFileData(sFileName, aBytes)
                    'Else

                    '    'oParentCore.oCurrentData.oDataFromPost.Add(sFileName, New clsLoadedFileData(sFileName, aBytes))
                    '    aAttachments.Add(sFileName, New clsAttachedFileData(sFileName, aBytes))
                    'End If
                Next

                aFileLinksToLoadLabels = New ArrayList(Split((GetMySetting("FieldsForFileLinks").ToString.ToUpper), ","))

                If aFileLinksToLoadLabels IsNot Nothing Then

                    'For Each sKey In oParentCore.oCurrentData.oDataFromPost.aFieldValues.Keys
                    For Each sKey In oParentCore.ValuesFromHTTPRequest.SubmittedDataValues.Keys

                        If aFileLinksToLoadLabels.Contains(sKey.ToUpper) Then
                            ' Get the file using http
                            'sValue = oParentCore.oCurrentData.oDataFromPost.aFieldValues.Item(sKey).ToString
                            'sValue = aAttachments.Item(sKey).ToString
                            sValue = oParentCore.AssociatedFiles.Item(sKey).SavedFilePath

                            oMatches = Regex.Matches(sValue, sRegexURL)
                            For Each oMatch In oMatches
                                oGrp = oMatch.Groups("FullFilePath")
                                If oGrp IsNot Nothing Then
                                    sURL = oGrp.Value
                                    aFileURLs.Add(sURL)
                                    aFilesDetected.Add(sURL)
                                End If
                            Next

                        End If

                    Next
                End If

                sFilesDetected = ""


                If aFileURLs.Count > 0 Then
                    For Each sURL In aFileURLs

                        sFileName = ""
                        aPathParts = Split(sURL.Replace("/", "\"), "\")
                        For iPos = UBound(aPathParts) To 0 Step -1
                            sTemp = aPathParts(iPos)
                            If sTemp.LastIndexOf(".") > 0 And sTemp.LastIndexOf(".") > sTemp.Length - 8 Then
                                sFileName = sTemp
                                Exit For
                            End If
                        Next
                        If sFileName = "" Then sFileName = aPathParts(UBound(aPathParts))
                        sFilesDetected &= "File name : " & sFileName & "<br/>" & sURL & "<br/>"

                        'End If

                        'If oParentCore.oCurrentData.oDataFromPost.ContainsKey(sFileName) Then
                        'If aAttachments.ContainsKey(sFileName) Then
                        'Else
                        If Not oParentCore.AssociatedFiles.ContainsKey(sFileName) Then
                            oHTTPRequester = WebRequest.Create(sURL)
                            oResponse = oHTTPRequester.GetResponse()
                            oStream = oResponse.GetResponseStream()

                            oNewAttachment = oParentCore.AssociatedFiles.NewAttachment(sFileName, oStream)

                            'oNewAttachment = New clsAttachedFileData(sFileName, oStream)

                            'oMemoryStream = New IO.MemoryStream
                            'oStream.CopyTo(oMemoryStream)

                            'aBytes = oMemoryStream.ToArray
                            'oMemoryStream.Close()
                            'oMemoryStream.Dispose()
                            'oMemoryStream = Nothing

                            'sFilesDetected &= "Length : " & aBytes.Length & "<br/>"
                            sFilesDetected &= "Length : " & oNewAttachment.FileData.Length & "<br/>"

                            'oParentCore.oCurrentData.oDataFromPost.Add(sFileName, New clsLoadedFileData(sFileName, aBytes))
                            'aNewAttachment = New clsAttachedFileData(sFileName, aBytes)

                            'oNewAttachment.oParentCollection = Me
                            oNewAttachment.IsFromSubmission = True
                            oNewAttachment.CanListOnEmail = True

                            oNewAttachment.SaveFileData()

                            'aAttachments.Add(sFileName, oNewAttachment)

                        End If

                    Next
                End If

                oParentCore.AssociatedFiles.UpdateStoredValues()

                'sTemp = oParentCore.oCurrentData.oDataFromPost.Count.ToString
                'sTemp = aAttachments.Count.ToString
                'sTemp = oParentCore.AssociatedFiles.SubmittedFiles.count.tostring
                'oParentCore.ApplyValueIfExists("AttachmentCountValLabels", sTemp, True)

                'sFieldKey = "FileCount" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "FileListCount" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "NoOfFilesLoaded" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "NoOfFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "CountLoadedFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "CountFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "CountOfLoadedFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "CountOfFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "SubmissionFileCount" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
                'sFieldKey = "FilesLoadedCount" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)


            End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sURL <> "" Then oError.AddDetail("File URL : " & sURL)
            If oFilePosted IsNot Nothing Then oError.AddDetail("Receiving file : " & oFilePosted.FileName)
            gsErr = goErrs.ToString

            'sErrors &= "ERROR : " & ex.ToString
        End Try


        If oStream IsNot Nothing Then Try : oStream.Close() : oStream = Nothing : Catch ex As Exception : End Try
        If oHTTPRequester IsNot Nothing Then Try : oHTTPRequester = Nothing : Catch ex As Exception : End Try
        If oFilePosted IsNot Nothing Then Try : oFilePosted = Nothing : Catch ex As Exception : End Try

    End Sub



End Class


