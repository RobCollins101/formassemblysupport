﻿Option Compare Text
Imports System.Net

Public Class clsGeneratedEmailsForRequest

    Protected Friend oParentCore As clsFormSupportCore


    Private sClientEmailTo As String = ""
    Private sClientEmailCC As String = ""
    Private sClientEMailBCC As String = ""
    Private sClientEMailFrom As String = ""
    Private sClientSubjectTemplate As String = ""
    Private sClientSubject As String = ""
    Private sClientBodyTemplate As String = ""
    Private sClientBody As String = ""
    Private bClientAttachDocument As Boolean = True



    Private sAdminEmailTo As String = ""
    Private sAdminEmailFrom As String = ""
    Private sAdminEmailCC As String = ""
    Private sAdminEMailBCC As String = ""
    Private sAdminSubjectTemplate As String = ""
    Private sAdminSubject As String = ""
    Private sAdminBodyTemplate As String = ""
    Private sAdminBody As String = ""
    Private bAdminAttachDocument As Boolean = True


    Friend Property ClientEmailTo As String
        Get
            If sClientEmailTo = "" Then sClientEmailTo = oParentCore.GetValueIfExists("ClientEMailToValLabels")
            Return sClientEmailTo
        End Get
        Set(value As String)
            If value <> sClientEmailTo Then
                oParentCore.ApplyValueIfExists("ClientEMailToValLabels", sClientEmailTo)
            End If
        End Set
    End Property

    Friend Property ClientEmailCC As String
        Get
            If sClientEmailCC = "" Then sClientEmailCC = oParentCore.GetValueIfExists("ClientEmailCCValLabels")
            Return sClientEmailCC
        End Get
        Set(value As String)
            If value <> sClientEmailCC Then
                oParentCore.ApplyValueIfExists("ClientEmailCCValLabels", sClientEmailCC)
            End If
        End Set
    End Property

    Friend Property ClientEmailBCC As String
        Get
            If sClientEMailBCC = "" Then sClientEMailBCC = oParentCore.GetValueIfExists("ClientEmailBCCValLabels")
            Return sClientEMailBCC
        End Get
        Set(value As String)
            If value <> sClientEMailBCC Then
                oParentCore.ApplyValueIfExists("ClientEmailBCCValLabels", sClientEMailBCC)
            End If
        End Set
    End Property

    Friend Property ClientEmailFrom As String
        Get
            If sClientEMailFrom = "" Then sClientEMailFrom = oParentCore.GetValueIfExists("ClientEmailFromValLabels")
            Return sClientEMailFrom
        End Get
        Set(value As String)
            If value <> sClientEMailFrom Then
                oParentCore.ApplyValueIfExists("ClientEmailFromValLabels", sClientEMailFrom)
            End If
        End Set
    End Property

    Friend Property ClientSubjectTemplate As String
        Get
            If sClientSubjectTemplate = "" Then sClientSubjectTemplate = oParentCore.GetValueIfExists("ClientSubjectTemplateValLabels")
            If sClientSubjectTemplate = "" Then Dim oTemp As Object = GetMySetting("DefaultClientEmailSubject") : If TypeOf (oTemp) Is String Then sClientSubjectTemplate = oTemp.ToString
            sClientSubjectTemplate = LoadFile(sClientSubjectTemplate)
            Return sClientSubjectTemplate
        End Get
        Set(value As String)
            If value <> sClientSubjectTemplate Then
                oParentCore.ApplyValueIfExists("ClientSubjectTemplateValLabels", sClientSubjectTemplate)
            End If
        End Set
    End Property

    Friend Property ClientSubject As String
        Get
            If sClientSubject = "" Then sClientSubject = oParentCore.GetValueIfExists("ClientSubjectValLabels")
            If sClientSubject = "" Then sClientSubject = oParentCore.ApplySubstitution(sClientSubjectTemplate)
            Return sClientSubject
        End Get
        Set(value As String)
            If value <> sClientSubject Then
                oParentCore.ApplyValueIfExists("ClientSubjectValLabels", sClientSubject)
            End If
        End Set
    End Property

    Friend Property ClientBodyTemplate As String
        Get
            If sClientBodyTemplate = "" Then sClientBodyTemplate = oParentCore.GetValueIfExists("ClientBodyTemplateValLabels")
            If sClientBodyTemplate = "" Then Dim oTemp As Object = GetMySetting("DefaultClientEmailBody") : If TypeOf (oTemp) Is String Then sClientBodyTemplate = oTemp.ToString
            sClientBodyTemplate = LoadFile(sClientBodyTemplate)
            Return sClientBodyTemplate
        End Get
        Set(value As String)
            If value <> sClientBodyTemplate Then
                oParentCore.ApplyValueIfExists("ClientBodyTemplateValLabels", sClientBodyTemplate)
            End If
        End Set
    End Property

    Friend Property ClientBody As String
        Get
            If sClientBody = "" Then sClientBody = oParentCore.GetValueIfExists("ClientBodyValLabels")
            If sClientBody = "" Then sClientBody = oParentCore.ApplySubstitution(sClientBodyTemplate)
            Return sClientBody
        End Get
        Set(value As String)
            If value <> sClientBody Then
                oParentCore.ApplyValueIfExists("ClientBodyValLabels", sClientBody)
            End If
        End Set
    End Property

    Private Property ClientAttachDocument As Boolean
        Get
            Return bClientAttachDocument
        End Get
        Set(value As Boolean)
            bClientAttachDocument = value
        End Set
    End Property




    Friend Property AdminEmailTo As String
        Get
            If sAdminEmailTo = "" Then sAdminEmailTo = oParentCore.GetValueIfExists("AdminEMailToValLabels")
            Return sAdminEmailTo
        End Get
        Set(value As String)
            If value <> sAdminEmailTo Then
                oParentCore.ApplyValueIfExists("AdminEMailToValLabels", sAdminEmailTo)
            End If
        End Set
    End Property

    Friend Property AdminEmailCC As String
        Get
            If sAdminEmailCC = "" Then sAdminEmailCC = oParentCore.GetValueIfExists("AdminEmailCCValLabels")
            Return sAdminEmailCC
        End Get
        Set(value As String)
            If value <> sAdminEmailCC Then
                oParentCore.ApplyValueIfExists("AdminEmailCCValLabels", sAdminEmailCC)
            End If
        End Set
    End Property

    Friend Property AdminEmailBCC As String
        Get
            If sAdminEMailBCC = "" Then sAdminEMailBCC = oParentCore.GetValueIfExists("AdminEmailBCCValLabels")
            Return sAdminEMailBCC
        End Get
        Set(value As String)
            If value <> sAdminEMailBCC Then
                oParentCore.ApplyValueIfExists("AdminEmailBCCValLabels", sAdminEMailBCC)
            End If
        End Set
    End Property

    Friend Property AdminEmailFrom As String
        Get
            If sAdminEmailFrom = "" Then sAdminEmailFrom = oParentCore.GetValueIfExists("AdminEmailFromValLabels")
            Return sAdminEmailFrom
        End Get
        Set(value As String)
            If value <> sAdminEmailFrom Then
                oParentCore.ApplyValueIfExists("AdminEmailFromValLabels", sAdminEmailFrom)
            End If
        End Set
    End Property

    Friend Property AdminSubjectTemplate As String
        Get
            If sAdminSubjectTemplate = "" Then sAdminSubjectTemplate = oParentCore.GetValueIfExists("AdminSubjectTemplateValLabels")
            If sAdminSubjectTemplate = "" Then Dim oTemp As Object = GetMySetting("") : If TypeOf (oTemp) Is String Then sAdminSubjectTemplate = oTemp.ToString
            sAdminSubjectTemplate = LoadFile(sAdminSubjectTemplate)
            Return sAdminSubjectTemplate
        End Get
        Set(value As String)
            If value <> sAdminSubjectTemplate Then
                oParentCore.ApplyValueIfExists("AdminSubjectTemplateValLabels", sAdminSubjectTemplate)
            End If
        End Set
    End Property

    Friend Property AdminSubject As String
        Get
            If sAdminSubject = "" Then sAdminSubject = oParentCore.GetValueIfExists("AdminSubjectValLabels")
            If sAdminSubject = "" Then sAdminSubject = oParentCore.ApplySubstitution(sAdminSubjectTemplate)
            Return sAdminSubject
        End Get
        Set(value As String)
            If value <> sAdminSubject Then
                oParentCore.ApplyValueIfExists("AdminSubjectValLabels", sAdminSubject)
            End If
        End Set
    End Property

    Friend Property AdminBodyTemplate As String
        Get
            If sAdminBodyTemplate = "" Then sAdminBodyTemplate = oParentCore.GetValueIfExists("AdminBodyTemplateValLabels")
            If sAdminBodyTemplate = "" Then Dim oTemp As Object = GetMySetting("") : If TypeOf (oTemp) Is String Then sAdminBodyTemplate = oTemp.ToString
            sAdminBodyTemplate = LoadFile(sAdminBodyTemplate)
            Return sAdminBodyTemplate
        End Get
        Set(value As String)
            If value <> sAdminBodyTemplate Then
                oParentCore.ApplyValueIfExists("AdminBodyTemplateValLabels", sAdminBodyTemplate)
            End If
        End Set
    End Property

    Friend Property AdminBody As String
        Get
            If sAdminBody = "" Then sAdminBody = oParentCore.GetValueIfExists("AdminBodyValLabels")
            If sAdminBody = "" Then sAdminBody = oParentCore.ApplySubstitution(sAdminBodyTemplate)
            Return sAdminBody
        End Get
        Set(value As String)
            If value <> sAdminBody Then
                oParentCore.ApplyValueIfExists("AdminBodyValLabels", sAdminBody)
            End If
        End Set
    End Property

    Private Property AdminAttachDocument As Boolean
        Get
            Return bAdminAttachDocument
        End Get
        Set(value As Boolean)
            bAdminAttachDocument = value
        End Set
    End Property

    Public Function SendEmails() As Boolean
        Dim bReturn As Boolean = False, sStorePath As String
        Try

            If ClientEmailTo <> "" And ClientBody <> "" And ClientEmailFrom <> "" Then
                sStorePath = "EMailToClient_{{DateTimeSort}}.txt"
                SendOneEMail(ClientEmailTo, ClientEmailCC, ClientEmailBCC, ClientEmailFrom, ClientSubject, ClientBody, True, sStorePath)
            End If


            If AdminEmailTo <> "" And AdminBody <> "" And AdminEmailFrom <> "" Then
                sStorePath = "EMailToAdmin_{{DateTimeSort}}.txt"
                SendOneEMail(AdminEmailTo, AdminEmailCC, AdminEmailBCC, AdminEmailFrom, AdminSubject, AdminBody, True, sStorePath)
            End If


        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sStorePath <> "" Then oError.AddDetail("Stored path : " & sStorePath)
            gsErr = goErrs.ToString

        End Try
        Return bReturn
    End Function


    Private Function SendOneEMail(sTo As String, sCC As String, sBCC As String, sFrom As String, sSubject As String, sBody As String, bAttachDoc As Boolean, sStorePath As String) As String
        Dim sreturn As String = "", oEmailer As clsEMail, sEMailFileName As String
        Dim oStoreFileInfo As IO.FileInfo, oTxtWriter As IO.TextWriter
        Dim aFiles As ArrayList, sFile As String
        Dim oAssociatedFile As clsAttachedFileData
        Try

            If sFrom <> "" And sTo <> "" And sSubject <> "" And sBody <> "" Then



                oEmailer = New clsEMail()
                oEmailer.To.Add(sTo)
                If sCC <> "" Then oEmailer.CC.Add(sCC.Replace(";", ",").Replace(":", ",").Replace(" ", ",").Replace(",,", ","))
                If sBCC <> "" Then oEmailer.Bcc.Add(sBCC.Replace(";", ",").Replace(":", ",").Replace(" ", ",").Replace(",,", ","))
                oEmailer.From = New Mail.MailAddress(sFrom)
                oEmailer.Subject = sSubject
                oEmailer.Body = sBody
                If bAttachDoc Then
                    aFiles = oParentCore.AssociatedFiles.AttachToEmailFilesAsList
                    For Each sFile In aFiles
                        oEmailer.AddFile(sFile)
                    Next
                End If

                oEmailer.SendEmail()

                sEMailFileName = ""
                If sStorePath = "" Then sStorePath = oParentCore.GetValueIfExists("", True,, "DefaultStoreFolderDest")

                oAssociatedFile = oParentCore.AssociatedFiles.NewAttachment(sStorePath, System.Text.Encoding.Unicode.GetBytes(oEmailer.ToString))
                oAssociatedFile.CanAttachToEmail = False
                oAssociatedFile.CanListOnEmail = False
                oAssociatedFile.IsFromSubmission = False
                oAssociatedFile.SaveFileData()


                'If sStorePath <> "" Then
                '    oStoreFileInfo = New IO.FileInfo(sStorePath)
                '    Try
                '        If Not oStoreFileInfo.Directory.Exists Then oStoreFileInfo.Directory.Create()
                '        oTxtWriter = oStoreFileInfo.CreateText
                '        oTxtWriter.WriteLine(oEmailer.ToString)
                '        oTxtWriter.Flush() : oTxtWriter.Close() : oTxtWriter = Nothing

                '    Catch ex As Exception

                '    End Try

                'End If




            End If


        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sSubject <> "" Then oError.AddDetail("Subject : " & sSubject)
            If sTo <> "" Then oError.AddDetail("To : " & sTo)
            If sFrom <> "" Then oError.AddDetail("From : " & sFrom)
            If sCC <> "" Then oError.AddDetail("CC : " & sCC)
            If oAssociatedFile IsNot Nothing Then oError.AddDetail("Stored Path : " & oAssociatedFile.SavedFilePath)
            gsErr = goErrs.ToString

        End Try
        Return sreturn
    End Function

End Class
