﻿Public Class colGeneratedDocumentsForRequest
    Inherits List(Of clsGeneratedDocumentForRequest)

    Friend oParentCore As clsFormSupportCore


    Friend Function AllDocsList() As ArrayList
        Dim aReturn As New ArrayList
        Try

        Catch ex As Exception

        End Try
        Return aReturn
    End Function

    Friend Function AttachableDocsList() As ArrayList
        Dim aReturn As New ArrayList
        Try

        Catch ex As Exception

        End Try
        Return aReturn
    End Function

    Friend Function OtherDocsList() As ArrayList
        Dim aReturn As New ArrayList
        Try

        Catch ex As Exception

        End Try
        Return aReturn
    End Function

    Friend Sub GenerateDocuments()
        'Dim oDocGenerated As clsGeneratedDocumentForRequest
        'Dim sHTMLPassed As String = "", sFileStorePathUsed As String, sTargetExtn As String
        'Dim oItems As Array, bCanConvert As Boolean = False
        'Dim oFileInfo As IO.FileInfo, owsConvert As wsConvertToImage.wsConvertToImage
        'Dim owsConvertibles As wsConvertToImage.FileTypesListed
        'Dim oConvParams As wsConvertToImage.RequestHTMLConvWithFields
        'Dim owrStream As IO.StreamWriter ', oWrStream As IO.StreamWriter
        'Dim oCreatedDoc As wsConvertToImage.ConvertedDoc, owsResponse As wsConvertToImage.clsWebResponseConvertedDocs

        Dim oDoc As clsGeneratedDocumentForRequest, sPath As String = ""

        Try

            For Each oDoc In Me

                sPath = oDoc.GenerateDocument

            Next

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            gsErr = goErrs.ToString

        End Try

    End Sub

End Class


Public Class clsGeneratedDocumentForRequest
    Friend oParentGeneratedDocs As colGeneratedDocumentsForRequest

    Friend sDocTemplateHTML As String

    Friend sTargetDocPath As String


    Friend Function GenerateDocument() As String
        Dim sHTMLPassed As String = "", sTargetDocPathUsed As String, sTargetExtn As String
        Dim oFileInfo As IO.FileInfo, owsConvert As wsConvertToImage.wsConvertToImage
        Dim owsConvertibles As wsConvertToImage.FileTypesListed
        Dim oConvParams As wsConvertToImage.RequestHTMLConvWithFields
        'Dim sLogFilePathUsed As String, 
        Dim sLogFileEntryUsed As String, sReturn As String
        'Dim aConvExtns As New ArrayList From {"pdf", "bmp", "jpg", "png", "gif", "tif"}
        Dim oItems As Array, bCanConvert As Boolean = False
        Dim owrStream As IO.StreamWriter ', oWrStream As IO.StreamWriter
        Dim oCreatedDoc As wsConvertToImage.ConvertedDoc, owsResponse As wsConvertToImage.clsWebResponseConvertedDocs
        Dim oFieldVals As New List(Of wsConvertToImage.FieldValue), oFieldVal As wsConvertToImage.FieldValue
        Dim sKey As String, sValue As String, iFileNo As Integer, oLogFileInfo As IO.FileInfo

        Dim oAttachedFileData As clsAttachedFileData

        Try

            'oParentGeneratedDocs.oParentCore.AddValuesToFieldData()
            'oSubst.AddNewValues(oFieldDataUsed)
            sHTMLPassed = LoadIfFile(sDocTemplateHTML, oParentGeneratedDocs.oParentCore.oFieldDataUsed, oParentGeneratedDocs.oParentCore.oFieldDataUsed)

            If sTargetDocPath <> "" Then
                sTargetDocPathUsed = oParentGeneratedDocs.oParentCore.ApplySubstitution(sTargetDocPath, clsValueSubstitutions.TargetModifier.ForFilePath)

                oFileInfo = New IO.FileInfo(sTargetDocPathUsed)
                If Not (oFileInfo.Directory.Exists) Then oFileInfo.Directory.Create()

                If oFileInfo.Directory.Exists Then

                    sTargetExtn = oFileInfo.Extension.Replace(".", "")
                    owsConvert = New wsConvertToImage.wsConvertToImage
                    owsConvertibles = owsConvert.ConvertibleExtensions("HTM")
                    oItems = owsConvertibles.Items.ToArray
                    If oItems.Length > 0 Then

                        For Each oItem In oItems
                            If oItem.ToString = sTargetExtn Then
                                bCanConvert = True
                                Exit For
                            End If
                        Next
                    End If
                    If bCanConvert Then
                        oConvParams = New wsConvertToImage.RequestHTMLConvWithFields
                        oConvParams.DestinationFileNameTemplate = sTargetDocPathUsed
                        oConvParams.DestinationFileType = oFileInfo.Extension
                        oConvParams.CanOverwriteDestination = "True"
                        oConvParams.HTMLToConvert = sHTMLPassed
                        oConvParams.FileType = "HTM"

                        oFieldVal = New wsConvertToImage.FieldValue
                        For Each sKey In oParentGeneratedDocs.oParentCore.oFieldDataUsed.Keys
                            sValue = oParentGeneratedDocs.oParentCore.oFieldDataUsed.Item(sKey).ToString
                            oFieldVal.FieldName = sKey
                            oFieldVal.FieldData = sValue
                            oFieldVals.Add(oFieldVal)
                        Next

                        oConvParams.FieldData = oFieldVals.ToArray

                        owsResponse = owsConvert.HTMLImageConversionWithFields(oConvParams)
                        iFileNo = 0




                        For Each oCreatedDoc In owsResponse.ConvertedDocuments
                            oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocPathValLabels", oCreatedDoc.DestinationPath, True)
                            oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocFolderValLabels", IO.Path.GetDirectoryName(oCreatedDoc.DestinationPath), True)


                            'oSubst.AddNewValue("DestinationPath", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DestinationDocumentPath", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DocumentPath", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DocPath", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DestDocumentPath", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DestDocPath", oCreatedDoc.DestinationPath)

                            'oSubst.AddNewValue("DestinationFolder", IO.Path.GetDirectoryName(oCreatedDoc.DestinationPath))
                            'oSubst.AddNewValue("DestinationFilePath", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DestinationFileName", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DestinationFile", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("FilePath", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DestFilePath", oCreatedDoc.DestinationPath)
                            'oSubst.AddNewValue("DestFolder", IO.Path.GetDirectoryName(oCreatedDoc.DestinationPath))

                            iFileNo += 1
                            'oSubst.AddNewValue("DocumentNo", iFileNo.ToString)
                            'oSubst.AddNewValue("PageNo", iFileNo.ToString)
                            'oSubst.AddNewValue("FileNo", iFileNo.ToString)
                            'oSubst.AddNewValue("DocumentNumber", iFileNo.ToString)
                            'oSubst.AddNewValue("PageNumber", iFileNo.ToString)
                            'oSubst.AddNewValue("FileNumber", iFileNo.ToString)

                            oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocFileNoValLabels", iFileNo.ToString, True)
                            oAttachedFileData = New clsAttachedFileData(oCreatedDoc.DestinationPath)
                            oParentGeneratedDocs.oParentCore.AssociatedFiles.Add(oAttachedFileData)
                            oAttachedFileData.WriteToLogFile()

                            'If sLogFilePath <> "" And sLogFileEntry <> "" Then
                            '    sLogFilePathUsed = oSubst.GetSubstitutionForString(sLogFilePath, clsValueSubstitutions.TargetModifier.ForFilePath)
                            '    sLogFileEntryUsed = oSubst.GetSubstitutionForString(sLogFileEntry)

                            '    oLogFileInfo = New IO.FileInfo(sLogFilePathUsed)
                            '    owrStream = oLogFileInfo.AppendText()
                            '    owrStream.AutoFlush = True
                            '    owrStream.WriteLine(sLogFileEntryUsed)
                            '    owrStream.Flush() : owrStream.Close() : owrStream = Nothing

                            'End If

                            sReturn = oCreatedDoc.DestinationPath

                        Next


                    Else

                        oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocPathValLabels", oFileInfo.FullName, True)
                        oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocFolderValLabels", oFileInfo.DirectoryName, True)


                        'oSubst.AddNewValue("DestinationPath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DestinationDocumentPath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DocumentPath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DocPath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DestDocumentPath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DestDocPath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DestinationFolder", oFileInfo.DirectoryName)
                        'oSubst.AddNewValue("DestinationFilePath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DestinationFileName", oFileInfo.FullName)
                        'oSubst.AddNewValue("DestinationFile", oFileInfo.FullName)
                        'oSubst.AddNewValue("FilePath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DestFilePath", oFileInfo.FullName)
                        'oSubst.AddNewValue("DestFolder", oFileInfo.DirectoryName)

                        sHTMLPassed = oParentGeneratedDocs.oParentCore.ApplySubstitution(sHTMLPassed)
                        'sHTMLPassed = oSubst.GetSubstitutionForString(sHTMLPassed)

                        'oFileInfo.AppendText().WriteLine(sHTMLPassed)
                        owrStream = oFileInfo.AppendText()
                        owrStream.AutoFlush = True
                        owrStream.WriteLine(sHTMLPassed)
                        owrStream.Flush() : owrStream.Close() : owrStream = Nothing

                        oAttachedFileData = New clsAttachedFileData(oFileInfo.FullName)
                        oParentGeneratedDocs.oParentCore.AssociatedFiles.Add(oAttachedFileData)
                        oAttachedFileData.WriteToLogFile()

                        'IO.File.WriteAllText(oFileInfo.FullName, sHTMLPassed)

                        'If sLogFilePath <> "" And sLogFileEntry <> "" Then
                        '    sLogFilePathUsed = oSubst.GetSubstitutionForString(sLogFilePath, clsValueSubstitutions.TargetModifier.ForFilePath)
                        '    sLogFileEntryUsed = oSubst.GetSubstitutionForString(sLogFileEntry)

                        '    oLogFileInfo = New IO.FileInfo(sLogFilePathUsed)
                        '    owrStream = oLogFileInfo.AppendText()
                        '    owrStream.AutoFlush = True
                        '    owrStream.WriteLine(sLogFileEntryUsed)
                        '    owrStream.Flush() : owrStream.Close() : owrStream = Nothing

                        'End If

                        sReturn = oFileInfo.FullName

                    End If

                    '    If aConvExtns.Contains(oFileInfo.Extension) Then

                    '        owrStream = oFileInfo.Create
                    '        owrStream.Write()
                End If

            End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sDocTemplateHTML <> "" Then oError.AddDetail("Raw HTML : " & sDocTemplateHTML)
            If oAttachedFileData IsNot Nothing Then oError.AddDetail("Attached file : " & oAttachedFileData.FileName)
            gsErr = goErrs.ToString

        End Try

        If owrStream IsNot Nothing Then owrStream.Flush() : owrStream.Close() : owrStream = Nothing
        If oFileInfo IsNot Nothing Then oFileInfo = Nothing
        If owsConvert IsNot Nothing Then owsConvert = Nothing
        If owsConvertibles IsNot Nothing Then owsConvertibles = Nothing
        If oConvParams IsNot Nothing Then oConvParams = Nothing


        Return sReturn
    End Function


End Class

