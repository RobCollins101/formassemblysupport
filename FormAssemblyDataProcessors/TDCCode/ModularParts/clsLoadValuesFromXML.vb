﻿Option Compare Text

Imports System.IO
Imports System.Xml

Public Class clsValuesFromStoredXML

    Protected Friend oParentCore As clsFormSupportCore

    Friend sFolderOfXMLFile As String
    Friend sPathOfXMLFile As String
    Friend oFileInfoOfXMLFile As FileInfo

    Friend aXMLDataValues As SortedList(Of String, String)


    Friend Property XMLDataValues As SortedList(Of String, String)
        Get
            If aXMLDataValues Is Nothing Then
                aXMLDataValues = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)


            End If
            Return aXMLDataValues
        End Get
        Set(value As SortedList(Of String, String))
            aXMLDataValues = value
        End Set
    End Property


    Friend Property FolderNameOfXMLFile As String
        Get
            If sFolderOfXMLFile = "" And oFileInfoOfXMLFile IsNot Nothing Then sFolderOfXMLFile = oFileInfoOfXMLFile.DirectoryName

            Return sFolderOfXMLFile
        End Get
        Set(value As String)
            If sFolderOfXMLFile <> value Then
                sFolderOfXMLFile = value
                oFileInfoOfXMLFile = Nothing
            End If

        End Set
    End Property

    Friend Property FilePathOfXMLFile As String
        Get
            If sPathOfXMLFile = "" And oFileInfoOfXMLFile IsNot Nothing Then sPathOfXMLFile = oFileInfoOfXMLFile.FullName

            Return sPathOfXMLFile
        End Get
        Set(value As String)
            If value <> sPathOfXMLFile Then
                sPathOfXMLFile = value
                oFileInfoOfXMLFile = Nothing
            End If

        End Set
    End Property

    Friend Property FileInfoOfXMLFile As FileInfo
        Get
            If oFileInfoOfXMLFile Is Nothing And sPathOfXMLFile <> "" Then
                oFileInfoOfXMLFile = New FileInfo(sPathOfXMLFile)
            End If
            Return oFileInfoOfXMLFile
        End Get
        Set(value As FileInfo)
            oFileInfoOfXMLFile = value
            sPathOfXMLFile = ""
            sFolderOfXMLFile = ""
        End Set
    End Property

    Protected Friend Sub LoadFromXML()

        Dim oValues As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
        Dim sKey As String, sValue As String

        Try
            If Me.aXMLDataValues.Count = 0 Then

                oValues = LoadValuesFromTempXMLFile(oParentCore.SessionID)
                For Each sKey In oValues.Keys
                    sValue = oValues.Item(sKey)

                    If Not oParentCore.oCurrentData.oDataFromPost.aFieldValues.ContainsKey(sKey) Then oParentCore.oCurrentData.oDataFromPost.aFieldValues.Add(sKey, sValue) Else oParentCore.oCurrentData.oDataFromPost.aFieldValues.Item(sKey) = sValue
                    If oParentCore.oFieldDataUsed.ContainsKey(sKey) Then oParentCore.oFieldDataUsed.Item(sKey) = sValue Else oParentCore.oFieldDataUsed.Add(sKey, sValue)

                Next
            End If

            'ProcessPostedDataValues(oCurrentData)

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKey <> "" Then oError.AddDetail("Key : " & sKey)
            If sValue <> "" Then oError.AddDetail("Value : " & sValue)
            gsErr = goErrs.ToString

        End Try
    End Sub


    Public Function LoadValuesFromTempXMLFile(sSessionID As String) As SortedList(Of String, String)
        'Dim oReturn As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
        Dim oDOM As XmlDocument, oRootElement As XmlElement
        Dim oValueSetElement As XmlElement, oValElement As XmlElement, oAttr As XmlAttribute
        Dim sKey As String, sValue As String, oNode As XmlNode
        Dim oTemp As Object, sFileKey As String ', sFolderOfXMLFile As String ', sPathOfXMLFile As String
        Dim aChildren As XmlNodeList, oOneValElement As XmlElement
        Dim oTblRow As TableRow, oTblCell As TableCell, sTemp As String

        Try
            If sFolderOfXMLFile = "" Then
                sFolderOfXMLFile = oParentCore.GetValueIfExists("TempValueStoreValLabels",,, "TempValueStore")
                If sFolderOfXMLFile = "" Then
                    oTemp = GetMySetting("TempValueStore")
                    If oTemp IsNot Nothing Then
                        sFolderOfXMLFile = oTemp.ToString
                    Else
                        sFolderOfXMLFile = IO.Path.GetTempPath
                    End If
                End If
                sFolderOfXMLFile &= IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1, 1) <> "\", "\", "").ToString
            End If

            sFileKey = oParentCore.SessionID & "_Values.xml"
            FilePathOfXMLFile = sFolderOfXMLFile & IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1, 1) <> "\", "\", "").ToString & sFileKey

            If FileInfoOfXMLFile.Exists Then
                'If IO.File.Exists(sPathOfXMLFile) Then

                oDOM = New XmlDocument()
                oDOM.Load(FilePathOfXMLFile)
                oRootElement = TryCast(oDOM.SelectSingleNode("Details"), XmlElement)
                If oRootElement IsNot Nothing Then

                    oNode = oRootElement.GetAttributeNode("DateCreated")
                    If oNode IsNot Nothing Then
                        sKey = "DateCreated"
                        sValue = oNode.Value
                        If XMLDataValues.ContainsKey(sKey) Then XMLDataValues.Item(sKey) = sValue Else XMLDataValues.Add(sKey, sValue)
                    End If

                    oNode = oRootElement.GetAttributeNode("SessionKey")
                    If oNode IsNot Nothing Then
                        sKey = "SessionKey"
                        sValue = oNode.Value
                        If XMLDataValues.ContainsKey(sKey) Then XMLDataValues.Item(sKey) = sValue Else XMLDataValues.Add(sKey, sValue)
                    End If

                    oValElement = TryCast(oRootElement.SelectSingleNode("Values"), XmlElement)
                    If oValElement IsNot Nothing Then


                        aChildren = oValElement.ChildNodes
                        For Each oNode In aChildren
                            oOneValElement = TryCast(oNode, XmlElement)
                            If oOneValElement IsNot Nothing Then
                                sKey = oOneValElement.Name
                                sValue = oOneValElement.InnerText
                                If XMLDataValues.ContainsKey(sKey) Then XMLDataValues.Item(sKey) = sValue Else XMLDataValues.Add(sKey, sValue)

                                If oParentCore.tblRecoveredStringValues IsNot Nothing Then
                                    oTblRow = New TableRow
                                    oTblCell = New TableCell
                                    oTblCell.Text = HttpUtility.HtmlEncode(sKey)
                                    oTblRow.Cells.Add(oTblCell)

                                    oTblCell = New TableCell
                                    sTemp = sValue
                                    oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
                                    oTblRow.Cells.Add(oTblCell)
                                    oParentCore.tblRecoveredStringValues.Rows.Add(oTblRow)
                                End If
                            End If
                        Next

                    End If
                    oParentCore.AddValuesToFieldData(XMLDataValues)
                End If
            End If


        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKey <> "" Then oError.AddDetail("Key : " & sKey)
            If sValue <> "" Then oError.AddDetail("Value : " & sValue)
            If oNode IsNot Nothing Then oError.AddDetail("Node name : " & oNode.Name)
            gsErr = goErrs.ToString

        End Try

        Return XMLDataValues
    End Function


    Friend Sub SaveSubmissionValuesToTempXMLFile()
        Try
            SaveValuesToTempXMLFile(oParentCore.ValuesFromHTTPRequest.SubmittedDataValues)
        Catch ex As Exception

        End Try
    End Sub


    Friend Sub SaveValuesToTempXMLFile(aValues As SortedList(Of String, String))
        Dim oDOM As XmlDocument, oRootElement As XmlElement
        Dim oElement As XmlElement, oValElement As XmlElement, oAttr As XmlAttribute
        Dim sKey As String, sValue As String
        Dim oTemp As Object, sFileKey As String ', sFileFolder As String, sFilePath As String

        Try

            'oTemp = GetMySetting("TempValueStore")
            'If oTemp IsNot Nothing Then
            '    sFileFolder = oTemp.ToString
            'Else
            '    sFileFolder = IO.Path.GetTempPath
            'End If
            'sFileFolder &= IIf(sFileFolder.Substring(sFileFolder.Length - 1, 1) <> "\", "\", "").ToString

            If sFolderOfXMLFile = "" Then
                sFolderOfXMLFile = oParentCore.GetValueIfExists("TempValueStoreValLabels",,, "TempValueStore")
                If sFolderOfXMLFile = "" Then
                    oTemp = GetMySetting("TempValueStore")
                    If oTemp IsNot Nothing Then
                        sFolderOfXMLFile = oTemp.ToString
                    Else
                        sFolderOfXMLFile = IO.Path.GetTempPath
                    End If
                End If
                sFolderOfXMLFile &= IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1, 1) <> "\", "\", "").ToString
            End If

            sFileKey = oParentCore.SessionID & "_Values.xml"
            FilePathOfXMLFile = sFolderOfXMLFile & IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1, 1) <> "\", "\", "").ToString & sFileKey


            oDOM = New XmlDocument
            oRootElement = oDOM.CreateElement("Details")
            oDOM.AppendChild(oRootElement)

            oAttr = oDOM.CreateAttribute("DateCreated")
            oAttr.Value = Now.ToString("o")
            oRootElement.Attributes.Append(oAttr)

            oAttr = oDOM.CreateAttribute("SessionKey")
            oAttr.Value = oParentCore.SessionID
            oRootElement.Attributes.Append(oAttr)

            oElement = oDOM.CreateElement("Values")
            oRootElement.AppendChild(oElement)

            'For Each sKey In aFieldValues.Keys

            'For Each sKey In oParentCore.ValuesFromHTTPRequest.SubmittedDataValues.Keys
            For Each sKey In aValues.Keys
                sValue = aValues.Item(sKey).ToString
                oValElement = oDOM.CreateElement(sKey)
                oValElement.InnerText = sValue
                oElement.AppendChild(oValElement)
            Next

            'sFileKey = sPaymentSessionID & "_Values.xml"
            'sFilePath = sFileFolder & IIf(sFileFolder.Substring(sFileFolder.Length - 1, 1) <> "\", "\", "").ToString & sFileKey

            'oDOM.Save(sFilePath)

            oDOM.Save(FilePathOfXMLFile)

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKey <> "" Then oError.AddDetail("Key : " & sKey)
            If sValue <> "" Then oError.AddDetail("Value : " & sValue)
            If oAttr IsNot Nothing Then oError.AddDetail("Attribute : " & oAttr.Name)
            If oElement IsNot Nothing Then oError.AddDetail("Element : " & oElement.Name)
            gsErr = goErrs.ToString

        End Try

    End Sub


End Class
