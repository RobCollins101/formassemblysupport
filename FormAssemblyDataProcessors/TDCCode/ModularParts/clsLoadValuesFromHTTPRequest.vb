﻿Option Compare Text

Public Class clsValuesFromHTTPRequest
    Private oDataValues As SortedList(Of String, String)

    Protected Friend oParentCore As clsFormSupportCore
    Protected Friend oDataFromPost As colLoadedFileData

    Friend ReadOnly Property SubmittedDataValues As SortedList(Of String, String)
        Get
            If oDataValues Is Nothing Then
                oDataValues = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

                ProcessFieldValues()

            End If
            Return oDataValues
        End Get
    End Property


    Friend Function ProcessFieldValues() As String 'oRequest As HttpRequest) As String ', aFilesDetected As ArrayList) As String ', oCurrentData As clsCurrentData) As String

        Dim sReturn As String = "", sKey As String, sValue As String
        Dim sTemp As String
        Dim oTblRow As TableRow, oTblCell As TableCell
        Dim aSpecialDebugFieldNames As New ArrayList, oTemp As Object

        Try

            If oDataValues.Count = 0 Then

                If oDataFromPost Is Nothing Then oDataFromPost = New colLoadedFileData
                oDataFromPost.Clear()
                If oParentCore.oRequest.QueryString.Count > 0 Then
                    For Each sKey In oParentCore.oRequest.QueryString.Keys
                        sKey = UCase(sKey)
                        sValue = oParentCore.oPage.Server.UrlDecode(oParentCore.oRequest.QueryString.Item(sKey))
                        If Not oDataFromPost.aFieldValues.ContainsKey(sKey) Then oDataFromPost.aFieldValues.Add(sKey, sValue) Else oDataFromPost.aFieldValues.Item(sKey) = sValue
                    Next
                End If

                For Each sKey In oParentCore.oRequest.Form.Keys
                    sKey = UCase(sKey)
                    sValue = oParentCore.oPage.Server.UrlDecode(oParentCore.oRequest.Form.Item(sKey))
                    If Not oDataFromPost.aFieldValues.ContainsKey(sKey) Then oDataFromPost.aFieldValues.Add(sKey, sValue) Else oDataFromPost.aFieldValues.Item(sKey) = sValue
                Next

                oParentCore.AddValuesToFieldData(oDataFromPost.aFieldValues)

                ' Find any debug indicators

                'oTemp = GetMySetting("DebugFieldNames")
                'If oTemp IsNot Nothing Then aSpecialDebugFieldNames = New ArrayList(CType(oTemp, Specialized.StringCollection))

                'For Each sTemp In aSpecialDebugFieldNames
                '    If Not oParentCore.bShowValues And oDataFromPost.aFieldValues.ContainsKey(sTemp) Then oParentCore.bShowValues = StringIsTrue(oDataFromPost.aFieldValues.Item(sTemp).ToString)
                '    'Exit For
                'Next

                oParentCore.bShowValues = StringIsTrue(oParentCore.GetValueIfExists("DebugFieldNames"))

                oDataValues = oDataFromPost.aFieldValues

                'oSubst.AddNewValues(oParentCore.oFieldDataUsed)

                If oParentCore.tblQueryStringValues IsNot Nothing Then

                    If oParentCore.oRequest.QueryString.Count > 0 Then
                        For Each sKey In oParentCore.oRequest.QueryString.Keys
                            oTblRow = New TableRow
                            sValue = oParentCore.oPage.Server.UrlDecode(oParentCore.oRequest.QueryString.Item(sKey))
                            oTblCell = New TableCell
                            oTblCell.Text = HttpUtility.HtmlEncode(sKey)
                            oTblRow.Cells.Add(oTblCell)

                            oTblCell = New TableCell
                            oTblCell.Text = HttpUtility.HtmlEncode(sValue)
                            oTblRow.Cells.Add(oTblCell)
                            oParentCore.tblQueryStringValues.Rows.Add(oTblRow)

                            oTblCell = New TableCell
                            sTemp = "<input type=""hidden"" name=""" & sKey & """ id=""" & sKey & """ value=""" & oParentCore.oPage.Server.UrlEncode(sValue) & """ />"
                            oTblCell.Text = sTemp
                            oTblRow.Cells.Add(oTblCell)
                            oParentCore.tblQueryStringValues.Rows.Add(oTblRow)
                            If sKey = "returnurl" Then oParentCore.sReturnURLToCaller = sValue
                        Next
                    End If
                End If

                If oParentCore.tblPostStringValues IsNot Nothing Then

                    If oParentCore.oRequest.Form.Count > 0 Then
                        For Each sKey In oParentCore.oRequest.Form.Keys
                            oTblRow = New TableRow
                            sValue = oParentCore.oPage.Server.UrlDecode(oParentCore.oRequest.Form.Item(sKey))
                            oTblCell = New TableCell
                            oTblCell.Text = HttpUtility.HtmlEncode(sKey)
                            oTblRow.Cells.Add(oTblCell)

                            oTblCell = New TableCell
                            sTemp = sValue
                            oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
                            oTblRow.Cells.Add(oTblCell)

                            oTblCell = New TableCell
                            sTemp = "<input type=""hidden"" name=""" & sKey & """ id=""" & sKey & """ value=""" & oParentCore.oPage.Server.UrlEncode(sValue) & """ />"
                            oTblCell.Text = sTemp
                            oTblRow.Cells.Add(oTblCell)
                            oParentCore.tblPostStringValues.Rows.Add(oTblRow)
                            If sKey = "returnurl" Then oParentCore.sReturnURLToCaller = sValue
                        Next
                    End If
                End If

            End If

        Catch ex As Exception
            Dim oError As wRuntimeError = goErrs.NewError(ex)
            If sKey <> "" Then oError.AddDetail("Key : " & sKey)
            If sValue <> "" Then oError.AddDetail("Value : " & sValue)
            gsErr = goErrs.ToString

            'lblProcessing.Text &= "ERROR : " & ex.ToString
            'sErrors &= "ERROR : " & ex.ToString
        End Try


        Return sReturn
    End Function


End Class
