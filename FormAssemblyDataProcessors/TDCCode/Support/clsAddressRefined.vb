Imports System.Text.RegularExpressions
Imports System.Xml.Serialization

' Version 1.1 
' Author : R Collins
' Date of initial creation : 1 Mar 2011
' =========================================
' Modified (1.1) By : R Collins : on ; 1 June 2011
' Change done : Proper case corrections for end results
' =========================================
' Modified (1.2) By : R Collins : on ; 20 July 2011
' Change done : Use Regex to identify the house number properly.
'										Use label prefixes as part of house number, eg Unit 1b
'										Use street parts to substitute for town details if no town detected
' =========================================
' Modified (1.3) By : R Collins : on ; 21 July 2011
'										Allow for partial postcode and near postcode detection
'                             Be more determined to identify town names from street
'										Use commas in street name
'										Join up all town names, county names in lists
' =========================================
' Modified (1.4) By : R Collins : on ; 26 July 2011
'                             Fix interpretation of street where town is in street by using a regex
'										Fix regex to identify numbering more accurately
'										Improved street/town differentiation
' =========================================
' Modified (1.5) By : R Collins : on 28 July 2011
'										Improved some aspects of town, street and Postcode recognition Regex
'										Implemented a special place as a street only address
' =========================================
' Modified (1.6) By : R Collins : on 10 Aug 2011
'										Updated Regex for street identification
'										Added some extra trimming of special characters to SuperTrim
' Modified (1.6.1) By : R Collins : on 10 Aug 2011
'										Minor additions to the SpecialPlace regex string
' =========================================
' Modified (1.7) By : R Collins : on 12 August 2011
'										Add detection for locality
'										Extend house and street address lines to 4
'										Add Thoroughfare and DependentThoroughfare
'										Add detection for Manchester, Birmingham, Glasgow and unusual London postcodes
' =========================================
' Modified (1.8) By : R Collins : on 12 Jan 2012
'										Added support for ArrayList on declaration (New) and sub InterpretAddress
' =========================================
' Modified (1.9.1) By : R Collins : on 30 Jan 2012
'										Prevented blank line entry from causing errors
' =========================================
' Modified (2.0.0) By : R Collins : on 31 Dec 2012
'										Amended street identification regex
' =========================================
' Modified (2.0.1) By : R Collins : on 20 May 2013
'										Prevented duplicating locality information
'										Amended postcode identification to take account the last line has any numeric character.
'										This is for foreign and testing postcode entries
'										Also added special recognition for PO boxes and town name start components
' =========================================
' Modified (2.0.2) By : R Collins : on 18 June 2014
'										Amended street identification regex with 'rise' as street identifier
' =========================================
' Modified (2.0.3) By : R Collins : on 15 January 2015
'										Added replacement of abbreviated street identifiers with full identifiers and
'										For VB.Net 3.5 onwards only
' =========================================
' Modified (2.0.4) By : R Collins : on 27 February 2015
'										Added a method to get a line based on zero based line number
'										Also made postcode upper case in full address
' =========================================
' Modified (2.0.5) By : R Collins : on 1 April 2015
'										Adjusted the town identifier to only take a full line or a word if the 
'										word is not the first word of the line
' =========================================
' Modified (2.0.6) By : R Collins : on 11 September 2015
'										Adjusted the street identifier where there is no house 
'                                       name/number and only one house name part
'										Sets the house name if the line does not match a street part
'										Also removed identification of 'end' as a street part in street ID regex
' =========================================
' Modified (2.1.0) By : R Collins : on 14 September 2015
'										Revamped the splitting of lines based on street identification to
'										allow correct identification of dependent thoroughtfares and similar
' =========================================
' Modified (2.1.1) By : R Collins : on 17 September 2015
'										Fixed an error where addresses like 3 St Catherines Close was having the St become Street
' =========================================
' Modified (2.1.2) By : R Collins : on 17 September 2015
'										Amended detection of house number to include potenial name part for everification
' =========================================
' Modified (2.1.3) By : R Collins : on 18 September 2015
'										Altered regex for house number detection to include caravan and No and PO Box
'										Also amended routine to isolate the house name/number correctly
'										Also added W for West, E for East, S for South and N for North for the counties
' =========================================
' Modified (2.1.4) By : R Collins : on 21 September 2015
'										Added regex to identify number as part of street line for the rare occurance where a number
'										has been specified on a separate line by itself
'										Also corrected Street identifier to not include just the house identifier
' =========================================
' Modified (2.1.5) By : R Collins : on 2 October 2015
'										Amended FullAddress to not return postcode unless specifically asked to
' =========================================
' Modified (2.1.6) By : R Collins : on 8 August 2016
'										Amended to use array declaration with assignment
'										Also amended properties to manage fake sets to appear as web service result
'                                       Also amended street definition to allow east|north|south|west after road identifier,
'                                       e.g. 8 Station Road east
' =========================================
' Modified (2.1.7) By : R Collins : on 28 November 2016
'										Amended street name identification in SPlitStreet to prevent inpappropriate 
'                                       joining of names when a room/flat exists in a house
'                                       Also adjusted 2 part county names from being mistaken as the one part overall, ie Sussex, West Sussex etc
' =========================================
' Modified (2.1.8) By : R Collins : on 6 March 2017
'                                       Corrected a bug in county identification where the county was on a line by itself
' =========================================
'
'
Public Class clsAddressRefine

    Private sPostcode As String = ""
    Private sCounty As String = ""
    Private sPostTown As String = ""
    Private sTown As String = ""
    Private sLocality As String = ""
    Private AddrLines As New ArrayList

    Private sRegexForPostcode As String = "^(([A-PR-UWYZ][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]?|([BEGLMNSW][0-9]{1,2})|(L\d{1,2}[A-Z])) {1,2}[0-9][ABD-HJLN-UW-Z]{1,2}|GIR 0AA)$"
    Private sRegexPartPostcode As String = "^(([A-Z$][A-Y0-9£$][AEHMNPRTVXY0-9£$]?[ABEHMNPRVWXY0-9]?)|([BEGLMNSW]\d{1,2})|(L\d{1,2}[A-Z]))$"
    Private sRegexNearPostcode As String = "([A-Z$][A-HK-Y$][oOIlLZzEASsbTB0-9£$]{1,2}|([BEGLMNSW]\d{1,2})|(L\d{1,2}[A-Z]))\s[oOIlLZzEASsbTB0-9£$]{1,2}[A-Z$]{1,2}$"
    Private sRegexJoinedPostcode As String = "(^|\s)([A-Z$][A-Y$]?[0-9$£]{1,2}|L\d{1,2}[A-Z])[0-9£$]{1,2}[A-Z£$]{1,2}$"
    Private sRegexIsHouseNumber As String = "^((at|all?otm?e?n?t?s?|ho?u?ses?|lo?dge?s?|co?tta?g?e?s?|fi?e?lds?|stores?|sites?|plots?|sta?ble?s?|hangers?|features?|la?nd|sho?ps?|cha?le?ts?|ca?bi?ns?|caravans?|units?|fla?ts?|bu?i?ldi?ngs?|offices?|rooms?|home|mobile\shome|No|P[.\s]?\\?O\.?(\sbox)?)(\s(no|at|on|in|((rear|left|right|north|south|east|west|(r|l)|/|(of?)?/?)\s)))*\s+)?(([C-Lc-lN-Zn-z0-9]?\d+[A-Za-z0-9]?)+\s*([-,/&+]|to|and))?\s*(([C-Lc-lN-Zn-z0-9]?\d+[A-Za-z0-9]?))$"
    Private sRegexIsHouseOnStreetNumber As String = "^(([C-Lc-lN-Zn-z0-9]?\d+[A-Za-z0-9]?)+\s*([-,/&+]|to|and))?\s*(([C-Lc-lN-Zn-z0-9]?\d+[A-Za-z0-9]?))$"
    Private sRegexIsStreet As String = "^(([aAmMbB]?[1-9+]\d*[aAbBcC]?(\s\w+)*)?(([a-zA-Z0-9-.,\\\/]+))\s+)+(access|all?e?y|appro?a?c?h?|aven?u?e?|aerodrome|ba?nk|be?a?ch|bri?d?ge?|bro?a?dwa?y|by-?pass|ca?na?l|ca?u?se?wa?y|ce?nte?re?|chase|clo?se?|co?mm?o?n|copse|co?r?ne?r|cottages|co?u?r?t|cresc?e?n?t?|cross|da?le?|dri?ve?|edge|end|espla?n?a?n?d?e?|esta?te?|fa?rm|fi?e?lds?|ga?r?a?ges?|ga?r?de?ns?|glen|gre?e?nwa?y?|gro?ve?|he?a?th|he?i?ghts?|hurst|inte?rcha?n?ge?|isla?n?d?e?|knoll|la?ne?|ma?no?r|me?a?de?s|me?a?do?ws?|mews|mount|oaks?|orcha?rds?|pa?ddo?cks?|pa?ra?de?|pa?rks?i?d?e?|pla?c?e?|precinct|rise|ro?a?d|rows?|shaw|shops|site|slip|squ?a?r?e?|sta?t?i?o?n|str?e?e?t?|terr?a?c?e?|to?we?rs?|trees|vale|view|villas?|walk|way|wharfs?|wood(land)?s?|walk)(\s+(east|north|south|west))?$"
    Private aUpdateStreetID As New Dictionary(Of String, String) From {{"ACC", "ACCESS"},
              {"AER", "AERODROME"},
              {"ALLEE", "ALLEY"},
              {"ALLY", "ALLEY"},
              {"ALY", "ALLEY"},
              {"ANNX", "ANEX"},
              {"ANX", "ANEX"},
              {"ARC", "ARCADE"},
              {"AV", "AVENUE"},
              {"AVE", "AVENUE"},
              {"AVEN", "AVENUE"},
              {"AVENU", "AVENUE"},
              {"AVN", "AVENUE"},
              {"AVNUE", "AVENUE"},
              {"BAYOO", "BAYOU"},
              {"BCH", "BEACH"},
              {"BLF", "BLUFF"},
              {"BLUF", "BLUFF"},
              {"BLUFFS", "BLUFFS"},
              {"BLVD", "BOULEVARD"},
              {"BND", "BEND"},
              {"BNK", "BANK"},
              {"BOT", "BOTTOM"},
              {"BOTTM", "BOTTOM"},
              {"BOUL", "BOULEVARD"},
              {"BOULV", "BOULEVARD"},
              {"BR", "BRANCH"},
              {"BRD", "BROADWAY"},
              {"BRDGE", "BRIDGE"},
              {"BRG", "BRIDGE"},
              {"BRK", "BROOK"},
              {"BRNCH", "BRANCH"},
              {"BROOKS", "BROOKS"},
              {"BTM", "BOTTOM"},
              {"BYP", "BYPASS"},
              {"BYPA", "BYPASS"},
              {"BYPAS", "BYPASS"},
              {"BYPS", "BYPASS"},
              {"CANYN", "CANYON"},
              {"CAUSWA", "CAUSEWAY"},
              {"CEN", "CENTER"},
              {"CENT", "CENTER"},
              {"CENTERS", "CENTERS"},
              {"CENTR", "CENTER"},
              {"CENTRE", "CENTER"},
              {"CHS", "CHASE"},
              {"CIR", "CIRCLE"},
              {"CIRC", "CIRCLE"},
              {"CIRCL", "CIRCLE"},
              {"CL", "CLOSE"},
              {"CLB", "CLUB"},
              {"CLF", "CLIFF"},
              {"CLFS", "CLIFFS"},
              {"CLS", "CLOSE"},
              {"CMN", "COMMON"},
              {"CMP", "CAMP"},
              {"CNL", "CANAL"},
              {"CNTER", "CENTER"},
              {"CNTR", "CENTER"},
              {"CNYN", "CANYON"},
              {"COR", "CORNER"},
              {"CORS", "CORNERS"},
              {"COTT", "COTTAGE"},
              {"COTTS", "COTTAGES"},
              {"CP", "CAMP"},
              {"CPE", "CAPE"},
              {"CPS", "COPSE"},
              {"CRCL", "CIRCLE"},
              {"CRCLE", "CIRCLE"},
              {"CRES", "CRESCENT"},
              {"CRK", "CREEK"},
              {"CRNR", "CORNER"},
              {"CROSSING", "CROSSING"},
              {"CRS", "CROSS"},
              {"CRSE", "COURSE"},
              {"CRSENT", "CRESCENT"},
              {"CRSNT", "CRESCENT"},
              {"CRSSNG", "CROSSING"},
              {"CSWY", "CAUSEWAY"},
              {"CT", "COURT"},
              {"CTR", "CENTER"},
              {"CTS", "COURTS"},
              {"CV", "COVE"},
              {"DALE", "DALE"},
              {"DAM", "DAM"},
              {"DIV", "DIVIDE"},
              {"DL", "DALE"},
              {"DM", "DAM"},
              {"DR", "DRIVE"},
              {"DRIV", "DRIVE"},
              {"DRIVES", "DRIVES"},
              {"DRV", "DRIVE"},
              {"DV", "DIVIDE"},
              {"DVD", "DIVIDE"},
              {"END", "END"},
              {"ESPL", "ESPLANADE"},
              {"EST", "ESTATE"},
              {"ESTS", "ESTATES"},
              {"EXP", "EXPRESSWAY"},
              {"EXPR", "EXPRESSWAY"},
              {"EXPRESS", "EXPRESSWAY"},
              {"EXPW", "EXPRESSWAY"},
              {"EXPY", "EXPRESSWAY"},
              {"EXT", "EXTENSION"},
              {"EXTN", "EXTENSION"},
              {"EXTNSN", "EXTENSION"},
              {"EXTS", "EXTENSIONS"},
              {"FLD", "FIELD"},
              {"FLDS", "FIELDS"},
              {"FLS", "FALLS"},
              {"FLT", "FLAT"},
              {"FLTS", "FLATS"},
              {"FORESTS", "FOREST"},
              {"FORG", "FORGE"},
              {"FRD", "FORD"},
              {"FREEWY", "FREEWAY"},
              {"FRG", "FORGE"},
              {"FRK", "FORK"},
              {"FRKS", "FORKS"},
              {"FRM", "FARM"},
              {"FRRY", "FERRY"},
              {"FRST", "FOREST"},
              {"FRT", "FORT"},
              {"FRWAY", "FREEWAY"},
              {"FRWY", "FREEWAY"},
              {"FRY", "FERRY"},
              {"FT", "FORT"},
              {"FWY", "FREEWAY"},
              {"GARDN", "GARDEN"},
              {"GATEWY", "GATEWAY"},
              {"GATWAY", "GATEWAY"},
              {"GDNS", "GARDENS"},
              {"GGE", "GARAGE"},
              {"GLN", "GLEN"},
              {"GRDEN", "GARDEN"},
              {"GRDN", "GARDEN"},
              {"GRDNS", "GARDENS"},
              {"GRN", "GREEN"},
              {"GRNWY", "GREENWAY"},
              {"GROV", "GROVE"},
              {"GRV", "GROVE"},
              {"GTWAY", "GATEWAY"},
              {"GTWY", "GATEWAY"},
              {"HARB", "HARBOR"},
              {"HARBR", "HARBOR"},
              {"HBR", "HARBOR"},
              {"HIGHWY", "HIGHWAY"},
              {"HIWAY", "HIGHWAY"},
              {"HIWY", "HIGHWAY"},
              {"HL", "HILL"},
              {"HLLW", "HOLLOW"},
              {"HLS", "HILLS"},
              {"HOLLOWS", "HOLLOW"},
              {"HOLW", "HOLLOW"},
              {"HOLWS", "HOLLOW"},
              {"HRBOR", "HARBOR"},
              {"HST", "HURST"},
              {"HT", "HEIGHTS"},
              {"HTH", "HEATH"},
              {"HTS", "HEIGHTS"},
              {"HVN", "HAVEN"},
              {"HWAY", "HIGHWAY"},
              {"HWY", "HIGHWAY"},
              {"INLT", "INLET"},
              {"INTCH", "INTERCHANGE"},
              {"IS", "ISLAND"},
              {"ISLES", "ISLE"},
              {"ISLND", "ISLAND"},
              {"ISLNDS", "ISLANDS"},
              {"ISS", "ISLANDS"},
              {"JCT", "JUNCTION"},
              {"JCTION", "JUNCTION"},
              {"JCTN", "JUNCTION"},
              {"JCTNS", "JUNCTIONS"},
              {"JCTS", "JUNCTIONS"},
              {"JUNCTN", "JUNCTION"},
              {"JUNCTON", "JUNCTION"},
              {"KNL", "KNOLL"},
              {"KNLS", "KNOLLS"},
              {"KNOL", "KNOLL"},
              {"KY", "KEY"},
              {"KYS", "KEYS"},
              {"LCK", "LOCK"},
              {"LCKS", "LOCKS"},
              {"LDG", "LODGE"},
              {"LDGE", "LODGE"},
              {"LF", "LOAF"},
              {"LGT", "LIGHT"},
              {"LK", "LAKE"},
              {"LKS", "LAKES"},
              {"LN", "LANE"},
              {"LNDG", "LANDING"},
              {"LNDNG", "LANDING"},
              {"LODG", "LODGE"},
              {"LOOPS", "LOOP"},
              {"MDW", "MEADOW"},
              {"MDWS", "MEADOWS"},
              {"MEDOWS", "MEADOWS"},
              {"MEDS", "MEADES"},
              {"MISSN", "MISSION"},
              {"MNR", "MANOR"},
              {"MNRS", "MANORS"},
              {"MNT", "MOUNT"},
              {"MNTAIN", "MOUNTAIN"},
              {"MNTN", "MOUNTAIN"},
              {"MNTNS", "MOUNTAINS"},
              {"MOUNTIN", "MOUNTAIN"},
              {"MSSN", "MISSION"},
              {"MT", "MOUNT"},
              {"MTIN", "MOUNTAIN"},
              {"MTN", "MOUNTAIN"},
              {"MWS", "MEWS"},
              {"NCK", "NECK"},
              {"OAKS", "OAKS"},
              {"ORCH", "ORCHARD"},
              {"ORCHRD", "ORCHARD"},
              {"OVL", "OVAL"},
              {"PARKWY", "PARKWAY"},
              {"PATHS", "PATH"},
              {"PDCK", "PADDOCK"},
              {"PDCKS", "PADDOCKS"},
              {"PIKES", "PIKE"},
              {"PKSD", "PARKSIDE"},
              {"PKWAY", "PARKWAY"},
              {"PKWY", "PARKWAY"},
              {"PKWYS", "PARKWAYS"},
              {"PKY", "PARKWAY"},
              {"PL", "PLACE"},
              {"PLN", "PLAIN"},
              {"PLNS", "PLAINS"},
              {"PLZ", "PLAZA"},
              {"PLZA", "PLAZA"},
              {"PNES", "PINES"},
              {"PR", "PRAIRIE"},
              {"PRC", "PRECINCT"},
              {"PRD", "PARADE"},
              {"PRK", "PARK"},
              {"PRR", "PRAIRIE"},
              {"PRT", "PORT"},
              {"PRTS", "PORTS"},
              {"PT", "POINT"},
              {"PTS", "POINTS"},
              {"RAD", "RADIAL"},
              {"RADIEL", "RADIAL"},
              {"RADL", "RADIAL"},
              {"RANCHES", "RANCH"},
              {"RD", "ROAD"},
              {"RDG", "RIDGE"},
              {"RDGE", "RIDGE"},
              {"RDGS", "RIDGES"},
              {"RDS", "ROADS"},
              {"RIS", "RISE"},
              {"RIV", "RIVER"},
              {"RIVR", "RIVER"},
              {"RNCH", "RANCH"},
              {"RNCHS", "RANCH"},
              {"RPD", "RAPID"},
              {"RPDS", "RAPIDS"},
              {"RST", "REST"},
              {"RVR", "RIVER"},
              {"RW", "ROW"},
              {"RWS", "ROWS"},
              {"SHL", "SHOAL"},
              {"SHLS", "SHOALS"},
              {"SHOAR", "SHORE"},
              {"SHOARS", "SHORES"},
              {"SHP", "SHOPS"},
              {"SHR", "SHORE"},
              {"SHRS", "SHORES"},
              {"SHT", "SITE"},
              {"SHW", "SHAW"},
              {"SLP", "SLIP"},
              {"SMT", "SUMMIT"},
              {"SPG", "SPRING"},
              {"SPGS", "SPRINGS"},
              {"SPNG", "SPRING"},
              {"SPNGS", "SPRINGS"},
              {"SPRNG", "SPRING"},
              {"SPRNGS", "SPRINGS"},
              {"SQ", "SQUARE"},
              {"SQR", "SQUARE"},
              {"SQRE", "SQUARE"},
              {"SQRS", "SQUARES"},
              {"SQU", "SQUARE"},
              {"STA", "STATION"},
              {"STATN", "STATION"},
              {"STN", "STATION"},
              {"STR", "STREET"},
              {"STRA", "STRAVENUE"},
              {"STRAV", "STRAVENUE"},
              {"STRAVEN", "STRAVENUE"},
              {"STRAVN", "STRAVENUE"},
              {"STREME", "STREAM"},
              {"STRM", "STREAM"},
              {"STRT", "STREET"},
              {"STRVN", "STRAVENUE"},
              {"STRVNUE", "STRAVENUE"},
              {"SUMIT", "SUMMIT"},
              {"SUMITT", "SUMMIT"},
              {"TER", "TERRACE"},
              {"TERR", "TERRACE"},
              {"TRACES", "TRACE"},
              {"TRACKS", "TRACK"},
              {"TRAILS", "TRAIL"},
              {"TRAK", "TRACK"},
              {"TRCE", "TRACE"},
              {"TRK", "TRACK"},
              {"TRKS", "TRACK"},
              {"TRL", "TRAIL"},
              {"TRLR", "TRAILER"},
              {"TRLRS", "TRAILER"},
              {"TRLS", "TRAIL"},
              {"TRNPK", "TURNPIKE"},
              {"TRS", "TREES"},
              {"TUNEL", "TUNNEL"},
              {"TUNL", "TUNNEL"},
              {"TUNLS", "TUNNEL"},
              {"TUNNELS", "TUNNEL"},
              {"TUNNL", "TUNNEL"},
              {"TURNPK", "TURNPIKE"},
              {"TWR", "TOWER"},
              {"TWRS", "TOWERS"},
              {"UN", "UNION"},
              {"VALLY", "VALLEY"},
              {"VDCT", "VIADUCT"},
              {"VIA", "VIADUCT"},
              {"VIADCT", "VIADUCT"},
              {"VILL", "VILLAGE"},
              {"VILLAG", "VILLAGE"},
              {"VILLG", "VILLAGE"},
              {"VILLIAGE", "VILLAGE"},
              {"VIS", "VISTA"},
              {"VIST", "VISTA"},
              {"VL", "VALE"},
              {"VLA", "VILLA"},
              {"VLAS", "VILLAS"},
              {"VLG", "VILLAGE"},
              {"VLGS", "VILLAGES"},
              {"VLLY", "VALLEY"},
              {"VLY", "VALLEY"},
              {"VLYS", "VALLEYS"},
              {"VST", "VISTA"},
              {"VSTA", "VISTA"},
              {"VW", "VIEW"},
              {"VWS", "VIEWS"},
              {"WD", "WOOD"},
              {"WDLND", "WOODLAND"},
              {"WDLNDS", "WOODLANDS"},
              {"WDS", "WOODS"},
              {"WK", "WALK"},
              {"WLS", "WELLS"},
              {"WY", "WAY"},
              {"XING", "CROSSING"}}


    Private sRegexSpecialPlace As String = "^(([a-zA-Z:;,.']+\s+)+(HOSPITAL|(PRE)?SCHOOL|ABBEY|ACADEMY|UNIVERSITY|COLLEGE|CAMP|CAMPUS|ESTATE|MANOR|COMMUNE|COMMUNITY|CASTLE|FORT|AIRFIELD|FARM|PRISON|COMPOUND|HOUSE|COTTAGE|CLUB|CENTRE|CENTER|OFFICES?|MALL|INN|HOTEL|HALLS?|HOME|CHURCH)|P[\/.,-]?O(\s|[\/_\-.:;])BOX(\s|[\/_\-.:;])(\w|\s|[.,:;])+|(HOSPITAL|(PRE)?SCHOOL|ABBEY|ACADEMY|UNIVERSITY|COLLEGE|CAMP|CAMPUS|ESTATE|MANOR|COMMUNE|COMMUNITY|CASTLE|FORT|AIRFIELD|FARM|PRISON|COMPOUND|INN|HOTEL|HALLS?|HOME|CHURCH)(\s+[a-zA-Z:;,.']+)+)$"
    Private sRegexTownPartName As String = "^(NEW|E(AST)?|W(EST)?|N(ORTH)?|S(OUTH)?|OLD|HIGH|LOW)(\s|\.)*$"
    Private LineDelims As String = vbCrLf & ",:;|" & vbTab
    protected friend hlCounties As ArrayList
    protected friend hlPostTowns As ArrayList
    protected friend hlTowns As ArrayList
    Private hlWordsNotToCapitalise As ArrayList
    Private hlSubsequentLCIfPrevLC As ArrayList
    Public NoTownPassed As Boolean
    Private sHouseName As String = ""
    Private sHouseNum As String = ""
    Private sStreetName As String = ""

    Public ErrorText As String = ""

    '<System.ComponentModel.Description("Interpret the address by breaking it down using known lists to it's component parts from arrayList")> _
    'Public Sub InterpretAddress(ByVal arrLines As ArrayList)
    '	Dim aLines() As String
    '	aLines = CType(arrLines.ToArray(GetType(System.String)), String())
    '	InterpretAddress(aLines)

    'End Sub



    <System.ComponentModel.Description("Interpret the address by breaking it down using known lists to it's component parts from string array")>
    Public Sub InterpretAddress(ByVal aLines() As String)
        InterpretAddress(New ArrayList(aLines))
    End Sub


    <System.ComponentModel.Description("Interpret the address by breaking it down using known lists to it's component parts from ArrayList")>
    Public Sub InterpretAddress(ByVal aLines As ArrayList)
        Dim sWord As String, iLinePos As Integer, sLine As String, sWords() As String, iPos2 As Integer
        Dim sString As String = "", sTestString As String, aTestString() As String, iPos3 As Integer
        Dim bFoundPostcode As Boolean = False, bFoundCounty As Boolean = False, bFoundPostTown As Boolean = False, bFoundTown As Boolean = False
        Dim sPostcodeFound As String = "", sCountyFound As String = "", sTownFound As String = "", sPostTownFound As String = "", sTestFullTown As String
        Dim oRegexPostcode As New Regex(sRegexForPostcode, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Dim oRegexNearPostcode As New Regex(sRegexNearPostcode, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Dim oRegexPartPostcode As New Regex(sRegexPartPostcode, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Dim oRegexStreetName As New Regex(sRegexIsStreet, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Dim oRegexJoinedPostcode As New Regex(sRegexJoinedPostcode, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Dim oRegexNum As New Regex(sRegexIsHouseNumber, RegexOptions.IgnoreCase), sAccumStreet As String = ""
        Dim oRegexNumOnStreet As New Regex(sRegexIsHouseOnStreetNumber, RegexOptions.IgnoreCase)
        Dim oRegexSpecialPlace As New Regex(sRegexSpecialPlace, RegexOptions.IgnoreCase)
        Dim oRegesIsPartTownName As New Regex(sRegexTownPartName, RegexOptions.IgnoreCase)

        Dim oMatchString As Match, iParts As Integer
        Dim sErrorPoint As String = "Initialisation", sChar As String = "", sChar2 As String = "", sTemp As String = "", aParts() As String, bFoundStreet As Boolean = False, sTemp2 As String = ""
        Dim sTestLine As String, aWords() As String, iWordPos As Integer, aTemp As ArrayList, iAddrLinePos As Integer
        Dim iTemp As Integer, sNextWord As String = "", sTestString2 As String = ""


        Try

            If (hlCounties Is Nothing OrElse hlCounties.Count = 0) Or (hlPostTowns Is Nothing OrElse hlPostTowns.Count = 0) Or
            (hlWordsNotToCapitalise Is Nothing OrElse hlWordsNotToCapitalise.Count = 0) Or (hlTowns Is Nothing OrElse hlTowns.Count = 0) Then IntialiseContantArrays()

            If NoTownPassed Then bFoundTown = True

            If aLines IsNot Nothing AndAlso aLines.Count > 0 Then

                sErrorPoint = "Stage 1 : Preparing aLines by sub DivideLines"

                For iLinePos = 0 To aLines.Count - 1
                    sTemp = Trim(Replace(CStr(aLines(iLinePos)), "   ", vbTab))
                    Do While Regex.IsMatch(sTemp, "\d+\w?(,)\s+[a-zA-Z]+")
                        sTemp = Regex.Replace(sTemp, "(?<Before>\d+\w?\s*)(,)(?<After>\s*[a-zA-Z]+)", "${Before} ${After}")
                    Loop
                Next

                aLines = DivideLines(aLines, LineDelims)

                For iLinePos = aLines.Count - 2 To 0 Step -1
                    'aLines(iLinePos) = SuperTrim(CStr(aLines(iLinePos)))

                    If iLinePos < aLines.Count - 1 Then
                        sTestLine = aLines(iLinePos).ToString
                        sTestLine = SuperTrim(sTestLine)
                        aWords = Split(sTestLine, " ")

                        If aWords.Count > 2 Then
                            sAccumStreet = ""
                            For iWordPos = UBound(aWords) To 0 Step -1
                                sWord = aWords(iWordPos)
                                If iWordPos > 1 Then
                                    If sAccumStreet = "" AndAlso aUpdateStreetID.ContainsKey(UCase(sWord)) Then sWord = aUpdateStreetID.Item(UCase(sWord)).ToString
                                    If aUpdateStreetID.ContainsValue(UCase(sWord)) And iLinePos > 0 Then
                                        If sAccumStreet <> "" And oRegexStreetName.IsMatch(sAccumStreet) Then
                                            sTemp = ""
                                            For iTemp = 0 To iWordPos : sTemp = sTemp & IIf(sTemp <> "", " ", "").ToString & aWords(iTemp) : Next
                                            If oRegexStreetName.IsMatch(sTemp) And Not (oRegexNum.IsMatch(aWords(iWordPos - 1))) Then
                                                aLines.Insert(iLinePos + 1, sAccumStreet)
                                                sAccumStreet = ""
                                            End If
                                        End If
                                    End If
                                End If
                                sAccumStreet = Trim(sWord & " " & sAccumStreet)
                            Next
                            aLines(iLinePos) = sAccumStreet
                        End If

                    End If

                Next
                AddrLines = New ArrayList

                sString = ""

                sErrorPoint = "Stage 2 : Searching for the address town, county and postcode"

                If aLines.Count > 0 Then

                    For iLinePos = aLines.Count - 1 To 0 Step -1
                        sLine = aLines(iLinePos).ToString
                        sLine = FullTrim(sLine)
                        If oRegexStreetName.IsMatch(sLine) And Not bFoundStreet And Not (oRegexNearPostcode.IsMatch(sLine) And iLinePos = aLines.Count - 1) And (bFoundPostTown Or bFoundTown) Then
                            '							sWords = Split(UCase(sLine), " ")

                            'If aUpdateStreetID.ContainsKey(sWords(UBound(sWords))) Then
                            '	sWords(UBound(sWords)) = aUpdateStreetID.Item(sWords(UBound(sWords)))
                            '	sLine = CapitaliseLine(Join(sWords, " "))
                            '	aLines(iLinePos) = sLine
                            'End If
                            bFoundStreet = True
                            If Trim(sString) <> "" Then
                                bFoundTown = True
                                If sString <> sTownFound Then sTownFound = Trim(sString & IIf(sTownFound <> "", ", " & sTownFound, "").ToString)
                            End If
                            sString = ""
                        Else

                        End If

                        If bFoundStreet Or AddrLines.Count > 0 Then
                            AddrLines.Insert(0, CapitaliseLine(sLine))
                            sLine = ""
                        Else


                            If oRegexJoinedPostcode.IsMatch(sLine) And iLinePos = aLines.Count - 1 Then
                                sLine = Left(sLine, Len(sLine) - 3) & " " & Mid(sLine, Len(sLine) - 2)
                            End If

                            If oRegexNearPostcode.IsMatch(sLine) And iLinePos > aLines.Count - 1 And Not (bFoundPostcode Or bFoundCounty Or bFoundTown Or bFoundPostTown) Then
                                oMatchString = oRegexNearPostcode.Match(sLine)
                                If oMatchString.Index > 0 Then
                                    'ReDim Preserve aLines(aLines.Count)
                                    iLinePos += 1
                                    aLines(iLinePos) = oMatchString.Value
                                    aLines(iLinePos - 1) = Left(sLine, oMatchString.Index)
                                    '						Else

                                End If
                                sTemp = oMatchString.Value
                                '						sTemp = sLine
                                For iPos2 = 3 To Len(sTemp) - 2
                                    sChar = Mid(sTemp, iPos2, 1)
                                    Select Case sChar
                                        Case "o", "O" : sChar2 = "0"
                                        Case "l", "I", "L" : sChar2 = "1"
                                        Case "z", "Z" : sChar2 = "2"

                                        Case "E", "£" : sChar2 = "3"
                                        Case "A" : sChar2 = "4"
                                        Case "s", "S", "$" : sChar2 = "5"
                                        Case "b" : sChar2 = "6"
                                        Case "T" : sChar2 = "7"
                                        Case "B" : sChar2 = "8"
                                        Case Else : sChar2 = sChar
                                    End Select
                                    sTemp = Left(sTemp, iPos2 - 1) & sChar2 & Mid(sTemp, iPos2 + 1)
                                Next
                                sLine = sTemp
                            End If

                            sTemp = UCase(sLine)
                            If (oRegexPostcode.IsMatch(sLine) Or oRegexPartPostcode.IsMatch(sLine) Or oRegexNearPostcode.IsMatch(sLine)) And (Not (hlPostTowns.Contains(sTemp) Or hlTowns.Contains(sTemp) Or hlCounties.Contains(sTemp))) And (Not (bFoundPostcode Or bFoundCounty Or bFoundPostTown Or bFoundTown) Or (iLinePos = aLines.Count - 1)) And aLines.Count > 1 And Regex.IsMatch(sLine, "\d") And Len(sLine) < 10 Then
                                aParts = Split(sLine, " ")
                                If UBound(aParts) >= 2 Then
                                    sPostcodeFound = UCase(Trim(aParts(UBound(aParts) - 1) & " " & aParts(UBound(aParts))))
                                    If oRegexNearPostcode.IsMatch(sPostcodeFound) Or oRegexPostcode.IsMatch(sPostcodeFound) Then
                                        bFoundPostcode = True
                                        aParts(UBound(aParts)) = ""
                                        aParts(UBound(aParts) - 1) = ""
                                        sLine = Trim(Join(aParts, " "))
                                    ElseIf (iLinePos = aLines.Count - 1 And aLines.Count > 1 And Regex.IsMatch(sLine, "\d")) Then
                                        sPostcodeFound = UCase(sLine)
                                        bFoundPostcode = True
                                        sString = ""
                                        aLines(iLinePos) = ""
                                        sLine = ""
                                    Else
                                        sPostcodeFound = ""
                                    End If
                                Else
                                    sPostcodeFound = UCase(sLine)
                                    bFoundPostcode = True
                                    sString = ""
                                    aLines(iLinePos) = ""
                                    sLine = ""
                                End If
                            End If

                            If sLine > "" Then

                                If Not bFoundStreet And ((sString <> "" And Not bFoundTown) Xor (Not bFoundTown And bFoundPostTown And Not oRegexNum.IsMatch(sLine) And iLinePos > 1)) Then

                                    If oRegexNum.IsMatch(sLine) Or iLinePos > 1 Then
                                        If bFoundPostTown And sString = "" Then
                                            If sLine <> sPostTown Then
                                                sTownFound = sLine
                                                bFoundTown = True
                                            End If
                                            sLine = ""
                                        Else
                                            sTownFound = sString
                                            bFoundTown = True
                                        End If
                                        sString = ""
                                    End If
                                End If

                                sWords = Split(sLine, " ")
                                For iPos2 = sWords.Length - 1 To 0 Step -1

                                    If Not (bFoundPostTown And bFoundTown) And iPos2 > 0 And iLinePos > 0 Then

                                        sErrorPoint = "Stage 2.1 : Preparing the line by checking the individual words"

                                        sTestFullTown = ""
                                        For iPos3 = 0 To iPos2
                                            If sWords(iPos3) <> "" Then sTestFullTown &= IIf(sTestFullTown <> "", " ", "").ToString & sWords(iPos3)
                                        Next

                                        If iPos2 > 0 Then
                                            sErrorPoint = "Stage 2.2 : Checking for components using hlCounties, hlPostTowns, hlTowns and " & sTestFullTown
                                            If Not (bFoundCounty Or bFoundPostTown Or bFoundTown) And (hlCounties.Contains(UCase(sTestFullTown))) Then
                                                bFoundCounty = True
                                                sCountyFound = sTestFullTown
                                                Exit For
                                                sString = ""

                                            ElseIf Not (bFoundPostTown Or bFoundTown) And (hlPostTowns.Contains(UCase(sTestFullTown))) Then
                                                bFoundPostTown = True
                                                sPostTownFound = sTestFullTown
                                                Exit For
                                                sString = ""

                                            ElseIf Not (bFoundTown) And (hlTowns.Contains(UCase(sTestFullTown))) Then
                                                bFoundTown = True
                                                sTownFound = sTestFullTown
                                                Exit For
                                                sString = ""

                                            End If
                                        End If
                                    End If

                                    sWord = CStr(sWords.GetValue(iPos2))
                                    If iPos2 > 0 Then sNextWord = CStr(sWords.GetValue(iPos2 - 1)) Else sNextWord = ""

                                    sErrorPoint = "Stage 3 : Checking second time for components using hlCounties, hlPostTowns, hlTowns and " & sWord

                                    If sWord > "" Then
                                        sString = Trim(sWords(iPos2) & " " & sString)
                                        aTestString = Split(sString, " ")
                                        sTestString = ""
                                        For iPos3 = 0 To CInt((IIf(UBound(aTestString) = 0, 0, 1)))
                                            sTestString = CStr(IIf(iPos3 > 0, sTestString & " ", "")) & aTestString(iPos3)
                                        Next

                                        sErrorPoint = "Stage 3.2 : Checking second time for components using Postcode Regex, hlCounties, hlPostTowns, hlTowns and " & sTestString
                                        sTestString2 = Trim(sNextWord & " " & sTestString)

                                        If oRegexPostcode.IsMatch(sTestString) And Not (bFoundPostcode Or bFoundCounty Or bFoundPostTown Or bFoundTown) Then
                                            sPostcodeFound = sTestString
                                            bFoundPostcode = True
                                            sString = ""

                                        ElseIf Not (bFoundCounty Or bFoundPostTown Or bFoundTown) And (
                                            (hlCounties.Contains(UCase(sTestString)) And (Not hlCounties.Contains(UCase(sTestString2)) Or sTestString2 = "" Or sTestString2 = sTestString)) Or
                                            (hlCounties.Contains(UCase(sWord)) And Not hlCounties.Contains(UCase(sNextWord & " " & sWord)) And iPos2 > 0)) Then
                                            bFoundCounty = True
                                            sCountyFound = CStr(IIf(hlCounties.Contains(UCase(sTestString)), sTestString, sWord))
                                            sString = ""

                                        ElseIf Not (bFoundPostTown Or bFoundTown) And (hlPostTowns.Contains(UCase(sTestString)) Or (hlPostTowns.Contains(UCase(sWord)) And iPos2 > 0)) Then
                                            bFoundPostTown = True
                                            sPostTownFound = CStr(IIf(hlPostTowns.Contains(UCase(sTestString)), sTestString, sWord))
                                            sString = ""

                                        ElseIf Not (bFoundTown) And (hlTowns.Contains(UCase(sTestString))) Then
                                            bFoundTown = True
                                            sTownFound = sTestString
                                            sString = ""

                                        End If
                                    End If
                                Next

                                sErrorPoint = "Stage 3.2 : Capitalising the current line " & sString & " and adding it to the array"
                                If sString > "" Then
                                    If oRegexStreetName.IsMatch(sString) Then
                                        sWords = Split(UCase(sString), " ")
                                        If aUpdateStreetID.ContainsKey(sWords(UBound(sWords))) Then
                                            sWords(UBound(sWords)) = aUpdateStreetID.Item(sWords(UBound(sWords))).ToString
                                            sString = CapitaliseLine(Trim(Join(sWords, " ")))
                                        End If
                                    End If

                                    If sString > "" And (bFoundTown Or bFoundPostTown) Then
                                        AddrLines.Insert(0, CapitaliseLine(sString))
                                        sString = ""
                                    ElseIf bFoundPostcode And sString > "" And Not (bFoundTown Or bFoundPostTown) Then
                                        sTown = sString
                                        sTownFound = sString
                                        bFoundTown = True
                                        sString = ""
                                    End If
                                End If
                            End If
                            If sString = "" Then aLines(iLinePos) = ""
                        End If
                        If bFoundTown And bFoundPostTown And sTownFound = sPostTownFound Then
                            sTown = ""
                            sTownFound = ""
                            bFoundTown = False
                            'ElseIf Not oRegexStreetName.IsMatch(sLine) And iLinePos = aLines.Count - 1 And aLines.Count > 1 And sLine > "" Then
                        ElseIf Not oRegexStreetName.IsMatch(sString) And iLinePos = aLines.Count - 1 And aLines.Count > 1 And sString > "" Then
                            If Not bFoundTown Then
                                sTownFound = sString
                                bFoundTown = True
                            Else
                                sLocality = sString & IIf(sLocality = "", "", " ").ToString & sLocality
                            End If
                            sString = ""
                        ElseIf sString > "" Then
                            AddrLines.Insert(0, CapitaliseLine(sString))
                            sString = ""
                        End If
                    Next

                    sErrorPoint = "Stage 4 : Capitalising the remainder line " & sString & " and adding it to the array"

                    If (bFoundPostTown Or bFoundTown) And sString <> "" Then
                        AddrLines.Insert(0, CapitaliseLine(sString))
                        sString = ""
                    End If

                    If sTownFound = "" And sPostTownFound > "" Then
                        sErrorPoint = "Stage 5 : Eliminating duplicate town and posttown"
                        sTownFound = sPostTownFound
                        bFoundTown = True
                        sPostTownFound = ""
                    End If

                    sErrorPoint = "Stage 6 : Capitalising the town, posttown and county, and upper case for the Postcode"

                    Postcode = UCase(sPostcodeFound)
                    County = CapitaliseLine(sCountyFound)
                    PostTown = CapitaliseLine(sPostTownFound)
                    Town = CapitaliseLine(sTownFound)
                    sLocality = ""

                    sErrorPoint = "Stage 7 : Formatting the address as best as can be done with no town found with address " & Join(aLines.ToArray, ",")

                    If bFoundTown = False Then
                        If aLines.Count > 1 Then

                            sErrorPoint = "Stage 7.1 : Finding the last line"

                            'ReDim Preserve aLines(6)
                            For iLinePos = aLines.Count - 1 To 1 Step -1
                                If aLines(iLinePos).ToString <> "" Then Exit For
                            Next

                            sErrorPoint = "Stage 7.2 : Check AddrLines is ok to use"

                            If AddrLines Is Nothing Then AddrLines = New ArrayList
                            AddrLines.Clear()

                            sErrorPoint = "Stage 7.3 : Adding the first line to the AddrLines"

                            sTestString = aLines(0).ToString
                            sErrorPoint = "Stage 7.3.1 : Adding line " & sTestString
                            AddrLines.Insert(0, CapitaliseLine(sTestString))
                            sErrorPoint = "Stage 7.3.2 : Redimming the aLines to size " & iLinePos
                            'ReDim Preserve aLines(iPos)
                            sErrorPoint = "Stage 7.3.3 : Clearing the first line af aLines"
                            aLines(0) = ""

                            sErrorPoint = "Stage 7.4 : Joining up the lines"

                            sString = Join(aLines.ToArray, ",")
                            If Left(sString, 1) = "," And sString <> "," Then sString = Mid(sString, 2)
                            Do While InStr(sString, ",,") > 0
                                sString = Replace(sString, ",,", ",")
                            Loop

                            sErrorPoint = "Stage 7.5 : Making the second line the rest of the string"

                            AddrLines.Insert(1, CapitaliseLine(sString))
                        End If
                    End If

                    If bFoundTown And bFoundPostTown And sTown = sPostTown Then
                        sTown = ""
                        sTownFound = ""
                        bFoundTown = False
                    End If

                    If Not (bFoundTown Or bFoundPostTown) And AddrLines.Count > 1 Then
                        sTemp = AddrLines.Item(AddrLines.Count - 1).ToString
                        aParts = Split(sTemp, ",")
                        iParts = UBound(aParts)
                        If iParts > 0 Then

                            If iParts > 1 And Not bFoundCounty Then
                                sCounty = StrConv(SuperTrim(aParts(iParts)), VbStrConv.ProperCase)
                                bFoundCounty = True
                                aParts(iParts) = ""
                                iParts = iParts - 1
                                ReDim Preserve aParts(iParts)
                            End If

                            If iParts > 0 Then
                                If iParts > 1 And Not bFoundPostTown Then
                                    sPostTown = StrConv(SuperTrim(aParts(iParts)), VbStrConv.ProperCase)
                                    bFoundPostTown = True
                                    aParts(iParts) = ""
                                    iParts = iParts - 1
                                End If
                                sTown = StrConv(SuperTrim(aParts(iParts)), VbStrConv.ProperCase)
                                bFoundTown = True
                                aParts(iParts) = ""
                                iParts = iParts - 1
                                ReDim Preserve aParts(iParts)
                            End If
                            AddrLines(AddrLines.Count - 1) = StrConv(SuperTrim(Join(aParts, ", ")), VbStrConv.ProperCase)
                        End If
                    End If

                    sTemp = Trim(AddrLines.Item(AddrLines.Count - 1).ToString)
                    If AddrLines.Count > 1 And oRegesIsPartTownName.IsMatch(sTemp) Then
                        If sTown <> "" Then
                            sTown = Trim(sTemp & " " & sTown)
                        Else
                            sPostTown = Trim(sTemp & " " & sPostTown)
                        End If
                        AddrLines.RemoveAt(AddrLines.Count - 1)
                    End If

                    'For iAddrLinePos = AddrLines.Count - 1 To 0 Step -1
                    iAddrLinePos = AddrLines.Count - 1
                    sTestLine = AddrLines(iAddrLinePos).ToString
                    aWords = Split(sTestLine, " ")

                    If aWords.Count > 2 Then
                        sAccumStreet = ""
                        For iWordPos = UBound(aWords) To 0 Step -1
                            sWord = aWords(iWordPos)
                            If aUpdateStreetID.ContainsKey(UCase(sWord)) Then sWord = aUpdateStreetID.Item(UCase(sWord))
                            If aUpdateStreetID.ContainsValue(UCase(sWord)) And iWordPos > CInt(IIf(oRegexNum.IsMatch(aWords(0)), 1, 0)) Then
                                If sAccumStreet <> "" And Not (oRegexNum.IsMatch(sAccumStreet)) And oRegexStreetName.IsMatch(sAccumStreet) And Not (oRegexNum.IsMatch(aWords(iWordPos - 1))) Then
                                    AddrLines.Insert(iAddrLinePos + 1, sAccumStreet)
                                    sAccumStreet = ""
                                End If
                            End If
                            sAccumStreet = Trim(sWord & " " & sAccumStreet)
                        Next
                        AddrLines(iAddrLinePos) = sAccumStreet
                    End If
                    'Next

                    If AddrLines.Count > 1 Then
                        iLinePos = AddrLines.Count - 1
                        sTemp = AddrLines.Item(iLinePos).ToString
                        Do While sTemp <> "" And iLinePos >= 1 And AddrLines.Count > 1 And Not (oRegexStreetName.IsMatch(sTemp) Or oRegexNum.IsMatch(sTemp) Or oRegexSpecialPlace.IsMatch(sTemp))
                            aParts = Split(sTemp, ",")
                            If UBound(aParts) > 0 Then
                                sTemp2 = aParts(UBound(aParts))
                                ReDim Preserve aParts(UBound(aParts) - 1)
                                AddrLines.Item(AddrLines.Count - 1) = Join(aParts, ", ")
                                sTemp = sTemp2

                            Else
                                AddrLines.RemoveAt(AddrLines.Count - 1)

                            End If

                            If sTemp <> "" Then
                                If sTown = "" Then
                                    sTown = sTemp
                                Else
                                    sLocality = sTemp & IIf(sLocality <> "", ", " & sLocality, "").ToString
                                End If
                            End If
                            iLinePos = AddrLines.Count - 1
                            sTemp = AddrLines.Item(iLinePos).ToString
                        Loop
                        Do While iLinePos >= 0 And sTemp <> ""
                            If oRegexStreetName.IsMatch(sTemp) Then
                                sWords = Split(UCase(sTemp), " ")
                                If aUpdateStreetID.ContainsKey(sWords(UBound(sWords))) Then sWords(UBound(sWords)) = aUpdateStreetID.Item(sWords(UBound(sWords))).ToString
                                If aUpdateStreetID.ContainsValue(sWords(UBound(sWords))) Then
                                    sTemp = CapitaliseLine(Join(sWords, " "))
                                    AddrLines.Item(iLinePos) = sTemp
                                End If
                            ElseIf iLinePos < AddrLines.Count - 1 And oRegexNumOnStreet.IsMatch(sTemp) Then
                                AddrLines(iLinePos + 1) = sTemp & " " & AddrLines(iLinePos + 1).ToString
                                sTemp = ""
                                AddrLines.RemoveAt(iLinePos)
                            End If
                            iLinePos -= 1
                            If iLinePos >= 0 Then
                                sTemp = AddrLines.Item(iLinePos).ToString
                            End If
                        Loop
                    End If
                End If
            End If

            If AddrLines.Count > 1 Then
                If AddrLines.Count > 2 AndAlso (AddrLines(AddrLines.Count - 1).ToString = sTown And AddrLines(AddrLines.Count - 2).ToString = sLocality) And sPostTown <> "" Then
                    sLocality = ""
                    sTown = sPostTown
                    sPostTown = ""
                ElseIf AddrLines.Count > 1 AndAlso (oRegexStreetName.IsMatch(AddrLines(0).ToString) And AddrLines(AddrLines.Count - 1).ToString = sTown And sPostTown <> "" And sLocality = "") Then
                    sTown = sPostTown
                    sPostTown = ""
                ElseIf AddrLines.Count > 2 AndAlso AddrLines(AddrLines.Count - 1).ToString = AddrLines(AddrLines.Count - 2).ToString Then
                    AddrLines.RemoveAt(AddrLines.Count - 1)
                ElseIf AddrLines.Count > 4 AndAlso (AddrLines(AddrLines.Count - 1).ToString = AddrLines(AddrLines.Count - 3).ToString And AddrLines(AddrLines.Count - 2).ToString = AddrLines(AddrLines.Count - 4).ToString) Then
                    AddrLines.RemoveAt(AddrLines.Count - 1)
                    AddrLines.RemoveAt(AddrLines.Count - 1)
                ElseIf AddrLines.Count > 2 And oRegexStreetName.IsMatch(AddrLines(AddrLines.Count - 2).ToString) And sLocality = "" And sTown = "" And sPostTown = "" Then
                    sTown = AddrLines(AddrLines.Count - 1).ToString
                    AddrLines.RemoveAt(AddrLines.Count - 1)
                ElseIf AddrLines.Count > 1 And (AddrLines(AddrLines.Count - 1).ToString = sLocality Or AddrLines(AddrLines.Count - 1).ToString = sTown Or AddrLines(AddrLines.Count - 1).ToString = sPostTown) Then
                    AddrLines.RemoveAt(AddrLines.Count - 1)

                End If
            End If

            If sLocality = sTown & ", " & sPostTown Or sLocality = sTown Then
                sLocality = ""
            End If

            aTemp = New ArrayList(Split(sLocality, ","))
            If aTemp.Count > 1 Then
                If aTemp.Count > 3 AndAlso (Trim(aTemp(aTemp.Count - 2).ToString) = Trim(aTemp(aTemp.Count - 4).ToString)) Then aTemp.RemoveAt(aTemp.Count - 2)
                If aTemp.Count > 2 AndAlso (Trim(aTemp(aTemp.Count - 2).ToString) = sTown Or Trim(aTemp(aTemp.Count - 2).ToString) = sPostTown) Then aTemp.RemoveAt(aTemp.Count - 2)
                If aTemp.Count > 1 AndAlso (Trim(aTemp(aTemp.Count - 1).ToString) = Trim(aTemp(aTemp.Count - 2).ToString)) Then aTemp.RemoveAt(aTemp.Count - 1)
                If AddrLines.Count < 2 And aTemp.Count > 1 Then
                    AddrLines.Add(aTemp(0))
                    aTemp.RemoveAt(0)
                End If
                sLocality = SuperTrim(Join(aTemp.ToArray, ", "))
            End If


        Catch ex As Exception
            ErrorText &= IIf(ErrorText <> "", vbCrLf, "").ToString & sErrorPoint & vbCrLf & ex.ToString
        End Try

    End Sub

    Private Sub SplitHousePart()
        Dim sLine As String, sWord As String, iPosLine As Integer, aWords() As String, iPosWord As Integer
        Dim oRegexNum As New Regex(sRegexIsHouseNumber, RegexOptions.IgnoreCase), sAccumStreet As String = ""
        Dim oRegexSpecialPlace As New Regex(sRegexSpecialPlace, RegexOptions.IgnoreCase)
        Dim oRegexStreetName As New Regex(sRegexIsStreet, System.Text.RegularExpressions.RegexOptions.IgnoreCase)

        Dim oMatches As MatchCollection, oMatch As Match
        Dim iMax As Integer, sWords() As String, aParts() As String, sTemp As String, sTempStreet As String
        Dim sFirstWord As String, sFirstLine As String, sNextLine As String
        Dim sNewHouseNum As String, sNewHouseName As String


        Try

            If AddrLines.Count > 0 Then
                iPosLine = 0
                Do
                    sFirstLine = ""
                    sNextLine = ""
                    sLine = AddrLines(iPosLine).ToString
                    If sLine <> "" Then
                        sWords = Split(sLine, " ")
                        sFirstWord = sWords(0)
                        For iWordPos = 0 To UBound(sWords)
                            sWord = sWords(iWordPos)
                            If sWord <> "" Then
                                If iWordPos > CLng(IIf(oRegexNum.IsMatch(sFirstWord), 1, 0)) And oRegexNum.IsMatch(sWord) And Not (oRegexNum.IsMatch(sFirstLine + " " + sWord)) Then
                                    sNextLine = sWord
                                    For iPos = iWordPos + 1 To UBound(sWords)
                                        sNextLine = sNextLine & " " & sWords(iPos)
                                    Next
                                    If oRegexStreetName.IsMatch(sNextLine) Then
                                        AddrLines.Insert(iPosLine + 1, sNextLine)
                                        AddrLines(iPosLine) = sFirstLine
                                        iWordPos = UBound(sWords)
                                    Else
                                        sFirstLine = sFirstLine & IIf(sFirstLine <> "", " ", "").ToString & sWord
                                    End If
                                Else
                                    sFirstLine = sFirstLine & IIf(sFirstLine <> "", " ", "").ToString & sWord
                                End If
                            End If
                        Next
                    End If
                    iPosLine += 1
                Loop Until iPosLine >= AddrLines.Count

                If sStreetName = "" Or (Not (oRegexSpecialPlace.IsMatch(sStreetName)) And (sHouseName = "" Or sHouseNum = "")) Then

                    If AddrLines.Count = 1 And oRegexSpecialPlace.IsMatch(AddrLines(0).ToString) Then
                        sStreetName = AddrLines(0).ToString
                    Else

                        For iPosLine = AddrLines.Count - 1 To 0 Step -1
                            sLine = AddrLines(iPosLine).ToString
                            If sLine <> "" Then

                                If sStreetName = "" Then
                                    If oRegexNum.IsMatch(sLine) And sHouseNum = "" Then
                                        oMatches = oRegexNum.Matches(sLine)
                                        oMatch = oMatches.Item(oMatches.Count - 1)
                                        sHouseNum = oMatch.Value
                                        If oMatch.Index + oMatch.Length < Len(sLine) Then
                                            sAccumStreet = Mid(sLine, oMatch.Index + oMatch.Length) & IIf(sAccumStreet <> "", ", " & sAccumStreet, "").ToString
                                            sStreetName = sAccumStreet
                                        End If
                                        If oMatch.Index > 0 Then
                                            sLine = Left(sLine, oMatch.Index)
                                        Else
                                            sLine = ""
                                            sStreetName = sAccumStreet
                                        End If
                                    End If

                                    If sLine > "" Then
                                        aWords = Split(sLine, " ")
                                        If AddrLines.Count = 1 And (aWords.Count = 1 And Not (oRegexNum.IsMatch(sLine))) Then
                                            sHouseName = sLine
                                            sLine = ""
                                        Else

                                            sTempStreet = ""
                                            For iPosWord = UBound(aWords) To 0 Step -1
                                                sWord = aWords(iPosWord)
                                                If sWord > "" Then
                                                    sTemp = ""
                                                    For iTemp = 0 To iPosWord : sTemp &= " " & aWords(iTemp) : Next
                                                    sTemp = Trim(sTemp)
                                                    If sStreetName = "" Then
                                                        If oRegexNum.IsMatch(sWord) Then
                                                            sStreetName = sAccumStreet
                                                            sHouseNum = sWord
                                                        ElseIf oRegexSpecialPlace.IsMatch(sTemp) And oRegexStreetName.IsMatch(sTempStreet) Then
                                                            sStreetName = sAccumStreet
                                                            sAccumStreet = ""
                                                            If sTempStreet <> "" Then
                                                                AddrLines.Insert(iPosLine + 1, sTempStreet)
                                                                AddrLines(iPosLine) = sTemp
                                                            End If
                                                            sHouseName = sHouseName & " " & sWord
                                                        Else
                                                            sAccumStreet = sWord & IIf(iPosWord = UBound(aWords) And sAccumStreet <> "", ", ", " ").ToString & sAccumStreet
                                                            sTempStreet = Trim(sWord & " " & sTempStreet)
                                                        End If
                                                    ElseIf oRegexSpecialPlace.IsMatch(sTemp) And oRegexStreetName.IsMatch(sHouseName) Then
                                                        AddrLines.Insert(iPosLine + 1, sHouseName)
                                                        sStreetName = sHouseName & IIf(sStreetName <> "", ", ", " ").ToString & sStreetName
                                                        AddrLines(iPosLine) = sTemp
                                                        sHouseName = sWord
                                                    Else
                                                        sHouseName = sWord & " " & sHouseName
                                                    End If
                                                End If
                                            Next
                                        End If
                                    End If

                                Else
                                    sHouseName = sLine & IIf(Trim(sHouseName) <> "" And Trim(sLine) <> "", ", ", "").ToString & sHouseName
                                End If
                            End If
                        Next
                        sAccumStreet = SuperTrim(sAccumStreet)

                        If sHouseName = "" And sHouseNum = "" Then
                            For iPosLine = AddrLines.Count - 1 To 0 Step -1
                                sLine = AddrLines(iPosLine).ToString
                                If sLine <> "" Then

                                    If sStreetName = "" Or (Not (oRegexSpecialPlace.IsMatch(sLine)) And oRegexStreetName.IsMatch(sLine)) Then
                                        sWords = Split(UCase(sLine), " ")
                                        If aUpdateStreetID.ContainsKey(sWords(UBound(sWords))) Then sWords(UBound(sWords)) = aUpdateStreetID.Item(sWords(UBound(sWords)))
                                        If aUpdateStreetID.ContainsValue(sWords(UBound(sWords))) Then
                                            sLine = CapitaliseLine(Trim(Join(sWords, " ")))
                                            AddrLines(iPosLine) = sLine
                                        End If

                                        If Not oRegexStreetName.IsMatch(sLine) And AddrLines.Count = 1 And sHouseName = "" And sHouseNum = "" Then
                                            sHouseName = sLine
                                        Else
                                            sStreetName = sLine & IIf(sStreetName <> "", ", " & sStreetName, "").ToString
                                        End If
                                    Else
                                        sHouseName = sLine & IIf(Trim(sHouseName) <> "" And Trim(sLine) <> "", ", ", "").ToString & sHouseName
                                    End If
                                End If
                            Next
                        ElseIf sStreetName = "" And sAccumStreet <> "" And sHouseName = "" Then
                            sNewHouseName = sAccumStreet & " " & sHouseNum
                            sNewHouseNum = ""
                            If oRegexNum.IsMatch(sHouseName) Then
                                sNewHouseNum = sHouseName
                                sNewHouseName = ""
                            Else
                                If sNewHouseName <> sHouseNum Then
                                    For iPos = AddrLines.Count - 1 To 0 Step -1
                                        If AddrLines(iPos).ToString = sHouseName And sHouseName > "" Then
                                            AddrLines(iPos) = sNewHouseName
                                        ElseIf AddrLines(iPos).ToString = sHouseName And sNewHouseName = "" And sHouseName > "" Then
                                            AddrLines.RemoveAt(iPos)
                                        ElseIf AddrLines(iPos).ToString = sHouseNum And sHouseNum > "" Then
                                            AddrLines(iPos) = sNewHouseNum
                                        ElseIf AddrLines(iPos).ToString = sHouseNum And sNewHouseNum = "" And sHouseNum > "" Then
                                            AddrLines.RemoveAt(iPos)
                                        End If
                                    Next
                                End If
                                sHouseName = sNewHouseName
                                sHouseNum = sNewHouseNum
                                aParts = Split(Trim(sHouseName), ",")
                                If aParts.Length > 1 Then
                                    If oRegexNum.IsMatch(Trim(aParts(1))) Then
                                        sHouseName = Trim(aParts(0))
                                        sHouseNum = Trim(aParts(1))
                                        If AddrLines.Count > 2 Then
                                            If Trim(AddrLines(1).ToString & " " & AddrLines(2).ToString) = sHouseNum Then
                                                AddrLines(1) = sHouseNum
                                                AddrLines.RemoveAt(2)
                                            End If
                                        End If
                                    Else
                                        For iWordPos = 1 To UBound(aParts)
                                            sStreetName = Trim(IIf(sStreetName <> "", ", ", "").ToString & aParts(iWordPos))
                                        Next
                                    End If
                                End If
                            End If
                        ElseIf sHouseName <> "" And sHouseNum <> "" And oRegexNum.IsMatch(Trim(sHouseName & " " & sHouseNum)) Then
                            sHouseNum = Trim(sHouseName & " " & sHouseNum)
                            sHouseName = ""
                           If AddrLines(0).ToString = sHouseNum Then
                                AddrLines(1) = AddrLines(0).ToString & " " & AddrLines(1).ToString
                                AddrLines.RemoveAt(0)
                            Else


                            End If
                        End If

                        If sHouseName = "" And sHouseNum = "" Then
                            If InStr(sAccumStreet, "") > 0 Then
                                If AddrLines.Count > 1 Then
                                    sStreetName = AddrLines.Item(AddrLines.Count - 1).ToString
                                    For iPosLine = 0 To AddrLines.Count - 2
                                        sHouseName &= " " & AddrLines(iPosLine).ToString
                                    Next
                                    sHouseName = SuperTrim(sHouseName)
                                Else
                                    aWords = Split(sAccumStreet, " ")
                                    If UBound(aWords) >= 1 Then
                                        Select Case UBound(aWords)
                                            Case 0, 1 : iMax = -1
                                            Case 2 : iMax = 0
                                            Case 3, 4 : iMax = 1
                                            Case 5 : iMax = 2
                                            Case Else : iMax = CInt((Int(UBound(aWords) / 2) - 0.5))
                                        End Select
                                        For iPosWord = 0 To iMax
                                            sHouseName = sHouseName & " " & aWords(iPosWord)
                                            aWords(iPosWord) = ""
                                        Next
                                    Else
                                        sHouseName = sAccumStreet
                                        aWords = Split(sLocality, " ")
                                        sLocality = ""
                                    End If
                                    sStreetName = Join(aWords, " ")

                                End If
                            End If
                        Else
                            aWords = Split(sStreetName, " ")

                        End If

                    End If

                    aParts = Split(sStreetName, ",")
                    If UBound(aParts) >= 0 Then
                        sTemp = FullTrim(aParts(UBound(aParts)))
                        If oRegexStreetName.IsMatch(sTemp) Then
                            If Left(sLocality, Len(sTemp)) = sTemp Then
                                sLocality = Trim(sLocality.Replace(sTemp, ""))
                            ElseIf sLocality = "" And Left(sTown, Len(sTemp)) = sTemp Then
                                sTown = Trim(sTown.Replace(sTemp, ""))
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            'Dim oError As New hscRuntimeError(ex)
            ErrorText &= IIf(ErrorText <> "", vbCrLf, "").ToString & vbCrLf & ex.ToString
        End Try

    End Sub

    Public Property HouseName() As String
        Get
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            Return FullTrim(sHouseName)

        End Get
        Set(value As String)

        End Set
    End Property

    Public Property HouseNum() As String
        Get
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            Return FullTrim(sHouseNum)

        End Get
        Set(value As String)

        End Set
    End Property

    Public Property MainThoroughfare() As String
        Get
            Dim sReturn As String = "", aParts() As String
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            aParts = Split(sStreetName, ",")
            If UBound(aParts) >= 0 Then sReturn = FullTrim(aParts(UBound(aParts)))

            Return sReturn

        End Get
        Set(value As String)

        End Set
    End Property

    Public Property DependentThoroughfare() As String
        Get
            Dim sReturn As String = "", aParts() As String
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            aParts = Split(sStreetName, ",")
            If UBound(aParts) > 0 Then
                ReDim Preserve aParts(UBound(aParts) - 1)
                sReturn = FullTrim(Join(aParts, ", "))

            End If

            Return sReturn

        End Get
        Set(value As String)

        End Set
    End Property

    Public Property StreetName() As String
        Get
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            Return FullTrim(sStreetName)

        End Get
        Set(value As String)

        End Set
    End Property



    Public Property Postcode() As String
        Get
            Return UCase(sPostcode)
        End Get
        Set(ByVal value As String)
            sPostcode = UCase(value)
        End Set
    End Property

    Public Property PostTown() As String
        Get
            Return FullTrim(sPostTown)
        End Get
        Set(ByVal value As String)
            sPostTown = FullTrim(value)
        End Set
    End Property

    Public Property Locality() As String
        Get
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            Return sLocality
        End Get
        Set(ByVal value As String)
            sLocality = FullTrim(value)
        End Set
    End Property

    Public Property Town() As String
        Get
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            Return sTown
        End Get
        Set(ByVal value As String)
            sTown = FullTrim(value)
        End Set
    End Property

    Public Property LocalityTown() As String
        Get
            Return sLocality & IIf(sLocality <> "" And sTown <> "", ", ", "").ToString & sTown
        End Get
        Set(value As String)

        End Set
    End Property

    Public Property County() As String
        Get
            Return FullTrim(sCounty)
        End Get
        Set(ByVal value As String)
            sCounty = FullTrim(value)
        End Set
    End Property


    Public Function FullTrim(ByVal LineIn As String) As String
        Dim sReturn As String = SuperTrim(StrConv(LineIn, VbStrConv.ProperCase))
        If sReturn > "" Then
            Do While InStr(sReturn, "  ") > 0
                sReturn = Replace(sReturn, "  ", " ")
            Loop
        End If

        Return sReturn
    End Function

    Public Property HseAddress1() As String
        Get
            Dim sReturn As String = ""
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            If AddrLines Is Nothing Then AddrLines = New ArrayList
            If AddrLines.Count > 0 Then sReturn = FullTrim(AddrLines.Item(0).ToString)
            Return sReturn
        End Get
        Set(value As String)

        End Set
    End Property

    Public Property HseAddress2() As String
        Get
            Dim sReturn As String = ""
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            If AddrLines Is Nothing Then AddrLines = New ArrayList
            If AddrLines.Count > 1 Then sReturn = FullTrim(AddrLines.Item(1).ToString)
            Return sReturn
        End Get
        Set(value As String)

        End Set
    End Property

    Public Property HseAddress3() As String
        Get
            Dim sReturn As String = ""
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            If AddrLines Is Nothing Then AddrLines = New ArrayList
            If AddrLines.Count > 2 Then sReturn = FullTrim(AddrLines.Item(2).ToString)
            Return sReturn
        End Get
        Set(value As String)

        End Set
    End Property

    Public Property HseAddress4() As String
        Get
            Dim sReturn As String = ""
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            If AddrLines Is Nothing Then AddrLines = New ArrayList
            If AddrLines.Count > 3 Then sReturn = FullTrim(AddrLines.Item(3).ToString)
            Return sReturn
        End Get
        Set(value As String)

        End Set
    End Property

    <System.ComponentModel.Description("Create a new  instance of this class with data to interpret supplied")>
    Public Sub New(ByVal aLines() As String)

        IntialiseContantArrays()
        If aLines IsNot Nothing Then InterpretAddress(aLines)
    End Sub

    <System.ComponentModel.Description("Create a new  instance of this class with data to interpret supplied as a string")>
    Public Sub New(ByVal addr As String)
        Dim aLines As ArrayList

        aLines = New ArrayList(Split(Replace(Replace(Replace(Replace(addr, vbTab, " "), vbCr, ","), vbLf, ","), vbCrLf, ","), ","))
        IntialiseContantArrays()
        If aLines IsNot Nothing Then InterpretAddress(aLines)

    End Sub

    <System.ComponentModel.Description("Create a new  instance of this class with data to interpret supplied")>
    Public Sub New(ByVal arrLines As ArrayList)
        Dim aLines() As String
        aLines = CType(arrLines.ToArray(GetType(System.String)), String())

        IntialiseContantArrays()
        If aLines IsNot Nothing Then InterpretAddress(aLines)
    End Sub

    <System.ComponentModel.Description("Create a new blank instance of this class")>
    Public Sub New()

        IntialiseContantArrays()
    End Sub

    Private Sub IntialiseContantArrays()
        Dim sTemp As String = ""
        hlCounties = New ArrayList From {{"AVON"}, {"BEDFORDSHIRE"}, {"BERKSHIRE"}, {"BUCKINGHAMSHIRE"},
            {"CAMBRIDGESHIRE"}, {"CHESHIRE"}, {"CLEVELAND"}, {"CORNWALL"}, {"CUMBRIA"}, {"DERBYSHIRE"},
            {"DEVON"}, {"DORSET"}, {"DURHAM"}, {"EAST SUSSEX"}, {"E SUSSEX"}, {"ESSEX"},
            {"GLOUCESTERSHIRE"}, {"HAMPSHIRE"}, {"HEREFORDSHIRE"}, {"HERTFORDSHIRE"}, {"ISLE OF WIGHT"},
            {"KENT"}, {"LANCASHIRE"}, {"LEICESTERSHIRE"}, {"LINCOLNSHIRE"}, {"MERSEYSIDE"},
            {"MIDDLESEX"}, {"NORFOLK"}, {"NORTHAMPTONSHIRE"}, {"NORTHUMBERLAND"}, {"NORTH HUMBERSIDE"},
            {"N HUMBERSIDE"}, {"NORTH YORKSHIRE"}, {"N YORKSHIRE"}, {"NOTTINGHAMSHIRE"}, {"OXFORDSHIRE"},
            {"RUTLAND"}, {"SHROPSHIRE"}, {"SOMERSET"}, {"SOUTH HUMBERSIDE"}, {"S HUMBERSIDE"},
            {"SOUTH YORKSHIRE"}, {"S YORKSHIRE"}, {"STAFFORDSHIRE"}, {"SUFFOLK"}, {"SURREY"}, {"SUSSEX"},
            {"TYNE AND WEAR"}, {"WARWICKSHIRE"}, {"WEST MIDLANDS"}, {"W MIDLANDS"}, {"WEST SUFFOLK"},
            {"W SUFFOLK"}, {"WEST SUSSEX"}, {"W SUSSEX"}, {"WEST YORKSHIRE"}, {"W YORKSHIRE"},
            {"WILTSHIRE"}, {"WORCESTERSHIRE"}, {"YORKSHIRE"}}
 
        hlPostTowns = New ArrayList From {{"GREATER LONDON"}, {"INNER LONDON"}, {"OUTER LONDON"}, {"CITY OF LONDON"}, {"LONDON"}, {"ABERDEEN"}, {"ABERLOUR"},
            {"ABOYNE"}, {"ALFORD"}, {"BALLATER"}, {"BALLINDALLOCH"}, {"BANCHORY"}, {"BANFF"}, {"BUCKIE"}, {"ELLON"},
            {"FRASERBURGH"}, {"HUNTLY"}, {"INSCH"}, {"INVERURIE"}, {"KEITH"}, {"LAURENCEKIRK"}, {"MACDUFF"}, {"MILLTIMBER"},
            {"PETERCULTER"}, {"PETERHEAD"}, {"STONEHAVEN"}, {"STRATHDON"}, {"TURRIFF"}, {"WESTHILL"},
            {"HARPENDEN"}, {"HATFIELD"}, {"ST. ALBANS"}, {"WELWYN"}, {"WELWYN GARDEN CITY"},
            {"ALCESTER"}, {"BIRMINGHAM"}, {"BROMSGROVE"}, {"CRADLEY HEATH"}, {"HALESOWEN"}, {"HENLEY-IN-ARDEN"},
            {"OLDBURY"}, {"REDDITCH"}, {"ROWLEY REGIS"}, {"SMETHWICK"}, {"SOLIHULL"}, {"STUDLEY"}, {"SUTTON COLDFIELD"},
            {"TAMWORTH"}, {"WEST BROMWICH"},
            {"BATH"}, {"BRADFORD-ON-AVON"}, {"BRUTON"}, {"CASTLE CARY"}, {"FROME"}, {"GLASTONBURY"}, {"RADSTOCK"},
            {"SHEPTON MALLET"}, {"STREET"}, {"TEMPLECOMBE"}, {"TROWBRIDGE"}, {"WARMINSTER"}, {"WELLS"}, {"WESTBURY"},
            {"WINCANTON"}, {"YEOVIL"},
            {"ACCRINGTON"}, {"BARNOLDSWICK"}, {"BLACKBURN"}, {"BURNLEY"}, {"CLITHEROE"}, {"COLNE"}, {"DARWEN"}, {"NELSON"},
            {"ROSSENDALE"},
            {"BINGLEY"}, {"BRADFORD"}, {"CLECKHEATON"}, {"KEIGHLEY"}, {"SETTLE"}, {"SHIPLEY"}, {"SKIPTON"},
            {"BOURNEMOUTH"}, {"BROADSTONE"}, {"CHRISTCHURCH"}, {"FERNDOWN"}, {"NEW MILTON"}, {"POOLE"}, {"RINGWOOD"},
            {"SWANAGE"}, {"VERWOOD"}, {"WAREHAM"}, {"WIMBORNE"},
            {"BOLTON"}, {"BURY"},
            {"ARUNDEL"}, {"BRIGHTON"}, {"EASTBOURNE"}, {"HAILSHAM"}, {"HASSOCKS"}, {"HENFIELD"}, {"HOVE"}, {"LANCING"},
            {"LEWES"}, {"LITTLEHAMPTON"}, {"NEWHAVEN"}, {"PEACEHAVEN"}, {"PEVENSEY"}, {"POLEGATE"}, {"SEAFORD"},
            {"SHOREHAM-BY-SEA"}, {"STEYNING"}, {"WORTHING"},
            {"BECKENHAM"}, {"BROMLEY"}, {"CHISLEHURST"}, {"KESTON"}, {"ORPINGTON"}, {"SWANLEY"}, {"WEST WICKHAM"},
            {"AXBRIDGE"}, {"BANWELL"}, {"BRISTOL"}, {"CHEDDAR"}, {"CLEVEDON"}, {"WEDMORE"}, {"WESTON-SUPER-MARE"},
            {"WINSCOMBE"},
            {"ANTRIM"}, {"ARMAGH"}, {"AUGHER"}, {"AUGHNACLOY"}, {"BALLYCASTLE"}, {"BALLYCLARE"}, {"BALLYMENA"},
            {"BALLYMONEY"}, {"BALLYNAHINCH"}, {"BANBRIDGE"}, {"BANGOR"}, {"BELFAST"}, {"BUSHMILLS"}, {"CALEDON"},
            {"CARRICKFERGUS"}, {"CASTLEDERG"}, {"CASTLEWELLAN"}, {"CLOGHER"}, {"COLERAINE"}, {"COOKSTOWN"}, {"CRAIGAVON"},
            {"CRUMLIN"}, {"DONAGHADEE"}, {"DOWNPATRICK"}, {"DROMORE"}, {"DUNGANNON"}, {"ENNISKILLEN"}, {"FIVEMILETOWN"},
            {"HILLSBOROUGH"}, {"HOLYWOOD"}, {"LARNE"}, {"LIMAVADY"}, {"LISBURN"}, {"LONDONDERRY"}, {"DERRY"}, {"MAGHERA"},
            {"MAGHERAFELT"}, {"NEWCASTLE"}, {"NEWRY"}, {"NEWTOWNABBEY"}, {"NEWTOWNARDS"}, {"OMAGH"}, {"PORTRUSH"},
            {"PORTSTEWART"}, {"STRABANE"},
            {"ALSTON"}, {"APPLEBY-IN-WESTMORLAND"}, {"BECKERMET"}, {"BRAMPTON"}, {"CARLISLE"}, {"CLEATOR"},
            {"CLEATOR MOOR"}, {"COCKERMOUTH"}, {"EGREMONT"}, {"FRIZINGTON"}, {"HOLMROOK"}, {"KESWICK"}, {"KIRKBY STEPHEN"},
            {"MARYPORT"}, {"MOOR ROW"}, {"PENRITH"}, {"RAVENGLASS"}, {"SEASCALE"}, {"ST. BEES"}, {"WHITEHAVEN"},
            {"WIGTON"}, {"WORKINGTON"},
            {"CAMBRIDGE"}, {"ELY"}, {"HAVERHILL"}, {"NEWMARKET"}, {"SAFFRON WALDEN"},
            {"ABERDARE"}, {"BARGOED"}, {"BARRY"}, {"BRIDGEND"}, {"CAERPHILLY"}, {"CARDIFF"}, {"COWBRIDGE"},
            {"DINAS POWYS"}, {"FERNDALE"}, {"HENGOED"}, {"LLANTWIT MAJOR"}, {"MAESTEG"}, {"MERTHYR TYDFIL"}, {"MOUNTAIN ASH"},
            {"PENARTH"}, {"PENTRE"}, {"PONTYCLUN"}, {"PONTYPRIDD"}, {"PORTH"}, {"PORTHCAWL"}, {"TONYPANDY"}, {"TREHARRIS"},
            {"TREORCHY"},
            {"BAGILLT"}, {"BIRKENHEAD"}, {"BUCKLEY"}, {"CHESTER"}, {"DEESIDE"}, {"ELLESMERE PORT"}, {"FLINT"},
            {"HOLYWELL"}, {"MOLD"}, {"NESTON"}, {"PRENTON"}, {"WALLASEY"}, {"WIRRAL"},
            {"BILLERICAY"}, {"BISHOP'S STORTFORD"}, {"BRAINTREE"}, {"BRENTWOOD"}, {"BURNHAM-ON-CROUCH"}, {"CHELMSFORD"},
            {"DUNMOW"}, {"EPPING"}, {"HARLOW"}, {"INGATESTONE"}, {"MALDON"}, {"ONGAR"}, {"SAWBRIDGEWORTH"},
            {"SOUTHMINSTER"}, {"STANSTED"}, {"WITHAM"},
            {"BURES"}, {"CLACTON-ON-SEA"}, {"COLCHESTER"}, {"FRINTON-ON-SEA"}, {"HALSTEAD"}, {"HARWICH"}, {"MANNINGTREE"},
            {"SUDBURY"}, {"WALTON ON THE NAZE"},
            {"CATERHAM"}, {"COULSDON"}, {"CROYDON"}, {"KENLEY"}, {"MITCHAM"}, {"PURLEY"}, {"SOUTH CROYDON"},
            {"THORNTON HEATH"}, {"WARLINGHAM"}, {"WHYTELEAFE"},
            {"BIRCHINGTON"}, {"BROADSTAIRS"}, {"CANTERBURY"}, {"DEAL"}, {"DOVER"}, {"FOLKESTONE"}, {"HERNE BAY"},
            {"HYTHE"}, {"MARGATE"}, {"RAMSGATE"}, {"SANDWICH"}, {"WESTGATE-ON-SEA"}, {"WHITSTABLE"},
            {"ATHERSTONE"}, {"BEDWORTH"}, {"COVENTRY"}, {"KENILWORTH"}, {"LEAMINGTON SPA"}, {"NUNEATON"}, {"RUGBY"},
            {"SHIPSTON-ON-STOUR"}, {"SOUTHAM"}, {"STRATFORD-UPON-AVON"}, {"WARWICK"},
            {"CONGLETON"}, {"CREWE"}, {"MIDDLEWICH"}, {"NANTWICH"}, {"NORTHWICH"}, {"SANDBACH"}, {"TARPORLEY"},
            {"WINSFORD"},
            {"BELVEDERE"}, {"BEXLEY"}, {"BEXLEYHEATH"}, {"DARTFORD"}, {"ERITH"}, {"GRAVESEND"}, {"GREENHITHE"},
            {"LONGFIELD"}, {"SIDCUP"}, {"SWANSCOMBE"}, {"WELLING"},
            {"ARBROATH"}, {"BRECHIN"}, {"CARNOUSTIE"}, {"DUNDEE"}, {"FORFAR"}, {"KIRRIEMUIR"}, {"MONTROSE"},
            {"NEWPORT-ON-TAY"}, {"TAYPORT"},
            {"ALFRETON"}, {"ASHBOURNE"}, {"BAKEWELL"}, {"BELPER"}, {"BURTON-ON-TRENT"}, {"DERBY"}, {"HEANOR"},
            {"ILKESTON"}, {"MATLOCK"}, {"RIPLEY"}, {"SWADLINCOTE"},
            {"ANNAN"}, {"CANONBIE"}, {"CASTLE DOUGLAS"}, {"DALBEATTIE"}, {"DUMFRIES"}, {"GRETNA"}, {"KIRKCUDBRIGHT"},
            {"LANGHOLM"}, {"LOCKERBIE"}, {"MOFFAT"}, {"NEWTON STEWART"}, {"SANQUHAR"}, {"STRANRAER"}, {"THORNHILL"},
            {"CHESTER LE STREET"}, {"CONSETT"}, {"DURHAM"}, {"HOUGHTON LE SPRING"}, {"STANLEY"},
            {"BARNARD CASTLE"}, {"BEDALE"}, {"BISHOP AUCKLAND"}, {"CATTERICK GARRISON"}, {"CROOK"}, {"DARLINGTON"},
            {"FERRYHILL"}, {"HAWES"}, {"LEYBURN"}, {"NEWTON AYCLIFFE"}, {"NORTHALLERTON"}, {"RICHMOND"}, {"SHILDON"},
            {"SPENNYMOOR"},
            {"BARNETBY"}, {"BARROW-UPON-HUMBER"}, {"BARTON-UPON-HUMBER"}, {"BRIGG"}, {"CLEETHORPES"}, {"DONCASTER"},
            {"GAINSBOROUGH"}, {"GOOLE"}, {"GRIMSBY"}, {"IMMINGHAM"}, {"RETFORD"}, {"SCUNTHORPE"}, {"ULCEBY"},
            {"BEAMINSTER"}, {"BLANDFORD FORUM"}, {"BRIDPORT"}, {"DORCHESTER"}, {"LYME REGIS"}, {"PORTLAND"}, {"SHERBORNE"},
            {"STURMINSTER NEWTON"}, {"WEYMOUTH"},
            {"BEWDLEY"}, {"BRIERLEY HILL"}, {"DUDLEY"}, {"KIDDERMINSTER"}, {"KINGSWINFORD"}, {"STOURBRIDGE"},
            {"STOURPORT-ON-SEVERN"}, {"TIPTON"},
            {"BALERNO"}, {"BATHGATE"}, {"BO'NESS"}, {"BONNYRIGG"}, {"BROXBURN"}, {"CURRIE"}, {"DALKEITH"}, {"DUNBAR"},
            {"EAST LINTON"}, {"EDINBURGH"}, {"GOREBRIDGE"}, {"GULLANE"}, {"HADDINGTON"}, {"HERIOT"}, {"HUMBIE"},
            {"INNERLEITHEN"}, {"JUNIPER GREEN"}, {"KIRKLISTON"}, {"KIRKNEWTON"}, {"LASSWADE"}, {"LINLITHGOW"},
            {"LIVINGSTON"}, {"LOANHEAD"}, {"LONGNIDDRY"}, {"MUSSELBURGH"}, {"NEWBRIDGE"}, {"NORTH BERWICK"}, {"PATHHEAD"},
            {"PEEBLES"}, {"PENICUIK"}, {"PRESTONPANS"}, {"ROSEWELL"}, {"ROSLIN"}, {"SOUTH QUEENSFERRY"}, {"TRANENT"},
            {"WALKERBURN"}, {"WEST CALDER"}, {"WEST LINTON"},
            {"BARNET"}, {"BROXBOURNE"}, {"ENFIELD"}, {"HODDESDON"}, {"POTTERS BAR"}, {"WALTHAM ABBEY"}, {"WALTHAM CROSS"},
            {"AXMINSTER"}, {"BARNSTAPLE"}, {"BEAWORTHY"}, {"BIDEFORD"}, {"BRAUNTON"}, {"BUDE"}, {"BUDLEIGH SALTERTON"},
            {"CHULMLEIGH"}, {"COLYTON"}, {"CREDITON"}, {"CULLOMPTON"}, {"DAWLISH"}, {"EXETER"}, {"EXMOUTH"},
            {"HOLSWORTHY"}, {"HONITON"}, {"ILFRACOMBE"}, {"LYNMOUTH"}, {"LYNTON"}, {"NORTH TAWTON"}, {"OKEHAMPTON"},
            {"OTTERY ST. MARY"}, {"SEATON"}, {"SIDMOUTH"}, {"SOUTH MOLTON"}, {"TIVERTON"}, {"TORRINGTON"}, {"UMBERLEIGH"},
            {"WINKLEIGH"}, {"WOOLACOMBE"},
            {"ALLOA"}, {"ALVA"}, {"BONNYBRIDGE"}, {"CALLANDER"}, {"CLACKMANNAN"}, {"CRIANLARICH"}, {"DENNY"}, {"DOLLAR"},
            {"DOUNE"}, {"DUNBLANE"}, {"FALKIRK"}, {"GRANGEMOUTH"}, {"KILLIN"}, {"LARBERT"}, {"LOCHEARNHEAD"}, {"MENSTRIE"},
            {"STIRLING"}, {"TILLICOULTRY"},
            {"BLACKPOOL"}, {"FLEETWOOD"}, {"LYTHAM ST. ANNES"}, {"POULTON-LE-FYLDE"}, {"THORNTON-CLEVELEYS"},
            {"ALEXANDRIA"}, {"ARROCHAR"}, {"CLYDEBANK"}, {"DUMBARTON"}, {"GLASGOW"}, {"HELENSBURGH"},
            {"BADMINTON"}, {"BERKELEY"}, {"BLAKENEY"}, {"CHELTENHAM"}, {"CHIPPING CAMPDEN"}, {"CINDERFORD"},
            {"CIRENCESTER"}, {"COLEFORD"}, {"DRYBROOK"}, {"DURSLEY"}, {"DYMOCK"}, {"FAIRFORD"}, {"GLOUCESTER"},
            {"LECHLADE"}, {"LONGHOPE"}, {"LYDBROOK"}, {"LYDNEY"}, {"MITCHELDEAN"}, {"MORETON-IN-MARSH"}, {"NEWENT"},
            {"NEWNHAM"}, {"RUARDEAN"}, {"STONEHOUSE"}, {"STROUD"}, {"TETBURY"}, {"TEWKESBURY"}, {"WESTBURY-ON-SEVERN"},
            {"WOTTON-UNDER-EDGE"},
            {"ALDERSHOT"}, {"ALTON"}, {"BAGSHOT"}, {"BORDON"}, {"CAMBERLEY"}, {"CRANLEIGH"}, {"FARNBOROUGH"}, {"FARNHAM"},
            {"FLEET"}, {"GODALMING"}, {"GUILDFORD"}, {"HASLEMERE"}, {"HINDHEAD"}, {"LIGHTWATER"}, {"LIPHOOK"}, {"LISS"},
            {"MIDHURST"}, {"PETERSFIELD"}, {"PETWORTH"}, {"SANDHURST"}, {"VIRGINIA WATER"}, {"WINDLESHAM"}, {"WOKING"},
            {"YATELEY"},
            {"EDGWARE"}, {"HARROW"}, {"NORTHWOOD"}, {"PINNER"}, {"RUISLIP"}, {"STANMORE"}, {"WEMBLEY"},
            {"BRIGHOUSE"}, {"HOLMFIRTH"}, {"HUDDERSFIELD"},
            {"HARROGATE"}, {"KNARESBOROUGH"}, {"RIPON"},
            {"AMERSHAM"}, {"AYLESBURY"}, {"BEACONSFIELD"}, {"BERKHAMSTED"}, {"CHALFONT ST. GILES"}, {"CHESHAM"},
            {"GREAT MISSENDEN"}, {"HEMEL HEMPSTEAD"}, {"HIGH WYCOMBE"}, {"PRINCES RISBOROUGH"}, {"TRING"},
            {"BROMYARD"}, {"HEREFORD"}, {"KINGTON"}, {"LEDBURY"}, {"LEOMINSTER"}, {"ROSS-ON-WYE"},
            {"ISLE OF BARRA"}, {"ISLE OF BENBECULA"}, {"ISLE OF HARRIS"}, {"ISLE OF LEWIS"}, {"ISLE OF NORTH UIST"},
            {"ISLE OF SCALPAY"}, {"ISLE OF SOUTH UIST"}, {"STORNOWAY"},
            {"BEVERLEY"}, {"BROUGH"}, {"COTTINGHAM"}, {"HESSLE"}, {"HORNSEA"}, {"HULL"}, {"NORTH FERRIBY"}, {"WITHERNSEA"},
            {"ELLAND"}, {"HALIFAX"}, {"HEBDEN BRIDGE"}, {"SOWERBY BRIDGE"},
            {"BARKING"}, {"BUCKHURST HILL"}, {"CHIGWELL"}, {"ILFORD"}, {"LOUGHTON"}, {"WOODFORD GREEN"},
            {"ALDEBURGH"}, {"BRANDON"}, {"BURY ST. EDMUNDS"}, {"DISS"}, {"EYE"}, {"FELIXSTOWE"}, {"HALESWORTH"},
            {"HARLESTON"}, {"IPSWICH"}, {"LEISTON"}, {"SAXMUNDHAM"}, {"SOUTHWOLD"}, {"STOWMARKET"}, {"THETFORD"},
            {"WOODBRIDGE"},
            {"ACHNASHEEN"}, {"ALNESS"}, {"ARDGAY"}, {"AVOCH"}, {"BEAULY"}, {"CROMARTY"}, {"DINGWALL"}, {"DORNOCH"},
            {"ELGIN"}, {"FOCHABERS"}, {"FORRES"}, {"FORTROSE"}, {"GAIRLOCH"}, {"GARVE"}, {"INVERGORDON"}, {"INVERNESS"},
            {"ISLE OF SKYE"}, {"KYLE"}, {"LAIRG"}, {"LOSSIEMOUTH"}, {"MUIR OF ORD"}, {"MUNLOCHY"}, {"NAIRN"}, {"PLOCKTON"},
            {"PORTREE"}, {"ROGART"}, {"STRATHCARRON"}, {"STRATHPEFFER"}, {"STROME FERRY"}, {"TAIN"}, {"ULLAPOOL"},
            {"ARDROSSAN"}, {"AYR"}, {"BEITH"}, {"CUMNOCK"}, {"DALRY"}, {"DARVEL"}, {"GALSTON"}, {"GIRVAN"}, {"IRVINE"},
            {"ISLE OF ARRAN"}, {"ISLE OF CUMBRAE"}, {"KILBIRNIE"}, {"KILMARNOCK"}, {"KILWINNING"}, {"LARGS"},
            {"MAUCHLINE"}, {"MAYBOLE"}, {"NEWMILNS"}, {"PRESTWICK"}, {"SALTCOATS"}, {"STEVENSTON"}, {"TROON"}, {"WEST KILBRIDE"},
            {"ADDLESTONE"}, {"ASHTEAD"}, {"CHERTSEY"}, {"CHESSINGTON"}, {"COBHAM"}, {"EAST MOLESEY"}, {"EPSOM"}, {"ESHER"},
            {"KINGSTON UPON THAMES"}, {"LEATHERHEAD"}, {"NEW MALDEN"}, {"SURBITON"}, {"TADWORTH"}, {"THAMES DITTON"},
            {"WALTON-ON-THAMES"}, {"WEST BYFLEET"}, {"WEST MOLESEY"}, {"WEYBRIDGE"}, {"WORCESTER PARK"},
            {"BERRIEDALE"}, {"BRORA"}, {"DUNBEATH"}, {"FORSINARD"}, {"GOLSPIE"}, {"HALKIRK"}, {"HELMSDALE"}, {"KINBRACE"},
            {"KIRKWALL"}, {"LATHERON"}, {"LYBSTER"}, {"ORKNEY"}, {"STROMNESS"}, {"THURSO"}, {"WICK"},
            {"ANSTRUTHER"}, {"BURNTISLAND"}, {"COWDENBEATH"}, {"CUPAR"}, {"DUNFERMLINE"}, {"GLENROTHES"},
            {"INVERKEITHING"}, {"KELTY"}, {"KINROSS"}, {"KIRKCALDY"}, {"LEVEN"}, {"LOCHGELLY"}, {"ST. ANDREWS"},
            {"BOOTLE"}, {"LIVERPOOL"}, {"ORMSKIRK"}, {"PRESCOT"}, {"ST ALBANS"}, {"ST ASAPH"}, {"ST DAVID"}, {""},
            {"AMBLESIDE"}, {"ASKAM-IN-FURNESS"}, {"BARROW-IN-FURNESS"}, {"BROUGHTON-IN-FURNESS"}, {"CARNFORTH"},
            {"CONISTON"}, {"DALTON-IN-FURNESS"}, {"GRANGE-OVER-SANDS"}, {"KENDAL"}, {"KIRKBY-IN-FURNESS"}, {"LANCASTER"},
            {"MILLOM"}, {"MILNTHORPE"}, {"MORECAMBE"}, {"SEDBERGH"}, {"ULVERSTON"}, {"WINDERMERE"},
            {"BRECON"}, {"BUILTH WELLS"}, {"KNIGHTON"}, {"LLANDRINDOD WELLS"}, {"LLANGAMMARCH WELLS"}, {"LLANWRTYD WELLS"},
            {"PRESTEIGNE"}, {"RHAYADER"},
            {"ASHBY-DE-LA-ZOUCH"}, {"COALVILLE"}, {"HINCKLEY"}, {"IBSTOCK"}, {"LEICESTER"}, {"LOUGHBOROUGH"},
            {"LUTTERWORTH"}, {"MARKET HARBOROUGH"}, {"MARKFIELD"}, {"MELTON MOWBRAY"}, {"OAKHAM"}, {"WIGSTON"},
            {"ABERDOVEY"}, {"ABERGELE"}, {"AMLWCH"}, {"ARTHOG"}, {"BALA"}, {"BANGOR"}, {"BARMOUTH"}, {"BEAUMARIS"},
            {"BETWS-Y-COED"}, {"BLAENAU FFESTINIOG"}, {"BODORGAN"}, {"BRYNTEG"}, {"CAERNARFON"}, {"CEMAES BAY"},
            {"COLWYN BAY"}, {"CONWY"}, {"CORWEN"}, {"CRICCIETH"}, {"DENBIGH"}, {"DOLGELLAU"}, {"DOLWYDDELAN"}, {"DULAS"},
            {"DYFFRYN ARDUDWY"}, {"FAIRBOURNE"}, {"GAERWEN"}, {"GARNDOLBENMAEN"}, {"HARLECH"}, {"HOLYHEAD"}, {"LLANBEDR"},
            {"LLANBEDRGOCH"}, {"LLANDUDNO"}, {"LLANDUDNO JUNCTION"}, {"LLANERCHYMEDD"}, {"LLANFAIRFECHAN"},
            {"LLANFAIRPWLLGWYNGYLL"}, {"LLANGEFNI"}, {"LLANGOLLEN"}, {"LLANRWST"}, {"LLWYNGWRIL"}, {"MARIANGLAS"},
            {"MENAI BRIDGE"}, {"MOELFRE"}, {"PENMAENMAWR"}, {"PENRHYNDEUDRAETH"}, {"PENTRAETH"}, {"PENYSARN"}, {"PORTHMADOG"},
            {"PRESTATYN"}, {"PWLLHELI"}, {"RHOSGOCH"}, {"RHOSNEIGR"}, {"RHYL"}, {"RUTHIN"}, {"ST. ASAPH"}, {"TALSARNAU"},
            {"TALYBONT"}, {"TREFRIW"}, {"TY CROES"}, {"TYN-Y-GONGL"}, {"TYWYN"}, {"WREXHAM"}, {"Y FELINHELI"},
            {"ALFORD"}, {"HORNCASTLE"}, {"LINCOLN"}, {"LOUTH"}, {"MABLETHORPE"}, {"MARKET RASEN"}, {"WOODHALL SPA"},
            {"ILKLEY"}, {"LEEDS"}, {"OTLEY"}, {"PUDSEY"}, {"TADCASTER"}, {"WETHERBY"},
            {"DUNSTABLE"}, {"LEIGHTON BUZZARD"}, {"LUTON"},
            {"MANCHESTER"}, {"SALE"}, {"SALFORD"},
            {"AYLESFORD"}, {"CHATHAM"}, {"FAVERSHAM"}, {"GILLINGHAM"}, {"MAIDSTONE"}, {"QUEENBOROUGH"}, {"ROCHESTER"},
            {"SHEERNESS"}, {"SITTINGBOURNE"}, {"SNODLAND"}, {"WEST MALLING"},
            {"BEDFORD"}, {"BUCKINGHAM"}, {"MILTON KEYNES"}, {"NEWPORT PAGNELL"}, {"OLNEY"},
            {"AIRDRIE"}, {"BELLSHILL"}, {"BIGGAR"}, {"CARLUKE"}, {"COATBRIDGE"}, {"HAMILTON"}, {"LANARK"}, {"LARKHALL"},
            {"MOTHERWELL"}, {"SHOTTS"}, {"STRATHAVEN"}, {"WISHAW"},
            {"ALNWICK"}, {"ASHINGTON"}, {"BAMBURGH"}, {"BEDLINGTON"}, {"BELFORD"}, {"BLAYDON-ON-TYNE"}, {"BLYTH"},
            {"BOLDON COLLIERY"}, {"CHATHILL"}, {"CHOPPINGTON"}, {"CORBRIDGE"}, {"CRAMLINGTON"}, {"EAST BOLDON"}, {"GATESHEAD"},
            {"HALTWHISTLE"}, {"HEBBURN"}, {"HEXHAM"}, {"JARROW"}, {"MORPETH"}, {"NEWBIGGIN-BY-THE-SEA"},
            {"NEWCASTLE UPON TYNE"}, {"NORTH SHIELDS"}, {"PRUDHOE"}, {"RIDING MILL"}, {"ROWLANDS GILL"}, {"RYTON"},
            {"SEAHOUSES"}, {"SOUTH SHIELDS"}, {"STOCKSFIELD"}, {"WALLSEND"}, {"WASHINGTON"}, {"WHITLEY BAY"}, {"WOOLER"}, {"WYLAM"},
            {"GRANTHAM"}, {"MANSFIELD"}, {"NEWARK"}, {"NOTTINGHAM"}, {"SLEAFORD"}, {"SOUTHWELL"}, {"SUTTON-IN-ASHFIELD"},
            {"BRACKLEY"}, {"CORBY"}, {"DAVENTRY"}, {"KETTERING"}, {"NORTHAMPTON"}, {"RUSHDEN"}, {"TOWCESTER"},
            {"WELLINGBOROUGH"},
            {"ABERGAVENNY"}, {"ABERTILLERY"}, {"BLACKWOOD"}, {"CALDICOT"}, {"CHEPSTOW"}, {"CRICKHOWELL"}, {"CWMBRAN"},
            {"EBBW VALE"}, {"MONMOUTH"}, {"NEW TREDEGAR"}, {"NEWPORT"}, {"PONTYPOOL"}, {"TREDEGAR"}, {"USK"},
            {"ATTLEBOROUGH"}, {"BECCLES"}, {"BUNGAY"}, {"CROMER"}, {"DEREHAM"}, {"FAKENHAM"}, {"GREAT YARMOUTH"}, {"HOLT"},
            {"LOWESTOFT"}, {"MELTON CONSTABLE"}, {"NORTH WALSHAM"}, {"NORWICH"}, {"SHERINGHAM"}, {"WALSINGHAM"},
            {"WELLS-NEXT-THE-SEA"}, {"WYMONDHAM"},
            {"ASHTON-UNDER-LYNE"}, {"BACUP"}, {"HEYWOOD"}, {"LITTLEBOROUGH"}, {"OLDHAM"}, {"ROCHDALE"}, {"TODMORDEN"},
            {"ABINGDON"}, {"BAMPTON"}, {"BANBURY"}, {"BICESTER"}, {"BURFORD"}, {"CARTERTON"}, {"CHINNOR"},
            {"CHIPPING NORTON"}, {"DIDCOT"}, {"KIDLINGTON"}, {"OXFORD"}, {"THAME"}, {"WALLINGFORD"}, {"WANTAGE"}, {"WATLINGTON"},
            {"WITNEY"}, {"WOODSTOCK"},
            {"APPIN"}, {"BISHOPTON"}, {"BRIDGE OF ORCHY"}, {"BRIDGE OF WEIR"}, {"CAIRNDOW"}, {"CAMPBELTOWN"},
            {"COLINTRAIVE"}, {"DALMALLY"}, {"DUNOON"}, {"ERSKINE"}, {"GOUROCK"}, {"GREENOCK"}, {"INVERARAY"},
            {"ISLE OF BUTE"}, {"ISLE OF COLL"}, {"ISLE OF COLONSAY"}, {"ISLE OF GIGHA"}, {"ISLE OF IONA"}, {"ISLE OF ISLAY"},
            {"ISLE OF JURA"}, {"ISLE OF MULL"}, {"ISLE OF TIREE"}, {"JOHNSTONE"}, {"KILMACOLM"}, {"LOCHGILPHEAD"},
            {"LOCHWINNOCH"}, {"OBAN"}, {"PAISLEY"}, {"PORT GLASGOW"}, {"RENFREW"}, {"SKELMORLIE"}, {"TARBERT"},
            {"TAYNUILT"}, {"TIGHNABRUAICH"}, {"WEMYSS BAY"},
            {"BOSTON"}, {"BOURNE"}, {"CHATTERIS"}, {"DOWNHAM MARKET"}, {"HUNSTANTON"}, {"HUNTINGDON"}, {"KING'S LYNN"},
            {"MARCH"}, {"PETERBOROUGH"}, {"SANDRINGHAM"}, {"SKEGNESS"}, {"SPALDING"}, {"SPILSBY"}, {"ST. IVES"},
            {"ST. NEOTS"}, {"STAMFORD"}, {"SWAFFHAM"}, {"WISBECH"},
            {"ABERFELDY"}, {"ACHARACLE"}, {"ARISAIG"}, {"AUCHTERARDER"}, {"AVIEMORE"}, {"BALLACHULISH"}, {"BLAIRGOWRIE"},
            {"BOAT OF GARTEN"}, {"CARRBRIDGE"}, {"CORROUR"}, {"CRIEFF"}, {"DALWHINNIE"}, {"DUNKELD"}, {"FORT AUGUSTUS"},
            {"FORT WILLIAM"}, {"GLENFINNAN"}, {"GRANTOWN-ON-SPEY"}, {"INVERGARRY"}, {"ISLE OF CANNA"}, {"ISLE OF EIGG"},
            {"ISLE OF RUM"}, {"KINGUSSIE"}, {"KINLOCHLEVEN"}, {"LOCHAILORT"}, {"MALLAIG"}, {"NETHY BRIDGE"},
            {"NEWTONMORE"}, {"PERTH"}, {"PITLOCHRY"}, {"ROY BRIDGE"}, {"SPEAN BRIDGE"},
            {"BODMIN"}, {"BOSCASTLE"}, {"CALLINGTON"}, {"CALSTOCK"}, {"CAMELFORD"}, {"DELABOLE"}, {"FOWEY"},
            {"GUNNISLAKE"}, {"IVYBRIDGE"}, {"LAUNCESTON"}, {"LIFTON"}, {"LISKEARD"}, {"LOOE"}, {"LOSTWITHIEL"},
            {"PADSTOW"}, {"PAR"}, {"PLYMOUTH"}, {"PORT ISAAC"}, {"SALTASH"}, {"ST. AUSTELL"}, {"TAVISTOCK"}, {"TINTAGEL"},
            {"TORPOINT"}, {"WADEBRIDGE"}, {"YELVERTON"},
            {"BEMBRIDGE"}, {"BOGNOR REGIS"}, {"CHICHESTER"}, {"COWES"}, {"EAST COWES"}, {"EMSWORTH"}, {"FAREHAM"},
            {"FRESHWATER"}, {"GOSPORT"}, {"HAVANT"}, {"HAYLING ISLAND"}, {"LEE-ON-THE-SOLENT"}, {"NEWPORT"},
            {"PORTSMOUTH"}, {"ROWLAND'S CASTLE"}, {"RYDE"}, {"SANDOWN"}, {"SEAVIEW"}, {"SHANKLIN"}, {"SOUTHSEA"},
            {"TOTLAND BAY"}, {"VENTNOR"}, {"WATERLOOVILLE"}, {"YARMOUTH"},
            {"CHORLEY"}, {"LEYLAND"}, {"PRESTON"}, {"SOUTHPORT"},
            {"BASINGSTOKE"}, {"BRACKNELL"}, {"CROWTHORNE"}, {"HENLEY-ON-THAMES"}, {"HOOK"}, {"HUNGERFORD"}, {"NEWBURY"},
            {"READING"}, {"TADLEY"}, {"THATCHAM"}, {"WHITCHURCH"}, {"WOKINGHAM"},
            {"BETCHWORTH"}, {"BILLINGSHURST"}, {"BURGESS HILL"}, {"CRAWLEY"}, {"DORKING"}, {"EAST GRINSTEAD"},
            {"FOREST ROW"}, {"GATWICK"}, {"GODSTONE"}, {"HAYWARDS HEATH"}, {"HORLEY"}, {"HORSHAM"}, {"LINGFIELD"}, {"OXTED"},
            {"PULBOROUGH"}, {"REDHILL"}, {"REIGATE"},
            {"DAGENHAM"}, {"GRAYS"}, {"HORNCHURCH"}, {"PURFLEET"}, {"RAINHAM"}, {"ROMFORD"}, {"SOUTH OCKENDON"},
            {"TILBURY"}, {"UPMINSTER"},
            {"BARNSLEY"}, {"CHESTERFIELD"}, {"DRONFIELD"}, {"HOPE VALLEY"}, {"MEXBOROUGH"}, {"ROTHERHAM"}, {"SHEFFIELD"},
            {"WORKSOP"},
            {"ABERAERON"}, {"AMMANFORD"}, {"BONCATH"}, {"BURRY PORT"}, {"CARDIGAN"}, {"CARMARTHEN"}, {"CLARBESTON ROAD"},
            {"CLYNDERWEN"}, {"CRYMYCH"}, {"FERRYSIDE"}, {"FISHGUARD"}, {"GLOGUE"}, {"GOODWICK"}, {"HAVERFORDWEST"},
            {"KIDWELLY"}, {"KILGETTY"}, {"LAMPETER"}, {"LLANARTH"}, {"LLANDEILO"}, {"LLANDOVERY"}, {"LLANDYSUL"},
            {"LLANELLI"}, {"LLANFYRNACH"}, {"LLANGADOG"}, {"LLANWRDA"}, {"LLANYBYDDER"}, {"MILFORD HAVEN"}, {"NARBERTH"},
            {"NEATH"}, {"NEW QUAY"}, {"NEWCASTLE EMLYN"}, {"NEWPORT"}, {"PEMBROKE"}, {"PEMBROKE DOCK"}, {"PENCADER"},
            {"PORT TALBOT"}, {"SAUNDERSFOOT"}, {"SWANSEA"}, {"TENBY"}, {"WHITLAND"},
            {"ARLESEY"}, {"BALDOCK"}, {"BIGGLESWADE"}, {"BUNTINGFORD"}, {"HENLOW"}, {"HERTFORD"}, {"HITCHIN"},
            {"KNEBWORTH"}, {"LETCHWORTH GARDEN CITY"}, {"MUCH HADHAM"}, {"ROYSTON"}, {"SANDY"}, {"SHEFFORD"},
            {"STEVENAGE"}, {"WARE"},
            {"ALDERLEY EDGE"}, {"BUXTON"}, {"CHEADLE"}, {"DUKINFIELD"}, {"GLOSSOP"}, {"HIGH PEAK"}, {"HYDE"},
            {"MACCLESFIELD"}, {"STALYBRIDGE"}, {"STOCKPORT"}, {"WILMSLOW"},
            {"ASCOT"}, {"BOURNE END"}, {"GERRARDS CROSS"}, {"IVER"}, {"MAIDENHEAD"}, {"MARLOW"}, {"SLOUGH"}, {"WINDSOR"},
            {"BANSTEAD"}, {"CARSHALTON"}, {"MORDEN"}, {"SUTTON"}, {"WALLINGTON"},
            {"CALNE"}, {"CHIPPENHAM"}, {"CORSHAM"}, {"DEVIZES"}, {"FARINGDON"}, {"MALMESBURY"}, {"MARLBOROUGH"},
            {"MELKSHAM"}, {"PEWSEY"}, {"SWINDON"},
            {"ALRESFORD"}, {"BROCKENHURST"}, {"EASTLEIGH"}, {"LYMINGTON"}, {"LYNDHURST"}, {"ROMSEY"}, {"SOUTHAMPTON"},
            {"STOCKBRIDGE"}, {"WINCHESTER"},
            {"ANDOVER"}, {"FORDINGBRIDGE"}, {"GILLINGHAM"}, {"SALISBURY"}, {"SHAFTESBURY"}, {"TIDWORTH"},
            {"PETERLEE"}, {"SEAHAM"}, {"SUNDERLAND"},
            {"BASILDON"}, {"BENFLEET"}, {"CANVEY ISLAND"}, {"HOCKLEY"}, {"LEIGH-ON-SEA"}, {"RAYLEIGH"}, {"ROCHFORD"},
            {"SOUTHEND-ON-SEA"}, {"STANFORD-LE-HOPE"}, {"WESTCLIFF-ON-SEA"}, {"WICKFORD"},
            {"LEEK"}, {"NEWCASTLE"}, {"STAFFORD"}, {"STOKE-ON-TRENT"}, {"STONE"}, {"UTTOXETER"},
            {"ABERYSTWYTH"}, {"BISHOPS CASTLE"}, {"BORTH"}, {"BOW STREET"}, {"BUCKNELL"}, {"CAERSWS"}, {"CHURCH STRETTON"},
            {"CRAVEN ARMS"}, {"ELLESMERE"}, {"LLANBRYNMAIR"}, {"LLANDINAM"}, {"LLANFECHAIN"}, {"LLANFYLLIN"},
            {"LLANIDLOES"}, {"LLANON"}, {"LLANRHYSTUD"}, {"LLANSANTFFRAID"}, {"LLANYMYNECH"}, {"LUDLOW"},
            {"LYDBURY NORTH"}, {"MACHYNLLETH"}, {"MALPAS"}, {"MEIFOD"}, {"MONTGOMERY"}, {"NEWTOWN"}, {"OSWESTRY"}, {"SHREWSBURY"},
            {"TALYBONT"}, {"TREGARON"}, {"WELSHPOOL"}, {"WHITCHURCH"}, {"YSTRAD MEURIG"},
            {"BRIDGWATER"}, {"BURNHAM-ON-SEA"}, {"CHARD"}, {"CREWKERNE"}, {"DULVERTON"}, {"HIGHBRIDGE"},
            {"HINTON ST. GEORGE"}, {"ILMINSTER"}, {"LANGPORT"}, {"MARTOCK"}, {"MERRIOTT"}, {"MINEHEAD"}, {"MONTACUTE"}, {"SOMERTON"},
            {"SOUTH PETHERTON"}, {"STOKE-SUB-HAMDON"}, {"TAUNTON"}, {"WATCHET"}, {"WELLINGTON"},
            {"BERWICK-UPON-TWEED"}, {"COCKBURNSPATH"}, {"COLDSTREAM"}, {"CORNHILL-ON-TWEED"}, {"DUNS"}, {"EARLSTON"},
            {"EYEMOUTH"}, {"GALASHIELS"}, {"GORDON"}, {"HAWICK"}, {"JEDBURGH"}, {"KELSO"}, {"LAUDER"}, {"MELROSE"},
            {"MINDRUM"}, {"NEWCASTLETON"}, {"SELKIRK"},
            {"BROSELEY"}, {"MARKET DRAYTON"}, {"MUCH WENLOCK"}, {"NEWPORT"}, {"SHIFNAL"}, {"TELFORD"},
            {"ASHFORD"}, {"BATTLE"}, {"BEXHILL-ON-SEA"}, {"CRANBROOK"}, {"CROWBOROUGH"}, {"EDENBRIDGE"}, {"ETCHINGHAM"},
            {"HARTFIELD"}, {"HASTINGS"}, {"HEATHFIELD"}, {"MAYFIELD"}, {"NEW ROMNEY"}, {"ROBERTSBRIDGE"}, {"ROMNEY MARSH"},
            {"RYE"}, {"SEVENOAKS"}, {"ST. LEONARDS-ON-SEA"}, {"TENTERDEN"}, {"TONBRIDGE"}, {"TUNBRIDGE WELLS"},
            {"UCKFIELD"}, {"WADHURST"}, {"WESTERHAM"}, {"WINCHELSEA"},
            {"BRIXHAM"}, {"BUCKFASTLEIGH"}, {"DARTMOUTH"}, {"KINGSBRIDGE"}, {"NEWTON ABBOT"}, {"PAIGNTON"}, {"SALCOMBE"},
            {"SOUTH BRENT"}, {"TEIGNMOUTH"}, {"TORQUAY"}, {"TOTNES"},
            {"CAMBORNE"}, {"FALMOUTH"}, {"HAYLE"}, {"HELSTON"}, {"ISLES OF SCILLY"}, {"MARAZION"}, {"NEWQUAY"}, {"PENRYN"},
            {"PENZANCE"}, {"PERRANPORTH"}, {"REDRUTH"}, {"ST. AGNES"}, {"ST. COLUMB"}, {"ST. IVES"}, {"TRURO"},
            {"BILLINGHAM"}, {"GUISBOROUGH"}, {"HARTLEPOOL"}, {"MIDDLESBROUGH"}, {"REDCAR"}, {"SALTBURN-BY-THE-SEA"},
            {"STOCKTON-ON-TEES"}, {"TRIMDON STATION"}, {"WINGATE"}, {"YARM"},
            {"ASHFORD"}, {"BRENTFORD"}, {"EGHAM"}, {"FELTHAM"}, {"HAMPTON"}, {"HOUNSLOW"}, {"ISLEWORTH"}, {"RICHMOND"},
            {"SHEPPERTON"}, {"STAINES"}, {"SUNBURY-ON-THAMES"}, {"TEDDINGTON"}, {"TWICKENHAM"},
            {"GREENFORD"}, {"HAYES"}, {"NORTHOLT"}, {"SOUTHALL"}, {"UXBRIDGE"}, {"WEST DRAYTON"},
            {"ALTRINCHAM"}, {"FRODSHAM"}, {"KNUTSFORD"}, {"LYMM"}, {"NEWTON-LE-WILLOWS"}, {"RUNCORN"}, {"ST. HELENS"},
            {"WARRINGTON"}, {"WIDNES"},
            {"ABBOTS LANGLEY"}, {"BOREHAMWOOD"}, {"BUSHEY"}, {"KINGS LANGLEY"}, {"RADLETT"}, {"RICKMANSWORTH"},
            {"WATFORD"},
            {"BATLEY"}, {"CASTLEFORD"}, {"DEWSBURY"}, {"HECKMONDWIKE"}, {"KNOTTINGLEY"}, {"LIVERSEDGE"}, {"MIRFIELD"},
            {"NORMANTON"}, {"OSSETT"}, {"PONTEFRACT"}, {"WAKEFIELD"},
            {"LEIGH"}, {"SKELMERSDALE"}, {"WIGAN"},
            {"BROADWAY"}, {"DROITWICH"}, {"EVESHAM"}, {"MALVERN"}, {"PERSHORE"}, {"TENBURY WELLS"}, {"WORCESTER"},
            {"BURNTWOOD"}, {"CANNOCK"}, {"LICHFIELD"}, {"RUGELEY"}, {"WALSALL"}, {"WEDNESBURY"},
            {"BILSTON"}, {"BRIDGNORTH"}, {"WILLENHALL"}, {"WOLVERHAMPTON"}, {"WELLS"},
            {"BRIDLINGTON"}, {"DRIFFIELD"}, {"FILEY"}, {"MALTON"}, {"PICKERING"}, {"SCARBOROUGH"}, {"SELBY"}, {"THIRSK"},
            {"WHITBY"}, {"YORK"},
            {"SHETLAND"}, {"CITY OF WESTMINSTER"}, {"WESTMINSTER"}}


 
        hlTowns = New ArrayList From {{"ADDINGTON"}, {"ALDERMASTON"}, {"ALFORD"}, {"AMERSHAM"}, {"ASHFORD"}, {"ASHURST WOOD"},
                {"BALDWINS HILL"}, {"BANSTEAD"}, {"BARKING"}, {"BEACONSFIELD"}, {"BECKENHAM"},
                {"BEDMINSTER"}, {"BERKHAMSTED"}, {"BIGGIN HILL"}, {"BIRMINGHAM"}, {"BLETCHINGLEY"},
                {"BLETHCHINGLEY"},
                {"BLINDLEY HEATH"}, {"BLINDLEYHEATH"}, {"BRACKNELL"}, {"BRADFORD"},
                {"BRENTWOOD"}, {"BRIGHTON"}, {"BRISTOL"}, {"BROCKHAM"}, {"BROMLEY"}, {"BUCKHURST HILL"},
                {"BURGESS HILL"}, {"BURNLEY"}, {"BURSTOW"}, {"BURTON ON TRENT"}, {"BURTON UPON TRENT"},
                {"BURTON-UPON-TRENT"}, {"BURY ST EDMUNDS"}, {"CAMBRIDGE"}, {"CANARY WHARF"},
                {"CANNOCK"}, {"CARDIFF"}, {"CARSHALTON"}, {"CATERHAM"}, {"CHALDON"}, {"CHELSHAM"},
                {"CHELSMFORD"}, {"CHERTSEY"}, {"CHESHUNT"}, {"CHESTER"}, {"CHICHESTER"},
                {"CHIPSTEAD"},
                {"CHISWICK"}, {"CLEARWATER BEACH"}, {"COLCHESTER"}, {"COPTHORNE"},
                {"COULCHESTER"}, {"COULSDON"}, {"COVENTRY"}, {"CRAWLEY"}, {"CRAWLEY DOWN"},
                {"CROWBOROUGH"}, {"CROWHURST"}, {"CROYDON"}, {"CRYSTAL PALACE"}, {"DAGENHAM"},
                {"DARTFORD"},
                {"DMALLFIELD"}, {"DORKING"}, {"DORMANS"}, {"DORMANSLAND"}, {"DUNSATBLE"},
                {"DUNSTABLE"}, {"EALING"}, {"EAST GRINSTEAD"}, {"E GRINSTEAD"}, {"EAST HAM"},
                {"E HAM"}, {"EAST HOATHLY"}, {"E HOATHLY"}, {"EASTBOURNE"}, {"EDENBRIDGE"},
                {"EDGBASTON"}, {"EPSOM"}, {"ESSEX"}, {"FAREHAM"},
                {"FELBRIDGE"}, {"FELCOURT"}, {"FELTHAM"}, {"FERNDOWN"}, {"GERRARDS CROSS"},
                {"GODSTONE"}, {"GRAVESEND"}, {"GUILDFORD"}, {"HAILSHAM"}, {"HAMPTONWICK"}, {"HANGER GREEN"},
                {"HASTINGS"}, {"HAWKHURST"}, {"HAYES"}, {"HEATHFIELD"}, {"HEATHGFIELD"},
                {"HERTFORDSHIRE"}, {"HERTS"}, {"HILDENBOROUGH"}, {"HOLBURN"}, {"HORLEY"},
                {"HORNE"}, {"HORSHAM"}, {"HOVE"}, {"HULL"}, {"HURST GREEN"}, {"HUYTON"},
                {"KENLEY"}, {"KENSINGTON"}, {"KIDLINGTON"}, {"KINGSTON"}, {"KINGSTON ON THAMS"},
                {"KINGSTON UPON THAMES"},
                {"LANGLEY"}, {"LARLING"}, {"LEAMINGTON SPA"}, {"LEATHERHEAD"}, {"LEICESTER"},
                {"LEWES"}, {"LIGFIELD"}, {"LIMPSFIELD"}, {"LIMPSFIELD CHART"}, {"LINGFIELD"},
                {"LOUGHTON"}, {"MAIDSTONE"}, {"MANCHESTER"}, {"MANNINGS HEATH"}, {"MARLOW"},
                {"MARSH GREEN"}, {"MERSTHAM"}, {"MICKLEHAM"}, {"MIDDLESEX"}, {"MILTON KEYNES"},
                {"MITCHAM"}, {"NEW MALDEN"}, {"NEWBURY"}, {"NEWBURYEL"}, {"NEWCHAPEL"},
                {"NEWICK"}, {"NORBURY"}, {"NORTHCHAPEL"}, {"NORTHOLT"}, {"NORWICH"}, {"NOTTINGHAM"},
                {"NUTFIELD"}, {"ORPINGTON"}, {"ORPINTON"}, {"OTTERSHAW"}, {"OUTWOOD"},
                {"OXFORD"}, {"OXTED"}, {"PEMBURY"}, {"PENRITH"}, {"PETERBOROUGH"}, {"PEVENSEY BAY"},
                {"PLOUGH ROAD SMALLFIE"}, {"POOLE"}, {"PORTSMOUTH"}, {"PURFLEET"}, {"PURLEY"},
                {"READING"}, {"REDHILL"}, {"REIGATE"}, {"RICHMOND"}, {"RICHMOND ON THAMES"},
                {"ROWTHANT"}, {"SALFORDS"}, {"SAMLLFIELD"}, {"SANDERSTEAD"}, {"SEVENOAKS"},
                {"SHEFFIELD"}, {"SHIPLEY BRIDGE"}, {"SIDCUP"}, {"SLINFOLD"}, {"SMALLFIELD"},
                {"SOUTH CROYDON"}, {"S CROYDON"}, {"SOUTH GODSTONE"}, {"S GODSTONE"},
                {"SOUTH NUTFIELD"}, {"S NUTFIELD"}, {"SOUTH OCKENDEN"}, {"S OCKENDEN"}, {"STEVENAGE"},
                {"STH GODSTONE"}, {"STOCKTON ON TEES"}, {"STOKE POGES"}, {"STRATFORD UPON AVON"},
                {"SUDBURY"}, {"SUNNIGDALE"}, {"SURBITON"}, {"SUTTON"},
                {"SUTTON COLDFIELD"}, {"SWINDON"}, {"TANDRIDGE"},
                {"TATSFIELD"}, {"TEDDINGTON"}, {"THAME"}, {"THAMES DITTON"}, {"THORNTON HEATH"},
                {"THUNDERSLEY"}, {"TIVERTON"}, {"TONBRIDGE"}, {"TOOTING"},
                {"TUNBRDIGE"}, {"TUNBRIDGE WELLS"}, {"UCKFIELD"}, {"UNDERRIVER"},
                {"UXBRIDGE"}, {"VAUXHALL"}, {"VIRGINIA WATER"}, {"WALLINGTON"}, {"WALSALL"},
                {"WALTON ON THE HILL"}, {"WANDSWORTH"}, {"WARLIGHAM"},
                {"WARLINGHAM"}, {"WATERHOUSE LANE"}, {"WATFORD"}, {"WELWYN GARDEN CITY"},
                {"WEST CROYDON"}, {"W CROYDON"}, {"WEST SUSSEX"}, {"W SUSSEX"}, {"WEST WICKHAM"},
                {"W WICKHAM"}, {"WEST YORK"}, {"W YORK"},
                {"WESTERHAM"}, {"WHYTELEAF"}, {"WHYTELEAFE"}, {"WHYTLEAFE"},
                {"WIMBLEDON"}, {"WINCHESTER"}, {"WOKING"}, {"WOLDINGHAM"}, {"WOLVERHAMPTON"},
                {"WORCESTER PARK"}, {"WORTHING"}, {"WYTELEAFE"}, {"YEOVIL"}}

        hlWordsNotToCapitalise = New ArrayList From {{"and"}, {"of"}, {"for"}, {"in"}, {"at"}, {"to"}, {"adjacent"}}
        hlSubsequentLCIfPrevLC = New ArrayList From {{"the"}}

    End Sub

    Private Function CapitaliseLine(ByVal Linein As String) As String
        Dim sReturn As String = Trim(Linein), aWords() As String
        Dim sWord As String, iWordNo As Integer = 0, sPrevWord As String
        Dim bChanged As Boolean = False, bWasMadeLC As Boolean
        Try
            If sReturn <> "" Then
                Do While InStr(sReturn, "  ") > 0
                    sReturn = Replace(sReturn, "  ", " ")
                Loop
                aWords = Split(sReturn, " ")
                sPrevWord = ""

                For Each sWord In aWords
                    If Not (Regex.IsMatch(sWord, "^[a-zA-Z]+$")) Then
                        aWords(iWordNo) = UCase(sWord)
                        bWasMadeLC = False
                    ElseIf iWordNo > 0 And (hlWordsNotToCapitalise.Contains(LCase(sWord)) Or ((bWasMadeLC Or (iWordNo = 1 AndAlso hlWordsNotToCapitalise.Contains(LCase(sPrevWord)))) And hlSubsequentLCIfPrevLC.Contains(LCase(sWord)))) Then
                        aWords(iWordNo) = LCase(sWord)
                        bWasMadeLC = True
                    Else
                        aWords(iWordNo) = StrConv(sWord, VbStrConv.ProperCase)
                        bWasMadeLC = False
                    End If

                    bChanged = (StrComp(aWords(iWordNo), sWord, CompareMethod.Binary) <> 0) Or bChanged
                    iWordNo += 1
                    sPrevWord = sWord

                Next

                If bChanged Then sReturn = Replace(Join(aWords, " "), "'S", "'s")

            End If

        Catch ex As Exception

        End Try

        Return sReturn
    End Function


    Public Function DivideLines(ByVal aLinesIn As ArrayList, ByVal DelimListIn As String) As ArrayList
        Dim sTemp As String, aTemp As ArrayList, sDelim As String = Left(DelimListIn, 1)
        Dim iLinePos As Integer, sTestAll As String = Join(aLinesIn.ToArray, " ")


        Try

            For iDelimPos = 1 To Len(DelimListIn)

                sDelim = DelimListIn.Substring(iDelimPos - 1, 1)
                If InStr(sTestAll, sDelim) > 0 Then

                    For iLinePos = aLinesIn.Count - 1 To 0 Step -1
                        sTemp = aLinesIn(iLinePos).ToString
                        If InStr(sTemp, sDelim) > 0 Then
                            aTemp = New ArrayList(Split(sTemp, sDelim))
                            If aTemp.Count > 1 Then
                                aLinesIn(iLinePos) = aTemp(0)
                                aTemp.RemoveAt(0)
                                aLinesIn.InsertRange(iLinePos + 1, aTemp)
                            End If
                        End If

                    Next
                End If
            Next

            For iLinePos = aLinesIn.Count - 1 To 0 Step -1
                If Trim(aLinesIn(iLinePos).ToString) = "" Then aLinesIn.RemoveAt(iLinePos)
                If iLinePos < aLinesIn.Count - 1 AndAlso aLinesIn(iLinePos).ToString = aLinesIn(iLinePos + 1).ToString Then aLinesIn.RemoveAt(iLinePos + 1)
            Next


        Catch ex As Exception

        End Try

        Return aLinesIn



    End Function


    Public Function FullAddress(Optional WithPostCode As Boolean = False) As String
        Dim sReturn As String = ""
        Try
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            sReturn = HseAddress1
            sReturn &= IIf(sReturn <> "" And HseAddress2 <> "", ", ", "").ToString & HseAddress2
            sReturn &= IIf(sReturn <> "" And HseAddress3 <> "", ", ", "").ToString & HseAddress3
            sReturn &= IIf(sReturn <> "" And HseAddress4 <> "", ", ", "").ToString & HseAddress4
            sReturn &= IIf(sReturn <> "" And Locality <> "", ", ", "").ToString & Locality
            sReturn &= IIf(sReturn <> "" And Town <> "", ", ", "").ToString & Town
            sReturn &= IIf(sReturn <> "" And PostTown <> "", ", ", "").ToString & PostTown
            sReturn &= IIf(sReturn <> "" And County <> "", ", ", "").ToString & County
            sReturn = FullTrim(sReturn)
            If WithPostCode Then sReturn = sReturn & " " & Postcode.ToUpper

        Catch ex As Exception

        End Try
        Return sReturn
    End Function


    Public Function ArraylistAddress() As ArrayList
        Dim oReturn As New ArrayList, aLines() As String
        Try
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            aLines = Split(Replace(Replace(FullAddress(), ", ", ","), ",,", ","), ",")
            oReturn = New ArrayList(aLines.ToArray)
        Catch ex As Exception

        End Try
        Return oReturn
    End Function


    Public Function AddressLine(ByVal LineNo As Integer) As String

        Dim sReturn As String = "", aLines As ArrayList
        Try
            If sStreetName = "" And sHouseName = "" Then SplitHousePart()
            aLines = ArraylistAddress()
            If LineNo < aLines.Count Then
                sReturn = aLines(LineNo).ToString
            End If
        Catch ex As Exception

        End Try
        Return sReturn
    End Function

    <XmlArray("FullAddressLines")>
    <XmlArrayItem("Addressline")>
    Public Property FullAddressLines As ArrayList
        Get
            Dim oReturn As ArrayList = ArraylistAddress()
            Return oReturn
        End Get
        Set(value As ArrayList)

        End Set
    End Property


    <XmlArray("StreetAddressLines")>
    <XmlArrayItem(ElementName:="Addressline")>
    Public Property StreetAddressLines As ArrayList
        Get
            Dim oReturn As ArrayList = AddrLines
            Return oReturn
        End Get
        Set(value As ArrayList)

        End Set
    End Property

    Public Function SuperTrim(ByVal Source As String) As String
        Dim sReturn As String = Trim(Source), Regex As New Regex("\s{2,}")
        Try

            Do While Regex.IsMatch(sReturn)
                sReturn = Regex.Replace(sReturn, "\s{2,}", " ")
            Loop

            Do While Regex.IsMatch(sReturn, "\s+\-")
                sReturn = Regex.Replace(sReturn, "\s+\-", "-")
            Loop

            Do While Regex.IsMatch(sReturn, "\w+((['`’][S])|['’]s)(\s|[.,]|$)(\s+|\w+|$)+")
                sReturn = Regex.Replace(sReturn, "['`’][Ss]", "`s")
            Loop

            'Do While Len(sReturn) > 0 AndAlso InStr(".*;:'""", Left(sReturn, 1), CompareMethod.Text) > 0
            '	sReturn = Mid(sReturn, 2)
            'Loop

            'Do While Len(sReturn) > 0 AndAlso InStr(".*;:'""", Right(sReturn, 1), CompareMethod.Text) > 0
            '	sReturn = Left(sReturn, Len(sReturn) - 1)
            'Loop

        Catch ex As Exception

        End Try

        Return sReturn
    End Function


End Class
