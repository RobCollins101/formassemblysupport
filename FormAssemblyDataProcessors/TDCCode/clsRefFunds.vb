﻿Imports System.Web.Script.Serialization

Public Class clsRefFunds
	Inherits SortedList(Of String, clsRefFund)
	Public Sub New()
		Dim sTemp As String = "", sTempDelim As String = ""
		Dim aValues As New ArrayList
		Dim iMax As Integer, iPos As Integer, oNewRec As clsRefFund

		sTemp = GetMySetting("RefSystemTypes").ToString
		sTempDelim = GetMySetting("RefSystemTypesColDelimiter").ToString
		If sTempDelim = "" Then sTempDelim = "~"
		aValues = New ArrayList(Split(sTemp, vbCrLf))

		iMax = aValues.Count - 1
		For iPos = 0 To iMax
			sTemp = aValues.Item(iPos).ToString
			oNewRec = New clsRefFund(sTemp, sTempDelim)
			oNewRec.ArrayPos = iPos
			If Not Me.ContainsKey(oNewRec.SortKeyToUse) Then Me.Add(oNewRec.SortKeyToUse, oNewRec)
		Next

	End Sub

    'Protected Friend Function asJavascriptArrayDeclaration() As String
    '	Dim sReturn As String = "", oRec As clsRefFund, iMax As Integer, iPos As Integer

    '       'sReturn = "{"
    '       iMax = Me.Count - 1
    '	For iPos = 0 To iMax
    '		oRec = Me.ElementAt(iPos).Value
    '		sReturn &= oRec.JavascriptObjectDeclaration
    '		If iPos < iMax Then sReturn &= ","
    '	Next
    '       'sReturn &= "}"

    '       Return sReturn
    'End Function


    Protected Friend Function FindByName(FundName As String, Optional oPrevList As List(Of clsRefFund) = Nothing) As List(Of clsRefFund)
		Dim oReturn As New List(Of clsRefFund)
		If oPrevList IsNot Nothing AndAlso oPrevList.Count > 0 Then
			For Each oRefFund In oPrevList
				If oRefFund.FundName = FundName Then oReturn.Add(oRefFund)
			Next

		Else
			For Each oRefFund In Me.Values
				If oRefFund.FundName = FundName Then oReturn.Add(oRefFund)
			Next
		End If

		Return oReturn
	End Function


	Protected Friend Function FindByPaymentCode(FundPaymentCode As String, Optional oPrevList As List(Of clsRefFund) = Nothing) As List(Of clsRefFund)
		Dim oReturn As New List(Of clsRefFund)
		If oPrevList IsNot Nothing AndAlso oPrevList.Count > 0 Then
			For Each oRefFund In oPrevList
				If oRefFund.FundPaymentCode = FundPaymentCode Then oReturn.Add(oRefFund)
			Next

		Else
			For Each oRefFund In Me.Values
				If oRefFund.FundPaymentCode = FundPaymentCode Then oReturn.Add(oRefFund)
			Next
		End If

		Return oReturn
	End Function


	Protected Friend Function FindByCostCode(CostCode As String, Optional oPrevList As List(Of clsRefFund) = Nothing) As List(Of clsRefFund)
		Dim oReturn As New List(Of clsRefFund)
		If oPrevList IsNot Nothing AndAlso oPrevList.Count > 0 Then
			For Each oRefFund In oPrevList
				If oRefFund.CostCode = CostCode Then oReturn.Add(oRefFund)
			Next

		Else
			For Each oRefFund In Me.Values
				If oRefFund.CostCode = CostCode Then oReturn.Add(oRefFund)
			Next

		End If

		Return oReturn
	End Function


	Protected Friend Function FindByFundID(FundID As String, Optional oPrevList As List(Of clsRefFund) = Nothing) As List(Of clsRefFund)
		Dim oReturn As New List(Of clsRefFund)
		If oPrevList IsNot Nothing AndAlso oPrevList.Count > 0 Then
			For Each oRefFund In oPrevList
				If oRefFund.FundID = FundID Then oReturn.Add(oRefFund)
			Next

		Else
			For Each oRefFund In Me.Values
				If oRefFund.FundID = FundID Then oReturn.Add(oRefFund)
			Next

		End If

		Return oReturn
	End Function


End Class


Public Class clsRefFund
	Protected Friend FundID As String = ""
	Protected Friend FundName As String = ""
	Protected Friend FundPaymentCode As String = ""
	Protected Friend CostCode As String = ""
	Protected Friend EmailList As String = ""
	Protected Friend RegexVerifyString As String = ""
	Protected Friend InternalEmailReadOnly As String = ""
	Protected Friend DefaultAmount As String = ""
	Protected Friend RefMandatory As String = ""
	Protected Friend RefAdvisoryMessage As String = ""
	Protected Friend ArrayPos As Integer = 0
    Protected Friend ShowActivityAddress As String = ""

    Public Sub New()

	End Sub

	Public Sub New(sValues As String, sDelimUsed As String)
		Me.New(New ArrayList(Split(sValues, sDelimUsed)))
	End Sub


	Public Sub New(aValues As ArrayList)
		If aValues.Count > 0 Then FundName = aValues(0).ToString
		If aValues.Count > 1 Then FundID = aValues(1).ToString
		If aValues.Count > 2 Then FundPaymentCode = aValues(2).ToString
		If aValues.Count > 3 Then CostCode = aValues(3).ToString
		If aValues.Count > 4 Then EmailList = aValues(4).ToString
		If aValues.Count > 5 Then RegexVerifyString = aValues(5).ToString
		If aValues.Count > 6 Then InternalEmailReadOnly = aValues(6).ToString
		If aValues.Count > 7 Then DefaultAmount = aValues(7).ToString
		If aValues.Count > 8 Then RefMandatory = aValues(8).ToString
		If aValues.Count > 9 Then RefAdvisoryMessage = aValues(9).ToString
        If aValues.Count > 10 Then ShowActivityAddress = aValues(10).ToString

    End Sub


	Protected Friend Function SortKeyToUse() As String
		'Return Left(Me.FundID & Space(10), 10) & ":" & FundName
		Return Left(Me.FundID & Space(10), 10) & ":" & ArrayPos.ToString("000") & ":" & FundName
	End Function


    'Protected Friend Function JavascriptObjectDeclaration() As String
    '	Dim sReturn As String = ""

    '       sReturn = "{" &
    '                   " FundID :  """ & EncodeStringForJavascript(Me.FundID) &
    '                   """, Name : """ & EncodeStringForJavascript(Me.FundName) &
    '                   """, FundPaymentCode : """ & EncodeStringForJavascript(Me.FundPaymentCode) &
    '                   """, CostCode : """ & EncodeStringForJavascript(Me.CostCode) &
    '                   """, EmailList : """ & EncodeStringForJavascript(Me.EmailList) &
    '                   """, RegexVerifyString : """ & EncodeStringForJavascript(Me.RegexVerifyString.Replace("""", """""")) &
    '                   """, InternalEmailReadOnly : " & StringIsTrue(Me.InternalEmailReadOnly.ToLower).ToString.ToLower &
    '                   ", DefaultAmount : """ & DefaultAmount.ToString &
    '                   """, ReferenceMandatory : " & StringIsTrue(Me.RefMandatory.ToLower).ToString.ToLower &
    '                   ", RefAdvisoryMessage : """ & EncodeStringForJavascript(RefAdvisoryMessage) & """" &
    '                   ", ShowActivityAddress : " & StringIsTrue(Me.ShowActivityAddress.ToLower).ToString.ToLower &
    '                   " }"

    '       Return sReturn
    'End Function

End Class
