﻿Option Compare Text

Imports System.Data.SqlClient
Imports System.Data
Imports System.Reflection


Public Class clsCurrentData
    ' This class is initiated and recovered in the StyleTemplate.master vb code on loading.
    ' It is designed to carry a number of variabes, object and dependent functions that woudl be desirable to maintain over a MySession.
    ' Having just one class instance is easier to instanciate than having a number of object instances that need checking individually

    Private oErrorLog As wRuntimeErrors
    Private sURLParentPath As String, sApplicationPath As String
    Private sStartPage As String

    Public SessionUserID As String = ""
    Public SessionComputerID As String = ""
    Public AppSessionID As String = ""

    Public bShowErrors As Boolean = True

    Public oDataFromPost As New colLoadedFileData

    Public aValidFileExtensions As New ArrayList
    Public sListOfExtensions As String = ""
    Public sValidFileExtensions As String = ""
    Public sHTMLListOfExtensions As String = ""

    Public sReturnURL As String = ""

    Public Sub New()
        Dim oTemp As Object, sTemp As String = ""
        oTemp = GetMySetting("ValidFileExtensions")
        If oTemp IsNot Nothing Then
            sTemp = oTemp.ToString

            sListOfExtensions = Replace(Replace(Replace(Replace(Replace(Replace(sTemp, ";", ","), ":", ","), ".", ","), " ", ","), "|", ","), vbTab, ",")
            Do While sListOfExtensions.IndexOf(",,") >= 0 : sListOfExtensions = sListOfExtensions.Replace(",,", ",") : Loop

            sValidFileExtensions = Replace(Replace(Replace(sListOfExtensions, vbCrLf, ","), vbCr, ","), vbLf, ",")
            Do While sValidFileExtensions.IndexOf(",,") >= 0 : sValidFileExtensions = sValidFileExtensions.Replace(",,", ",") : Loop
            Do While Left(sValidFileExtensions, 1) = "," : sValidFileExtensions = sValidFileExtensions.Substring(1) : Loop
            Do While Right(sValidFileExtensions, 1) = "," : sValidFileExtensions = sValidFileExtensions.Substring(0, sValidFileExtensions.Length - 1) : Loop
            aValidFileExtensions = New ArrayList(Split(sValidFileExtensions.ToUpper, ","))
            sValidFileExtensions = """" & Join(Split(sValidFileExtensions, ","), """,""") & """"

            sListOfExtensions = Replace(Replace(Replace(sListOfExtensions, vbCrLf, "|"), vbCr, "|"), vbLf, "|")
            Do While sListOfExtensions.IndexOf("|,") >= 0 : sListOfExtensions = sListOfExtensions.Replace("|,", "|") : Loop
            Do While sListOfExtensions.IndexOf("||") >= 0 : sListOfExtensions = sListOfExtensions.Replace("||", "|") : Loop
            Do While Left(sListOfExtensions, 1) = "|" : sListOfExtensions = sListOfExtensions.Substring(1) : Loop
            Do While Right(sListOfExtensions, 1) = "|" : sListOfExtensions = sListOfExtensions.Substring(0, sListOfExtensions.Length - 1) : Loop
            'sHTMLListOfExtensions = Replace(sListOfExtensions, vbLf, "<br />")
            sHTMLListOfExtensions = Replace(sListOfExtensions, "|", "<br/>")

        End If
    End Sub

    Property ErrorLog As wRuntimeErrors
        Set(value As wRuntimeErrors)
            oErrorLog = value
        End Set
        Get
            If oErrorLog Is Nothing Then oErrorLog = New wRuntimeErrors
            Return oErrorLog
        End Get
    End Property

End Class
