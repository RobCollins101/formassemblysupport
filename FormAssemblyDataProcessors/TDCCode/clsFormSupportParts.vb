﻿Imports System.Net
Imports System.Xml

Public Class clsFormSupportParts
    Protected Friend bShowValues As Boolean = False
    Protected Friend sProcessing As String = ""
    Protected Friend sFileList As String = ""
    Protected Friend sFilesDetected As String = ""
    Protected Friend sErrors As String = ""
    Protected Friend oSubst As New clsValueSubstitutions
    Protected Friend sReturnURLToCaller As String = ""
    Protected Friend sSessionID As String = ""
    Protected Friend oPage As Page
    Protected Friend tblQueryStringValues As Table
    Protected Friend tblPostStringValues As Table
    Protected Friend tblRecoveredStringValues As Table

    Protected Friend sBaseDestDirectoryTemplate As String = ""
    Protected Friend sBaseDestLogFilePathTemplate As String = ""
    Protected Friend sBaseLogFileEntryTemplate As String = ""
    Protected Friend sBaseDestFilePathTemplate As String = ""
    Protected Friend sBaseDestFilenameTemplate As String

    Protected Friend sDestDirectoryTemplate As String = ""
    Protected Friend sDestLogFilePathTemplate As String = ""
    Protected Friend sLogFileEntryTemplate As String = ""
    Protected Friend sDestFilePathTemplate As String = ""
    Protected Friend sDestFilenameTemplate As String = ""
    Protected Friend oRequest As HttpRequest


    'Protected Friend oFieldDataUsed As New SortedList(Of String, String)
    Protected Friend oFieldDataUsed As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
    Private sLogFilePathUsed As String = ""



    Friend Sub ProcessPostedData(oRequestIn As HttpRequest, oCurrentData As clsCurrentData)

        'Dim oFieldDataUsedInPath As SortedList(Of String, String)
        Dim aDestPaths As New List(Of String), aFilesDetected As New ArrayList()
        'Dim sKey As String, sValue As String, oTemp As Object
        Dim sTemp As String = ""
        Dim aFileLinksToLoadLabels As ArrayList
        Dim oDestFile As clsLoadedFileData



        Try

            oRequest = oRequestIn

            ' Process the field values and the passed files

            'oFieldDataUsed = New SortedList(Of String, String)

            If oCurrentData.oDataFromPost Is Nothing Then oCurrentData.oDataFromPost = New colLoadedFileData

            aFileLinksToLoadLabels = New ArrayList(Split((GetMySetting("FieldsForFileLinks").ToString.ToUpper), ","))

            ' Get the GET and POST form fields

            sTemp = ProcessFieldValues(oRequest, aFilesDetected, oCurrentData)
            sProcessing &= sTemp

            ' Apply the fields found

            ProcessPostedDataValues(oCurrentData)

            'sKey = "LogFilePath" : If oFieldDataUsed.ContainsKey(sKey) Then sDestLogFilePathTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogFile" : If sDestLogFilePathTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestLogFilePathTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogPath" : If sDestLogFilePathTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestLogFilePathTemplate = oFieldDataUsed.Item(sKey).ToString

            'sKey = "LogLine" : If oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogFileEntry" : If sLogFileEntryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogEntry" : If sLogFileEntryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogData" : If sLogFileEntryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "LineData" : If sLogFileEntryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString

            'sKey = "ToCivicaFolderDest" : If oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "FileDest" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "FileDestination" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "FileUpload" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "FileUploadDest" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "FileUploadDestination" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "DestinationFolder" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            'sKey = "DestFolder" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString


            'If sDestDirectoryTemplate = "" Then sDestDirectoryTemplate = sBaseDestDirectoryTemplate
            'If sDestFilePathTemplate = "" Then sDestFilePathTemplate = sBaseDestFilePathTemplate
            'If sDestLogFilePathTemplate = "" Then sDestLogFilePathTemplate = sBaseDestLogFilePathTemplate
            'If sLogFileEntryTemplate = "" Then sLogFileEntryTemplate = sBaseLogFileEntryTemplate

            'sTemp = LoadFilesOfSubmission(oRequest, aFilesDetected, aFileLinksToLoadLabels, oCurrentData)
            'sProcessing &= sTemp

            'oFieldDataUsedInPath = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
            'For Each sKey In oCurrentData.oDataFromPost.aFieldValues.Keys
            '    sValue = oCurrentData.oDataFromPost.aFieldValues.Item(sKey).ToString
            '    If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sValue Else oFieldDataUsed.Add(sKey, sValue)
            'Next

            'If oCurrentData.oDataFromPost.Count > 0 Then
            '    For Each sDestFileKey In oCurrentData.oDataFromPost.Keys

            '        sProcessing &= "Processing Key : " & sDestFileKey & "<br/>"
            '        oDestFile = oCurrentData.oDataFromPost.Item(sDestFileKey)

            '        'sTemp = LoadFilesOfSubmission(oRequest, aFilesDetected, aFileLinksToLoadLabels, oCurrentData)
            '        sTemp = ProcessOneFile(oDestFile, oFieldDataUsed, oFieldDataUsedInPath, oSubst, aDestPaths, oCurrentData)
            '        sProcessing &= sTemp

            '    Next
            '    If aDestPaths.Count > 0 Then
            '        sTemp = Join(aDestPaths.ToArray, "<br/>")
            '        sKey = "HTMLFilesLoaded" : If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sTemp Else oFieldDataUsed.Add(sKey, sTemp)
            '        sKey = "HTMLFileList" : If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sTemp Else oFieldDataUsed.Add(sKey, sTemp)
            '        sKey = "HTMLFiles" : If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sTemp Else oFieldDataUsed.Add(sKey, sTemp)
            '    End If


            'End If




        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString
        End Try

        'If oCurrentData IsNot Nothing Then
        '    If oCurrentData.oDataFromPost IsNot Nothing Then
        '        If oCurrentData.oDataFromPost.aFieldValues IsNot Nothing Then oCurrentData.oDataFromPost.aFieldValues.Clear()
        '        oCurrentData.oDataFromPost.Clear()
        '        oCurrentData.oDataFromPost = Nothing
        '    End If
        '    oCurrentData = Nothing
        'End If

        If oDestFile IsNot Nothing Then Try : oDestFile = Nothing : Catch ex As Exception : End Try



        'If oCurrentData.sReturnURL <> "" Then
        '    oCurrentData.sReturnURL = oSubst.GetSubstitutionForString(oCurrentData.sReturnURL)

        '    Response.Redirect(oCurrentData.sReturnURL, True)
        'End If


    End Sub



    Friend Sub ProcessPostedDataValues(oCurrentData As clsCurrentData)


        Dim oFieldDataUsedInPath As SortedList(Of String, String)
        Dim aDestPaths As New List(Of String), aFilesDetected As New ArrayList()
        Dim sKey As String, sValue As String, oTemp As Object
        Dim sTemp As String = ""
        Dim aFileLinksToLoadLabels As New ArrayList
        Dim oDestFile As clsLoadedFileData

        Try

            ' Apply the fields found

            sKey = "SessionID" : If oFieldDataUsed.ContainsKey(sKey) Then sSessionID = oFieldDataUsed.Item(sKey).ToString
            sKey = "SessionIdentifier" : If sSessionID = "" And oFieldDataUsed.ContainsKey(sKey) Then sSessionID = oFieldDataUsed.Item(sKey).ToString
            sKey = "SessionUID" : If sSessionID = "" And oFieldDataUsed.ContainsKey(sKey) Then sSessionID = oFieldDataUsed.Item(sKey).ToString
            sKey = "Session" : If sSessionID = "" And oFieldDataUsed.ContainsKey(sKey) Then sSessionID = oFieldDataUsed.Item(sKey).ToString


            sKey = "LogFilePath" : If oFieldDataUsed.ContainsKey(sKey) Then sDestLogFilePathTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "LogFile" : If sDestLogFilePathTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestLogFilePathTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "LogPath" : If sDestLogFilePathTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestLogFilePathTemplate = oFieldDataUsed.Item(sKey).ToString

            sKey = "LogLine" : If oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "LogFileEntry" : If sLogFileEntryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "LogEntry" : If sLogFileEntryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "LogData" : If sLogFileEntryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "LineData" : If sLogFileEntryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntryTemplate = oFieldDataUsed.Item(sKey).ToString

            sKey = "ToCivicaFolderDest" : If oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "FileDest" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "FileDestination" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "FileUpload" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "FileUploadDest" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "FileUploadDestination" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "DestinationFolder" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString
            sKey = "DestFolder" : If sDestDirectoryTemplate = "" And oFieldDataUsed.ContainsKey(sKey) Then sDestDirectoryTemplate = oFieldDataUsed.Item(sKey).ToString


            If sDestDirectoryTemplate = "" Then sDestDirectoryTemplate = sBaseDestDirectoryTemplate
            If sDestFilePathTemplate = "" Then sDestFilePathTemplate = sBaseDestFilePathTemplate
            If sDestLogFilePathTemplate = "" Then sDestLogFilePathTemplate = sBaseDestLogFilePathTemplate
            If sLogFileEntryTemplate = "" Then sLogFileEntryTemplate = sBaseLogFileEntryTemplate

            sTemp = LoadFilesOfSubmission(oRequest, aFilesDetected, aFileLinksToLoadLabels, oCurrentData)
            sProcessing &= sTemp

            oFieldDataUsedInPath = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
            For Each sKey In oCurrentData.oDataFromPost.aFieldValues.Keys
                sValue = oCurrentData.oDataFromPost.aFieldValues.Item(sKey).ToString
                If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sValue Else oFieldDataUsed.Add(sKey, sValue)
            Next

            If oCurrentData.oDataFromPost.Count > 0 Then
                For Each sDestFileKey In oCurrentData.oDataFromPost.Keys

                    sProcessing &= "Processing Key : " & sDestFileKey & "<br/>"
                    oDestFile = oCurrentData.oDataFromPost.Item(sDestFileKey)

                    'sTemp = LoadFilesOfSubmission(oRequest, aFilesDetected, aFileLinksToLoadLabels, oCurrentData)
                    sTemp = ProcessOneFile(oDestFile, oFieldDataUsed, oFieldDataUsedInPath, oSubst, aDestPaths, oCurrentData)
                    sProcessing &= sTemp

                Next
                If aDestPaths.Count > 0 Then
                    sTemp = Join(aDestPaths.ToArray, "<br/>")
                    sKey = "HTMLFilesLoaded" : If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sTemp Else oFieldDataUsed.Add(sKey, sTemp)
                    sKey = "HTMLFileList" : If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sTemp Else oFieldDataUsed.Add(sKey, sTemp)
                    sKey = "HTMLFiles" : If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sTemp Else oFieldDataUsed.Add(sKey, sTemp)
                End If


            End If




        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString
        End Try

        'If oCurrentData IsNot Nothing Then
        '    If oCurrentData.oDataFromPost IsNot Nothing Then
        '        If oCurrentData.oDataFromPost.aFieldValues IsNot Nothing Then oCurrentData.oDataFromPost.aFieldValues.Clear()
        '        oCurrentData.oDataFromPost.Clear()
        '        oCurrentData.oDataFromPost = Nothing
        '    End If
        '    oCurrentData = Nothing
        'End If

        If oDestFile IsNot Nothing Then Try : oDestFile = Nothing : Catch ex As Exception : End Try



        'If oCurrentData.sReturnURL <> "" Then
        '    oCurrentData.sReturnURL = oSubst.GetSubstitutionForString(oCurrentData.sReturnURL)

        '    Response.Redirect(oCurrentData.sReturnURL, True)
        'End If


    End Sub













    Friend Function GenerateDocument(sHTMLSource As String, sFileStorePath As String, sLogFilePath As String, sLogFileEntry As String) As String
        Dim sHTMLPassed As String = "", sFileStorePathUsed As String, sTargetExtn As String
        Dim oFileInfo As IO.FileInfo, owsConvert As wsConvertToImage.wsConvertToImage
        Dim owsConvertibles As wsConvertToImage.FileTypesListed
        Dim oConvParams As wsConvertToImage.RequestHTMLConvWithFields
        'Dim sLogFilePathUsed As String, 
        Dim sLogFileEntryUsed As String, sReturn As String
        'Dim aConvExtns As New ArrayList From {"pdf", "bmp", "jpg", "png", "gif", "tif"}
        Dim oItems As Array, bCanConvert As Boolean = False
        Dim owrStream As IO.StreamWriter ', oWrStream As IO.StreamWriter
        Dim oCreatedDoc As wsConvertToImage.ConvertedDoc, owsResponse As wsConvertToImage.clsWebResponseConvertedDocs
        Dim oFieldVals As New List(Of wsConvertToImage.FieldValue), oFieldVal As wsConvertToImage.FieldValue
        Dim sKey As String, sValue As String, iFileNo As Integer, oLogFileInfo As IO.FileInfo

        Try
            oSubst.AddNewValues(oFieldDataUsed)
            sHTMLPassed = LoadIfFile(sHTMLSource, oSubst.oAllSubstituteStrings, oSubst.oAllSubstituteStrings)

            If sFileStorePath <> "" Then
                sFileStorePathUsed = oSubst.GetSubstitutionForString(sFileStorePath, clsValueSubstitutions.TargetModifier.ForFilePath)

                oFileInfo = New IO.FileInfo(sFileStorePathUsed)
                If Not (oFileInfo.Directory.Exists) Then oFileInfo.Directory.Create()

                If oFileInfo.Directory.Exists Then

                    sTargetExtn = oFileInfo.Extension.Replace(".", "")
                    owsConvert = New wsConvertToImage.wsConvertToImage
                    owsConvertibles = owsConvert.ConvertibleExtensions("HTM")
                    oItems = owsConvertibles.Items.ToArray
                    If oItems.Length > 0 Then

                        For Each oItem In oItems
                            If oItem.ToString = sTargetExtn Then
                                bCanConvert = True
                                Exit For
                            End If
                        Next
                    End If
                    If bCanConvert Then
                        oConvParams = New wsConvertToImage.RequestHTMLConvWithFields
                        oConvParams.DestinationFileNameTemplate = sFileStorePathUsed
                        oConvParams.DestinationFileType = oFileInfo.Extension
                        oConvParams.CanOverwriteDestination = "True"
                        oConvParams.HTMLToConvert = sHTMLPassed
                        oConvParams.FileType = "HTM"

                        oFieldVal = New wsConvertToImage.FieldValue
                        For Each sKey In oSubst.oExtraSubstituteStrings.Keys
                            sValue = oSubst.oExtraSubstituteStrings.Item(sKey).ToString
                            oFieldVal.FieldName = sKey
                            oFieldVal.FieldData = sValue
                            oFieldVals.Add(oFieldVal)
                        Next

                        oConvParams.FieldData = oFieldVals.ToArray

                        owsResponse = owsConvert.HTMLImageConversionWithFields(oConvParams)
                        iFileNo = 0

                        For Each oCreatedDoc In owsResponse.ConvertedDocuments
                            oSubst.AddNewValue("DestinationPath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DestinationDocumentPath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DocumentPath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DocPath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DestDocumentPath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DestDocPath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DestinationFolder", IO.Path.GetDirectoryName(oCreatedDoc.DestinationPath))
                            oSubst.AddNewValue("DestinationFilePath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DestinationFileName", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DestinationFile", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("FilePath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DestFilePath", oCreatedDoc.DestinationPath)
                            oSubst.AddNewValue("DestFolder", IO.Path.GetDirectoryName(oCreatedDoc.DestinationPath))

                            iFileNo += 1
                            oSubst.AddNewValue("DocumentNo", iFileNo.ToString)
                            oSubst.AddNewValue("PageNo", iFileNo.ToString)
                            oSubst.AddNewValue("FileNo", iFileNo.ToString)
                            oSubst.AddNewValue("DocumentNumber", iFileNo.ToString)
                            oSubst.AddNewValue("PageNumber", iFileNo.ToString)
                            oSubst.AddNewValue("FileNumber", iFileNo.ToString)

                            If sLogFilePath <> "" And sLogFileEntry <> "" Then
                                sLogFilePathUsed = oSubst.GetSubstitutionForString(sLogFilePath, clsValueSubstitutions.TargetModifier.ForFilePath)
                                sLogFileEntryUsed = oSubst.GetSubstitutionForString(sLogFileEntry)

                                oLogFileInfo = New IO.FileInfo(sLogFilePathUsed)
                                owrStream = oLogFileInfo.AppendText()
                                owrStream.AutoFlush = True
                                owrStream.WriteLine(sLogFileEntryUsed)
                                owrStream.Flush() : owrStream.Close() : owrStream = Nothing

                            End If

                            sReturn = oCreatedDoc.DestinationPath

                        Next


                    Else

                        oSubst.AddNewValue("DestinationPath", oFileInfo.FullName)
                        oSubst.AddNewValue("DestinationDocumentPath", oFileInfo.FullName)
                        oSubst.AddNewValue("DocumentPath", oFileInfo.FullName)
                        oSubst.AddNewValue("DocPath", oFileInfo.FullName)
                        oSubst.AddNewValue("DestDocumentPath", oFileInfo.FullName)
                        oSubst.AddNewValue("DestDocPath", oFileInfo.FullName)
                        oSubst.AddNewValue("DestinationFolder", oFileInfo.DirectoryName)
                        oSubst.AddNewValue("DestinationFilePath", oFileInfo.FullName)
                        oSubst.AddNewValue("DestinationFileName", oFileInfo.FullName)
                        oSubst.AddNewValue("DestinationFile", oFileInfo.FullName)
                        oSubst.AddNewValue("FilePath", oFileInfo.FullName)
                        oSubst.AddNewValue("DestFilePath", oFileInfo.FullName)
                        oSubst.AddNewValue("DestFolder", oFileInfo.DirectoryName)

                        sHTMLPassed = oSubst.GetSubstitutionForString(sHTMLPassed)

                        'oFileInfo.AppendText().WriteLine(sHTMLPassed)
                        owrStream = oFileInfo.AppendText()
                        owrStream.AutoFlush = True
                        owrStream.WriteLine(sHTMLPassed)
                        owrStream.Flush() : owrStream.Close() : owrStream = Nothing


                        'IO.File.WriteAllText(oFileInfo.FullName, sHTMLPassed)

                        If sLogFilePath <> "" And sLogFileEntry <> "" Then
                            sLogFilePathUsed = oSubst.GetSubstitutionForString(sLogFilePath, clsValueSubstitutions.TargetModifier.ForFilePath)
                            sLogFileEntryUsed = oSubst.GetSubstitutionForString(sLogFileEntry)

                            oLogFileInfo = New IO.FileInfo(sLogFilePathUsed)
                            owrStream = oLogFileInfo.AppendText()
                            owrStream.AutoFlush = True
                            owrStream.WriteLine(sLogFileEntryUsed)
                            owrStream.Flush() : owrStream.Close() : owrStream = Nothing

                        End If

                        sReturn = oFileInfo.FullName

                    End If

                    '    If aConvExtns.Contains(oFileInfo.Extension) Then

                    '        owrStream = oFileInfo.Create
                    '        owrStream.Write()
                End If


            End If

        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString
        End Try

        If owrStream IsNot Nothing Then owrStream.Flush() : owrStream.Close() : owrStream = Nothing
        If oFileInfo IsNot Nothing Then oFileInfo = Nothing
        If owsConvert IsNot Nothing Then owsConvert = Nothing
        If owsConvertibles IsNot Nothing Then owsConvertibles = Nothing
        If oConvParams IsNot Nothing Then oConvParams = Nothing

        Return sReturn

    End Function



    Friend Function ProcessFieldValues(oRequest As HttpRequest, aFilesDetected As ArrayList, oCurrentData As clsCurrentData) As String

        Dim sReturn As String = "", sKey As String, sValue As String
        Dim sTemp As String
        Dim oTblRow As TableRow, oTblCell As TableCell
        Dim aSpecialDebugFieldNames As New ArrayList, oTemp As Object

        Try

            If oCurrentData.oDataFromPost Is Nothing Then oCurrentData.oDataFromPost = New colLoadedFileData
            oCurrentData.oDataFromPost.Clear()
            If oRequest.QueryString.Count > 0 Then
                For Each sKey In oRequest.QueryString.Keys
                    sKey = UCase(sKey)
                    sValue = oPage.Server.UrlDecode(oRequest.QueryString.Item(sKey))
                    If Not oCurrentData.oDataFromPost.aFieldValues.ContainsKey(sKey) Then oCurrentData.oDataFromPost.aFieldValues.Add(sKey, sValue) Else oCurrentData.oDataFromPost.aFieldValues.Item(sKey) = sValue
                    If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sValue Else oFieldDataUsed.Add(sKey, sValue)
                Next
            End If

            For Each sKey In oRequest.Form.Keys
                sKey = UCase(sKey)
                sValue = oPage.Server.UrlDecode(oRequest.Form.Item(sKey))
                If Not oCurrentData.oDataFromPost.aFieldValues.ContainsKey(sKey) Then oCurrentData.oDataFromPost.aFieldValues.Add(sKey, sValue) Else oCurrentData.oDataFromPost.aFieldValues.Item(sKey) = sValue
                If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sValue Else oFieldDataUsed.Add(sKey, sValue)
            Next

            ' Find any debug indicators

            oTemp = GetMySetting("DebugFieldNames")
            If oTemp IsNot Nothing Then aSpecialDebugFieldNames = New ArrayList(CType(oTemp, Specialized.StringCollection))

            For Each sTemp In aSpecialDebugFieldNames
                If Not bShowValues And oCurrentData.oDataFromPost.aFieldValues.ContainsKey(sTemp) Then bShowValues = StringIsTrue(oCurrentData.oDataFromPost.aFieldValues.Item(sTemp).ToString)
                'Exit For
            Next

            oSubst.AddNewValues(oFieldDataUsed)

            If tblQueryStringValues IsNot Nothing Then

                If oRequest.QueryString.Count > 0 Then
                    For Each sKey In oRequest.QueryString.Keys
                        oTblRow = New TableRow
                        sValue = oPage.Server.UrlDecode(oRequest.QueryString.Item(sKey))
                        oTblCell = New TableCell
                        oTblCell.Text = HttpUtility.HtmlEncode(sKey)
                        oTblRow.Cells.Add(oTblCell)

                        oTblCell = New TableCell
                        oTblCell.Text = HttpUtility.HtmlEncode(sValue)
                        oTblRow.Cells.Add(oTblCell)
                        tblQueryStringValues.Rows.Add(oTblRow)

                        oTblCell = New TableCell
                        sTemp = "<input type=""hidden"" name=""" & sKey & """ id=""" & sKey & """ value=""" & oPage.Server.UrlEncode(sValue) & """ />"
                        oTblCell.Text = sTemp
                        oTblRow.Cells.Add(oTblCell)
                        tblQueryStringValues.Rows.Add(oTblRow)
                        If sKey = "returnurl" Then sReturnURLToCaller = sValue
                    Next
                End If
            End If

            If tblPostStringValues IsNot Nothing Then

                If oRequest.Form.Count > 0 Then
                    For Each sKey In oRequest.Form.Keys
                        oTblRow = New TableRow
                        sValue = oPage.Server.UrlDecode(oRequest.Form.Item(sKey))
                        oTblCell = New TableCell
                        oTblCell.Text = HttpUtility.HtmlEncode(sKey)
                        oTblRow.Cells.Add(oTblCell)

                        oTblCell = New TableCell
                        sTemp = sValue
                        oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
                        oTblRow.Cells.Add(oTblCell)

                        oTblCell = New TableCell
                        sTemp = "<input type=""hidden"" name=""" & sKey & """ id=""" & sKey & """ value=""" & oPage.Server.UrlEncode(sValue) & """ />"
                        oTblCell.Text = sTemp
                        oTblRow.Cells.Add(oTblCell)
                        tblPostStringValues.Rows.Add(oTblRow)
                        If sKey = "returnurl" Then sReturnURLToCaller = sValue
                    Next
                End If
            End If


        Catch ex As Exception
            'lblProcessing.Text &= "ERROR : " & ex.ToString
            sErrors &= "ERROR : " & ex.ToString
        End Try


        Return sReturn
    End Function



    Friend Function LoadFilesOfSubmission(oRequest As HttpRequest, aFilesDetected As ArrayList, aFileLinksToLoadLabels As ArrayList, oCurrentData As clsCurrentData) As String
        Dim sReturn As String = "", aFileURLs As New ArrayList
        Dim sFileName As String, oFilePosted As HttpPostedFile
        Dim iCharPos As Integer, oStream As IO.Stream, aBytes As Byte()
        Dim sValue As String, sKey As String, sTemp As String
        Dim sRegexURL As String, oMatches As MatchCollection, oMatch As Match, oGrp As Group
        Dim aPathParts As String(), sURL As String
        Dim oResponse As WebResponse, oHTTPRequester As WebRequest, aLocalURLs As New ArrayList
        Dim oMemoryStream As IO.MemoryStream, sFieldKey As String

        Try

            sRegexURL = GetMySetting("RegexURLInText").ToString

            For Each oFilePosted In oRequest.Files
                sFileName = oFilePosted.FileName.Replace("\", "/")
                aFilesDetected.Add(sFileName)
                iCharPos = sFileName.LastIndexOf("/")
                If iCharPos > 0 Then sFileName = sFileName.Substring(iCharPos + 1)

                oStream = New IO.MemoryStream()
                ReDim aBytes(CInt(oFilePosted.InputStream.Length))
                oFilePosted.InputStream.Read(aBytes, 0, CInt(oFilePosted.InputStream.Length))

                If oCurrentData.oDataFromPost.ContainsKey(sFileName) Then
                    oCurrentData.oDataFromPost.Item(sFileName) = New clsLoadedFileData(sFileName, aBytes)
                Else

                    oCurrentData.oDataFromPost.Add(sFileName, New clsLoadedFileData(sFileName, aBytes))
                End If
            Next

            If aFileLinksToLoadLabels IsNot Nothing Then

                For Each sKey In oCurrentData.oDataFromPost.aFieldValues.Keys

                    If aFileLinksToLoadLabels.Contains(sKey.ToUpper) Then
                        ' Get the file using http
                        sValue = oCurrentData.oDataFromPost.aFieldValues.Item(sKey).ToString

                        oMatches = Regex.Matches(sValue, sRegexURL)
                        For Each oMatch In oMatches
                            oGrp = oMatch.Groups("FullFilePath")
                            If oGrp IsNot Nothing Then
                                sURL = oGrp.Value
                                aFileURLs.Add(sURL)
                                aFilesDetected.Add(sURL)
                            End If
                        Next

                    End If

                Next
            End If

            sFilesDetected = ""


            If aFileURLs.Count > 0 Then
                For Each sURL In aFileURLs

                    sFileName = ""
                    aPathParts = Split(sURL.Replace("/", "\"), "\")
                    For iPos = UBound(aPathParts) To 0 Step -1
                        sTemp = aPathParts(iPos)
                        If sTemp.LastIndexOf(".") > 0 And sTemp.LastIndexOf(".") > sTemp.Length - 8 Then
                            sFileName = sTemp
                            Exit For
                        End If
                    Next
                    If sFileName = "" Then sFileName = aPathParts(UBound(aPathParts))
                    sFilesDetected &= "File name : " & sFileName & "<br/>" & sURL & "<br/>"

                    'End If

                    If oCurrentData.oDataFromPost.ContainsKey(sFileName) Then
                    Else
                        oHTTPRequester = WebRequest.Create(sURL)
                        oResponse = oHTTPRequester.GetResponse()
                        oStream = oResponse.GetResponseStream()
                        oMemoryStream = New IO.MemoryStream
                        oStream.CopyTo(oMemoryStream)

                        aBytes = oMemoryStream.ToArray
                        oMemoryStream.Close()
                        oMemoryStream.Dispose()
                        oMemoryStream = Nothing

                        sFilesDetected &= "Length : " & aBytes.Length & "<br/>"

                        oCurrentData.oDataFromPost.Add(sFileName, New clsLoadedFileData(sFileName, aBytes))
                    End If

                Next
            End If



            sTemp = oCurrentData.oDataFromPost.Count.ToString
            sFieldKey = "FileCount" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "FileListCount" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "NoOfFilesLoaded" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "NoOfFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "CountLoadedFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "CountFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "CountOfLoadedFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "CountOfFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "SubmissionFileCount" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)
            sFieldKey = "FilesLoadedCount" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sTemp Else oFieldDataUsed.Add(sFieldKey, sTemp)



        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString
        End Try


        If oStream IsNot Nothing Then Try : oStream.Close() : oStream = Nothing : Catch ex As Exception : End Try
        If oHTTPRequester IsNot Nothing Then Try : oHTTPRequester = Nothing : Catch ex As Exception : End Try
        If oFilePosted IsNot Nothing Then Try : oFilePosted = Nothing : Catch ex As Exception : End Try

        Return sReturn
    End Function




    Friend Function ProcessOneFile(oDestFile As clsLoadedFileData, oFieldDataUsedIn As SortedList(Of String, String),
                               oFieldDataUsedInPathIn As SortedList(Of String, String),
                               oSubst As clsValueSubstitutions, aDestPaths As List(Of String),
                               oCurrentData As clsCurrentData) As String

        Dim sFileName As String, sDestFileUsed As String, sSourceExt As String
        Dim iCharPos As Integer, aBytes As Byte(), oTemp As Object
        Dim sFieldKey As String, aLines As List(Of String), sDestLogFilePathUsed As String = ""
        'Dim sDestDirectoryTemplate As String = "", 
        Dim sDestFolderUsed As String = "" ', sDestFilenameTemplate As String = ""
        Dim sDestFileExtension As String = "" ', sDestLogFilePathTemplate As String = ""
        Dim sLogEntryLine As String, sLogFileEntryUsed As String, oSourceFileInfo As IO.FileInfo, oDestFileInfo As IO.FileInfo
        Dim oConvParams As wsConvertToImage.RequestConversionParams, owsConv As wsConvertToImage.wsConvertToImage
        Dim oConvertResponse As wsConvertToImage.clsWebResponseConvertedDocs = Nothing
        Dim sStoredFilePath As String = "", sStoredFileName As String = "", sStoredFileFolder As String = ""
        Dim sReturn As String = "", aConvertExtn As New ArrayList
        Dim sTemp As String = ""
        Dim oFieldDataUsed = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
        Dim oFieldDataUsedInPath = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)


        Try

            oFieldDataUsed = oFieldDataUsedIn
            oFieldDataUsedInPath = oFieldDataUsedInPathIn

            'sDestLogFilePathTemplate = GetMySetting("PlanToCivicaLogFilePath").ToString
            'sLogFileEntryTemplate = GetMySetting("PlanToCivicaLogFileEntry").ToString

            'If oFieldDataUsed.ContainsKey("ToCivicaFolderDest") Then sDestDirectoryTemplate = oFieldDataUsed.Item("ToCivicaFolderDest").ToString
            'If oFieldDataUsed.ContainsKey("FileDest") Then sDestDirectoryTemplate = oFieldDataUsed.Item("FileDest").ToString
            'If oFieldDataUsed.ContainsKey("DestinationFolder") Then sDestDirectoryTemplate = oFieldDataUsed.Item("DestinationFolder").ToString
            'If oFieldDataUsed.ContainsKey("DestFolder") Then sDestDirectoryTemplate = oFieldDataUsed.Item("DestFolder").ToString

            'If sDestDirectoryTemplate = "" Then sDestDirectoryTemplate = GetMySetting("PlanToCivicaFolderDest").ToString
            'If sDestFilenameTemplate = "" Then sDestFilenameTemplate = GetMySetting("PlanToCivicaFilenameDest").ToString

            If oFieldDataUsed.ContainsKey("FileType") Then sDestFileExtension = oFieldDataUsed.Item("FileType").ToString
            If oFieldDataUsed.ContainsKey("DestFileType") Then sDestFileExtension = oFieldDataUsed.Item("DestFileType").ToString
            If oFieldDataUsed.ContainsKey("OutputFileType") Then sDestFileExtension = oFieldDataUsed.Item("OutputFileType").ToString
            If oFieldDataUsed.ContainsKey("OutputType") Then sDestFileExtension = oFieldDataUsed.Item("OutputType").ToString
            If oFieldDataUsed.ContainsKey("StorageType") Then sDestFileExtension = oFieldDataUsed.Item("StorageType").ToString
            If oFieldDataUsed.ContainsKey("StorageFileType") Then sDestFileExtension = oFieldDataUsed.Item("StorageFileType").ToString
            If sDestFileExtension <> "" Then sDestFileExtension = GetMySetting("PlanToCivicaFileType").ToString

            'If oFieldDataUsed.ContainsKey("LogFilePath") Then sDestLogFilePathTemplate = oFieldDataUsed.Item("LogFilePath").ToString
            'If oFieldDataUsed.ContainsKey("LogPath") Then sDestLogFilePathTemplate = oFieldDataUsed.Item("LogPath").ToString
            ''If sDestLogFilePathTemplate = "" Then sDestLogFilePathTemplate = GetMySetting("PlanToCivicaLogFilePath").ToString

            'If oFieldDataUsed.ContainsKey("LogFileEntry") Then sDestLogFilePathTemplate = oFieldDataUsed.Item("LogFileEntry").ToString
            'If oFieldDataUsed.ContainsKey("LogEntry") Then sDestLogFilePathTemplate = oFieldDataUsed.Item("LogEntry").ToString
            'If oFieldDataUsed.ContainsKey("LogLine") Then sDestLogFilePathTemplate = oFieldDataUsed.Item("LogLine").ToString
            'If oFieldDataUsed.ContainsKey("LogRecord") Then sDestLogFilePathTemplate = oFieldDataUsed.Item("LogRecord").ToString

            'If sDestLogFilePathTemplate <> "" Then sDestLogFilePathUsed = oSubst.GetSubstitutionForString(sDestLogFilePathTemplate, clsValueSubstitutions.TargetModifier.ForFilePath)
            If sLogFileEntryTemplate = "" Then sLogFileEntryTemplate = "<<ReferenceIn>>|<<DestinationFileName>>"

            oTemp = GetMySetting("PlanConvertTypes")
            If oTemp IsNot Nothing Then aConvertExtn = New ArrayList(CType(oTemp, Specialized.StringCollection))

            sFileName = oDestFile.FileName
            sReturn &= "File name : " & sFileName & "<br/>"
            sDestFileUsed = ""
            If sFileName = "" Then sFileName = oDestFile.FileName
            iCharPos = sFileName.LastIndexOf("/")
            If iCharPos >= 0 Then sFileName = sFileName.Substring(iCharPos + 1)

            sSourceExt = IIf(IO.Path.GetExtension(sFileName).Substring(0, 1) = ".", IO.Path.GetExtension(sFileName).Substring(1), IO.Path.GetExtension(sFileName)).ToString

            If Not oCurrentData.aValidFileExtensions.Contains(sSourceExt.ToUpper) Then

                sFilesDetected &= "File name : " & sFileName & " : File Extension not approved<br/>"

            Else

                sReturn &= "Extension : " & sSourceExt & "<br/>"

                'sFieldKey = "FileName" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sFileName Else oFieldDataUsed.Add(sFieldKey, sFileName)
                'sFieldKey = "File" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sFileName Else oFieldDataUsed.Add(sFieldKey, sFileName)
                'sFieldKey = "FileNameNoExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = IO.Path.GetFileNameWithoutExtension(sFileName) Else oFieldDataUsed.Add(sFieldKey, IO.Path.GetFileNameWithoutExtension(sFileName))
                'sFieldKey = "FileNoExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = IO.Path.GetFileNameWithoutExtension(sFileName) Else oFieldDataUsed.Add(sFieldKey, IO.Path.GetFileNameWithoutExtension(sFileName))
                'sFieldKey = "SourceFileNameExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SourceExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SourceFileNameExtension" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SourceExtension" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SourceFileNameType" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SourceType" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SrcFileNameExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SrcExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SrcFileNameExtension" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SrcExtension" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SrcFileNameType" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)
                'sFieldKey = "SrcType" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sSourceExt Else oFieldDataUsed.Add(sFieldKey, sSourceExt)

                oSourceFileInfo = New IO.FileInfo(sFileName)
                UpdateSourceFileValuesToFields(oSourceFileInfo, oFieldDataUsed)

                sFieldKey = "DestinationFileNameExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestinationExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestinationFileNameExtension" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestinationExtension" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestinationFileNameType" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestinationType" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestFileNameExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestExt" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestFileNameExtension" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestExtension" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestFileNameType" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)
                sFieldKey = "DestType" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sDestFileExtension Else oFieldDataUsed.Add(sFieldKey, sDestFileExtension)

                For Each sKey In oFieldDataUsed.Keys
                    If oFieldDataUsedInPath.ContainsKey(sKey) Then oFieldDataUsedInPath(sKey) = SanitizeFilePath(oFieldDataUsed(sKey).ToString) Else oFieldDataUsedInPath.Add(sKey, SanitizeFilePath(oFieldDataUsed(sKey).ToString))
                Next

                oSubst.PopulateReplacementArray(oFieldDataUsed)
                sLogFileEntryUsed = oSubst.GetSubstitutionForString(sLogFileEntryTemplate)
                oDestFile.LogRecordLineTemplate = sLogFileEntryUsed

                If oDestFile.SavedFilePath = "" And sDestDirectoryTemplate <> "" Then

                    oSubst.PopulateReplacementArray(oFieldDataUsedInPath)
                    sDestFolderUsed = oSubst.GetSubstitutionForString(sDestDirectoryTemplate, clsValueSubstitutions.TargetModifier.ForFilePath)
                    sDestFileUsed = sDestFolderUsed
                    If sDestFilenameTemplate = "" Then
                        sDestFileUsed &= IIf(Right(sDestFileUsed, 1) <> "\", "\", "").ToString & sFileName
                    Else
                        sDestFileUsed &= IIf(Right(sDestFileUsed, 1) <> "\", "\", "").ToString & oSubst.GetSubstitutionForString(sDestFilenameTemplate, clsValueSubstitutions.TargetModifier.ForFilePath)
                    End If

                    oDestFile.SavedFilePath = sDestFileUsed

                End If

                aBytes = oDestFile.FileData

                oConvParams = New wsConvertToImage.RequestConversionParams
                oConvParams.DocumentToConvertAsArray = aBytes
                oConvParams.SourceFilename = oDestFile.FileName
                oConvParams.ReturnAsByteArray = "False"
                oConvParams.ReturnAsBase64String = "False"
                oConvParams.CanOverwriteDestination = "True"
                oConvParams.ForceTiffToMonochrome = "False"
                oConvParams.DestinationFileType = sDestFileExtension

                If sDestFileUsed <> "" Then
                    oConvParams.DestinationFileNameTemplate = sDestFileUsed
                Else
                    oConvParams.ReturnAsByteArray = "True"
                End If

                sReturn &= "      Sending for conversion<br/>"
                aLines = New List(Of String)

                owsConv = New wsConvertToImage.wsConvertToImage

                If aConvertExtn.Contains(sSourceExt) Then oConvertResponse = owsConv.ConvertToImage(oConvParams)
                If oConvertResponse Is Nothing OrElse oConvertResponse.ConvertedDocuments.Length = 0 Then

                    sStoredFileName = oDestFile.FileName
                    sStoredFileFolder = sDestFolderUsed & IIf(Right(sDestFileUsed, 1) <> "\", "\", "").ToString
                    sStoredFilePath = sStoredFileFolder & sStoredFileName

                    If Not IO.Directory.Exists(sStoredFileFolder) Then IO.Directory.CreateDirectory(sStoredFileFolder)
                    IO.File.WriteAllBytes(sStoredFilePath, aBytes)

                    oDestFileInfo = New IO.FileInfo(oDestFile.SavedFilePath)
                    UpdateDestFileValuesToFields(oDestFileInfo, oFieldDataUsed)

                    'sFieldKey = "DestinationFileName" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFilePath Else oFieldDataUsed.Add(sFieldKey, sStoredFilePath)
                    'sFieldKey = "DestinationFolder" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFilePath)
                    'sFieldKey = "StoredFileName" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileName Else oFieldDataUsed.Add(sFieldKey, sStoredFileName)
                    'sFieldKey = "StoredFile" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileName Else oFieldDataUsed.Add(sFieldKey, sStoredFileName)
                    'sFieldKey = "StoredFolder" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFileFolder)
                    'sFieldKey = "Folder" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFileFolder)
                    'sFieldKey = "StoredDirectory" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFileFolder)
                    'sFieldKey = "Directory" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFileFolder)

                    oSubst.PopulateReplacementArray(oFieldDataUsed)
                    sLogEntryLine = oSubst.GetSubstitutionForString(oDestFile.LogRecordLineTemplate)
                    aLines.Add(sLogEntryLine)
                    aDestPaths.Add("<a href=""" & sStoredFileFolder & """ target=""_blank"">" & sStoredFileFolder & "</a>" & IIf(sStoredFileFolder <> "" And Right(sStoredFileFolder, 1) <> Left(sStoredFileFolder, 1), Left(sStoredFileFolder, 1), "").ToString & sStoredFileName)



                    sFieldKey = "StoredFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                        oFieldDataUsed(sFieldKey) = sStoredFileName
                    Else
                        oFieldDataUsed.Add(sFieldKey, sStoredFileName)
                    End If
                    sFieldKey = "FileNames" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                        oFieldDataUsed(sFieldKey) = sStoredFileName
                    Else
                        oFieldDataUsed.Add(sFieldKey, sStoredFileName)
                    End If
                    sFieldKey = "HTMLStoredFileName" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                        oFieldDataUsed(sFieldKey) = HttpUtility.HtmlEncode(sStoredFileName)
                    Else
                        oFieldDataUsed.Add(sFieldKey, HttpUtility.HtmlEncode(sStoredFileName))
                    End If
                    sFieldKey = "HTMLStoredFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                        oFieldDataUsed(sFieldKey) = HttpUtility.HtmlEncode(sStoredFileName)
                    Else
                        oFieldDataUsed.Add(sFieldKey, HttpUtility.HtmlEncode(sStoredFileName))
                    End If
                    sFieldKey = "HTMLFileNames" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                        oFieldDataUsed(sFieldKey) = HttpUtility.HtmlEncode(sStoredFileName)
                    Else
                        oFieldDataUsed.Add(sFieldKey, HttpUtility.HtmlEncode(sStoredFileName))
                    End If


                    sReturn &= sStoredFileName & " Stored but not converted<br/>"

                Else

                    For Each oImageEntry In oConvertResponse.ConvertedDocuments

                        sStoredFileName = oImageEntry.ObjectName
                        sStoredFileFolder = oImageEntry.DestinationFolder
                        sStoredFilePath = oImageEntry.DestinationPath

                        sReturn &= "      Examining converted doc<br/>"
                        sReturn &= "      doc has path " & sStoredFilePath & "<br/>"

                        'sFieldKey = "DestinationFileName" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFilePath Else oFieldDataUsed.Add(sFieldKey, sStoredFilePath)
                        'sFieldKey = "DestinationFolder" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFilePath Else oFieldDataUsed.Add(sFieldKey, sStoredFilePath)
                        'sFieldKey = "StoredFileName" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileName Else oFieldDataUsed.Add(sFieldKey, sStoredFileName)
                        'sFieldKey = "StoredFile" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileName Else oFieldDataUsed.Add(sFieldKey, sStoredFileName)
                        'sFieldKey = "StoredFolder" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFileFolder)
                        'sFieldKey = "Folder" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFileFolder)
                        'sFieldKey = "StoredDirectory" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFileFolder)
                        'sFieldKey = "Directory" : If oFieldDataUsed.ContainsKey(sFieldKey) Then oFieldDataUsed(sFieldKey) = sStoredFileFolder Else oFieldDataUsed.Add(sFieldKey, sStoredFileFolder)

                        oDestFileInfo = New IO.FileInfo(oDestFile.SavedFilePath)
                        UpdateDestFileValuesToFields(oDestFileInfo, oFieldDataUsed)

                        oSubst.PopulateReplacementArray(oFieldDataUsed)
                        sLogEntryLine = oSubst.GetSubstitutionForString(oDestFile.LogRecordLineTemplate)
                        aLines.Add(sLogEntryLine)
                        aDestPaths.Add("<a href=""" & sStoredFileFolder & """ target=""_blank"">" & sStoredFileFolder & "</a>" & IIf(sStoredFileFolder <> "" And Right(sStoredFileFolder, 1) <> Left(sStoredFileFolder, 1), Left(sStoredFileFolder, 1), "").ToString & sStoredFileName)

                        sFieldKey = "StoredFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                            sTemp = oFieldDataUsed(sFieldKey) & vbCrLf & sStoredFileName
                            oFieldDataUsed(sFieldKey) = sTemp
                        Else
                            oFieldDataUsed.Add(sFieldKey, sStoredFileName)
                        End If
                        sFieldKey = "FileNames" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                            sTemp = oFieldDataUsed(sFieldKey) & vbCrLf & sStoredFileName
                            oFieldDataUsed(sFieldKey) = sTemp
                        Else
                            oFieldDataUsed.Add(sFieldKey, sStoredFileName)
                        End If
                        sFieldKey = "HTMLStoredFileName" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                            sTemp = oFieldDataUsed(sFieldKey) & "<br/>" & HttpUtility.HtmlEncode(sStoredFileName)
                            oFieldDataUsed(sFieldKey) = sTemp
                        Else
                            oFieldDataUsed.Add(sFieldKey, HttpUtility.HtmlEncode(sStoredFileName))
                        End If
                        sFieldKey = "HTMLStoredFiles" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                            sTemp = oFieldDataUsed(sFieldKey) & "<br/>" & HttpUtility.HtmlEncode(sStoredFileName)
                            oFieldDataUsed(sFieldKey) = sTemp
                        Else
                            oFieldDataUsed.Add(sFieldKey, HttpUtility.HtmlEncode(sStoredFileName))
                        End If
                        sFieldKey = "HTMLFileNames" : If oFieldDataUsed.ContainsKey(sFieldKey) Then
                            sTemp = oFieldDataUsed(sFieldKey) & "<br/>" & HttpUtility.HtmlEncode(sStoredFileName)
                            oFieldDataUsed(sFieldKey) = sTemp
                        Else
                            oFieldDataUsed.Add(sFieldKey, HttpUtility.HtmlEncode(sStoredFileName))
                        End If

                    Next

                End If

                If sDestLogFilePathTemplate <> "" Then sDestLogFilePathUsed = oSubst.GetSubstitutionForString(sDestLogFilePathTemplate, clsValueSubstitutions.TargetModifier.ForFilePath)

                If aLines.Count > 0 And sDestLogFilePathUsed <> "" Then
                    Try
                        IO.File.AppendAllLines(sDestLogFilePathUsed, aLines)
                    Catch ex As Exception
                        If TypeOf (ex) Is System.UnauthorizedAccessException Then
                            sErrors &= "ERROR : No access to the log file location"
                        End If


                    End Try
                End If

                oConvParams.DestinationLocalFilePath = Join(aDestPaths.ToArray, ",")
                sFileList &= Join(aDestPaths.ToArray, "<br/>")
                If oConvertResponse IsNot Nothing Then
                    For Each oErroNode In oConvertResponse.ErrorList
                        sErrors &= oErroNode.ErrorDescription & "<br/><br/>"
                    Next
                End If

            End If

        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString
        End Try

        If owsConv IsNot Nothing Then Try : owsConv = Nothing : Catch ex As Exception : End Try
        If oConvParams IsNot Nothing Then Try : oConvParams = Nothing : Catch ex As Exception : End Try
        If oConvertResponse IsNot Nothing Then Try : oConvertResponse = Nothing : Catch ex As Exception : End Try


        Return sReturn
    End Function



    Friend Sub SendEmailsUsingFieldValues()
        Dim sClientEmailTo As String = "", sClientEmailFrom As String = "", sClientEmailCC As String = ""
        Dim sClientEmailSubject As String = "", sClientEmailBody As String = "", sClientAttachDoc As Boolean = False
        Dim sInternalEmailTo As String = "", sInternalEmailFrom As String = "", sInternalEmailCC As String = ""
        Dim sInternalEmailSubject As String = "", sInternalEmailBody As String = "", sInternalAttachDoc As Boolean = False
        Dim sEmailStorePath As String = ""
        Dim sKey As String = "", sValue As String = "", oTemp As Object

        Try

            sKey = "ClientEmail" : If oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "ClientsEmail" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "ClientEmailTo" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailClient" : If oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailClients" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailToClient" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "Email" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailTo" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "UserEmail" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "SubmittingEmail" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "SubmissionEmail" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailSubmitting" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailSubmission" : If sClientEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailTo = oFieldDataUsed.Item(sKey)
            sClientEmailTo = oSubst.GetSubstitutionForString(sClientEmailTo)
            If sClientEmailTo <> "" Then
                sKey = "ClientEmailCC" : If oFieldDataUsed.ContainsKey(sKey) Then sClientEmailCC = oFieldDataUsed.Item(sKey)
                sKey = "EmailCCClient" : If sClientEmailCC = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailCC = oFieldDataUsed.Item(sKey)
                sKey = "EmailCC" : If sClientEmailCC = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailCC = oFieldDataUsed.Item(sKey)

                sKey = "ClientEMailFrom" : If oFieldDataUsed.ContainsKey(sKey) Then sClientEmailFrom = oFieldDataUsed.Item(sKey)
                sKey = "EmailFrom" : If sClientEmailFrom = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailFrom = oFieldDataUsed.Item(sKey)
                sKey = "From" : If sClientEmailFrom = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailFrom = oFieldDataUsed.Item(sKey)
                If sClientEmailFrom = "" Then
                    oTemp = GetMySetting("DefaultClientEMailFrom")
                    If oTemp IsNot Nothing Then sClientEmailFrom = oTemp.ToString
                End If

                sKey = "ClientSubject" : If oFieldDataUsed.ContainsKey(sKey) Then sClientEmailSubject = oFieldDataUsed.Item(sKey)
                sKey = "ClientEmailSubject" : If sClientEmailSubject = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailSubject = oFieldDataUsed.Item(sKey)
                sKey = "EmailSubjectClient" : If sClientEmailSubject = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailSubject = oFieldDataUsed.Item(sKey)
                If sClientEmailFrom = "" Then
                    oTemp = GetMySetting("DefaultClientEmailSubject")
                    If oTemp IsNot Nothing Then sClientEmailSubject = oTemp.ToString
                End If

                sKey = "ClientEmailBody" : If oFieldDataUsed.ContainsKey(sKey) Then sClientEmailBody = oFieldDataUsed.Item(sKey)
                'sKey = "ClientEmail" : If sClientEmailBody = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailBody = oFieldDataUsed.Item(sKey)
                sKey = "BodyClientEmail" : If sClientEmailBody = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailBody = oFieldDataUsed.Item(sKey)
                sKey = "BodyOfClientEmail" : If sClientEmailBody = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailBody = oFieldDataUsed.Item(sKey)
                sKey = "BodyEmailClient" : If sClientEmailBody = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailBody = oFieldDataUsed.Item(sKey)
                sKey = "BodyOfEmailClient" : If sClientEmailBody = "" And oFieldDataUsed.ContainsKey(sKey) Then sClientEmailBody = oFieldDataUsed.Item(sKey)
                If sClientEmailBody = "" Then
                    oTemp = GetMySetting("DefaultClientEmailBody")
                    If oTemp IsNot Nothing Then sClientEmailBody = oTemp.ToString
                End If


            End If


            sKey = "InternalEmail" : If oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "InternalEmailTo" : If sInternalEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailInternal" : If sInternalEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailToInternal" : If sInternalEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "AdminEmail" : If sInternalEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "AdminEmailTo" : If sInternalEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailAdmin" : If sInternalEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailTo = oFieldDataUsed.Item(sKey)
            sKey = "EmailToAdmin" : If sInternalEmailTo = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailTo = oFieldDataUsed.Item(sKey)

            sKey = "InternalEmailSubject" : If oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailSubject = oFieldDataUsed.Item(sKey)
            sKey = "EmailSubjectAdmin" : If sInternalEmailSubject = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailSubject = oFieldDataUsed.Item(sKey)
            sKey = "AdminEmailSubject" : If sInternalEmailSubject = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailSubject = oFieldDataUsed.Item(sKey)
            sKey = "EmailSubjectInternal" : If sInternalEmailSubject = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailSubject = oFieldDataUsed.Item(sKey)

            sKey = "InternalEmailBody" : If oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailBody = oFieldDataUsed.Item(sKey)
            sKey = "EmailBodyAdmin" : If sInternalEmailBody = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailBody = oFieldDataUsed.Item(sKey)
            sKey = "AdminEmailBody" : If sInternalEmailBody = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailBody = oFieldDataUsed.Item(sKey)
            sKey = "EmailBodyInternal" : If sInternalEmailBody = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailBody = oFieldDataUsed.Item(sKey)

            If sInternalEmailTo <> "" Or sInternalEmailSubject <> "" Or sInternalEmailBody <> "" Then

                If sInternalEmailTo = "" Then
                    oTemp = GetMySetting("DefaultInternalEMailTo")
                    If oTemp IsNot Nothing Then sInternalEmailTo = oTemp.ToString
                End If

                If sInternalEmailSubject = "" Then
                    oTemp = GetMySetting("DefaultInternalEMailSubject")
                    If oTemp IsNot Nothing Then sInternalEmailSubject = oTemp.ToString
                End If

                If sInternalEmailBody = "" Then
                    oTemp = GetMySetting("DefaultInternalEMailBody")
                    If oTemp IsNot Nothing Then sInternalEmailBody = oTemp.ToString
                End If

                sKey = "InternalEmailCC" : If oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailCC = oFieldDataUsed.Item(sKey)
                sKey = "EmailCCInternal" : If sInternalEmailCC = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailCC = oFieldDataUsed.Item(sKey)
                sKey = "AdminEmailCC" : If sInternalEmailCC = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailCC = oFieldDataUsed.Item(sKey)
                sKey = "EmailCCAdmin" : If sInternalEmailCC = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailCC = oFieldDataUsed.Item(sKey)

                sKey = "InternalEmailFrom" : If oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailFrom = oFieldDataUsed.Item(sKey)
                sKey = "EmailFromInternal" : If sInternalEmailFrom = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailFrom = oFieldDataUsed.Item(sKey)
                sKey = "AdminEmailFrom" : If sInternalEmailFrom = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailFrom = oFieldDataUsed.Item(sKey)
                sKey = "EmailFromAdmin" : If sInternalEmailFrom = "" And oFieldDataUsed.ContainsKey(sKey) Then sInternalEmailFrom = oFieldDataUsed.Item(sKey)
                If sInternalEmailFrom = "" Then
                    oTemp = GetMySetting("DefaultInternalEmailFrom")
                    If oTemp IsNot Nothing Then sInternalEmailFrom = oTemp.ToString
                End If

            End If

            sKey = "DestinationFolder" : If oFieldDataUsed.ContainsKey(sKey) Then sEmailStorePath = oFieldDataUsed.Item(sKey)
            sKey = "StoredFolder" : If sEmailStorePath = "" And oFieldDataUsed.ContainsKey(sKey) Then sEmailStorePath = oFieldDataUsed.Item(sKey)
            sKey = "Folder" : If sEmailStorePath = "" And oFieldDataUsed.ContainsKey(sKey) Then sEmailStorePath = oFieldDataUsed.Item(sKey)
            sKey = "StoredDirectory" : If sEmailStorePath = "" And oFieldDataUsed.ContainsKey(sKey) Then sEmailStorePath = oFieldDataUsed.Item(sKey)
            sKey = "Directory" : If sEmailStorePath = "" And oFieldDataUsed.ContainsKey(sKey) Then sEmailStorePath = oFieldDataUsed.Item(sKey)

            SendEmails(sClientEmailTo, sClientEmailFrom, sClientEmailCC, sClientEmailSubject, sClientEmailBody, sClientAttachDoc,
                       sInternalEmailTo, sInternalEmailFrom, sInternalEmailCC, sInternalEmailSubject, sInternalEmailBody, sInternalAttachDoc, sEmailStorePath)



        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString
        End Try


    End Sub



    Public Function SendEmails(sClientEmailTo As String, sClientEmailFrom As String, sClientEmailCC As String,
                               sClientEmailSubject As String, sClientEmailBody As String, sClientAttachDoc As Boolean,
                               sInternalEmailTo As String, sInternalEmailFrom As String, sInternalEmailCC As String,
                               sInternalEmailSubject As String, sInternalEmailBody As String, sInternalAttachDoc As Boolean,
                               sStorePath As String) As Boolean

        Dim oEMail As clsEMail, oTemp As Object, sTemp As String, iTemp As Integer, bReturn As Boolean = False
        Dim sStorePathEmail As String, oStoreFileInfo As IO.FileInfo, oTxtWriter As IO.TextWriter
        Try
            If (sClientEmailTo <> "" And sClientEmailSubject <> "" And sClientEmailBody <> "") Or
                (sInternalEmailTo <> "" And sInternalEmailSubject <> "" And sInternalEmailBody <> "") Then
                oEMail = New clsEMail()
                oTemp = GetMySetting("SMTPServer") : If oTemp IsNot Nothing Then oEMail.SMTPServer = oTemp.ToString
                oTemp = GetMySetting("SMTPPort") : If oTemp IsNot Nothing Then oEMail.SMTPPort = CInt(oTemp)
                oTemp = GetMySetting("SMTPUserID") : If oTemp IsNot Nothing Then oEMail.SMTPUserID = oTemp.ToString
                oTemp = GetMySetting("SMTPPassword") : If oTemp IsNot Nothing Then oEMail.SMTPPassword = oTemp.ToString


                If sClientEmailTo <> "" And sClientEmailSubject <> "" And sClientEmailBody <> "" Then

                    sTemp = oSubst.GetSubstitutionForString(sClientEmailTo)
                    If sTemp <> "" Then oEMail.To.Add(New Mail.MailAddress(sTemp))

                    sTemp = oSubst.GetSubstitutionForString(sClientEmailCC)
                    oEMail.CC.Add(New Mail.MailAddress(sTemp))

                    sTemp = oSubst.GetSubstitutionForString(sClientEmailFrom)
                    oEMail.From = New Mail.MailAddress(sTemp)

                    sTemp = LoadIfFile(sClientEmailSubject, oFieldDataUsed, oFieldDataUsed)
                    oEMail.Subject = sTemp

                    sTemp = LoadIfFile(sClientEmailBody, oFieldDataUsed, oFieldDataUsed)
                    oEMail.Subject = sTemp

                    If sClientAttachDoc And oFieldDataUsed.ContainsKey("DestinationDocumentPath") Then
                        sTemp = oFieldDataUsed.Item("DestinationDocumentPath").ToString
                        If IO.File.Exists(sTemp) Then oEMail.Attachments.Add(New Mail.Attachment(sTemp))
                    End If

                    If sStorePath <> "" Then
                        sStorePathEmail = sStorePath & IIf(Right(sStorePath, 1) <> "\", "\", "").ToString & "ClientEmail_" & Now.ToString("yyyyMMdd_HHmmss") & ".txt"
                        oStoreFileInfo = New IO.FileInfo(sStorePathEmail)
                        Try
                            If Not oStoreFileInfo.Directory.Exists Then oStoreFileInfo.Directory.Create()
                            oTxtWriter = oStoreFileInfo.CreateText
                            oTxtWriter.WriteLine(oEMail.ToString)
                            oTxtWriter.Flush() : oTxtWriter.Close() : oTxtWriter = Nothing

                        Catch ex As Exception

                        End Try

                        If sLogFilePathUsed <> "" Then
                            UpdateSourceFileValuesToFields(oStoreFileInfo, oFieldDataUsed)
                            UpdateDestFileValuesToFields(oStoreFileInfo, oFieldDataUsed)
                            oSubst.AddNewValues(oFieldDataUsed)
                            sTemp = oSubst.GetSubstitutionForString(sLogFileEntryTemplate)
                            oTxtWriter = oStoreFileInfo.AppendText
                            oTxtWriter.WriteLine(oEMail.ToString)
                            oTxtWriter.Flush() : oTxtWriter.Close() : oTxtWriter = Nothing

                        End If
                    End If

                    oEMail.SendEmail()

                End If


                If sInternalEmailTo <> "" And sInternalEmailSubject <> "" And sInternalEmailBody <> "" Then

                    sTemp = oSubst.GetSubstitutionForString(sInternalEmailTo)
                    If sTemp <> "" Then oEMail.To.Add(New Mail.MailAddress(sTemp))

                    sTemp = oSubst.GetSubstitutionForString(sInternalEmailCC)
                    oEMail.CC.Add(New Mail.MailAddress(sTemp))

                    sTemp = oSubst.GetSubstitutionForString(sInternalEmailFrom)
                    oEMail.From = New Mail.MailAddress(sTemp)

                    sTemp = LoadIfFile(sInternalEmailSubject, oFieldDataUsed, oFieldDataUsed)
                    oEMail.Subject = sTemp

                    sTemp = LoadIfFile(sInternalEmailBody, oFieldDataUsed, oFieldDataUsed)
                    oEMail.Subject = sTemp

                    If sInternalAttachDoc And oFieldDataUsed.ContainsKey("DestinationDocumentPath") Then
                        sTemp = oFieldDataUsed.Item("DestinationDocumentPath").ToString
                        If IO.File.Exists(sTemp) Then oEMail.Attachments.Add(New Mail.Attachment(sTemp))
                    End If

                    If sStorePath <> "" Then
                        sStorePathEmail = sStorePath & IIf(Right(sStorePath, 1) <> "\", "\", "").ToString & "AdminEmail_" & Now.ToString("yyyyMMdd_HHmmss") & ".txt"
                        oStoreFileInfo = New IO.FileInfo(sStorePathEmail)
                        Try
                            If Not oStoreFileInfo.Directory.Exists Then oStoreFileInfo.Directory.Create()
                            oTxtWriter = oStoreFileInfo.CreateText
                            oTxtWriter.WriteLine(oEMail.ToString)
                            oTxtWriter.Flush() : oTxtWriter.Close() : oTxtWriter = Nothing

                        Catch ex As Exception

                        End Try

                        If sLogFilePathUsed <> "" Then
                            UpdateSourceFileValuesToFields(oStoreFileInfo, oFieldDataUsed)
                            UpdateDestFileValuesToFields(oStoreFileInfo, oFieldDataUsed)
                            oSubst.AddNewValues(oFieldDataUsed)
                            sTemp = oSubst.GetSubstitutionForString(sLogFileEntryTemplate)
                            oTxtWriter = oStoreFileInfo.AppendText
                            oTxtWriter.WriteLine(oEMail.ToString)
                            oTxtWriter.Flush() : oTxtWriter.Close() : oTxtWriter = Nothing

                        End If
                    End If

                    oEMail.SendEmail()

                End If



            End If

            bReturn = True

        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString

        End Try
        Return bReturn
    End Function


    Private Sub UpdateSourceFileValuesToFields(oFileInfo As IO.FileInfo, oFieldDataInSub As SortedList(Of String, String))
        Dim sFieldKey As String

        Try

            sFieldKey = "FileName" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Name Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Name)
            sFieldKey = "File" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Name Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Name)
            sFieldKey = "FileNameNoExt" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = IO.Path.GetFileNameWithoutExtension(oFileInfo.Name) Else oFieldDataInSub.Add(sFieldKey, IO.Path.GetFileNameWithoutExtension(oFileInfo.Name))
            sFieldKey = "FileNoExt" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = IO.Path.GetFileNameWithoutExtension(oFileInfo.Name) Else oFieldDataInSub.Add(sFieldKey, IO.Path.GetFileNameWithoutExtension(oFileInfo.Name))
            sFieldKey = "SourceFileNameExt" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SourceExt" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SourceFileNameExtension" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SourceExtension" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SourceFileNameType" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SourceType" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SrcFileNameExt" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SrcExt" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SrcFileNameExtension" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SrcExtension" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SrcFileNameType" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))
            sFieldKey = "SrcType" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.Extension.Replace(".", "") Else oFieldDataInSub.Add(sFieldKey, oFileInfo.Extension.Replace(".", ""))

        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString

        End Try
    End Sub
    Private Sub UpdateDestFileValuesToFields(oFileInfo As IO.FileInfo, oFieldDataInSub As SortedList(Of String, String))
        Dim sFieldKey As String

        Try

            sFieldKey = "DestinationFileName" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.DirectoryName Else oFieldDataInSub.Add(sFieldKey, oFileInfo.DirectoryName)
            sFieldKey = "DestinationFolder" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.DirectoryName Else oFieldDataInSub.Add(sFieldKey, oFileInfo.DirectoryName)
            sFieldKey = "StoredFileName" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.FullName Else oFieldDataInSub.Add(sFieldKey, oFileInfo.FullName)
            sFieldKey = "StoredFile" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.FullName Else oFieldDataInSub.Add(sFieldKey, oFileInfo.FullName)
            sFieldKey = "StoredFolder" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.DirectoryName Else oFieldDataInSub.Add(sFieldKey, oFileInfo.DirectoryName)
            sFieldKey = "Folder" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.DirectoryName Else oFieldDataInSub.Add(sFieldKey, oFileInfo.DirectoryName)
            sFieldKey = "StoredDirectory" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.DirectoryName Else oFieldDataInSub.Add(sFieldKey, oFileInfo.DirectoryName)
            sFieldKey = "Directory" : If oFieldDataInSub.ContainsKey(sFieldKey) Then oFieldDataInSub(sFieldKey) = oFileInfo.DirectoryName Else oFieldDataInSub.Add(sFieldKey, oFileInfo.DirectoryName)
        Catch ex As Exception
            sErrors &= "ERROR : " & ex.ToString

        End Try
    End Sub


    Friend Function generateHTMLForm(SubmitToURL As String) As String
        Dim sReturn As String = "", sTemp As String
        Dim sKey As String, sValue As String
        Try

            sReturn &= "id='frmPassThrough' name='frmPassThrough' action='" & SubmitToURL & "' method='post'  enctype='application/x-www-form-urlencoded' target='_self'>"

            For Each sKey In oFieldDataUsed.Keys
                sValue = oFieldDataUsed.Item(sKey)
                sTemp = "<input type='hidden' id='" & HttpUtility.UrlEncode(sKey) & "' name='" & HttpUtility.UrlEncode(sKey) & "' value=" & HttpUtility.UrlEncode(sValue) & "'/>"
            Next

            sReturn &= "<noscript >" &
                    "<p>Your browser javascript has not been enabled</p>" &
                    "<input type='submit' value='Click here To proceed' />" &
                "</noscript>" &
                "</form>" &
                "<script type='text/javascript'>" &
                    "if (frmPassThrough.action != '') frmPassThrough.submit();" &
                "</script>"

        Catch ex As Exception

        End Try
        Return sReturn
    End Function



    Private Sub SaveValuesToTempXMLFile()
        Dim oDOM As XmlDocument, oRootElement As XmlElement
        Dim oElement As XmlElement, oValElement As XmlElement, oAttr As XmlAttribute
        Dim sKey As String, sValue As String
        Dim oTemp As Object, sFileKey As String, sFileFolder As String, sFilePath As String

        Try

            oTemp = GetMySetting("TempValueStore")
            If oTemp IsNot Nothing Then
                sFileFolder = oTemp.ToString
            Else
                sFileFolder = IO.Path.GetTempPath
            End If
            sFileFolder &= IIf(sFileFolder.Substring(sFileFolder.Length, 1) <> "\", "\", "").ToString

            oDOM = New XmlDocument
            oRootElement = oDOM.CreateElement("Details")
            oDOM.AppendChild(oRootElement)

            oAttr = oDOM.CreateAttribute("DateCreated")
            oAttr.Value = Now.ToString("o")
            oRootElement.Attributes.Append(oAttr)

            oAttr = oDOM.CreateAttribute("SessionKey")
            oAttr.Value = sSessionID
            oRootElement.Attributes.Append(oAttr)

            oElement = oDOM.CreateElement("Values")
            oRootElement.AppendChild(oElement)

            For Each sKey In oFieldDataUsed.Keys
                sValue = oFieldDataUsed.Item(sKey).ToString
                oValElement = oDOM.CreateElement(sKey)
                oValElement.InnerText = sValue
                oElement.AppendChild(oValElement)
            Next

            sFileKey = sSessionID & "_Values.xml"
            sFilePath = sFileFolder & IIf(sFileFolder.Substring(sFileFolder.Length, 1) <> "\", "\", "").ToString & sFileKey

            oDOM.Save(sFilePath)

        Catch ex As Exception

        End Try

    End Sub


    Public Sub LoadValuesFromXML(oCurrentData As clsCurrentData)
        Dim oValues As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
        Dim sKey As String, sValue As String

        Try
            oValues = LoadValuesFromTempXMLFile(sSessionID)
            For Each sKey In oValues.Keys
                sValue = oValues.Item(sKey)

                If Not oCurrentData.oDataFromPost.aFieldValues.ContainsKey(sKey) Then oCurrentData.oDataFromPost.aFieldValues.Add(sKey, sValue) Else oCurrentData.oDataFromPost.aFieldValues.Item(sKey) = sValue
                If oFieldDataUsed.ContainsKey(sKey) Then oFieldDataUsed.Item(sKey) = sValue Else oFieldDataUsed.Add(sKey, sValue)

            Next

            ProcessPostedDataValues(oCurrentData)

        Catch ex As Exception

        End Try
    End Sub


    Public Function LoadValuesFromTempXMLFile(sSessionID As String) As SortedList(Of String, String)
        Dim oReturn As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
        Dim oDOM As XmlDocument, oRootElement As XmlElement
        Dim oValueSetElement As XmlElement, oValElement As XmlElement, oAttr As XmlAttribute
        Dim sKey As String, sValue As String, oNode As XmlNode
        Dim oTemp As Object, sFileKey As String, sFileFolder As String, sFilePath As String
        Dim aChildren As XmlNodeList, oOneValElement As XmlElement
        Dim oTblRow As TableRow, oTblCell As TableCell, sTemp As String

        Try

            oTemp = GetMySetting("TempValueStore")
            If oTemp IsNot Nothing Then
                sFileFolder = oTemp.ToString
            Else
                sFileFolder = IO.Path.GetTempPath
            End If
            sFileFolder &= IIf(sFileFolder.Substring(sFileFolder.Length - 1, 1) <> "\", "\", "").ToString

            sFileKey = sSessionID & "_Values.xml"
            sFilePath = sFileFolder & IIf(sFileFolder.Substring(sFileFolder.Length - 1, 1) <> "\", "\", "").ToString & sFileKey

            If IO.File.Exists(sFilePath) Then

                oDOM = New XmlDocument()
                oDOM.Load(sFilePath)
                oRootElement = TryCast(oDOM.SelectSingleNode("Details"), XmlElement)
                If oRootElement IsNot Nothing Then

                    oNode = oRootElement.GetAttributeNode("DateCreated")
                    If oNode IsNot Nothing Then
                        sKey = "DateCreated"
                        sValue = oNode.Value
                        If oReturn.ContainsKey(sKey) Then oReturn.Item(sKey) = sValue Else oReturn.Add(sKey, sValue)
                    End If

                    oNode = oRootElement.GetAttributeNode("SessionKey")
                    If oNode IsNot Nothing Then
                        sKey = "SessionKey"
                        sValue = oNode.Value
                        If oReturn.ContainsKey(sKey) Then oReturn.Item(sKey) = sValue Else oReturn.Add(sKey, sValue)
                    End If

                    oValElement = TryCast(oRootElement.SelectSingleNode("Values"), XmlElement)
                    If oValElement IsNot Nothing Then


                        aChildren = oValElement.ChildNodes
                        For Each oNode In aChildren
                            oOneValElement = TryCast(oNode, XmlElement)
                            If oOneValElement IsNot Nothing Then
                                sKey = oOneValElement.Name
                                sValue = oOneValElement.InnerText
                                If oReturn.ContainsKey(sKey) Then oReturn.Item(sKey) = sValue Else oReturn.Add(sKey, sValue)

                                If tblRecoveredStringValues IsNot Nothing Then
                                    oTblRow = New TableRow
                                    oTblCell = New TableCell
                                    oTblCell.Text = HttpUtility.HtmlEncode(sKey)
                                    oTblRow.Cells.Add(oTblCell)

                                    oTblCell = New TableCell
                                    sTemp = sValue
                                    oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
                                    oTblRow.Cells.Add(oTblCell)
                                    tblRecoveredStringValues.Rows.Add(oTblRow)
                                End If
                            End If
                        Next

                    End If
                End If
            End If

        Catch ex As Exception

        End Try

        Return oReturn
    End Function


    Public Function GetValueIfExists(sKeyList As System.Collections.Specialized.StringCollection, Optional bUseSubst As Boolean = False) As String
        Dim sReturn As String = "", sKey As String, aVals As ArrayList
        Try

            aVals = New ArrayList(sKeyList)
            For Each sKey In sKeyList
                If oFieldDataUsed.ContainsKey(sKey) Then
                    sReturn = oFieldDataUsed.Item(sKey)
                    If sReturn <> "" Then Exit For
                End If
            Next

        Catch ex As Exception

        End Try
        Return sReturn
    End Function
End Class
