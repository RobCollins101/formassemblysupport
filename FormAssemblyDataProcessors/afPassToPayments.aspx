﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="afPassToPayments.aspx.vb" Inherits="FormAssemblyDataProcessors.afPassToPayments" Title="TDC Payment Pass Through" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Pass details to payment for reference</title>
</head>
<body>
    <h1>Please wait - transferring to Payment page</h1>

    <form id="frmToPayment" name="frmToPayment" action="<%:sPaymentEngineTarget %>" method="post" enctype="application/x-www-form-urlencoded" target="_self">
        <input type="hidden" id="channel" name="channel" value="<% Response.Write(sPaymentChannel)%>" />
        <input type="hidden" id="sessionID" name="sessionID" value="<% Response.Write(sPaymentSessionID)%>" />

        <input type="hidden" id="amount" name="amount" value="<% Response.Write(sPaymentAmount)%>" />
        <input type="hidden" id="fundcode" name="fundcode" value="<% Response.Write(sPaymentFundPaymentCode)%>" />
        <input type="hidden" id="custref1" name="custref1" value="<% Response.Write(sPaymentCustRef1)%>" />
        <input type="hidden" id="custref2" name="custref2" value="<% Response.Write(sPaymentCustRef2)%>" />
        <input type="hidden" id="custref3" name="custref3" value="<% Response.Write(sPaymentCustRef3)%>" />
        <input type="hidden" id="custref4" name="custref4" value="<% Response.Write(sPaymentCustRef4)%>" />
        <input type="hidden" id="description" name="description" value="<% Response.Write(sPaymentDescription)%>" />

        <% if sPaymentCustRef1_a1 <> "" and sPaymentAmount_a1 <> "" and sPaymentFundPaymentCode_a1 <> "" %>
        <input type="hidden" id="amount_a1" name="amount_a1" value="<% Response.Write(sPaymentAmount_a1)%>" />
        <input type="hidden" id="fundcode_a1" name="fundcode_a1" value="<% Response.Write(sPaymentFundPaymentCode_a1)%>" />
        <input type="hidden" id="custref1_a1" name="custref1_a1" value="<% Response.Write(sPaymentCustRef1_a1)%>" />
        <input type="hidden" id="custref2_a1" name="custref2_a1" value="<% Response.Write(sPaymentCustRef2_a1)%>" />
        <input type="hidden" id="custref3_a1" name="custref3_a1" value="<% Response.Write(sPaymentCustRef3_a1)%>" />
        <input type="hidden" id="custref4_a1" name="custref4_a1" value="<% Response.Write(sPaymentCustRef4_a1)%>" />
        <input type="hidden" id="description_a1" name="description_a1" value="<% Response.Write(sPaymentDescription_a1)%>" />
        <% end if %>

        <% if sPaymentCustRef1_a2 <> "" and sPaymentAmount_a2 <> "" and sPaymentFundPaymentCode_a2 <> "" %>
        <input type="hidden" id="amount_a2" name="amount_a2" value="<% Response.Write(sPaymentAmount_a2)%>" />
        <input type="hidden" id="fundcode_a2" name="fundcode_a2" value="<% Response.Write(sPaymentFundPaymentCode_a2)%>" />
        <input type="hidden" id="custref1_a2" name="custref1_a2" value="<% Response.Write(sPaymentCustRef1_a2)%>" />
        <input type="hidden" id="custref2_a2" name="custref2_a2" value="<% Response.Write(sPaymentCustRef2_a2)%>" />
        <input type="hidden" id="custref3_a2" name="custref3_a2" value="<% Response.Write(sPaymentCustRef3_a2)%>" />
        <input type="hidden" id="custref4_a2" name="custref4_a2" value="<% Response.Write(sPaymentCustRef4_a2)%>" />
        <input type="hidden" id="description_a2" name="description_a2" value="<% Response.Write(sPaymentDescription_a2)%>" />
        <% end if %>

        <% if sPaymentCustRef1_a3 <> "" and sPaymentAmount_a3 <> "" and sPaymentFundPaymentCode_a3 <> "" %>
        <input type="hidden" id="amount_a3" name="amount_a3" value="<% Response.Write(sPaymentAmount_a3)%>" />
        <input type="hidden" id="fundcode_a3" name="fundcode_a3" value="<% Response.Write(sPaymentFundPaymentCode_a3)%>" />
        <input type="hidden" id="custref1_a3" name="custref1_a3" value="<% Response.Write(sPaymentCustRef1_a3)%>" />
        <input type="hidden" id="custref2_a3" name="custref2_a3" value="<% Response.Write(sPaymentCustRef2_a3)%>" />
        <input type="hidden" id="custref3_a3" name="custref3_a3" value="<% Response.Write(sPaymentCustRef3_a3)%>" />
        <input type="hidden" id="custref4_a3" name="custref4_a3" value="<% Response.Write(sPaymentCustRef4_a3)%>" />
        <input type="hidden" id="description_a3" name="description_a3" value="<% Response.Write(sPaymentDescription_a3)%>" />
        <% end if %>

        <% if sPaymentCustRef1_a4 <> "" and sPaymentAmount_a4 <> "" and sPaymentFundPaymentCode_a4 <> "" %>
        <input type="hidden" id="amount_a4" name="amount_a4" value="<% Response.Write(sPaymentAmount_a4)%>" />
        <input type="hidden" id="fundcode_a4" name="fundcode_a4" value="<% Response.Write(sPaymentFundPaymentCode_a4)%>" />
        <input type="hidden" id="custref1_a4" name="custref1_a4" value="<% Response.Write(sPaymentCustRef1_a4)%>" />
        <input type="hidden" id="custref2_a4" name="custref2_a4" value="<% Response.Write(sPaymentCustRef2_a4)%>" />
        <input type="hidden" id="custref3_a4" name="custref3_a4" value="<% Response.Write(sPaymentCustRef3_a4)%>" />
        <input type="hidden" id="custref4_a4" name="custref4_a4" value="<% Response.Write(sPaymentCustRef4_a4)%>" />
        <input type="hidden" id="description_a4" name="description_a4" value="<% Response.Write(sPaymentDescription_a4)%>" />
        <% end if %>

        <% if sPaymentCustRef1_a5 <> "" and sPaymentAmount_a5 <> "" and sPaymentFundPaymentCode_a5 <> "" %>
        <input type="hidden" id="amount_a5" name="amount_a5" value="<% Response.Write(sPaymentAmount_a5)%>" />
        <input type="hidden" id="fundcode_a5" name="fundcode_a5" value="<% Response.Write(sPaymentFundPaymentCode_a5)%>" />
        <input type="hidden" id="custref1_a5" name="custref1_a5" value="<% Response.Write(sPaymentCustRef1_a5)%>" />
        <input type="hidden" id="custref2_a5" name="custref2_a5" value="<% Response.Write(sPaymentCustRef2_a5)%>" />
        <input type="hidden" id="custref3_a5" name="custref3_a5" value="<% Response.Write(sPaymentCustRef3_a5)%>" />
        <input type="hidden" id="custref4_a5" name="custref4_a5" value="<% Response.Write(sPaymentCustRef4_a5)%>" />
        <input type="hidden" id="description_a5" name="description_a5" value="<% Response.Write(sPaymentDescription_a5)%>" />
        <% end if %>

        <% if sPaymentCustRef1_a6 <> "" and sPaymentAmount_a6 <> "" and sPaymentFundPaymentCode_a6 <> "" %>
        <input type="hidden" id="amount_a6" name="amount_a6" value="<% Response.Write(sPaymentAmount_a6)%>" />
        <input type="hidden" id="fundcode_a6" name="fundcode_a6" value="<% Response.Write(sPaymentFundPaymentCode_a6)%>" />
        <input type="hidden" id="custref1_a6" name="custref1_a6" value="<% Response.Write(sPaymentCustRef1_a6)%>" />
        <input type="hidden" id="custref2_a6" name="custref2_a6" value="<% Response.Write(sPaymentCustRef2_a6)%>" />
        <input type="hidden" id="custref3_a6" name="custref3_a6" value="<% Response.Write(sPaymentCustRef3_a6)%>" />
        <input type="hidden" id="custref4_a6" name="custref4_a6" value="<% Response.Write(sPaymentCustRef4_a6)%>" />
        <input type="hidden" id="description_a6" name="description_a6" value="<% Response.Write(sPaymentDescription_a6)%>" />
        <% end if %>

        <% if sPaymentCustRef1_a7 <> "" and sPaymentAmount_a7 <> "" and sPaymentFundPaymentCode_a7 <> "" %>
        <input type="hidden" id="amount_a7" name="amount_a7" value="<% Response.Write(sPaymentAmount_a7)%>" />
        <input type="hidden" id="fundcode_a7" name="fundcode_a7" value="<% Response.Write(sPaymentFundPaymentCode_a7)%>" />
        <input type="hidden" id="custref1_a7" name="custref1_a7" value="<% Response.Write(sPaymentCustRef1_a7)%>" />
        <input type="hidden" id="custref2_a7" name="custref2_a7" value="<% Response.Write(sPaymentCustRef2_a7)%>" />
        <input type="hidden" id="custref3_a7" name="custref3_a7" value="<% Response.Write(sPaymentCustRef3_a7)%>" />
        <input type="hidden" id="custref4_a7" name="custref4_a7" value="<% Response.Write(sPaymentCustRef4_a7)%>" />
        <input type="hidden" id="description_a7" name="description_a7" value="<% Response.Write(sPaymentDescription_a7)%>" />
        <% end if %>

        <% if sPaymentCustRef1_a8 <> "" and sPaymentAmount_a8 <> "" and sPaymentFundPaymentCode_a8 <> "" %>
        <input type="hidden" id="amount_a8" name="amount_a8" value="<% Response.Write(sPaymentAmount_a8)%>" />
        <input type="hidden" id="fundcode_a8" name="fundcode_a8" value="<% Response.Write(sPaymentFundPaymentCode_a8)%>" />
        <input type="hidden" id="custref1_a8" name="custref1_a8" value="<% Response.Write(sPaymentCustRef1_a8)%>" />
        <input type="hidden" id="custref2_a8" name="custref2_a8" value="<% Response.Write(sPaymentCustRef2_a8)%>" />
        <input type="hidden" id="custref3_a8" name="custref3_a8" value="<% Response.Write(sPaymentCustRef3_a8)%>" />
        <input type="hidden" id="custref4_a8" name="custref4_a8" value="<% Response.Write(sPaymentCustRef4_a8)%>" />
        <input type="hidden" id="description_a8" name="description_a8" value="<% Response.Write(sPaymentDescription_a8)%>" />
        <% end if %>

        <% if sPaymentCustRef1_a9 <> "" and sPaymentAmount_a9 <> "" and sPaymentFundPaymentCode_a9 <> "" %>
        <input type="hidden" id="amount_a9" name="amount_a9" value="<% Response.Write(sPaymentAmount_a9)%>" />
        <input type="hidden" id="fundcode_a9" name="fundcode_a9" value="<% Response.Write(sPaymentFundPaymentCode_a9)%>" />
        <input type="hidden" id="custref1_a9" name="custref1_a9" value="<% Response.Write(sPaymentCustRef1_a9)%>" />
        <input type="hidden" id="custref2_a9" name="custref2_a9" value="<% Response.Write(sPaymentCustRef2_a9)%>" />
        <input type="hidden" id="custref3_a9" name="custref3_a9" value="<% Response.Write(sPaymentCustRef3_a9)%>" />
        <input type="hidden" id="custref4_a9" name="custref4_a9" value="<% Response.Write(sPaymentCustRef4_a9)%>" />
        <input type="hidden" id="description_a9" name="description_a9" value="<% Response.Write(sPaymentDescription_a9)%>" />
        <% end if %>


        <input type="hidden" id="ctitle" name="ctitle" value="<% Response.Write(sPaymentTitle)%>" />
        <input type="hidden" id="cfname" name="cfname" value="<% Response.Write(sPaymentCfName)%>" />
        <input type="hidden" id="csname" name="csname" value="<% Response.Write(sPaymentCsName)%>" />
        <input type="hidden" id="chouse" name="chouse" value="<% Response.Write(sPaymentHouse)%>" />
        <input type="hidden" id="cadd1" name="cadd1" value="<% Response.Write(sPaymentAddr1)%>" />
        <input type="hidden" id="ctown" name="ctown" value="<% Response.Write(sPaymentAddrTown)%>" />
        <input type="hidden" id="cpostcode" name="cpostcode" value="<% Response.Write(sPaymentPostcode)%>" />
        <input type="hidden" id="ccounty" name="ccounty" value="<% Response.Write(sPaymentCounty)%>" />
        <input type="hidden" id="ctel" name="ctel" value="<% Response.Write(sPaymentTelNo)%>" />
        <input type="hidden" id="cemail" name="cemail" value="<% Response.Write(sPaymentEMail)%>" />
        <input type="hidden" id="sendmail" name="sendmail" value="<% Response.Write(sPaymentSendMail)%>" />
        <input type="hidden" id="displayReceipt" name="displayReceipt" value="<% Response.Write(sPaymentDisplayReceipt)%>" />
        <input type="hidden" id="returnmethod" name="returnmethod" value="<% Response.Write(sReturnMethod)%>" />
        <input type="hidden" id="returnurl" name="returnurl" value="<% Response.Write(sReturnURL)%>" />
        <input type="hidden" id="postbackurl" name="postbackurl" value="<% Response.Write(sPostbackURL)%>" />
        <input type="hidden" id="Pay" name="Pay" value="<% Response.Write(sPaymentSessionID)%>" />
        <br />
        <br />
        <noscript>
            <p>Your browser javascript is not available</p>
        </noscript>

        <% If bShowValues Then %>
            <div>
                <div>
                    Query string values (in URL)<br />
                    sent using protocol GET
		            <asp:Table ID="tblQueryStringValues" runat="server">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </div>

                <br />
                Form values (From form encoded) sent<br />
                using protocol POST
		        <asp:Table ID="tblPostStringValues" runat="server">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
            </div>
            <input type="submit" value="Submit" />
        <% Else %>
            <noscript>
                <input type="submit" value="Click here to proceed to payment" />
            </noscript>
        <% End if %>

    </form>
    <% If not bShowValues Then %>
        <script type="text/javascript">
            if (frmToPayment.action != "" && frmToPayment.Pay.value != "") frmToPayment.submit();
        </script>
    <% End if %>
</body>
</html>
