﻿Imports System.Net

Public Class afSupportFileUpload
    Inherits System.Web.UI.Page

    Dim moCurrentData As New clsCurrentData
    Public bShowValues As Boolean = False
    Public sReturnURL As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            ProcessPostedData()

        End If

    End Sub

    Protected Sub ProcessPostedData()

        Dim oFormSupportParts As New clsFormSupportParts

        Dim sSourceHTMLPath As String = "", sDestGeneratedDocFullPath As String = ""
        Dim sDestLogPath As String = "", sLogFileEntry As String = ""
        Dim sKey As String, sGeneratedDocFilePath As String = ""

        Try

            ' Process the field values and the passed files


            oFormSupportParts.bShowValues = bShowValues
            oFormSupportParts.oPage = Page
            oFormSupportParts.tblQueryStringValues = tblQueryStringValues
            oFormSupportParts.tblPostStringValues = tblPostStringValues

            oFormSupportParts.sBaseDestLogFilePathTemplate = GetMySetting("DefaultLogFilePath").ToString
            oFormSupportParts.sBaseLogFileEntryTemplate = GetMySetting("DefaultLogFileEntry").ToString
            oFormSupportParts.sBaseDestFilePathTemplate = GetMySetting("DefaultStoreFolderDest").ToString
            oFormSupportParts.sBaseDestDirectoryTemplate = GetMySetting("DefaultStoreFolderDest").ToString

            ' Get all field value and submitted files into array for processing
            ' Submitted files are stored

            oFormSupportParts.ProcessPostedData(Request, moCurrentData)
            bShowValues = oFormSupportParts.bShowValues


            ' Prepare field values for the generated document

            sKey = "HTMLDocumentFilePath" : If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            sKey = "SourceDocumentFilePath" : If sSourceHTMLPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            sKey = "HTMLFilePath" : If sSourceHTMLPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString

            'sKey = "LogFilePath" : If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestLogPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogFile" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestLogPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogPath" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestLogPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString

            'sKey = "LogLine" : If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntry = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogFileEntry" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntry = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogLine" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntry = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            'sKey = "LogData" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntry = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            'sKey = "LineData" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sLogFileEntry = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString

            sDestLogPath = oFormSupportParts.sDestLogFilePathTemplate
            sLogFileEntry = oFormSupportParts.sLogFileEntryTemplate

            sKey = "DocDestinationPath" : If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            sKey = "DocumentPath" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            sKey = "HTMLDocumentPath" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            sKey = "HTMLDocumentDest" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            sKey = "HTMLDocumentDestination" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            sKey = "HTMLDocumentFilePath" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
            sKey = "HTMLDocumentStorage" : If sDestLogPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString


            If sSourceHTMLPath = "" Then sSourceHTMLPath = GetMySetting("DefaultGeneratedHTMLDocument").ToString
            If sDestLogPath = "" Then sDestLogPath = GetMySetting("DefaultLogFilePath").ToString
            If sLogFileEntry = "" Then sLogFileEntry = GetMySetting("DefaultLogFileEntry").ToString
            If sDestGeneratedDocFullPath = "" Then sDestGeneratedDocFullPath = GetMySetting("DefaultGeneratedDocumentName").ToString

            ' Generate the actual document of the submission

            If bShowValues Then lblProcessing.Text &= "Document source used<br/>" & sSourceHTMLPath & "<br/>" & "<br/>"
            If bShowValues Then lblProcessing.Text &= "Document storage template to : " & sDestGeneratedDocFullPath & "<br/>"

            sGeneratedDocFilePath = oFormSupportParts.GenerateDocument(sSourceHTMLPath, sDestGeneratedDocFullPath, sDestLogPath, sLogFileEntry)
            If bShowValues Then lblProcessing.Text &= "Document stored at : " & sGeneratedDocFilePath & "<br/>"

            ' Note emails won't be fired unless the right parts are present and valid

            oFormSupportParts.SendEmailsUsingFieldValues()

            bShowValues = oFormSupportParts.bShowValues
            sReturnURL = oFormSupportParts.sReturnURLToCaller

            If bShowValues Then lblProcessing.Text &= oFormSupportParts.sProcessing
            If bShowValues Then lblFileList.Text &= oFormSupportParts.sFileList
            If bShowValues Then lblFilesDetected.Text &= oFormSupportParts.sFilesDetected
            If bShowValues Then lblErrors.Text &= oFormSupportParts.sErrors

        Catch ex As Exception
            lblErrors.Text &= "ERROR : " & ex.ToString
        End Try


        If sReturnURL <> "" Then
            sReturnURL = oFormSupportParts.oSubst.GetSubstitutionForString(sReturnURL, clsValueSubstitutions.TargetModifier.ForURI)

            Response.Redirect(sReturnURL, True)
        End If


    End Sub








End Class
