<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="afPaymentResponse2.aspx.vb" Inherits="FormAssemblyDataProcessors.afPaymentResponse2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>tfa Payment Response</title>
    <meta name="description" content="Tandridge District Council process payment response" />
    <meta name="distribution" content="UI" />
    <meta name="revisit-after" content="5 days" />
    <meta name="distribution" content="GLOBAL" />
    <meta name="author" content="Rob Collins ICT" />
    <meta name="copyright" content="Tandridge District Council 2015" />
    <meta name="googlebot" content="noodp" />
    <meta name="language" content="english" />
    <meta name="reply-to" content="CustomerService@Tandridge.gov.uk" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1 " />
 	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="ROBOTS" content="index, nofollow nofollow" />
    <meta name="rating" content="General" />



    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">-->
    <script src="//tdcws01.tandridge.gov.uk/PublicFiles/JQueryVersions/Versions1.x/jquery-1.12.4.js">
    </script>

    <script language="javascript" type="text/javascript">
        var g_sFormName = "SomeFormName v1";
        var g_thisDate = new Date();
        var sDate = g_thisDate.toDateString;
        var g_sTypeService = "";
        if (/(dev|localhost)/i.test(location.pathname)) g_sTypeService = "Test"; //"Dev";
        if (g_sTypeService == "" && /test/i.test(location.pathname)) g_sTypeService = "Test";
        //if (g_sTypeService == "" ) g_sTypeService = "Live";

    </script>

    <script language="javascript" type="text/javascript">
        //<![CDATA[
        var sURLBrandingLoc = "//tdcws01.tandridge.gov.uk/TDCWebAppsPublic/BrandingSplit2015" + g_sTypeService + "/";
        var sAlternativeRoot = sURLBrandingLoc + "StaticBranding/";
        var xmlHttp;
        var xmlCORSHttp; // See http://www.html5rocks.com/en/tutorials/cors/#toc-adding-cors-support-to-the-server


        function fnExtractFromURL(sURLLocation) {
            var theURL = "";
            var sHTTPResponse = "";
            var sProtocol;
            var sReturn;

            sProtocol = document.location.protocol;
            theURL = sURLLocation;
            if (sProtocol != "http:" && sProtocol != "https:") {
                try { sProtocol = (("http:" == document.location.protocol) ? "http:" : "https:"); } catch (e) { sProtocol = "https:"; }
                theURL = sProtocol + sURLLocation;
            }

            /* running locally on IE5.5, IE6, IE7 */
            if (location.protocol == "file:") {
                if (typeof xmlHttp === "undefined") try { xmlHttp = new ActiveXObject("MSXML2.XMLHTTP"); } catch (e) { }
                if (typeof xmlHttp === "undefined") try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }
            };
            if (typeof xmlHttp === "undefined" && !(typeof XMLHttpRequest === "undefined")) try { xmlHttp = new XMLHttpRequest(); } catch (e) { }
            /* IE7, Firefox, Safari, Opera...  */
            if (typeof xmlHttp === "undefined" && !(typeof XMLHttpRequest2 === "undefined")) try { xmlHttp = new XMLHttpRequest2(); } catch (e) { }
            /* IE6 */
            if (typeof xmlHttp === "undefined" && !(typeof ActiveXObject === "undefined")) try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }

            try {
                //if (!(typeof XDomainRequest === "undefined")) try { xmlCORSHttp = new XDomainRequest(); } catch (e) { }
                try {
                    if (!(typeof XDomainRequest === "undefined")) {
                        xmlCORSHttp = new XDomainRequest();
                        try {
                            if (!(typeof xmlCORSHttp === "undefined")) {
                                xmlCORSHttp.open("GET", theURL);
                                xmlCORSHttp.send(null);
                                if (xmlCORSHttp.status == 200) sReturn = xmlCORSHttp.responseText;
                            }
                        } catch (e) { };
                    }
                } catch (e) { };

                if (typeof sReturn === "undefined") {
                    sReturn = "";
                    if (!(typeof xmlHttp === "undefined")) {

                        xmlHttp.open("GET", theURL, false);
                        xmlHttp.send(null);
                        if (xmlHttp.status == 200) sReturn = xmlHttp.responseText;

                    }
                }
            } catch (err) {
                txt = "There was an error " + (err.number & 0xFFFF) + " " + err.name + " on this page.\n\n";
                txt += "Error message: " + err.message + "\n\n";
                if (err.description != undefined) { txt += "Error description: " + err.description + "\n\n"; }
                sReturn = "<!--" + txt + "-->";
            } finally {
                if (sReturn === "undefined") sReturn = '';
				if (sReturn.search(/Tandridge District Council Error response/i) > 0) sReturn = '';
                if (document.location.protocol == "http:") {
                    sReturn = sReturn.replace(/https:/ig, "http:");
                } else if (document.location.protocol == "https:") {
                    sReturn = sReturn.replace(/http:/ig, "https:");
                } else {
                    sReturn = sReturn.replace(/"\/\//ig, '"http://').replace(/'\/\//ig, "'http://").replace(/="www./ig, '="http://www.').replace(/='www./ig, "='http://www.")
                };
            }
            return sReturn;
        }


        //]]>
    </script>

    <script language="javascript" type="text/javascript">
        //<![CDATA[
        document.write(fnExtractFromURL(sURLBrandingLoc + "Branding1HeadTagPartSansScripts.aspx"));
        //]]>
    </script>


    <link rel="stylesheet" type="text/css" href="./TDCCSS/TDCCSSStyle.css" media="all" />


    <style type="text/css">
        .style2 { font-family: Arial, Helvetica, sans-serif; font-size: medium; font-weight: bold; color: #800000; margin: 0; }
    </style>

</head>
<body>
    <script type="text/javascript">
        //<![CDATA[
        document.write(fnExtractFromURL(sURLBrandingLoc + "Branding2BodyTagStartSansScripts.aspx", sAlternativeRoot + "Branding2BodyTagStartSansScripts.htm"));
        //]]>
    </script>
    <script type="text/javascript">
        //<![CDATA[
        document.write(fnExtractFromURL(sURLBrandingLoc + "Branding3_AllBodyPreMenuSansScripts.aspx", sAlternativeRoot + "Branding3_AllBodyPreMenuSansScripts.htm"));
        //]]>
    </script>
    <script type="text/javascript">
        //<![CDATA[
        document.write(fnExtractFromURL(sURLBrandingLoc + "Branding4BodyLHMenuPart.aspx", sAlternativeRoot + "Branding4BodyLHMenuPart.htm"));
        //]]>
    </script>

    <!-- ==========================================================================-->
    <!-- Here is where a designer would place the body of their content -->
    <!-- ==========================================================================-->


    <form id="frmResponse" runat="server" action="<%=sURLToGoTo %>">

        <!-- ==========================================================================-->
        <!-- Here is where a designer would place the body of their content -->
        <!-- ==========================================================================-->

        <div id="TDCInfo">
            <div style="background-color: #FFFFCC; border: medium double #CCFFFF" align="center">

                <asp:Label ID="lblPostValues" runat="server"></asp:Label>
                
                <asp:Panel ID="pnlPaymentSuccessful" runat="server">
                    <h2>Thank you for your submission and payment</h2>
                    <div><%=sSuccessfulText %></div>
                    <p>Your submission will be processed</p>
                </asp:Panel>
                <asp:Panel ID="pnlPaymentFailed" runat="server">
                    <h2>Thank you for your submission</h2>
                    <h3>unfortunately your payment was unsuccesful</h3>
                    <div><%=sUnsuccessfulText %></div>
                    <%--<asp:LinkButton ID="cmdReturnToSubmission" runat="server">Click here to return to your details</asp:LinkButton>--%>
                    <%--<a target="_self" href="<%=sURLToGoTo %>">Click here to return to your details</a>--%>
                </asp:Panel>

                <%--                
                              <h2 style="font-family: Arial, Helvetica, sans-serif; font-style: oblique; font-weight: bold; text-transform: capitalize; color: #000066">Under Construction
                </h2>
                <p>
                    The site you have reached is under construction and is not available.
                </p>
                <p class="style2">
                    What you see today
                </p>
                <p class="style2">
                    is the barest, flickering
                </p>
                <p class="style2">
                    glimpse of tomorrow
                </p>
                <p>
                    &nbsp;
                </p>--%>
            </div>
        </div>

        <%If bShowValues %>
        <p>
            Files detected<br />
            <asp:Label ID="lblFilesDetected" runat="server" Text=""></asp:Label>
        </p>
        <p>
            Files saved<br />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </p>
        <p>
            Processing<br />
            <asp:Label ID="lblProcessing" runat="server" Text=""></asp:Label>
        </p>
        <div>
            Query string (in URL)<br />
            sent using protocol GET
		        <asp:Table ID="tblQueryStringValues" runat="server">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
        </div>
        <div>
            <br />
            Form values (From form encoded)<br />sent using protocol POST
		    <asp:Table ID="tblPostStringValues" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                    <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
            <asp:Table ID="tblFilesPosted" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell>File Name</asp:TableHeaderCell>
                    <asp:TableHeaderCell>Size</asp:TableHeaderCell>
                    <asp:TableHeaderCell>Type</asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </div>
        <div>
            <div>
                <br />
                Recovered values from XML document
		    <asp:Table ID="tblRecoveredStringValues" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                    <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>

        </div>

        <% End If %>
        <asp:Label ID="lblFileList" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblErrors" runat="server" Text=""></asp:Label>

        <asp:Panel ID="pnlHTMLFormOut" runat="server">
            <asp:Label ID="lblHTMLFormOut" runat="server" />
        </asp:Panel>
    </form>

    <!-- ==========================================================================-->
    <!-- End of body content -->
    <!-- ==========================================================================-->


    <div id="AfterBranding5"></div>
    <script type="text/javascript">
        //<![CDATA[
        fnExtractFromURL(sURLBrandingLoc + "Branding5FootPartSansScripts.aspx", "AfterBranding5", "");
        //]]>
    </script>

    <script>
        //window.location = "./afPaymentResponse.aspx";
    </script>
    </div>

</body>
</html>
