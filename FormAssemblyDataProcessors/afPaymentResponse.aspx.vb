﻿Public Class afPaymentResponse
    Inherits System.Web.UI.Page

    Public sSuccessfulText As String = "", sUnsuccessfulText As String = ""

    Dim moCurrentData As New clsCurrentData
    Public bShowValues As Boolean = False
    Public bPaymentSuccessful As Boolean = False
    Public sReturnURLToCaller As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                ProcessPostedData()

            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ProcessPostedData()

        Dim oFormSupportParts As New clsFormSupportParts

        Dim sSourceHTMLPath As String = "", sDestGeneratedDocFullPath As String = ""
        Dim sDestLogPath As String = "", sLogFileEntry As String = ""
        Dim sKey As String, sGeneratedDocFilePath As String = ""
        Dim sTempURL As String = "", aTemp As ArrayList, oTemp As Object
        Dim sTempSessionID As String = ""

        Try

            ' Process the field values and the passed files

            oTemp = GetMySetting("DebugShowFieldVals")
            If oTemp IsNot Nothing Then bShowValues = StringIsTrue(oTemp.ToString)


            oFormSupportParts.bShowValues = bShowValues
            oFormSupportParts.oPage = Page
            oFormSupportParts.tblQueryStringValues = tblQueryStringValues
            oFormSupportParts.tblPostStringValues = tblPostStringValues
            oFormSupportParts.tblRecoveredStringValues = tblRecoveredStringValues

            oFormSupportParts.sBaseDestLogFilePathTemplate = GetMySetting("DefaultLogFilePath").ToString
            oFormSupportParts.sBaseLogFileEntryTemplate = GetMySetting("DefaultLogFileEntry").ToString
            oFormSupportParts.sBaseDestFilePathTemplate = GetMySetting("DefaultStoreFolderDest").ToString
            oFormSupportParts.sBaseDestDirectoryTemplate = GetMySetting("DefaultStoreFolderDest").ToString

            ' Get all recovered field value and submitted files into array for processing
            ' Submitted files are stored

            oFormSupportParts.ProcessPostedData(Request, moCurrentData)

            oFormSupportParts.LoadValuesFromXML(moCurrentData)

            bShowValues = oFormSupportParts.bShowValues

            pnlPaymentFailed.Visible = False
            bPaymentSuccessful = Not oFormSupportParts.oFieldDataUsed.ContainsKey("ErrorCode")

            If Not bPaymentSuccessful Then

                pnlPaymentFailed.Visible = True
                pnlPaymentSuccessful.Visible = False

            Else


                ' Prepare field values for the generated document

                sKey = "HTMLDocumentFilePath" : If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
                sKey = "SourceDocumentFilePath" : If sSourceHTMLPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
                sKey = "HTMLFilePath" : If sSourceHTMLPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString

                sDestLogPath = oFormSupportParts.sDestLogFilePathTemplate
                sLogFileEntry = oFormSupportParts.sLogFileEntryTemplate

                sKey = "DocDestinationPath" : If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
                sKey = "DocumentPath" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
                sKey = "HTMLDocumentPath" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
                sKey = "HTMLDocumentDest" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
                sKey = "HTMLDocumentDestination" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
                sKey = "HTMLDocumentFilePath" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
                sKey = "HTMLDocumentStorage" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString


                If sSourceHTMLPath = "" Then sSourceHTMLPath = GetMySetting("DefaultGeneratedHTMLDocument").ToString
                If sDestLogPath = "" Then sDestLogPath = GetMySetting("DefaultLogFilePath").ToString
                If sLogFileEntry = "" Then sLogFileEntry = GetMySetting("DefaultLogFileEntry").ToString
                If sDestGeneratedDocFullPath = "" Then sDestGeneratedDocFullPath = GetMySetting("DefaultGeneratedDocumentName").ToString

                ' Generate the actual document of the submission

                If bShowValues Then lblProcessing.Text &= "Document source used<br/>" & sSourceHTMLPath & "<br/>" & "<br/>"
                If bShowValues Then lblProcessing.Text &= "Document storage template to : " & sDestGeneratedDocFullPath & "<br/>"

                sGeneratedDocFilePath = oFormSupportParts.GenerateDocument(sSourceHTMLPath, sDestGeneratedDocFullPath, sDestLogPath, sLogFileEntry)
                If bShowValues Then lblProcessing.Text &= "Document stored at : " & sGeneratedDocFilePath & "<br/>"

            End If

            ' Note emails won't be fired unless the right parts are present and valid

            If bPaymentSuccessful Then oFormSupportParts.SendEmailsUsingFieldValues()

            bShowValues = oFormSupportParts.bShowValues
            sReturnURLToCaller = oFormSupportParts.sReturnURLToCaller

            If bShowValues Then lblProcessing.Text &= oFormSupportParts.sProcessing
            If bShowValues Then lblFileList.Text &= oFormSupportParts.sFileList
            If bShowValues Then lblFilesDetected.Text &= oFormSupportParts.sFilesDetected
            If bShowValues Then lblErrors.Text &= oFormSupportParts.sErrors

            sTempURL = ""
            aTemp = TryCast(GetMySetting("PassToURLFieldNames"), ArrayList)

            If aTemp IsNot Nothing Then

                For Each sKey In aTemp
                    If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then
                        sTempURL = oFormSupportParts.oFieldDataUsed.Item(sKey)
                        Exit For
                    End If
                Next
            End If

            sTempSessionID = ""
            aTemp = TryCast(GetMySetting("PassToURLSessionIDFieldNames"), ArrayList)

            If aTemp IsNot Nothing Then

                For Each sKey In aTemp
                    If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then
                        sTempSessionID = oFormSupportParts.oFieldDataUsed.Item(sKey)
                        Exit For
                    End If
                Next
            End If

            If bPaymentSuccessful Then

                If sTempURL <> "" And sTempSessionID <> "" Then

                    Session.Add("Fields_" & sTempSessionID, oFormSupportParts.oFieldDataUsed)
                    lblHTMLFormOut.Text = oFormSupportParts.generateHTMLForm(sTempURL)

                Else

                    oFormSupportParts.SendEmailsUsingFieldValues()

                End If

            End If

            If bShowValues Then lblProcessing.Text &= oFormSupportParts.sProcessing
            If bShowValues Then lblFileList.Text &= oFormSupportParts.sFileList
            If bShowValues Then lblFilesDetected.Text &= oFormSupportParts.sFilesDetected
            If bShowValues Then lblErrors.Text &= oFormSupportParts.sErrors


        Catch ex As Exception

        End Try
    End Sub

End Class