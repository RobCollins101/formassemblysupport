<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="afSupportSubmission.aspx.vb" Inherits="FormAssemblyActions.afSupportSubmission" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Process forms submission</title>

    <meta name="description" content="Tandridge District Council" />
    <meta name="distribution" content="UI" />
    <meta name="author" content="Rob Collins ICT" />
    <meta name="copyright" content="Tandridge District Council 2017" />
    <meta name="googlebot" content="noindex, nofollow" />
    <meta name="language" content="english" />
    <meta name="reply-to" content="CustomerServices@Tandridge.gov.uk" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1 " />
 	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="ROBOTS" content="noindex, nofollow" />
    <meta name="rating" content="General" />


    <!-- ==========================================================================
           HTML HEAD component to manage TDC functions import
      ========================================================================== -->

    <script type="text/javascript">
        //<![CDATA[
        if (typeof loadJsLibrary === 'undefined') {
            var script = document.createElement('script');
            script.src = '//tdcws01.tandridge.gov.uk/PublicFiles/TDCJsModules/TDCBrandingPlaced/TDCLoadJsLibraryFunction.js'; // Location of the LoadJsLibrary script
            document.write(script.outerHTML);
        }
        //]]>
    </script>

    <!-- ==========================================================================
            Javascript components for JQuery
     ========================================================================== -->
    <!--<script type="text/javascript">if (typeof jQuery === 'undefined') loadJsLibrary('//tdcws01.tandridge.gov.uk/PublicFiles/JQueryVersions/Versions1.x/jquery-1.12.4.js', null);</script>-->
    <script type="text/javascript">if (typeof jQuery === 'undefined') loadJsLibrary('//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js', null);</script>

    <!-- ==========================================================================
            Javascript components for JQuery UI. See http://jqueryui.com/
     ========================================================================== -->
    <!--<link rel="stylesheet" href="//tdcws01.tandridge.gov.uk/PublicFiles/JQueryUIVersions/jquery-ui-themes-1.12.0/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript">if (typeof jQuery.ui === 'undefined') loadJsLibrary('//tdcws01.tandridge.gov.uk/PublicFiles/JQueryUIVersions/jquery-ui-1.12.0/jquery-ui.min.js', null);</script>-->
    <!-- ==========================================================================
            Javascript components for TDC postcode search
            This assumes JQuery has been loaded up
     ========================================================================== -->

    <script src="//tdcws01.tandridge.gov.uk/PublicFiles/TDCJsModules/TDCBrandingPlaced/TDCBrandingImport_v3_0_1_20171004.js" type="text/javascript"></script>

    <!-- ==========================================================================
              End of HTML Import component to manage JQuery, JQuery.UI and TDC functions import
       ========================================================================== -->

      <script language="javascript" type="text/javascript">
          //<![CDATA[
          fnExtractFromURL(sURLBrandingLoc + "Branding1HeadTagPart.aspx", "", "", "", false);
        //]]>
    </script>


    <link rel="stylesheet" type="text/css" href="./TDCCSS/TDCCSSStyle.css" media="all" />


    <style type="text/css">
        .style2 { font-family: Arial, Helvetica, sans-serif; font-size: medium; font-weight: bold; color: #800000; margin: 0; }
    </style>

</head>
<body>
    <script type="text/javascript">
        //<![CDATA[
        //fnExtractFromURL(sURLBrandingLoc + "Branding1HeadTagPart.aspx", "", "", "", false);
        //]]>
    </script>
    <script type="text/javascript">
          //<![CDATA[
          fnExtractFromURL(sURLBrandingLoc + "Branding3_AllBodyPreMenu.aspx", sAlternativeRoot + "Branding3_AllBodyPreMenu.htm", "", "", true);
        //]]>
    </script>
    <script type="text/javascript">
        //<![CDATA[
        fnExtractFromURL(sURLBrandingLoc + "Branding4BodyLHMenuPart.aspx", sAlternativeRoot + "Branding4BodyLHMenuPart.htm", "", "", true);
        //]]>
    </script>

    <form id="frmReturn"  name="frmReturn" action="<%:sReturnURLToCaller %>" method="post" enctype="application/x-www-form-urlencoded" target="_self">
        <div>

            <% If sMessageToUser <> ""Then %>
                <%=sMessageToUser %>
            <% else %>
                <h2>Form data received</h2>
                <p>Thank you for your submission</p>
                <p>The submitted data is being processed.</p>
            <% End if %>

            <%If bShowValues %>
                <p>
                    Files detected<br />
                    <asp:Label ID="lblFilesDetected" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    Files saved<br />
                    <asp:Label ID="lblFilesSaved" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    Processing<br />
                    <asp:Label ID="lblProcessing" runat="server" Text=""></asp:Label>
                </p>
                <div>
                    Query string (in URL)<br />
                    sent using protocol GET
		            <asp:Table ID="tblQueryStringValues" runat="server">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </div>
                <div>
                    <br />
                    Form values (From form encoded) sent using protocol POST
		        <asp:Table ID="tblPostStringValues" runat="server">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
                    <asp:Table ID="tblFilesPosted" runat="server">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell>File Name</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Size</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Type</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </div>

                <div>
                    <br />
                    Form values of all details
		        <asp:Table ID="tblRecoveredStringValues" runat="server">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>

                <asp:Label ID="lblFileList" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblErrors" runat="server" Text=""></asp:Label>
            <% End If %>

            <asp:Panel ID="pnlHTMLFormOut" runat="server">
                <asp:Label ID="lblHTMLFormOut" runat="server" />
            </asp:Panel>
        </div>
    </form>
    <div id="AfterBranding5"></div>
    <script type="text/javascript">
        //<![CDATA[
        fnExtractFromURL(sURLBrandingLoc + "Branding5FootPart.aspx", sAlternativeRoot + "Branding5FootPart.htm", "", "", true);
        //]]>
    </script>

    <script>
        //window.location = "./afPaymentResponse.aspx";
    </script>
    </div>
</body>
</html>
