<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TestPaymentToAction.aspx.vb" Inherits="FormAssemblyActions.TestPaymentToAction" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>tfa Payment Response</title>
	<meta name="description" content="Tandridge District Council process payment response" />
	<meta name="distribution" content="UI" />
	<meta name="revisit-after" content="5 days" />
	<meta name="distribution" content="GLOBAL" />
	<meta name="author" content="Rob Collins ICT" />
	<meta name="copyright" content="Tandridge District Council 2015" />
	<meta name="googlebot" content="noodp" />
	<meta name="language" content="english" />
	<meta name="reply-to" content="CustomerService@Tandridge.gov.uk" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1 " />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta name="ROBOTS" content="index, nofollow nofollow" />
	<meta name="rating" content="General" />



	<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">-->
	<script src="//tdcws01.tandridge.gov.uk/PublicFiles/JQueryVersions/Versions1.x/jquery-1.12.4.js">
	</script>

	<script language="javascript" type="text/javascript">
		var g_sFormName = "SomeFormName v1";
		var g_thisDate = new Date();
		var sDate = g_thisDate.toDateString;
		var g_sTypeService = "";
		if (/(dev|localhost)/i.test(location.pathname)) g_sTypeService = "Test"; //"Dev";
		if (g_sTypeService == "" && /test/i.test(location.pathname)) g_sTypeService = "Test";
        //if (g_sTypeService == "" ) g_sTypeService = "Live";

	</script>

	<script language="javascript" type="text/javascript">
		//<![CDATA[
		var sURLBrandingLoc = "//tdcws01.tandridge.gov.uk/TDCWebAppsPublic/BrandingSplit2015" + g_sTypeService + "/";
		var sAlternativeRoot = sURLBrandingLoc + "StaticBranding/";
		var xmlHttp;
		var xmlCORSHttp; // See http://www.html5rocks.com/en/tutorials/cors/#toc-adding-cors-support-to-the-server


		function fnExtractFromURL(sURLLocation) {
			var theURL = "";
			var sHTTPResponse = "";
			var sProtocol;
			var sReturn;

			sProtocol = document.location.protocol;
			theURL = sURLLocation;
			if (sProtocol != "http:" && sProtocol != "https:") {
				try { sProtocol = (("http:" == document.location.protocol) ? "http:" : "https:"); } catch (e) { sProtocol = "https:"; }
				theURL = sProtocol + sURLLocation;
			}

			/* running locally on IE5.5, IE6, IE7 */
			if (location.protocol == "file:") {
				if (typeof xmlHttp === "undefined") try { xmlHttp = new ActiveXObject("MSXML2.XMLHTTP"); } catch (e) { }
				if (typeof xmlHttp === "undefined") try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }
			};
			if (typeof xmlHttp === "undefined" && !(typeof XMLHttpRequest === "undefined")) try { xmlHttp = new XMLHttpRequest(); } catch (e) { }
			/* IE7, Firefox, Safari, Opera...  */
			if (typeof xmlHttp === "undefined" && !(typeof XMLHttpRequest2 === "undefined")) try { xmlHttp = new XMLHttpRequest2(); } catch (e) { }
			/* IE6 */
			if (typeof xmlHttp === "undefined" && !(typeof ActiveXObject === "undefined")) try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }

			try {
				//if (!(typeof XDomainRequest === "undefined")) try { xmlCORSHttp = new XDomainRequest(); } catch (e) { }
				try {
					if (!(typeof XDomainRequest === "undefined")) {
						xmlCORSHttp = new XDomainRequest();
						try {
							if (!(typeof xmlCORSHttp === "undefined")) {
								xmlCORSHttp.open("GET", theURL);
								xmlCORSHttp.send(null);
								if (xmlCORSHttp.status == 200) sReturn = xmlCORSHttp.responseText;
							}
						} catch (e) { };
					}
				} catch (e) { };

				if (typeof sReturn === "undefined") {
					sReturn = "";
					if (!(typeof xmlHttp === "undefined")) {

						xmlHttp.open("GET", theURL, false);
						xmlHttp.send(null);
						if (xmlHttp.status == 200) sReturn = xmlHttp.responseText;

					}
				}
			} catch (err) {
				txt = "There was an error " + (err.number & 0xFFFF) + " " + err.name + " on this page.\n\n";
				txt += "Error message: " + err.message + "\n\n";
				if (err.description != undefined) { txt += "Error description: " + err.description + "\n\n"; }
				sReturn = "<!--" + txt + "-->";
			} finally {
				if (sReturn === "undefined") sReturn = '';
				if (sReturn.search(/Tandridge District Council Error response/i) > 0) sReturn = '';
				if (document.location.protocol == "http:") {
					sReturn = sReturn.replace(/https:/ig, "http:");
				} else if (document.location.protocol == "https:") {
					sReturn = sReturn.replace(/http:/ig, "https:");
				} else {
					sReturn = sReturn.replace(/"\/\//ig, '"http://').replace(/'\/\//ig, "'http://").replace(/="www./ig, '="http://www.').replace(/='www./ig, "='http://www.")
				};
			}
			return sReturn;
		}


        //]]>
	</script>

	<script language="javascript" type="text/javascript">
		//<![CDATA[
		document.write(fnExtractFromURL(sURLBrandingLoc + "Branding1HeadTagPartSansScripts.aspx"));
        //]]>
	</script>


	<link rel="stylesheet" type="text/css" href="./TDCCSS/TDCCSSStyle.css" media="all" />


	<style type="text/css">
		.style2 {
			font-family: Arial, Helvetica, sans-serif;
			font-size: medium;
			font-weight: bold;
			color: #800000;
			margin: 0;
		}
	</style>

</head>
<body>
	<script type="text/javascript">
		//<![CDATA[
		document.write(fnExtractFromURL(sURLBrandingLoc + "Branding2BodyTagStartSansScripts.aspx", sAlternativeRoot + "Branding2BodyTagStartSansScripts.htm"));
        //]]>
	</script>
	<script type="text/javascript">
		//<![CDATA[
		document.write(fnExtractFromURL(sURLBrandingLoc + "Branding3_AllBodyPreMenuSansScripts.aspx", sAlternativeRoot + "Branding3_AllBodyPreMenuSansScripts.htm"));
        //]]>
	</script>
	<script type="text/javascript">
		//<![CDATA[
		document.write(fnExtractFromURL(sURLBrandingLoc + "Branding4BodyLHMenuPart.aspx", sAlternativeRoot + "Branding4BodyLHMenuPart.htm"));
        //]]>
	</script>

	<!-- ==========================================================================-->
	<!-- Here is where a designer would place the body of their content -->
	<!-- ==========================================================================-->


	<form id="frmResponse" runat="server">

		<!-- ==========================================================================-->
		<!-- Here is where a designer would place the body of their content -->
		<!-- ==========================================================================-->

		<div id="TDCInfo">




			<h2>Payment to reference - Payment Process Return</h2>


			<div>
				Showing the query string and post values<br />
				<br />
				<br />
				<div style="text-wrap: avoid;">
					<asp:Button ID="cmdActAsFailure" runat="server" Text="Act as a failure" />&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button ID="cmdActAsDeclined" runat="server" Text="Act as declined" />&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button ID="cmdActAsSuccess" runat="server" Text="Act as a success" />
				</div>
				<br />
				<br />
				Query string (in URL)
		<asp:Table ID="tblQueryStringValues" runat="server">
			<asp:TableHeaderRow>
				<asp:TableHeaderCell>Field name</asp:TableHeaderCell>
				<asp:TableHeaderCell>Value</asp:TableHeaderCell>
			</asp:TableHeaderRow>
		</asp:Table>
				<br />
				<br />
				Post values (From form)
			<asp:Table ID="tblPostStringValues" runat="server">
				<asp:TableHeaderRow>
					<asp:TableHeaderCell>Field name</asp:TableHeaderCell>
					<asp:TableHeaderCell>Value</asp:TableHeaderCell>
				</asp:TableHeaderRow>
			</asp:Table>
				<br />
				<br />
	</form>

	<form action="<%=PostToTarget %>" id="frmTopayment" name="frmTopayment" method="get" enctype="application/x-www-form-urlencoded" target="">
		<button type="submit">Pass to payment engine</button>
	</form>

	<!-- ==========================================================================-->
	<!-- End of body content -->
	<!-- ==========================================================================-->


	<div id="AfterBranding5"></div>
	<script type="text/javascript">
		//<![CDATA[
		fnExtractFromURL(sURLBrandingLoc + "Branding5FootPartSansScripts.aspx", "AfterBranding5", "");
        //]]>
	</script>

	<script>
        //window.location = "./afPaymentResponse.aspx";
	</script>
	</div>

</body>
</html>
