<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="afPaymentResponse.aspx.vb" Inherits="FormAssemblyActions.afPaymentResponse2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>tfa Payment Response</title>
	<meta name="description" content="Tandridge District Council process payment response" />
	<meta name="distribution" content="UI" />
	<meta name="revisit-after" content="5 days" />
	<meta name="distribution" content="GLOBAL" />
	<meta name="author" content="Rob Collins ICT" />
	<meta name="copyright" content="Tandridge District Council 2015" />
	<meta name="googlebot" content="noodp" />
	<meta name="language" content="english" />
	<meta name="reply-to" content="CustomerService@Tandridge.gov.uk" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1 " />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta name="ROBOTS" content="index, nofollow nofollow" />
	<meta name="rating" content="General" />



	<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">-->
	<script src="//tdcws01.tandridge.gov.uk/PublicFiles/JQueryVersions/Versions1.x/jquery-1.12.4.js">
	</script>

	<script type="text/javascript">
		//<![CDATA[

		if (typeof g_sFormName === "undefined") var g_sFormName = "Form thingy v1";

		if (typeof g_thisDate === "undefined") var g_thisDate = new Date();
		if (typeof sDate === "undefined") var sDate = g_thisDate.toDateString;

		if (typeof g_sLocalHost === "undefined") var g_sLocalHost = "//" + location.host;
		if (typeof g_sLocalPath === "undefined") var g_sLocalPath = g_sLocalHost + (location.pathname.substr(-1, 1) == "/") ? location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1) : location.pathname;
		if (typeof g_sTargetHost === "undefined") var g_sTargetHost = g_sLocalHost;
		if (typeof g_sTestTargetHost === "undefined") {
			var g_sTestTargetHost = g_sTargetHost.toLowerCase();
			if (g_sTestTargetHost.indexOf("localhost") >= 0 || g_sTestTargetHost.indexOf("dev01") >= 0 || g_sTestTargetHost.indexOf("netint02") >= 0) g_sTargetHost = "//TDCWebApps.tandridge.gov.uk";
		}
		if (typeof g_sPublicHostPath === "undefined") var g_sPublicHostPath = g_sTargetHost + (location.hostname.toUpperCase().indexOf("TDCWS01") >= 0 ? "/TDCWebAppsPublic/" : "/");

		if (typeof g_sDevType === "undefined") var g_sDevType = "Test"; // Can be "Dev"
		if (typeof g_sTypeService === "undefined") {
			var g_sTypeService = "";
			g_sDevType = "Test";
			if (/(dev|localhost)/i.test(location.pathname)) g_sTypeService = g_sDevType;
			if (g_sTypeService == "" && (/(dev|localhost)/i.test(location.hostname))) g_sTypeService = g_sDevType;
			if (g_sTypeService == "" && (/test/i.test(location.pathname))) g_sTypeService = "Test";
			if (g_sTypeService == "" && (/test/i.test(location.hostname))) g_sTypeService = "Test";
		}
		if (typeof g_UseSynchronous === "undefined") var g_UseSynchronous = false;
		if (typeof bShowErrors === "undefined") var bShowErrors = true;

		if (typeof sURLBrandingLoc === "undefined") var sURLBrandingLoc = g_sPublicHostPath + "BrandingSplit2015" + g_sTypeService + "/";
		if (typeof sAlternativeRoot === "undefined") var sAlternativeRoot = sURLBrandingLoc + "StaticBranding/";

        //]]>
	</script>


	<script type="text/javascript">
		//<![CDATA[
		//var sURLBrandingLoc = "//tdcws01.tandridge.gov.uk/TDCWebAppsPublic/BrandingSplit2015" + g_sTypeService + "/";
		//var sURLBrandingLoc = "//tdcws01.tandridge.gov.uk/TDCWebAppsPublic/BrandingSplit2015/";
		//var sAlternativeRoot = sURLBrandingLoc + "StaticBranding/";
		var xmlCORSHttp; // See http://www.html5rocks.com/en/tutorials/cors/#toc-adding-cors-support-to-the-server


		function fnExtractFromURL(sURLLocation, sAlternative, TagIDToInsertBefore, HeaderTagNameToInsertBefore, UseSynchronous) {
			var xmlHttp;
			var theURL = "";
			var sHTTPResponse = "";
			var sProtocol;
			var sResponseText = "";
			var sReturn;
			var oBeforeElement;
			var bUsedAlternative = false;
			var bUseSynchronous = false;

			if (!(typeof g_UseSynchronous === "undefined")) bUseSynchronous = g_UseSynchronous;
			if (!(typeof UseSynchronous === "undefined")) bUseSynchronous = UseSynchronous;

			if (typeof TagIDToInsertBefore === "undefined" || TagIDToInsertBefore == "") {
				if (typeof HeaderTagNameToInsertBefore === "undefined" || HeaderTagNameToInsertBefore == "") {

				} else {
					try { oBeforeElement = document.getElementsByName(HeaderTagNameToInsertBefore)[0]; } catch (e) { };
				}
			} else {
				try { oBeforeElement = document.getElementById(TagIDToInsertBefore); } catch (e) { };
			};

			sProtocol = document.location.protocol;
			theURL = sURLLocation;
			do {

				if (sProtocol != "http:" && sProtocol != "https:") {
					try { sProtocol = (("http:" == document.location.protocol) ? "http:" : "https:"); } catch (e) { sProtocol = "https:"; }
					theURL = sProtocol + theURL;
				}

				/* running locally on IE5.5, IE6, IE7 */
				if (location.protocol == "file:") {
					if (typeof xmlHttp === "undefined") try { xmlHttp = new ActiveXObject("MSXML2.XMLHTTP"); } catch (e) { }
					if (typeof xmlHttp === "undefined") try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }
				};
				if (typeof xmlHttp === "undefined" && !(typeof XMLHttpRequest === "undefined")) try { xmlHttp = new XMLHttpRequest(); } catch (e) { }
				/* IE7, Firefox, Safari, Opera...  */
				if (typeof xmlHttp === "undefined" && !(typeof XMLHttpRequest2 === "undefined")) try { xmlHttp = new XMLHttpRequest2(); } catch (e) { }
				/* IE6 */
				if (typeof xmlHttp === "undefined" && !(typeof ActiveXObject === "undefined")) try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }

				try {

					try {
						if (!(typeof XDomainRequest === "undefined")) {
							xmlCORSHttp = new XDomainRequest();
							try {
								if (!(typeof xmlCORSHttp === "undefined")) {

									if (typeof oBeforeElement === undefined) {
										xmlCORSHttp.open("GET", theURL);
										xmlCORSHttp.send(null);
										if (xmlCORSHttp.status == 200) {
											sResponseText = xmlCORSHttp.responseText;
											sReturn = sResponseText;
										}
										document.write(sReturn);
										// This is needed as sometimes the entire HTML DOM is regenerated on HTML changes and triggers need to be reattached
										if (!(typeof fnSetJQuerytriggers === "undefined")) fnSetJQuerytriggers();

									} else {

										xmlCORSHttp.open("GET", theURL, true);
										xmlCORSHttp.onload = function () {
											if (xmlCORSHttp.status == 200) {
												sResponseText = xmlCORSHttp.responseText;
												fnInsertResponseBeforeTag(sResponseText, oBeforeElement);
											}
										}
										xmlCORSHttp.send(null);
									}
								}
							} catch (e) { };
						}
					} catch (e) { };

					if (typeof sResponseText === "undefined" || sResponseText == "") {
						sReturn = "";
						if (!(typeof xmlHttp === "undefined")) {
							if (typeof oBeforeElement === "undefined") {
								xmlHttp.open("GET", theURL, false);
								xmlHttp.send(null);
								if (xmlHttp.status == 200 || xmlHttp.status == 0) {
									sResponseText = xmlHttp.responseText;
									sReturn = sResponseText;
									document.write(sReturn);
									// This is needed as sometimes the entire HTML DOM is regenerated on HTML changes and triggers need to be reattached
									if (!(typeof fnSetJQuerytriggers === "undefined")) fnSetJQuerytriggers();

								}
							} else if (!bUseSynchronous) {
								xmlHttp.open("GET", theURL, true);
								xmlHttp.onload = function () {
									if (xmlHttp.status == 200 || xmlHttp.status == 0) {
										sResponseText = xmlHttp.responseText;
										fnInsertResponseBeforeTag(sResponseText, oBeforeElement);
									}
								}
								xmlHttp.ontimeout = function () {
									if (!(typeof sAlternative === "undefined" || sAlternative == "")) fnExtractFromURL(sAlternative, "", TagIDToInsertBefore, HeaderTagNameToInsertBefore);
								}
								xmlHttp.onerror = function () {
									if (!(typeof sAlternative === "undefined" || sAlternative == "")) fnExtractFromURL(sAlternative, "", TagIDToInsertBefore, HeaderTagNameToInsertBefore);
								}
								xmlHttp.send(null);

							} else {
								xmlHttp.open("GET", theURL, false);
								xmlHttp.send(null);
								if (xmlHttp.status == 200 || xmlHttp.status == 0) {
									sResponseText = xmlHttp.responseText;
									sReturn = sResponseText;
									fnInsertResponseBeforeTag(sReturn, oBeforeElement);
									// This is needed as sometimes the entire HTML DOM is regenerated on HTML changes and triggers need to be reattached
									if (!(typeof fnSetJQuerytriggers === "undefined")) fnSetJQuerytriggers();

								}
							}
						}
					}
				} catch (err) {
					txt = "There was an error " + (err.number & 0xFFFF) + " " + err.name + " on this page.\n\n";
					txt += "Error message: " + err.message + "\n\n";
					if (err.description != undefined) { txt += "Error description: " + err.description + "\n\n"; }
					sReturn = "<!--" + txt + "-->";
				} finally {
					if (sReturn === "undefined") sReturn = '';
					if (sReturn.search(/Tandridge District Council Error response/i) > 0) sReturn = '';
					if (document.location.protocol == "http:") {
						sReturn = sReturn.replace(/https:/ig, "http:");
					} else if (document.location.protocol == "https:") {
						sReturn = sReturn.replace(/http:/ig, "https:");
					} else {
						sReturn = sReturn.replace(/"\/\//ig, '"http://').replace(/'\/\//ig, "'http://").replace(/="www./ig, '="http://www.').replace(/='www./ig, "='http://www.")
					};
				}
				if (bUsedAlternative) {
					theURL = "";
				} else {
					if ((typeof sResponseText === "undefined" || sResponseText == "")) {
						theURL = sAlternative;
						bUsedAlternative = true;
						sReturn = "";
					}
				}
			}
			while (theURL != "" && (typeof sResponseText === "undefined" || sResponseText == "") && (typeof TagIDToInsertBefore === "undefined" || TagIDToInsertBefore == ""))

			return sReturn;
		}




		function fnInsertResponseBeforeTag(HTMLToInsert, oBeforeElement) {
			var sParentHTML = "", sReturn;
			var oParentNode = oBeforeElement.parentNode;

			try {
				if (typeof oBeforeElement === "undefined") {
					document.write(HTMLToInsert);
				} else {
					if (typeof oParentNode === "undefined" || oParentNode == null) {
						try { oParentNode = document.body; } catch (e) { oParentNode = void 0; };
					}

					if (typeof oParentNode === "undefined" || oParentNode == null) {
						oBeforeElement.insertAdjacentHTML("beforebegin", HTMLToInsert);
					} else {
						sParentHTML = oParentNode.innerHTML;
						sParentHTML = sParentHTML.substring(0, sParentHTML.indexOf(oBeforeElement.outerHTML)) + HTMLToInsert + sParentHTML.substring(sParentHTML.indexOf(oBeforeElement.outerHTML));

						oParentNode.innerHTML = sParentHTML;
					}

					// This is needed as sometimes the entire HTML DOM is regenerated on HTML changes and triggers need to be reattached
					if (!(typeof fnSetJQuerytriggers === "undefined")) fnSetJQuerytriggers();
				}

			} catch (err) {
				txt = "There was an error " + (err.number & 0xFFFF) + " " + err.name + " on this page.\n\n";
				txt += "Error message: " + err.message + "\n\n";
				if (err.description != undefined) { txt += "Error description: " + err.description + "\n\n"; }
				sReturn = "<!--" + txt + "-->";
			};
		}


        //]]>
	</script>

	<script language="javascript" type="text/javascript">
		//<![CDATA[
		fnExtractFromURL(sURLBrandingLoc + "Branding1HeadTagPart.aspx", "", "", "", false);
        //]]>
	</script>


	<link rel="stylesheet" type="text/css" href="./TDCCSS/TDCCSSStyle.css" media="all" />


	<style type="text/css">
		.style2 {
			font-family: Arial, Helvetica, sans-serif;
			font-size: medium;
			font-weight: bold;
			color: #800000;
			margin: 0;
		}
	</style>

</head>
<body class="tdc">
	<script type="text/javascript">
        //<![CDATA[
        //fnExtractFromURL(sURLBrandingLoc + "Branding1HeadTagPart.aspx", "", "", "", false);
        //]]>
	</script>
	<script type="text/javascript">
		//<![CDATA[
		fnExtractFromURL(sURLBrandingLoc + "Branding3_AllBodyPreMenu.aspx", sAlternativeRoot + "Branding3_AllBodyPreMenu.htm", "", "", true);
        //]]>
	</script>
	<script type="text/javascript">
		//<![CDATA[
		fnExtractFromURL(sURLBrandingLoc + "Branding4BodyLHMenuPart.aspx", sAlternativeRoot + "Branding4BodyLHMenuPart.htm", "", "", true);
        //]]>
	</script>

	<!-- ==========================================================================-->
	<!-- Here is where a designer would place the body of their content -->
	<!-- ==========================================================================-->

	<section style="min-height:500px;">

		<form id="frmResponse" runat="server" action="<%=sURLToGoTo %>">

			<!-- ==========================================================================-->
			<!-- Here is where a designer would place the body of their content -->
			<!-- ==========================================================================-->

			<div id="TDCInfo">
				<div style="background-color: #FFFFCC; border: medium double #CCFFFF" align="center">

					<asp:Label ID="lblPostValues" runat="server"></asp:Label>

					<asp:Panel ID="pnlPaymentSuccessful" runat="server">
						<h2>Thank you for your submission and payment</h2>
						<div><%=sSuccessfulText %></div>
						<p>Your submission will be processed</p>
					</asp:Panel>
					<asp:Panel ID="pnlPaymentFailed" runat="server">
						<h2>Thank you for your submission</h2>
						<h3>unfortunately your payment was unsuccesful</h3>
						<div><%=sUnsuccessfulText %></div>
					</asp:Panel>


				</div>
			</div>

			<% If sMessageToUser <> "" %><%= sMessageToUser %><% End if %>

			<%If bShowValues %>
			<p>
				Files detected<br />
				<asp:Label ID="lblFilesDetected" runat="server" Text=""></asp:Label>
			</p>
			<p>
				Files saved<br />
				<asp:Label ID="Label1" runat="server" Text=""></asp:Label>
			</p>
			<p>
				Processing<br />
				<asp:Label ID="lblProcessing" runat="server" Text=""></asp:Label>
			</p>
			<div>
				Query string (in URL)<br />
				sent using protocol GET
		        <asp:Table ID="tblQueryStringValues" runat="server">
					  <asp:TableHeaderRow>
						  <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
						  <asp:TableHeaderCell>Value</asp:TableHeaderCell>
					  </asp:TableHeaderRow>
				  </asp:Table>
			</div>
			<div>
				<br />
				Form values (From form encoded)<br />
				sent using protocol POST
		    <asp:Table ID="tblPostStringValues" runat="server">
				 <asp:TableHeaderRow>
					 <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
					 <asp:TableHeaderCell>Value</asp:TableHeaderCell>
				 </asp:TableHeaderRow>
			 </asp:Table>

				<asp:Table ID="tblFilesPosted" runat="server">
					<asp:TableHeaderRow>
						<asp:TableHeaderCell>File Name</asp:TableHeaderCell>
						<asp:TableHeaderCell>Size</asp:TableHeaderCell>
						<asp:TableHeaderCell>Type</asp:TableHeaderCell>
					</asp:TableHeaderRow>
				</asp:Table>

				<asp:Table ID="tblDocsSaved" runat="server">
					<asp:TableHeaderRow>
						<asp:TableHeaderCell>File</asp:TableHeaderCell>
					</asp:TableHeaderRow>
				</asp:Table>

				<asp:Table ID="tblEmailsSent" runat="server">
					<asp:TableHeaderRow>
						<asp:TableHeaderCell>To</asp:TableHeaderCell>
						<asp:TableHeaderCell>Subject</asp:TableHeaderCell>
					</asp:TableHeaderRow>
				</asp:Table>

			</div>
			<div>
				<div>
					<br />
					Recovered values from XML document
		    <asp:Table ID="tblRecoveredStringValues" runat="server">
				 <asp:TableHeaderRow>
					 <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
					 <asp:TableHeaderCell>Value</asp:TableHeaderCell>
				 </asp:TableHeaderRow>
			 </asp:Table>

				</div>
				<div>

					<% If sConfigID <> "" Then %>

            Config values<br />
					using stored config <%:sConfigID %>
					<asp:Table ID="tblConfigValues" runat="server">
						<asp:TableHeaderRow>
							<asp:TableHeaderCell>Field name</asp:TableHeaderCell>
							<asp:TableHeaderCell>Value</asp:TableHeaderCell>
						</asp:TableHeaderRow>
					</asp:Table>
					<br />
					<br />
					<% End if %>
				</div>

				<% End If %>

				<asp:Label ID="lblFileList" runat="server" Text=""></asp:Label>
				<asp:Label ID="lblErrors" runat="server" Text=""></asp:Label>

				<asp:Panel ID="pnlHTMLFormOut" runat="server">
					<asp:Label ID="lblHTMLFormOut" runat="server" />
				</asp:Panel>
		</form>
	</section>
	<!-- ==========================================================================-->
	<!-- End of body content -->
	<!-- ==========================================================================-->


	<div id="AfterBranding5"></div>
	<script type="text/javascript">
		//<![CDATA[
		fnExtractFromURL(sURLBrandingLoc + "Branding5FootPart.aspx", sAlternativeRoot + "Branding5FootPart.htm", "", "", true);
        //]]>
	</script>

	<script>
        //window.location = "./afPaymentResponse.aspx";
	</script>
	</div>

</body>
</html>
