﻿Public Class TestPaymentToAction
	Inherits System.Web.UI.Page

	Public PostToTarget As String = ""
	Public sSubmissionUID As String = ""

	Dim sResponseSessionID As String = "", sResponseErrorStatus As String = ""
	Dim sResponseErrorCode As String = "", sResponseErrorDescription As String = ""
	Dim sResponseAuthStatus As String = "", sResponseAuthCode As String = ""


	Dim URLQueryParams As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		Dim oTblRow As TableRow, oTblCell As TableCell
		Dim sKey As String, sValue As String, sTemp As String = ""
		Dim sReturnURL As String = ""
		Dim sSessionIDDelimier As String = "-"


		'If Not (IsPostBack) Then

		Try

			sTemp = GetMySettingString("sSessionIDDelimier") : If sTemp <> "" Then sSessionIDDelimier = sTemp

			If Request.QueryString.Count > 0 Then
				URLQueryParams.Clear()
				For Each sKey In Request.QueryString.Keys
					sKey = UCase(sKey)
					sValue = Server.UrlDecode(Request.QueryString.Item(sKey))
					'If Not URLQueryParams.ContainsKey(sKey) Then URLQueryParams.Add(sKey, sValue) Else URLQueryParams.Item(sKey) = sValue
					UpdStringSortedList(URLQueryParams, sKey, sValue)

				Next
			End If

			For Each sKey In Request.Form.Keys
				sKey = UCase(sKey)
				sValue = Server.UrlDecode(Request.Form.Item(sKey))
				'If Not URLQueryParams.ContainsKey(sKey) Then URLQueryParams.Add(sKey, sValue) Else URLQueryParams.Item(sKey) = sValue
				UpdStringSortedList(URLQueryParams, sKey, sValue)

			Next

			If URLQueryParams.ContainsKey("SUBMISSIONUID") Then
				sResponseSessionID = URLQueryParams.Item("SUBMISSIONUID").ToString
				'goCurrentData.QuikFormsSubmissionUID = sResponseSessionID
				sSubmissionUID = Split(sResponseSessionID, sSessionIDDelimier)(0)
			End If

			If URLQueryParams.ContainsKey("SUBMISSIONID") Then
				sResponseSessionID = URLQueryParams.Item("SUBMISSIONID").ToString
				'goCurrentData.QuikFormsSubmissionUID = sResponseSessionID
				sSubmissionUID = Split(sResponseSessionID, sSessionIDDelimier)(0)
			End If

			If URLQueryParams.ContainsKey("SESSIONID") Then
				sResponseSessionID = URLQueryParams.Item("SESSIONID").ToString
				'goCurrentData.QuikFormsSubmissionUID = sResponseSessionID
				sSubmissionUID = Split(sResponseSessionID, sSessionIDDelimier)(0)
			End If

			If URLQueryParams.ContainsKey("SESSIONUID") Then
				sResponseSessionID = URLQueryParams.Item("SESSIONUID").ToString
				'goCurrentData.QuikFormsSubmissionUID = sResponseSessionID
				sSubmissionUID = Split(sResponseSessionID, sSessionIDDelimier)(0)
			End If

			If Not IsPostBack Then
				If Request.QueryString.Count > 0 Then
					For Each sKey In Request.QueryString.Keys
						oTblRow = New TableRow
						sValue = Server.UrlDecode(Request.QueryString.Item(sKey))
						oTblCell = New TableCell
						oTblCell.Text = HttpUtility.HtmlEncode(sKey)
						oTblRow.Cells.Add(oTblCell)

						oTblCell = New TableCell
						oTblCell.Text = HttpUtility.HtmlEncode(sValue)
						oTblRow.Cells.Add(oTblCell)
						tblQueryStringValues.Rows.Add(oTblRow)

						oTblCell = New TableCell
						sTemp = "<input type=""hidden"" name=""" & sKey & """ id=""" & sKey & """ value=""" & Server.UrlEncode(sValue) & """ />"
						oTblCell.Text = sTemp
						oTblRow.Cells.Add(oTblCell)
						tblPostStringValues.Rows.Add(oTblRow)
						If sKey = "returnurl" Then sReturnURL = sValue
						If sKey = "returnurl" Then sReturnURL = sValue
					Next
				End If

				If Request.Form.Count > 0 Then
					For Each sKey In Request.Form.Keys
						oTblRow = New TableRow
						sValue = Server.UrlDecode(Request.Form.Item(sKey))
						oTblCell = New TableCell
						oTblCell.Text = HttpUtility.HtmlEncode(sKey)
						oTblRow.Cells.Add(oTblCell)

						oTblCell = New TableCell
						sTemp = sValue
						oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
						oTblRow.Cells.Add(oTblCell)

						oTblCell = New TableCell
						sTemp = "<input type=""hidden"" name=""" & sKey & """ id=""" & sKey & """ value=""" & Server.UrlEncode(sValue) & """ />"
						oTblCell.Text = sTemp
						oTblRow.Cells.Add(oTblCell)
						tblPostStringValues.Rows.Add(oTblRow)
						If sKey = "returnurl" Then sReturnURL = sValue
					Next
				End If

			End If
			'End If

			'sReturnURL = "/ShowPostValues.aspx"
			'sReturnURL = Page.ResolveClientUrl(GetMySetting("PaymentStartPage").ToString)
			'If sReturnURL = "" Then sReturnURL = GetMyURLPath(GetMySetting("StartPageURL").ToString, Page)

			'If Left(sReturnURL, 1) = "/" And Left(sReturnURL, 2) <> "//" Then sReturnURL = goCurrentData.URLParentPath & sReturnURL

		Catch ex As Exception

		End Try

		PostToTarget = sReturnURL

	End Sub



	Protected Sub cmdActAsSuccess_Click(sender As Object, e As EventArgs) Handles cmdActAsSuccess.Click
		Dim sReturnURL As String = "", sValue As String, sKey As String
		Try

			If Request.QueryString.Count > 0 Then
				For Each sKey In Request.QueryString.Keys
					sValue = Server.UrlDecode(Request.QueryString.Item(sKey))
					If sKey = "returnurl" Then sReturnURL = sValue
				Next
			End If

			If Request.Form.Count > 0 And sReturnURL = "" Then
				For Each sKey In Request.Form.Keys
					sValue = Server.UrlDecode(Request.Form.Item(sKey))
					If sKey = "returnurl" Then sReturnURL = sValue
				Next
			End If

			sReturnURL &= "?sessionID=" & Server.UrlEncode(sResponseSessionID) & "&errorStatus=1&authstatus=1&authcode=112233"
			Response.Redirect(sReturnURL, False)

		Catch ex As Exception

		End Try

	End Sub

	Protected Sub cmdActAsFailure_Click(sender As Object, e As EventArgs) Handles cmdActAsFailure.Click
		Dim sReturnURL As String = "", sValue As String, sKey As String
		Try

			If Request.QueryString.Count > 0 Then
				For Each sKey In Request.QueryString.Keys
					sValue = Server.UrlDecode(Request.QueryString.Item(sKey))
					If sKey = "returnurl" Then sReturnURL = sValue
				Next
			End If

			If Request.Form.Count > 0 And sReturnURL = "" Then
				For Each sKey In Request.Form.Keys
					sValue = Server.UrlDecode(Request.Form.Item(sKey))
					If sKey = "returnurl" Then sReturnURL = sValue
				Next
			End If
			sReturnURL &= "?sessionID=" & Server.UrlEncode(sResponseSessionID) & "&errorStatus=0&errorCode=Test&errorDescription=" & Server.UrlEncode("in a state of testing")
			Response.Redirect(sReturnURL, False)

		Catch ex As Exception

		End Try

	End Sub

	Private Sub cmdActAsDeclined_Click(sender As Object, e As EventArgs) Handles cmdActAsDeclined.Click
		Dim sReturnURL As String = "", sValue As String, sKey As String
		Try

			If Request.QueryString.Count > 0 Then
				For Each sKey In Request.QueryString.Keys
					sValue = Server.UrlDecode(Request.QueryString.Item(sKey))
					If sKey = "returnurl" Then sReturnURL = sValue
				Next
			End If

			If Request.Form.Count > 0 And sReturnURL = "" Then
				For Each sKey In Request.Form.Keys
					sValue = Server.UrlDecode(Request.Form.Item(sKey))
					If sKey = "returnurl" Then sReturnURL = sValue
				Next
			End If

			sReturnURL &= "?sessionID=" & Server.UrlEncode(sResponseSessionID) & "&errorStatus=1&authstatus=0&authcode=112233"
			Response.Redirect(sReturnURL, False)

		Catch ex As Exception

		End Try

	End Sub



	'Protected Sub cmdActAsFailure_Click(sender As Object, e As EventArgs) Handles cmdActAsFailure.Click
	'    Dim sReturnURL As String = "", sValue As String, sKey As String
	'    Try

	'        If Request.QueryString.Count > 0 Then
	'            For Each sKey In Request.QueryString.Keys
	'                sValue = Server.UrlDecode(Request.QueryString.Item(sKey))
	'                If sKey = "returnurl" Then sReturnURL = sValue
	'            Next
	'        End If

	'        If Request.Form.Count > 0 And sReturnURL = "" Then
	'            For Each sKey In Request.Form.Keys
	'                sValue = Server.UrlDecode(Request.Form.Item(sKey))
	'                If sKey = "returnurl" Then sReturnURL = sValue
	'            Next
	'        End If

	'        'sReturnURL = Page.ResolveClientUrl(GetMySetting("PaymentStartPage").ToString)
	'        'If sReturnURL = "" Then sReturnURL = GetMyURLPath(GetMySetting("PaymentReturnTo").ToString, Page)
	'        'If Left(sReturnURL, 1) = "/" And Left(sReturnURL, 2) <> "//" Then sReturnURL = goCurrentData.URLParentPath & sReturnURL

	'        'sReturnURL &= "?sessionID=" & Server.UrlEncode(goCurrentData.QuikFormsSubmissionUID) & "&errorStatus=1&errorCode=Test&errorDescription=" & Server.UrlEncode("in a state of testing")
	'        sReturnURL &= "?sessionID=" & Server.UrlEncode(sResponseSessionID) & "&errorStatus=1&errorCode=Test&errorDescription=" & Server.UrlEncode("in a state of testing")
	'        Response.Redirect(sReturnURL, False)

	'    Catch ex As Exception

	'    End Try

	'End Sub


	'Protected Sub cmdActAsSuccess_Click(sender As Object, e As EventArgs) Handles cmdActAsSuccess.Click
	'    Dim sReturnURL As String = "", sValue As String, sKey As String
	'    Try

	'        If Request.QueryString.Count > 0 Then
	'            For Each sKey In Request.QueryString.Keys
	'                sValue = Server.UrlDecode(Request.QueryString.Item(sKey))
	'                If sKey = "returnurl" Then sReturnURL = sValue
	'            Next
	'        End If

	'        If Request.Form.Count > 0 And sReturnURL = "" Then
	'            For Each sKey In Request.Form.Keys
	'                sValue = Server.UrlDecode(Request.Form.Item(sKey))
	'                If sKey = "returnurl" Then sReturnURL = sValue
	'            Next
	'        End If

	'        'sReturnURL = Page.ResolveClientUrl(GetMySetting("PaymentStartPage").ToString)
	'        'If sReturnURL = "" Then sReturnURL = GetMyURLPath(GetMySetting("PaymentReturnTo").ToString, Page)

	'        'If Left(sReturnURL, 1) = "/" And Left(sReturnURL, 2) <> "//" Then sReturnURL = goCurrentData.URLParentPath & sReturnURL

	'        'sReturnURL &= "?sessionID=" & Server.UrlEncode(goCurrentData.QuikFormsSubmissionUID) & "&errorStatus=0&authstatus=1&authcode=" & Server.UrlEncode("TestCode")
	'        sReturnURL &= "?sessionID=" & Server.UrlEncode(sResponseSessionID) & "&errorStatus=0&authstatus=1&authcode=" & Server.UrlEncode("TestCode")
	'        Response.Redirect(sReturnURL, False)

	'    Catch ex As Exception

	'    End Try

	'End Sub

End Class