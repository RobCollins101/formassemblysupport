<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="afPassToPayments.aspx.vb" Inherits="FormAssemblyActions.afPassToPayments" Title="TDC Payment Pass Through" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Pass details to payment for reference</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1 " />
  	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Pragma" content="no-cache" />
</head>
<body>
    <h1>Please wait - transferring to Payment page</h1>

    <form id="frmToPayment" name="frmToPayment" action="<%:sPaymentEngineTarget %>" method="post" enctype="application/x-www-form-urlencoded" target="_self">
        <input type="hidden" id="channel" name="channel" value="<%:sPaymentChannel %>" />
        <input type="hidden" id="sessionID" name="sessionID" value="<%:sPaymentSessionID %>" />
                
        <input type="hidden" id="ctitle" name="ctitle" value="<%:sPaymentTitle%>" />
        <input type="hidden" id="cfname" name="cfname" value="<%:sPaymentCfName%>" />
        <input type="hidden" id="csname" name="csname" value="<%:sPaymentCsName%>" />
        <input type="hidden" id="chouse" name="chouse" value="<%:sPaymentHouse %>" />
        <input type="hidden" id="cadd1" name="cadd1" value="<%:sPaymentAddr1%>" />
        <input type="hidden" id="ctown" name="ctown" value="<%:sPaymentAddrTown%>" />
        <input type="hidden" id="cpostcode" name="cpostcode" value="<%:sPaymentPostcode%>" />
        <input type="hidden" id="ccounty" name="ccounty" value="<%:sPaymentCounty%>" />
        <input type="hidden" id="ctel" name="ctel" value="<%:sPaymentTelNo%>" />
        <input type="hidden" id="cemail" name="cemail" value="<%:sPaymentEMail%>" />
        <input type="hidden" id="sendmail" name="sendmail" value="<%:sPaymentSendMail%>" />
        <input type="hidden" id="displayReceipt" name="displayReceipt" value="<%:sPaymentDisplayReceipt%>" />
        <input type="hidden" id="returnmethod" name="returnmethod" value="<%:sReturnMethod%>" />
        <input type="hidden" id="returnurl" name="returnurl" value="<%:sReturnURL%>" />
        <input type="hidden" id="postbackurl" name="postbackurl" value="<%:sPostbackURL%>" />
        <input type="hidden" id="Pay" name="Pay" value="<%:sPaymentSessionID%>" />
        
        <input type="hidden" id="amount" name="amount" value="<%:sPaymentAmount%>" />
        <input type="hidden" id="fundcode" name="fundcode" value="<%:sPaymentFundPaymentCode%>" />
        <input type="hidden" id="custref1" name="custref1" value="<%:sPaymentCustRef1%>" />
        <input type="hidden" id="custref2" name="custref2" value="<%:sPaymentCustRef2%>" />
        <input type="hidden" id="custref3" name="custref3" value="<%:sPaymentCustRef3%>" />
        <input type="hidden" id="custref4" name="custref4" value="<%:sPaymentCustRef4%>" />
        <input type="hidden" id="description" name="description" value="<%:sPaymentDescription%>" />

        <% if sPaymentCustRef1_a1 <> "" and sPaymentAmount_a1 <> "" and sPaymentFundPaymentCode_a1 <> "" %>
        <input type="hidden" id="amount_a1" name="amount_a1" value="<%:sPaymentAmount_a1%>" />
        <input type="hidden" id="fundcode_a1" name="fundcode_a1" value="<%:sPaymentFundPaymentCode_a1%>" />
        <input type="hidden" id="custref1_a1" name="custref1_a1" value="<%:sPaymentCustRef1_a1%>" />
        <input type="hidden" id="custref2_a1" name="custref2_a1" value="<%:sPaymentCustRef2_a1%>" />
        <input type="hidden" id="custref3_a1" name="custref3_a1" value="<%:sPaymentCustRef3_a1%>" />
        <input type="hidden" id="custref4_a1" name="custref4_a1" value="<%:sPaymentCustRef4_a1%>" />
        <input type="hidden" id="description_a1" name="description_a1" value="<%:sPaymentDescription_a1%>" />
        <% end if %>

        <% if sPaymentCustRef1_a2 <> "" and sPaymentAmount_a2 <> "" and sPaymentFundPaymentCode_a2 <> "" %>
        <input type="hidden" id="amount_a2" name="amount_a2" value="<%:sPaymentAmount_a2%>" />
        <input type="hidden" id="fundcode_a2" name="fundcode_a2" value="<%:sPaymentFundPaymentCode_a2%>" />
        <input type="hidden" id="custref1_a2" name="custref1_a2" value="<%:sPaymentCustRef1_a2%>" />
        <input type="hidden" id="custref2_a2" name="custref2_a2" value="<%:sPaymentCustRef2_a2%>" />
        <input type="hidden" id="custref3_a2" name="custref3_a2" value="<%:sPaymentCustRef3_a2%>" />
        <input type="hidden" id="custref4_a2" name="custref4_a2" value="<%:sPaymentCustRef4_a2%>" />
        <input type="hidden" id="description_a2" name="description_a2" value="<%:sPaymentDescription_a2%>" />
        <% end if %>

        <% if sPaymentCustRef1_a3 <> "" and sPaymentAmount_a3 <> "" and sPaymentFundPaymentCode_a3 <> "" %>
        <input type="hidden" id="amount_a3" name="amount_a3" value="<%:sPaymentAmount_a3%>" />
        <input type="hidden" id="fundcode_a3" name="fundcode_a3" value="<%:sPaymentFundPaymentCode_a3%>" />
        <input type="hidden" id="custref1_a3" name="custref1_a3" value="<%:sPaymentCustRef1_a3%>" />
        <input type="hidden" id="custref2_a3" name="custref2_a3" value="<%:sPaymentCustRef2_a3%>" />
        <input type="hidden" id="custref3_a3" name="custref3_a3" value="<%:sPaymentCustRef3_a3%>" />
        <input type="hidden" id="custref4_a3" name="custref4_a3" value="<%:sPaymentCustRef4_a3%>" />
        <input type="hidden" id="description_a3" name="description_a3" value="<%:sPaymentDescription_a3%>" />
        <% end if %>

        <% if sPaymentCustRef1_a4 <> "" and sPaymentAmount_a4 <> "" and sPaymentFundPaymentCode_a4 <> "" %>
        <input type="hidden" id="amount_a4" name="amount_a4" value="<%:sPaymentAmount_a4%>" />
        <input type="hidden" id="fundcode_a4" name="fundcode_a4" value="<%:sPaymentFundPaymentCode_a4%>" />
        <input type="hidden" id="custref1_a4" name="custref1_a4" value="<%:sPaymentCustRef1_a4%>" />
        <input type="hidden" id="custref2_a4" name="custref2_a4" value="<%:sPaymentCustRef2_a4%>" />
        <input type="hidden" id="custref3_a4" name="custref3_a4" value="<%:sPaymentCustRef3_a4%>" />
        <input type="hidden" id="custref4_a4" name="custref4_a4" value="<%:sPaymentCustRef4_a4%>" />
        <input type="hidden" id="description_a4" name="description_a4" value="<%:sPaymentDescription_a4%>" />
        <% end if %>

        <% if sPaymentCustRef1_a5 <> "" and sPaymentAmount_a5 <> "" and sPaymentFundPaymentCode_a5 <> "" %>
        <input type="hidden" id="amount_a5" name="amount_a5" value="<%:sPaymentAmount_a5%>" />
        <input type="hidden" id="fundcode_a5" name="fundcode_a5" value="<%:sPaymentFundPaymentCode_a5%>" />
        <input type="hidden" id="custref1_a5" name="custref1_a5" value="<%:sPaymentCustRef1_a5%>" />
        <input type="hidden" id="custref2_a5" name="custref2_a5" value="<%:sPaymentCustRef2_a5%>" />
        <input type="hidden" id="custref3_a5" name="custref3_a5" value="<%:sPaymentCustRef3_a5%>" />
        <input type="hidden" id="custref4_a5" name="custref4_a5" value="<%:sPaymentCustRef4_a5%>" />
        <input type="hidden" id="description_a5" name="description_a5" value="<%:sPaymentDescription_a5%>" />
        <% end if %>

        <% if sPaymentCustRef1_a6 <> "" and sPaymentAmount_a6 <> "" and sPaymentFundPaymentCode_a6 <> "" %>
        <input type="hidden" id="amount_a6" name="amount_a6" value="<%:sPaymentAmount_a6%>" />
        <input type="hidden" id="fundcode_a6" name="fundcode_a6" value="<%:sPaymentFundPaymentCode_a6%>" />
        <input type="hidden" id="custref1_a6" name="custref1_a6" value="<%:sPaymentCustRef1_a6%>" />
        <input type="hidden" id="custref2_a6" name="custref2_a6" value="<%:sPaymentCustRef2_a6%>" />
        <input type="hidden" id="custref3_a6" name="custref3_a6" value="<%:sPaymentCustRef3_a6%>" />
        <input type="hidden" id="custref4_a6" name="custref4_a6" value="<%:sPaymentCustRef4_a6%>" />
        <input type="hidden" id="description_a6" name="description_a6" value="<%:sPaymentDescription_a6%>" />
        <% end if %>

        <% if sPaymentCustRef1_a7 <> "" and sPaymentAmount_a7 <> "" and sPaymentFundPaymentCode_a7 <> "" %>
        <input type="hidden" id="amount_a7" name="amount_a7" value="<%:sPaymentAmount_a7%>" />
        <input type="hidden" id="fundcode_a7" name="fundcode_a7" value="<%:sPaymentFundPaymentCode_a7%>" />
        <input type="hidden" id="custref1_a7" name="custref1_a7" value="<%:sPaymentCustRef1_a7%>" />
        <input type="hidden" id="custref2_a7" name="custref2_a7" value="<%:sPaymentCustRef2_a7%>" />
        <input type="hidden" id="custref3_a7" name="custref3_a7" value="<%:sPaymentCustRef3_a7%>" />
        <input type="hidden" id="custref4_a7" name="custref4_a7" value="<%:sPaymentCustRef4_a7%>" />
        <input type="hidden" id="description_a7" name="description_a7" value="<%:sPaymentDescription_a7%>" />
        <% end if %>

        <% if sPaymentCustRef1_a8 <> "" and sPaymentAmount_a8 <> "" and sPaymentFundPaymentCode_a8 <> "" %>
        <input type="hidden" id="amount_a8" name="amount_a8" value="<%:sPaymentAmount_a8%>" />
        <input type="hidden" id="fundcode_a8" name="fundcode_a8" value="<%:sPaymentFundPaymentCode_a8%>" />
        <input type="hidden" id="custref1_a8" name="custref1_a8" value="<%:sPaymentCustRef1_a8%>" />
        <input type="hidden" id="custref2_a8" name="custref2_a8" value="<%:sPaymentCustRef2_a8%>" />
        <input type="hidden" id="custref3_a8" name="custref3_a8" value="<%:sPaymentCustRef3_a8%>" />
        <input type="hidden" id="custref4_a8" name="custref4_a8" value="<%:sPaymentCustRef4_a8%>" />
        <input type="hidden" id="description_a8" name="description_a8" value="<%:sPaymentDescription_a8%>" />
        <% end if %>

        <% if sPaymentCustRef1_a9 <> "" And sPaymentAmount_a9 <> "" And sPaymentFundPaymentCode_a9 <> "" %>
        <input type="hidden" id="amount_a9" name="amount_a9" value="<%:sPaymentAmount_a9%>" />
        <input type="hidden" id="fundcode_a9" name="fundcode_a9" value="<%:sPaymentAmount_a9%>" />
        <input type="hidden" id="custref1_a9" name="custref1_a9" value="<%:sPaymentFundPaymentCode_a9%>" />
        <input type="hidden" id="custref2_a9" name="custref2_a9" value="<%:sPaymentCustRef1_a9%>" />
        <input type="hidden" id="custref3_a9" name="custref3_a9" value="<%:sPaymentCustRef2_a9%>" />
        <input type="hidden" id="custref4_a9" name="custref4_a9" value="<%:sPaymentCustRef3_a9%>" />
        <input type="hidden" id="description_a9" name="description_a9" value="<%:sPaymentDescription_a9%>" />
        <% end if %>

        <br />
        <br />
        <noscript>
            <p>Your browser javascript is not available</p>
        </noscript>

        <% If bShowValues Then %>
        <div>
            <p>Passing to <%:sPaymentEngineTarget %></p>
            <p>Returning to <%:sReturnURL %></p>
            <input type="submit" value="Submit" />

            <div>
                Query string values (in URL)<br />
                sent using protocol GET
		            <asp:Table ID="tblQueryStringValues" runat="server">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
            </div>

            <br />
            Form values (From form encoded) sent<br />
            using protocol POST
		        <asp:Table ID="tblPostStringValues" runat="server">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
            <br />
            <br />
 
            <% If sConfigID <> "" Then %>

            Config values<br />
            using stored config <%:sConfigID %>
		        <asp:Table ID="tblConfigValues" runat="server">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>Field name</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Value</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
            <br />
            <br /><% End if %>
        </div>

        <% Else %>
        <noscript>
            <input type="submit" value="Click here to proceed to payment" />
        </noscript>
        <% End if %>
    </form>
    <% If not bShowValues Then %>
    <script type="text/javascript">
        if (frmToPayment.action != "" && frmToPayment.Pay.value != "") frmToPayment.submit();
    </script>
    <% End if %>
</body>
</html>
