﻿Option Compare Text
Imports System.Xml

Public Class afPassToPayments
	Inherits System.Web.UI.Page

	Public bShowValues As Boolean = False
	Public sConfigID As String = ""

	Public sPaymentEngineTarget As String = ""
	Public sPaymentChannel As String = ""
	Public sPaymentSessionID As String = ""

	Public sPaymentTitle As String = ""
	Public sPaymentCfName As String = ""
	Public sPaymentCsName As String = ""
	Public sPaymentName As String = ""
	Public sPaymentHouse As String = ""
	Public sPaymentAddr1 As String = ""
	Public sPaymentAddrTown As String = ""
	Public sPaymentPostcode As String = ""
	Public sPaymentCounty As String = ""
	Public sPaymentAddress As String = ""
	Public sPaymentTelNo As String = ""
	Public sPaymentEMail As String = ""
	Public sPaymentSendMail As String = ""
	Public sPaymentDisplayReceipt As String = ""
	Public sReturnMethod As String = ""
	Public sReturnURL As String = ""
	Public sPostBackURL As String = ""
	Public sReturnOnFailureURL As String = ""
	Public sReturnOnSuccessURL As String = ""
	Public sOrigReturnURL As String = ""
	Public sOrigPostBackURL As String = ""
	Public sOrigReturnOnFailureURL As String = ""
	Public sOrigReturnOnSuccessURL As String = ""

	Public sSubmissionUID As String = ""

	Public sPaymentAmount As String = ""
	Public sPaymentFundPaymentCode As String = ""
	Public sPaymentCustRef1 As String = ""
	Public sPaymentCustRef2 As String = ""
	Public sPaymentCustRef3 As String = ""
	Public sPaymentCustRef4 As String = ""
	Public sPaymentDescription As String = ""

	Public sPaymentAmount_a1 As String = ""
	Public sPaymentFundPaymentCode_a1 As String = ""
	Public sPaymentCustRef1_a1 As String = ""
	Public sPaymentCustRef2_a1 As String = ""
	Public sPaymentCustRef3_a1 As String = ""
	Public sPaymentCustRef4_a1 As String = ""
	Public sPaymentDescription_a1 As String = ""

	Public sPaymentAmount_a2 As String = ""
	Public sPaymentFundPaymentCode_a2 As String = ""
	Public sPaymentCustRef1_a2 As String = ""
	Public sPaymentCustRef2_a2 As String = ""
	Public sPaymentCustRef3_a2 As String = ""
	Public sPaymentCustRef4_a2 As String = ""
	Public sPaymentDescription_a2 As String = ""

	Public sPaymentAmount_a3 As String = ""
	Public sPaymentFundPaymentCode_a3 As String = ""
	Public sPaymentCustRef1_a3 As String = ""
	Public sPaymentCustRef2_a3 As String = ""
	Public sPaymentCustRef3_a3 As String = ""
	Public sPaymentCustRef4_a3 As String = ""
	Public sPaymentDescription_a3 As String = ""

	Public sPaymentAmount_a4 As String = ""
	Public sPaymentFundPaymentCode_a4 As String = ""
	Public sPaymentCustRef1_a4 As String = ""
	Public sPaymentCustRef2_a4 As String = ""
	Public sPaymentCustRef3_a4 As String = ""
	Public sPaymentCustRef4_a4 As String = ""
	Public sPaymentDescription_a4 As String = ""

	Public sPaymentAmount_a5 As String = ""
	Public sPaymentFundPaymentCode_a5 As String = ""
	Public sPaymentCustRef1_a5 As String = ""
	Public sPaymentCustRef2_a5 As String = ""
	Public sPaymentCustRef3_a5 As String = ""
	Public sPaymentCustRef4_a5 As String = ""
	Public sPaymentDescription_a5 As String = ""

	Public sPaymentAmount_a6 As String = ""
	Public sPaymentFundPaymentCode_a6 As String = ""
	Public sPaymentCustRef1_a6 As String = ""
	Public sPaymentCustRef2_a6 As String = ""
	Public sPaymentCustRef3_a6 As String = ""
	Public sPaymentCustRef4_a6 As String = ""
	Public sPaymentDescription_a6 As String = ""

	Public sPaymentAmount_a7 As String = ""
	Public sPaymentFundPaymentCode_a7 As String = ""
	Public sPaymentCustRef1_a7 As String = ""
	Public sPaymentCustRef2_a7 As String = ""
	Public sPaymentCustRef3_a7 As String = ""
	Public sPaymentCustRef4_a7 As String = ""
	Public sPaymentDescription_a7 As String = ""

	Public sPaymentAmount_a8 As String = ""
	Public sPaymentFundPaymentCode_a8 As String = ""
	Public sPaymentCustRef1_a8 As String = ""
	Public sPaymentCustRef2_a8 As String = ""
	Public sPaymentCustRef3_a8 As String = ""
	Public sPaymentCustRef4_a8 As String = ""
	Public sPaymentDescription_a8 As String = ""

	Public sPaymentAmount_a9 As String = ""
	Public sPaymentFundPaymentCode_a9 As String = ""
	Public sPaymentCustRef1_a9 As String = ""
	Public sPaymentCustRef2_a9 As String = ""
	Public sPaymentCustRef3_a9 As String = ""
	Public sPaymentCustRef4_a9 As String = ""
	Public sPaymentDescription_a9 As String = ""

	Dim oFormSupportCore As New clsFormSupportCore

	'Protected Friend aFieldValues As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

	Private Sub afSupportSubmission_Init(sender As Object, e As EventArgs) Handles Me.Init

		If StringIsTrue(CStr(GetMySettingString("SystemUnderMaintenance"))) Then Response.Redirect(GetMySettingString("MaintenancePage"), True)

	End Sub


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim oTemp As Object, oURL As Uri, sNewURL As String
		Dim iTemp1 As Integer, sTemp As String

		Try

			If StringIsTrue(GetMySettingString("SystemUnderMaintenance")) Then
				Response.Redirect(GetMySettingString("MaintenancePage"), True)
			Else

				If Not IsPostBack Then

					sReturnMethod = GetMySettingString("PaymentReturnMethod")
					sReturnURL = GetMyURLPath(GetMySettingString("PaymentReturnTo"), Page)
					sReturnOnFailureURL = GetMyURLPath(GetMySettingString("PaymentFailReturnTo"), Page)
					sReturnOnSuccessURL = GetMyURLPath(GetMySettingString("PaymentSuccessReturnTo"), Page)
					sPostBackURL = sReturnURL

					oTemp = GetMySetting("DebugShowFieldVals")
					If oTemp IsNot Nothing Then bShowValues = StringIsTrue(oTemp.ToString)

					DoLoadFieldValues()
					DoApplyValuesToFieldnames()

					If sOrigPostBackURL <> "" Then sPostBackURL = sOrigPostBackURL
					If sOrigReturnURL <> "" Then sReturnURL = sOrigReturnURL

					DoPreLoadAttachments()

					DoSaveValuesToTempXMLFile()

					'oTemp = GetMySetting("ReturnFromPayment")
					'If oTemp IsNot Nothing Then
					'    sReturnURL = oTemp.ToString
					'    If sReturnURL.Substring(0, 1) = "~" Then
					'        oURL = Request.Url
					'        sNewURL = oURL.AbsoluteUri
					'        iTemp1 = CInt(IIf(sNewURL.LastIndexOf("?") > 0, sNewURL.LastIndexOf("?"), 0))

					'        If iTemp1 > 0 Then
					'            sNewURL = sNewURL.Substring(0, sNewURL.LastIndexOf("/", iTemp1)) & sReturnURL.Substring(1)
					'        Else
					'            sNewURL = sNewURL.Substring(0, sNewURL.LastIndexOf("/")) & sReturnURL.Substring(1)
					'        End If

					'        sReturnURL = sNewURL
					'    End If
					'End If

				End If
			End If

		Catch ex As Exception

		End Try

	End Sub

	Private Sub DoLoadFieldValues()
		Dim sValue As String, sKey As String, sTemp As String

		Dim oTblRow As TableRow
		Dim oTblCell As TableCell

		Try
			oFormSupportCore.ProcessHTTPRequest(Request, Page, clsFormSupportCore.StagesToDo.FieldsOnly)
			If oFormSupportCore.ConfigDetailsFromXML IsNot Nothing Then sConfigID = oFormSupportCore.ConfigDetailsFromXML.XMLConfigKey


			'If Request.QueryString.Count > 0 Then
			'    For Each sKey In Request.QueryString.Keys
			'        sKey = UCase(sKey)
			'        sValue = Page.Server.UrlDecode(Request.QueryString.Item(sKey))
			'        If Not aFieldValues.ContainsKey(sKey) Then aFieldValues.Add(sKey, sValue) Else aFieldValues.Item(sKey) = sValue
			'    Next
			'End If

			'For Each sKey In Request.Form.Keys
			'    sKey = UCase(sKey)
			'    sValue = Page.Server.UrlDecode(Request.Form.Item(sKey))
			'    If Not aFieldValues.ContainsKey(sKey) Then aFieldValues.Add(sKey, sValue) Else aFieldValues.Item(sKey) = sValue
			'Next


			If tblQueryStringValues IsNot Nothing Then

				If Request.QueryString.Count > 0 Then
					For Each sKey In Request.QueryString.Keys
						oTblRow = New TableRow
						sValue = Me.Server.UrlDecode(Request.QueryString.Item(sKey))
						oTblCell = New TableCell
						oTblCell.Text = HttpUtility.HtmlEncode(sKey)
						oTblRow.Cells.Add(oTblCell)

						oTblCell = New TableCell
						oTblCell.Text = HttpUtility.HtmlEncode(sValue)
						oTblRow.Cells.Add(oTblCell)
						tblQueryStringValues.Rows.Add(oTblRow)

						tblQueryStringValues.Rows.Add(oTblRow)
					Next
				End If
			End If

			If tblPostStringValues IsNot Nothing Then

				If Request.Form.Count > 0 Then
					For Each sKey In Request.Form.Keys
						oTblRow = New TableRow
						sValue = Me.Server.UrlDecode(Request.Form.Item(sKey))
						oTblCell = New TableCell
						oTblCell.Text = HttpUtility.HtmlEncode(sKey)
						oTblRow.Cells.Add(oTblCell)

						oTblCell = New TableCell
						sTemp = sValue
						oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
						oTblRow.Cells.Add(oTblCell)

						tblPostStringValues.Rows.Add(oTblRow)
					Next
				End If
			End If

			If tblConfigValues IsNot Nothing Then

				If oFormSupportCore.ConfigDetailsFromXML.XMLDataValues.Count > 0 Then
					For Each sKey In oFormSupportCore.ConfigDetailsFromXML.XMLDataValues.Keys
						oTblRow = New TableRow
						sValue = Me.Server.UrlDecode(oFormSupportCore.ConfigDetailsFromXML.XMLDataValues.Item(sKey))
						oTblCell = New TableCell
						oTblCell.Text = HttpUtility.HtmlEncode(sKey)
						oTblRow.Cells.Add(oTblCell)

						oTblCell = New TableCell
						sTemp = sValue
						oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
						oTblRow.Cells.Add(oTblCell)

						tblConfigValues.Rows.Add(oTblRow)
					Next
				End If

			End If


		Catch ex As Exception

		End Try
	End Sub


	Private Sub DoPreLoadAttachments()
		Dim sValue As String, sKey As String, sTemp As String

		Dim oTblRow As TableRow
		Dim oTblCell As TableCell

		Try

			oFormSupportCore.ProcessHTTPRequest(Request, Page, clsFormSupportCore.StagesToDo.AttachmentsOnly)


		Catch ex As Exception

		End Try
	End Sub


	Private Sub DoSaveValuesToTempXMLFile()
		Dim oDOM As XmlDocument, oRootElement As XmlElement
		Dim oElement As XmlElement, oValElement As XmlElement, oAttr As XmlAttribute
		Dim sKey As String, sValue As String, aFundCodes As List(Of clsRefFund)
		Dim oTemp As Object, sFileKey As String, sFileFolder As String, sFilePath As String

		Try

			oFormSupportCore.ValuesFromXML.SaveSubmissionValuesToTempXMLFile()

			'oTemp = GetMySetting("TempValueStore")
			'If oTemp IsNot Nothing Then
			'    sFileFolder = oTemp.ToString
			'Else
			'    sFileFolder = IO.Path.GetTempPath
			'End If
			'sFileFolder &= IIf(sFileFolder.Substring(sFileFolder.Length - 1, 1) <> "\", "\", "").ToString

			'oDOM = New XmlDocument
			'oRootElement = oDOM.CreateElement("Details")
			'oDOM.AppendChild(oRootElement)

			'oAttr = oDOM.CreateAttribute("DateCreated")
			'oAttr.Value = Now.ToString("o")
			'oRootElement.Attributes.Append(oAttr)

			'oAttr = oDOM.CreateAttribute("SessionKey")
			'oAttr.Value = sPaymentSessionID
			'oRootElement.Attributes.Append(oAttr)

			'oElement = oDOM.CreateElement("Values")
			'oRootElement.AppendChild(oElement)

			''For Each sKey In aFieldValues.Keys
			''    sValue = aFieldValues.Item(sKey).ToString

			'For Each sKey In oFormSupportCore.ValuesFromHTTPRequest.SubmittedDataValues.Keys
			'    sValue = oFormSupportCore.ValuesFromHTTPRequest.SubmittedDataValues.Item(sKey)
			'    oValElement = oDOM.CreateElement(sKey)
			'    oValElement.InnerText = sValue
			'    oElement.AppendChild(oValElement)
			'Next

			'sFileKey = sPaymentSessionID & "_Values.xml"
			'sFilePath = sFileFolder & IIf(sFileFolder.Substring(sFileFolder.Length - 1, 1) <> "\", "\", "").ToString & sFileKey

			'oDOM.Save(sFilePath)

		Catch ex As Exception

		End Try

	End Sub


	Private Sub DoApplyValuesToFieldnames()
		Dim oAddrRefine As clsAddressRefine, oNameRefine As clsNameParts
		Dim sKey As String, sValue As String
		Dim oFundCodes As clsRefFunds, oFundCode As clsRefFund, aFundCodes As List(Of clsRefFund)
		Try

			oFundCodes = New clsRefFunds
			sPaymentEngineTarget = GetMyURLPath(GetMySettingString("PaymentEngineStartPage"), Page)


			' ==================================================================
			'   Get values from loaded data values identified by name in lists
			' ==================================================================
			' -------------------------------------------------------------------
			' Payment Identity definition
			' -------------------------------------------------------------------
			sPaymentChannel = oFormSupportCore.GetValueIfExists("PaymentChannelValLabels")
			sPaymentSessionID = oFormSupportCore.GetValueIfExists("SessionIDValLabels")
			' -------------------------------------------------------------------
			' Payee name
			' -------------------------------------------------------------------
			sPaymentTitle = oFormSupportCore.GetValueIfExists("PaymentTitleValLabels")
			sPaymentCfName = oFormSupportCore.GetValueIfExists("PaymentForenamesValLabels")
			sPaymentCsName = oFormSupportCore.GetValueIfExists("PaymentSurnameValLabels")
			sPaymentName = oFormSupportCore.GetValueIfExists("PaymentNameValLabels")
			If sPaymentName <> "" Then
				oNameRefine = New clsNameParts(sValue)
				If oNameRefine.OrganisationName <> "" Then
					sPaymentCsName = sValue
				Else
					If sPaymentTitle = "" Then sPaymentTitle = oNameRefine.Title
					If sPaymentCfName = "" Then sPaymentCfName = oNameRefine.Forenames
					If sPaymentCsName = "" Then sPaymentCsName = oNameRefine.Surname
				End If
				sPaymentName = oNameRefine.FullName
			End If
			' -------------------------------------------------------------------
			' Payee address
			' -------------------------------------------------------------------
			sPaymentHouse = oFormSupportCore.GetValueIfExists("PaymentHouseValLabels")
			sPaymentAddr1 = oFormSupportCore.GetValueIfExists("PaymentAddr1ValLabels")
			sPaymentAddrTown = oFormSupportCore.GetValueIfExists("PaymentTownValLabels")
			sPaymentCounty = oFormSupportCore.GetValueIfExists("PaymentCountyValLabels")
			sPaymentPostcode = oFormSupportCore.GetValueIfExists("PaymentPostcodeValLabels")
			sPaymentAddress = oFormSupportCore.GetValueIfExists("PaymentAddressValLabels")
			If sPaymentAddress <> "" Then
				If sPaymentHouse = "" And sPaymentAddr1 = "" And sPaymentAddrTown = "" Then
					oAddrRefine = New clsAddressRefine(sPaymentAddress)
					sPaymentHouse = oAddrRefine.HouseName
					sPaymentAddr1 = Join(oAddrRefine.StreetAddressLines.ToArray, ",")
				End If
			End If
			' -------------------------------------------------------------------
			' Payee contact details
			' -------------------------------------------------------------------
			sPaymentTelNo = oFormSupportCore.GetValueIfExists("PaymentTelNoValLabels")
			sPaymentEMail = oFormSupportCore.GetValueIfExists("PaymentEMailValLabels")
			' -------------------------------------------------------------------
			' Payment processing options
			' -------------------------------------------------------------------
			sPaymentSendMail = oFormSupportCore.GetValueIfExists("PaymentSendMailValLabels")
			sPaymentDisplayReceipt = oFormSupportCore.GetValueIfExists("PaymentDisplayReceiptValLabels")
			' -------------------------------------------------------------------
			' Process return details
			' -------------------------------------------------------------------
			sReturnMethod = oFormSupportCore.GetValueIfExists("PaymentReturnMethodValLabels")
			sReturnURL = oFormSupportCore.GetValueIfExists("PaymentReturnURLValLabels")
			sPostBackURL = oFormSupportCore.GetValueIfExists("PaymentPostBackURLValLabels")
			If sPostBackURL = "" And sReturnURL <> "" Then sPostBackURL = sReturnURL
			sReturnOnFailureURL = oFormSupportCore.GetValueIfExists("PaymentReturnOnFailureURLValLabels")
			sReturnOnSuccessURL = oFormSupportCore.GetValueIfExists("PaymentReturnOnSuccessURLValLabels")
			' -------------------------------------------------------------------
			' Transaction detail default line
			' -------------------------------------------------------------------
			sPaymentAmount = oFormSupportCore.GetValueIfExists("PaymentAmountInPenceValLabels")
			If sPaymentAmount = "" Then
				sPaymentAmount = oFormSupportCore.GetValueIfExists("PaymentAmountInPoundsValLabels")
				If sPaymentAmount <> "" Then sPaymentAmount = CInt(Math.Floor(Val(sPaymentAmount) * 100)).ToString
			End If
			sPaymentFundPaymentCode = oFormSupportCore.GetValueIfExists("PaymentFundCodeValLabels")
			sPaymentCustRef1 = oFormSupportCore.GetValueIfExists("PaymentCustRef1ValLabels")
			sPaymentCustRef2 = oFormSupportCore.GetValueIfExists("PaymentCustRef2ValLabels")
			sPaymentCustRef3 = oFormSupportCore.GetValueIfExists("PaymentCustRef3ValLabels")
			sPaymentCustRef4 = oFormSupportCore.GetValueIfExists("PaymentCustRef4ValLabels")
			sPaymentDescription = oFormSupportCore.GetValueIfExists("PaymentDescriptionValLabels")
			If sPaymentFundPaymentCode <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode = oFundCode.FundPaymentCode
					If sPaymentCustRef1 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1 = oFundCode.CostCode
					If sPaymentDescription = "" And oFundCode.FundName <> "" Then sPaymentDescription = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a1
			' -------------------------------------------------------------------
			sPaymentAmount_a1 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a1ValLabels")
			If sPaymentAmount_a1 = "" Then
				sPaymentAmount_a1 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a1ValLabels")
				If sPaymentAmount_a1 <> "" Then sPaymentAmount_a1 = CInt(Math.Floor(Val(sPaymentAmount_a1) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a1 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a1ValLabels")
			sPaymentCustRef1_a1 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a1ValLabels")
			sPaymentCustRef2_a1 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a1ValLabels")
			sPaymentCustRef3_a1 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a1ValLabels")
			sPaymentCustRef4_a1 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a1ValLabels")
			sPaymentDescription_a1 = oFormSupportCore.GetValueIfExists("PaymentDescription_a1ValLabels")
			If sPaymentFundPaymentCode_a1 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a1)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a1 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a1 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a1 = oFundCode.CostCode
					If sPaymentDescription_a1 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a1 = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a2
			' -------------------------------------------------------------------
			sPaymentAmount_a2 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a2ValLabels")
			If sPaymentAmount_a2 = "" Then
				sPaymentAmount_a2 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a2ValLabels")
				If sPaymentAmount_a2 <> "" Then sPaymentAmount_a2 = CInt(Math.Floor(Val(sPaymentAmount_a2) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a2 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a2ValLabels")
			sPaymentCustRef1_a2 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a2ValLabels")
			sPaymentCustRef2_a2 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a2ValLabels")
			sPaymentCustRef3_a2 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a2ValLabels")
			sPaymentCustRef4_a2 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a2ValLabels")
			sPaymentDescription_a2 = oFormSupportCore.GetValueIfExists("PaymentDescription_a2ValLabels")
			If sPaymentFundPaymentCode_a2 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a2)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a2 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a2 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a2 = oFundCode.CostCode
					If sPaymentDescription_a2 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a2 = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a3
			' -------------------------------------------------------------------
			sPaymentAmount_a3 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a3ValLabels")
			If sPaymentAmount_a3 = "" Then
				sPaymentAmount_a3 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a3ValLabels")
				If sPaymentAmount_a3 <> "" Then sPaymentAmount_a3 = CInt(Math.Floor(Val(sPaymentAmount_a3) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a3 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a3ValLabels")
			sPaymentCustRef1_a3 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a3ValLabels")
			sPaymentCustRef2_a3 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a3ValLabels")
			sPaymentCustRef3_a3 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a3ValLabels")
			sPaymentCustRef4_a3 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a3ValLabels")
			sPaymentDescription_a3 = oFormSupportCore.GetValueIfExists("PaymentDescription_a3ValLabels")
			If sPaymentFundPaymentCode_a3 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a3)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a3 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a3 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a3 = oFundCode.CostCode
					If sPaymentDescription_a3 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a3 = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a4
			' -------------------------------------------------------------------
			sPaymentAmount_a4 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a4ValLabels")
			If sPaymentAmount_a4 = "" Then
				sPaymentAmount_a4 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a4ValLabels")
				If sPaymentAmount_a4 <> "" Then sPaymentAmount_a4 = CInt(Math.Floor(Val(sPaymentAmount_a4) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a4 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a4ValLabels")
			sPaymentCustRef1_a4 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a4ValLabels")
			sPaymentCustRef2_a4 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a4ValLabels")
			sPaymentCustRef3_a4 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a4ValLabels")
			sPaymentCustRef4_a4 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a4ValLabels")
			sPaymentDescription_a4 = oFormSupportCore.GetValueIfExists("PaymentDescription_a4ValLabels")
			If sPaymentFundPaymentCode_a4 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a4)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a4 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a4 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a4 = oFundCode.CostCode
					If sPaymentDescription_a4 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a4 = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a5
			' -------------------------------------------------------------------
			sPaymentAmount_a5 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a5ValLabels")
			If sPaymentAmount_a5 = "" Then
				sPaymentAmount_a5 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a5ValLabels")
				If sPaymentAmount_a5 <> "" Then sPaymentAmount_a5 = CInt(Math.Floor(Val(sPaymentAmount_a5) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a5 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a5ValLabels")
			sPaymentCustRef1_a5 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a5ValLabels")
			sPaymentCustRef2_a5 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a5ValLabels")
			sPaymentCustRef3_a5 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a5ValLabels")
			sPaymentCustRef4_a5 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a5ValLabels")
			sPaymentDescription_a5 = oFormSupportCore.GetValueIfExists("PaymentDescription_a5ValLabels")
			If sPaymentFundPaymentCode_a5 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a5)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a5 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a5 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a5 = oFundCode.CostCode
					If sPaymentDescription_a5 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a5 = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a6
			' -------------------------------------------------------------------
			sPaymentAmount_a6 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a6ValLabels")
			If sPaymentAmount_a6 = "" Then
				sPaymentAmount_a6 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a6ValLabels")
				If sPaymentAmount_a6 <> "" Then sPaymentAmount_a6 = CInt(Math.Floor(Val(sPaymentAmount_a6) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a6 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a6ValLabels")
			sPaymentCustRef1_a6 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a6ValLabels")
			sPaymentCustRef2_a6 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a6ValLabels")
			sPaymentCustRef3_a6 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a6ValLabels")
			sPaymentCustRef4_a6 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a6ValLabels")
			sPaymentDescription_a6 = oFormSupportCore.GetValueIfExists("PaymentDescription_a6ValLabels")
			If sPaymentFundPaymentCode_a6 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a6)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a6 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a6 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a6 = oFundCode.CostCode
					If sPaymentDescription_a6 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a6 = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a7
			' -------------------------------------------------------------------
			sPaymentAmount_a7 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a7ValLabels") : If sPaymentAmount_a7 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a7ValLabels", sPaymentAmount_a7, True)
			If sPaymentAmount_a7 = "" Then
				sPaymentAmount_a7 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a7ValLabels")
				If sPaymentAmount_a7 <> "" Then sPaymentAmount_a7 = CInt(Math.Floor(Val(sPaymentAmount_a7) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a7 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a7ValLabels")
			sPaymentCustRef1_a7 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a7ValLabels")
			sPaymentCustRef2_a7 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a7ValLabels")
			sPaymentCustRef3_a7 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a7ValLabels")
			sPaymentCustRef4_a7 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a7ValLabels")
			sPaymentDescription_a7 = oFormSupportCore.GetValueIfExists("PaymentDescription_a7ValLabels")
			If sPaymentFundPaymentCode_a7 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a7)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a7 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a7 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a7 = oFundCode.CostCode
					If sPaymentDescription_a7 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a7 = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a8
			' -------------------------------------------------------------------
			sPaymentAmount_a8 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a8ValLabels")
			If sPaymentAmount_a8 = "" Then
				sPaymentAmount_a8 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a8ValLabels")
				If sPaymentAmount_a8 <> "" Then sPaymentAmount_a8 = CInt(Math.Floor(Val(sPaymentAmount_a8) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a8 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a8ValLabels")
			sPaymentCustRef1_a8 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a8ValLabels")
			sPaymentCustRef2_a8 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a8ValLabels")
			sPaymentCustRef3_a8 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a8ValLabels")
			sPaymentCustRef4_a8 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a8ValLabels")
			sPaymentDescription_a8 = oFormSupportCore.GetValueIfExists("PaymentDescription_a8ValLabels")
			If sPaymentFundPaymentCode_a8 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a8)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a8 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a8 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a8 = oFundCode.CostCode
					If sPaymentDescription_a8 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a8 = oFundCode.FundName
				End If
			End If
			' -------------------------------------------------------------------
			' Transaction detail extra Line a9
			' -------------------------------------------------------------------
			sPaymentAmount_a9 = oFormSupportCore.GetValueIfExists("PaymentAmountInPence_a9ValLabels")
			If sPaymentAmount_a9 = "" Then
				sPaymentAmount_a9 = oFormSupportCore.GetValueIfExists("PaymentAmountInPounds_a9ValLabels")
				If sPaymentAmount_a9 <> "" Then sPaymentAmount_a9 = CInt(Math.Floor(Val(sPaymentAmount_a9) * 100)).ToString
			End If
			sPaymentFundPaymentCode_a9 = oFormSupportCore.GetValueIfExists("PaymentFundCode_a9ValLabels")
			sPaymentCustRef1_a9 = oFormSupportCore.GetValueIfExists("PaymentCustRef1_a9ValLabels")
			sPaymentCustRef2_a9 = oFormSupportCore.GetValueIfExists("PaymentCustRef2_a9ValLabels")
			sPaymentCustRef3_a9 = oFormSupportCore.GetValueIfExists("PaymentCustRef3_a9ValLabels")
			sPaymentCustRef4_a9 = oFormSupportCore.GetValueIfExists("PaymentCustRef4_a9ValLabels")
			sPaymentDescription_a9 = oFormSupportCore.GetValueIfExists("PaymentDescription_a9ValLabels")
			If sPaymentFundPaymentCode_a9 <> "" Then
				aFundCodes = oFundCodes.FindByFundID(sPaymentFundPaymentCode_a9)
				If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
					oFundCode = aFundCodes(0)
					If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a9 = oFundCode.FundPaymentCode
					If sPaymentCustRef1_a9 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a9 = oFundCode.CostCode
					If sPaymentDescription_a9 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a9 = oFundCode.FundName
				End If
			End If



			' ==================================================================
			'   Identify any extra specific fields passed as form values
			' ==================================================================

			'For Each sKey In oFormSupportCore.oFieldDataUsed.Keys
			'	sValue = oFormSupportCore.oFieldDataUsed.Item(sKey)


			'	Select Case sKey

			'		Case "PaymentChannel", "Channel", "SourceID", "SourceIdentifier", "PaymentSourceID", "PaymentSourceIdentifier" : sPaymentChannel = sValue
			'		Case "PaymentSessionID", "SessionID", "SessionIdentifier", "Session" : sPaymentSessionID = sValue
			'		Case "PaymentTitle", "cTitle", "Title", "PayeeTitle", "PayerTitle", "PayTitle", "CustomerTitle", "CustTitle", "ClientTitle" : If sPaymentTitle = "" Then sPaymentTitle = sValue
			'		Case "PaymentCfName", "CfName", "Forename", "Forenames", "PayeeForename", "PayeeForenames", "PayerForename", "PayerForenames", "PayForename", "PayForenames", "CustomerForename", "CustomerForenames", "CustForename", "CustForenames", "ClientForename", "ClientForenames" : If sPaymentCfName = "" Then sPaymentCfName = sValue
			'		Case "PaymentCsName", "CsName", "Surname", "PayeeSurname", "PayerSurname", "PaySurname", "CustomerSurname", "CustSurname", "ClientSurname" : If sPaymentCsName = "" Then sPaymentCsName = sValue
			'		Case "PaymentHouse", "chouse", "PayeeHouse", "PayerHouse", "PayHouse", "CustomerHouse", "CustHouse", "ClientHouse", "House" : If sPaymentHouse = "" Then sPaymentHouse = sValue
			'		Case "PaymentAddr1", "caddr1", "PayeeAddr1", "PayerAddr1", "PayAddr1", "CustomerAddr1", "CustAddr1", "ClientAddr1", "Addr1", "Street", "StreetName" : sPaymentAddr1 = sValue
			'		Case "PaymentAddrTown", "cTown", "PayeeAddrTown", "PayerAddrTown", "PayAddrTown", "CustomerAddrTown", "CustAddrTown", "ClientAddrTown", "AddrTown", "PayeeTown", "PayerTown", "PayTown", "CustomerTown", "CustTown", "ClientTown", "Town" : If sPaymentAddrTown = "" Then sPaymentAddrTown = sValue
			'		Case "PaymentPostcode", "cPostCode", "PayeePostcode", "PayerPostcode", "PayPostcode", "CustomerPostcode", "CustPostcode", "ClientPostcode", "Postcode" : If sPaymentPostcode = "" Then sPaymentPostcode = sValue
			'		Case "PaymentCounty", "ccounty", "PayeeCounty", "PayerCounty", "PayCounty", "CustomerCounty", "CustCounty", "ClientCounty", "County" : If sPaymentCounty = "" Then sPaymentCounty = sValue
			'		Case "PaymentTelNo", "ctel", "PayeeTelNo", "PayerTelNo", "PayTelNo", "CustomerTelNo", "CustTelNo", "ClientTelNo", "TelNo", "PaymentTelephoneNo", "ctel", "PayeeTelephoneNo", "PayerTelephoneNo", "PayTelephoneNo", "CustomerTelephoneNo", "CustTelephoneNo", "ClientTelephoneNo", "TelephoneNo", "PaymentPhoneNo", "ctel", "PayeePhoneNo", "PayerPhoneNo", "PayPhoneNo", "CustomerPhoneNo", "CustPhoneNo", "ClientPhoneNo", "PhoneNo", "PaymentTelephone", "ctel", "PayeeTelephone", "PayerTelephone", "PayTelephone", "CustomerTelephone", "CustTelephone", "ClientTelephone", "Telephone", "PaymentPhone", "ctel", "PayeePhone", "PayerPhone", "PayPhone", "CustomerPhone", "CustPhone", "ClientPhone", "Phone", "PaymentMobile", "ctel", "PayeeMobile", "PayerMobile", "PayMobile", "CustomerMobile", "CustMobile", "ClientMobile", "Mobile", "PaymentMobileNo", "ctel", "PayeeMobileNo", "PayerMobileNo", "PayMobileNo", "CustomerMobileNo", "CustMobileNo", "ClientMobileNo", "MobileNo" : sPaymentTelNo = sValue
			'		Case "PaymentEMail", "cemail", "PayeeEMail", "PayerEMail", "PayEMail", "CustomerEMail", "CustEMail", "ClientEMail", "EMail" : sPaymentEMail = sValue
			'		Case "PaymentSendMail", "SendMail", "PaymentSendEMail", "SendEMail" : sPaymentSendMail = sValue
			'		Case "PaymentDisplayReceipt", "DisplayReceipt", "SendReceipt", "ShowReceipt", "GenerateReceipt", "Receipt" : sPaymentDisplayReceipt = sValue
			'		Case "PaymentDescription", "PayDescription", "Description" : sPaymentDescription = sValue
			'		Case "ReturnMethod", "ResponseMethod" : sReturnMethod = sValue
			'		Case "ReturnURL", "ReturnTarget", "ResponseReturnURL", "ResponseURL" : sOrigReturnURL = sValue
			'		Case "FailureReturnURL", "FailureReturnTarget", "FailureResponseReturnURL", "FailureResponseURL", "ReturnOnFailureURL", "ReturnOnFailureTarget", "ResponseReturnOnFailureURL", "ResponseOnFailureURL", "FailReturnURL", "FailReturnTarget", "FailResponseReturnURL", "FailResponseURL", "ErrorResponseURL", "ReturnOnFailURL", "ReturnOnFailTarget", "ResponseReturnOnFailURL", "ResponseOnFailURL", "ErrorReturnURL", "ErrorReturnTarget", "ErrorResponseReturnURL", "ErrorResponseURL", "ReturnOnErrorURL", "ReturnOnErrorTarget", "ResponseReturnOnErrorURL", "ResponseOnErrorURL" : sOrigReturnOnFailureURL = sValue
			'		Case "SuccessReturnURL", "SuccessReturnTarget", "SuccessResponseReturnURL", "SuccessResponseURL", "ReturnOnSuccessURL", "ReturnOnSuccessTarget", "ResponseReturnOnSuccessURL", "ResponseOnSuccessURL", "ReturnURLSuccess", "ReturnTargetSuccess", "ResponseReturnURLSuccess", "ResponseURLSuccess", "ReturnSuccessURL", "ReturnSuccessTarget", "ResponseSuccessReturnURL", "ResponseSuccessURL", "OKReturnURL", "OKReturnTarget", "OKResponseReturnURL", "OKResponseURL", "ReturnOnOKURL", "ReturnOnOKTarget", "ResponseReturnOnOKURL", "ResponseOnOKURL", "ReturnURLOK", "ReturnTargetOK", "ResponseReturnURLOK", "ResponseURLOK", "ReturnOKURL", "ReturnOKTarget", "ResponseOKReturnURL", "ResponseOKURL" : sOrigReturnOnSuccessURL = sValue
			'		Case "PostBackURL", "PostBackTarget" : sOrigPostBackURL = sValue

			'		Case "PaymentAddress", "PayeeAddress", "PayerAddress", "PayAddress", "CustomerAddress", "CustAddress", "ClientAddress", "Address", "PaymentAddr", "PayeeAddr", "PayerAddr", "PayAddr", "CustomerAddr", "CustAddr", "ClientAddr", "Addr", "PaymentAdd", "PayeeAdd", "PayerAdd", "PayAdd", "CustomerAdd", "CustAdd", "ClientAdd", "Add", "PaymentAdr", "PayeeAdr", "PayerAdr", "PayAdr", "CustomerAdr", "CustAdr", "ClientAdr", "Adr"
			'			oAddrRefine = New clsAddressRefine(sValue)
			'			sPaymentHouse = Trim(oAddrRefine.HouseName & IIf(oAddrRefine.HouseNum <> "", " " & oAddrRefine.HouseNum, "").ToString)
			'			sPaymentAddr1 = oAddrRefine.StreetName
			'			If sPaymentAddrTown = "" Then sPaymentAddrTown = oAddrRefine.LocalityTown
			'			If sPaymentCounty = "" Then sPaymentCounty = oAddrRefine.County
			'			If oAddrRefine.Postcode <> "" Then sPaymentPostcode = oAddrRefine.Postcode
			'		Case "PaymentName", "PaymentCustName", "PayeeName", "PayerName", "PayName", "CustomerName", "CustName", "Customer", "ClientName", "Client", "Name"
			'			oNameRefine = New clsNameParts(sValue)
			'			If oNameRefine.OrganisationName <> "" Then
			'				sPaymentCsName = sValue
			'			Else

			'				If sPaymentTitle = "" Then sPaymentTitle = oNameRefine.Title
			'				If sPaymentCfName = "" Then sPaymentCfName = oNameRefine.Forenames
			'				If sPaymentCsName = "" Then sPaymentCsName = oNameRefine.Surname
			'			End If

			'					'---------------------------------------------------
			'					' First and normally only transaction details
			'					'---------------------------------------------------

			'		Case "PaymentAmount", "Amount", "AmountInPence", "Amt", "AmtInPence", "Pence" : sPaymentAmount = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount", "Payment", "Pounds", "PoundsPence" : sPaymentAmount = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode", "PaymentFundCode", "FundCode", "FundID", "FundIdentifier", "FundPaymentCode", "TypeOfPayment", "Fund", "PaymentType"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1 = oFundCode.CostCode
			'				If sPaymentDescription = "" And oFundCode.FundName <> "" Then sPaymentDescription = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode = sValue
			'			End If

			'		Case "PaymentCustRef", "CustRef", "PaymentCustomerRef", "CustomerRef", "CustomerReference", "PayRef", "PayReference", "PaymentReference", "Reference", "ReferenceToPay", "RefToPay", "BillRef", "BillingRef", "BillingReference", "PaymentCustRef1", "CustRef1", "PaymentCustomerRef1", "CustomerRef1", "CustomerReference1", "PayRef1", "PayReference1", "PaymentReference1", "Reference1", "BillRef1", "BillingRef1", "BillingReference1" : sPaymentCustRef1 = sValue
			'		Case "PaymentCustRef2", "CustRef2", "PaymentCustomerRef2", "CustomerRef2", "CustomerReference2", "PayRef2", "PayReference2", "PaymentReference2", "Reference2", "BillRef2", "BillingRef2", "BillingReference2" : sPaymentCustRef2 = sValue
			'		Case "PaymentCustRef3", "CustRef3", "PaymentCustomerRef3", "CustomerRef3", "CustomerReference3", "PayRef3", "PayReference3", "PaymentReference3", "Reference3", "BillRef3", "BillingRef3", "BillingReference3" : sPaymentCustRef3 = sValue
			'		Case "PaymentCustRef4", "CustRef4", "PaymentCustomerRef4", "CustomerRef4", "CustomerReference4", "PayRef4", "PayReference4", "PaymentReference4", "Reference4", "BillRef4", "BillingRef4", "BillingReference4" : sPaymentCustRef4 = sValue
			'		Case "PaymentDescription", "PayDescription", "Description" : sPaymentDescription = sValue


			'					'---------------------------------------------------
			'					' Second transaction details
			'					'---------------------------------------------------


			'		Case "PaymentAmount_a1", "Amount_a1", "AmountInPence_a1", "Amt_a1", "AmtInPence_a1", "Pence_a1" : sPaymentAmount_a1 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a1", "Payment_a1", "Pounds_a1", "PoundsPence_a1" : sPaymentAmount_a1 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a1", "PaymentFundCode_a1", "FundCode_a1", "FundID_a1", "FundIdentifier_a1", "FundPaymentCode_a1", "TypeOfPayment_a1", "Fund_a1", "PaymentType_a1"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a1 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a1 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a1 = oFundCode.CostCode
			'				If sPaymentDescription_a1 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a1 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a1 = sValue
			'			End If

			'		Case "PaymentCustRef_a1", "CustRef_a1", "PaymentCustomerRef_a1", "CustomerRef_a1", "CustomerReference_a1", "PayRef_a1", "PayReference_a1", "PaymentReference_a1", "Reference_a1", "ReferenceToPay_a1", "RefToPay_a1", "BillRef_a1", "BillingRef_a1", "BillingReference_a1", "PaymentCustRef1_a1", "CustRef1_a1", "PaymentCustomerRef1_a1", "CustomerRef1_a1", "CustomerReference1_a1", "PayRef1_a1", "PayReference1_a1", "PaymentReference1_a1", "Reference1_a1", "BillRef1_a1", "BillingRef1_a1", "BillingReference1_a1" : sPaymentCustRef1_a1 = sValue
			'		Case "PaymentCustRef2_a1", "CustRef2_a1", "PaymentCustomerRef2_a1", "CustomerRef2_a1", "CustomerReference2_a1", "PayRef2_a1", "PayReference2_a1", "PaymentReference2_a1", "Reference2_a1", "BillRef2_a1", "BillingRef2_a1", "BillingReference2_a1" : sPaymentCustRef2_a1 = sValue
			'		Case "PaymentCustRef3_a1", "CustRef3_a1", "PaymentCustomerRef3_a1", "CustomerRef3_a1", "CustomerReference3_a1", "PayRef3_a1", "PayReference3_a1", "PaymentReference3_a1", "Reference3_a1", "BillRef3_a1", "BillingRef3_a1", "BillingReference3_a1" : sPaymentCustRef3_a1 = sValue
			'		Case "PaymentCustRef4_a1", "CustRef4_a1", "PaymentCustomerRef4_a1", "CustomerRef4_a1", "CustomerReference4_a1", "PayRef4_a1", "PayReference4_a1", "PaymentReference4_a1", "Reference4_a1", "BillRef4_a1", "BillingRef4_a1", "BillingReference4_a1" : sPaymentCustRef4_a1 = sValue
			'		Case "PaymentDescription_a1", "PayDescription_a1", "Description_a1" : sPaymentDescription_a1 = sValue


			'					'---------------------------------------------------
			'					' Third transaction details
			'					'---------------------------------------------------


			'		Case "PaymentAmount_a2", "Amount_a2", "AmountInPence_a2", "Amt_a2", "AmtInPence_a2", "Pence_a2" : sPaymentAmount_a2 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a2", "Payment_a2", "Pounds_a2", "PoundsPence_a2" : sPaymentAmount_a2 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a2", "PaymentFundCode_a2", "FundCode_a2", "FundID_a2", "FundIdentifier_a2", "FundPaymentCode_a2", "TypeOfPayment_a2", "Fund_a2", "PaymentType_a2"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a2 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a2 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a2 = oFundCode.CostCode
			'				If sPaymentDescription_a2 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a2 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a2 = sValue
			'			End If

			'		Case "PaymentCustRef_a2", "CustRef_a2", "PaymentCustomerRef_a2", "CustomerRef_a2", "CustomerReference_a2", "PayRef_a2", "PayReference_a2", "PaymentReference_a2", "Reference_a2", "ReferenceToPay_a2", "RefToPay_a2", "BillRef_a2", "BillingRef_a2", "BillingReference_a2", "PaymentCustRef1_a2", "CustRef1_a2", "PaymentCustomerRef1_a2", "CustomerRef1_a2", "CustomerReference1_a2", "PayRef1_a2", "PayReference1_a2", "PaymentReference1_a2", "Reference1_a2", "BillRef1_a2", "BillingRef1_a2", "BillingReference1_a2" : sPaymentCustRef1_a2 = sValue
			'		Case "PaymentCustRef2_a2", "CustRef2_a2", "PaymentCustomerRef2_a2", "CustomerRef2_a2", "CustomerReference2_a2", "PayRef2_a2", "PayReference2_a2", "PaymentReference2_a2", "Reference2_a2", "BillRef2_a2", "BillingRef2_a2", "BillingReference2_a2" : sPaymentCustRef2_a2 = sValue
			'		Case "PaymentCustRef3_a2", "CustRef3_a2", "PaymentCustomerRef3_a2", "CustomerRef3_a2", "CustomerReference3_a2", "PayRef3_a2", "PayReference3_a2", "PaymentReference3_a2", "Reference3_a2", "BillRef3_a2", "BillingRef3_a2", "BillingReference3_a2" : sPaymentCustRef3_a2 = sValue
			'		Case "PaymentCustRef4_a2", "CustRef4_a2", "PaymentCustomerRef4_a2", "CustomerRef4_a2", "CustomerReference4_a2", "PayRef4_a2", "PayReference4_a2", "PaymentReference4_a2", "Reference4_a2", "BillRef4_a2", "BillingRef4_a2", "BillingReference4_a2" : sPaymentCustRef4_a2 = sValue
			'		Case "PaymentDescription_a2", "PayDescription_a2", "Description_a2" : sPaymentDescription_a2 = sValue


			'					'---------------------------------------------------
			'					' Fourth transaction details
			'					'---------------------------------------------------


			'		Case "PaymentAmount_a3", "Amount_a3", "AmountInPence_a3", "Amt_a3", "AmtInPence_a3", "Pence_a3" : sPaymentAmount_a3 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a3", "Payment_a3", "Pounds_a3", "PoundsPence_a3" : sPaymentAmount_a3 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a3", "PaymentFundCode_a3", "FundCode_a3", "FundID_a3", "FundIdentifier_a3", "FundPaymentCode_a3", "TypeOfPayment_a3", "Fund_a3", "PaymentType_a3"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a3 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a3 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a3 = oFundCode.CostCode
			'				If sPaymentDescription_a3 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a3 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a3 = sValue
			'			End If

			'		Case "PaymentCustRef_a3", "CustRef_a3", "PaymentCustomerRef_a3", "CustomerRef_a3", "CustomerReference_a3", "PayRef_a3", "PayReference_a3", "PaymentReference_a3", "Reference_a3", "ReferenceToPay_a3", "RefToPay_a3", "BillRef_a3", "BillingRef_a3", "BillingReference_a3", "PaymentCustRef1_a3", "CustRef1_a3", "PaymentCustomerRef1_a3", "CustomerRef1_a3", "CustomerReference1_a3", "PayRef1_a3", "PayReference1_a3", "PaymentReference1_a3", "Reference1_a3", "BillRef1_a3", "BillingRef1_a3", "BillingReference1_a3" : sPaymentCustRef1_a3 = sValue
			'		Case "PaymentCustRef2_a3", "CustRef2_a3", "PaymentCustomerRef2_a3", "CustomerRef2_a3", "CustomerReference2_a3", "PayRef2_a3", "PayReference2_a3", "PaymentReference2_a3", "Reference2_a3", "BillRef2_a3", "BillingRef2_a3", "BillingReference2_a3" : sPaymentCustRef2_a3 = sValue
			'		Case "PaymentCustRef3_a3", "CustRef3_a3", "PaymentCustomerRef3_a3", "CustomerRef3_a3", "CustomerReference3_a3", "PayRef3_a3", "PayReference3_a3", "PaymentReference3_a3", "Reference3_a3", "BillRef3_a3", "BillingRef3_a3", "BillingReference3_a3" : sPaymentCustRef3_a3 = sValue
			'		Case "PaymentCustRef4_a3", "CustRef4_a3", "PaymentCustomerRef4_a3", "CustomerRef4_a3", "CustomerReference4_a3", "PayRef4_a3", "PayReference4_a3", "PaymentReference4_a3", "Reference4_a3", "BillRef4_a3", "BillingRef4_a3", "BillingReference4_a3" : sPaymentCustRef4_a3 = sValue
			'		Case "PaymentDescription_a3", "PayDescription_a3", "Description_a3" : sPaymentDescription_a3 = sValue


			'					'---------------------------------------------------
			'					' Fifth transaction details
			'					'---------------------------------------------------

			'		Case "PaymentAmount_a4", "Amount_a4", "AmountInPence_a4", "Amt_a4", "AmtInPence_a4", "Pence_a4" : sPaymentAmount_a4 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a4", "Payment_a4", "Pounds_a4", "PoundsPence_a4" : sPaymentAmount_a4 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a4", "PaymentFundCode_a4", "FundCode_a4", "FundID_a4", "FundIdentifier_a4", "FundPaymentCode_a4", "TypeOfPayment_a4", "Fund_a4", "PaymentType_a4"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a4 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a4 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a4 = oFundCode.CostCode
			'				If sPaymentDescription_a4 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a4 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a4 = sValue
			'			End If

			'		Case "PaymentCustRef_a4", "CustRef_a4", "PaymentCustomerRef_a4", "CustomerRef_a4", "CustomerReference_a4", "PayRef_a4", "PayReference_a4", "PaymentReference_a4", "Reference_a4", "ReferenceToPay_a4", "RefToPay_a4", "BillRef_a4", "BillingRef_a4", "BillingReference_a4", "PaymentCustRef1_a4", "CustRef1_a4", "PaymentCustomerRef1_a4", "CustomerRef1_a4", "CustomerReference1_a4", "PayRef1_a4", "PayReference1_a4", "PaymentReference1_a4", "Reference1_a4", "BillRef1_a4", "BillingRef1_a4", "BillingReference1_a4" : sPaymentCustRef1_a4 = sValue
			'		Case "PaymentCustRef2_a4", "CustRef2_a4", "PaymentCustomerRef2_a4", "CustomerRef2_a4", "CustomerReference2_a4", "PayRef2_a4", "PayReference2_a4", "PaymentReference2_a4", "Reference2_a4", "BillRef2_a4", "BillingRef2_a4", "BillingReference2_a4" : sPaymentCustRef2_a4 = sValue
			'		Case "PaymentCustRef3_a4", "CustRef3_a4", "PaymentCustomerRef3_a4", "CustomerRef3_a4", "CustomerReference3_a4", "PayRef3_a4", "PayReference3_a4", "PaymentReference3_a4", "Reference3_a4", "BillRef3_a4", "BillingRef3_a4", "BillingReference3_a4" : sPaymentCustRef3_a4 = sValue
			'		Case "PaymentCustRef4_a4", "CustRef4_a4", "PaymentCustomerRef4_a4", "CustomerRef4_a4", "CustomerReference4_a4", "PayRef4_a4", "PayReference4_a4", "PaymentReference4_a4", "Reference4_a4", "BillRef4_a4", "BillingRef4_a4", "BillingReference4_a4" : sPaymentCustRef4_a4 = sValue
			'		Case "PaymentDescription_a4", "PayDescription_a4", "Description_a4" : sPaymentDescription_a4 = sValue


			'					'---------------------------------------------------
			'					' Sixth transaction details
			'					'---------------------------------------------------


			'		Case "PaymentAmount_a5", "Amount_a5", "AmountInPence_a5", "Amt_a5", "AmtInPence_a5", "Pence_a5" : sPaymentAmount_a5 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a5", "Payment_a5", "Pounds_a5", "PoundsPence_a5" : sPaymentAmount_a5 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a5", "PaymentFundCode_a5", "FundCode_a5", "FundID_a5", "FundIdentifier_a5", "FundPaymentCode_a5", "TypeOfPayment_a5", "Fund_a5", "PaymentType_a5"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a5 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a5 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a5 = oFundCode.CostCode
			'				If sPaymentDescription_a5 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a5 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a5 = sValue
			'			End If

			'		Case "PaymentCustRef_a5", "CustRef_a5", "PaymentCustomerRef_a5", "CustomerRef_a5", "CustomerReference_a5", "PayRef_a5", "PayReference_a5", "PaymentReference_a5", "Reference_a5", "ReferenceToPay_a5", "RefToPay_a5", "BillRef_a5", "BillingRef_a5", "BillingReference_a5", "PaymentCustRef1_a5", "CustRef1_a5", "PaymentCustomerRef1_a5", "CustomerRef1_a5", "CustomerReference1_a5", "PayRef1_a5", "PayReference1_a5", "PaymentReference1_a5", "Reference1_a5", "BillRef1_a5", "BillingRef1_a5", "BillingReference1_a5" : sPaymentCustRef1_a5 = sValue
			'		Case "PaymentCustRef2_a5", "CustRef2_a5", "PaymentCustomerRef2_a5", "CustomerRef2_a5", "CustomerReference2_a5", "PayRef2_a5", "PayReference2_a5", "PaymentReference2_a5", "Reference2_a5", "BillRef2_a5", "BillingRef2_a5", "BillingReference2_a5" : sPaymentCustRef2_a5 = sValue
			'		Case "PaymentCustRef3_a5", "CustRef3_a5", "PaymentCustomerRef3_a5", "CustomerRef3_a5", "CustomerReference3_a5", "PayRef3_a5", "PayReference3_a5", "PaymentReference3_a5", "Reference3_a5", "BillRef3_a5", "BillingRef3_a5", "BillingReference3_a5" : sPaymentCustRef3_a5 = sValue
			'		Case "PaymentCustRef4_a5", "CustRef4_a5", "PaymentCustomerRef4_a5", "CustomerRef4_a5", "CustomerReference4_a5", "PayRef4_a5", "PayReference4_a5", "PaymentReference4_a5", "Reference4_a5", "BillRef4_a5", "BillingRef4_a5", "BillingReference4_a5" : sPaymentCustRef4_a5 = sValue
			'		Case "PaymentDescription_a5", "PayDescription_a5", "Description_a5" : sPaymentDescription_a5 = sValue


			'					'---------------------------------------------------
			'					' Seventh transaction details
			'					'---------------------------------------------------


			'		Case "PaymentAmount_a6", "Amount_a6", "AmountInPence_a6", "Amt_a6", "AmtInPence_a6", "Pence_a6" : sPaymentAmount_a6 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a6", "Payment_a6", "Pounds_a6", "PoundsPence_a6" : sPaymentAmount_a6 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a6", "PaymentFundCode_a6", "FundCode_a6", "FundID_a6", "FundIdentifier_a6", "FundPaymentCode_a6", "TypeOfPayment_a6", "Fund_a6", "PaymentType_a6"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a6 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a6 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a6 = oFundCode.CostCode
			'				If sPaymentDescription_a6 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a6 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a6 = sValue
			'			End If

			'		Case "PaymentCustRef_a6", "CustRef_a6", "PaymentCustomerRef_a6", "CustomerRef_a6", "CustomerReference_a6", "PayRef_a6", "PayReference_a6", "PaymentReference_a6", "Reference_a6", "ReferenceToPay_a6", "RefToPay_a6", "BillRef_a6", "BillingRef_a6", "BillingReference_a6", "PaymentCustRef1_a6", "CustRef1_a6", "PaymentCustomerRef1_a6", "CustomerRef1_a6", "CustomerReference1_a6", "PayRef1_a6", "PayReference1_a6", "PaymentReference1_a6", "Reference1_a6", "BillRef1_a6", "BillingRef1_a6", "BillingReference1_a6" : sPaymentCustRef1_a6 = sValue
			'		Case "PaymentCustRef2_a6", "CustRef2_a6", "PaymentCustomerRef2_a6", "CustomerRef2_a6", "CustomerReference2_a6", "PayRef2_a6", "PayReference2_a6", "PaymentReference2_a6", "Reference2_a6", "BillRef2_a6", "BillingRef2_a6", "BillingReference2_a6" : sPaymentCustRef2_a6 = sValue
			'		Case "PaymentCustRef3_a6", "CustRef3_a6", "PaymentCustomerRef3_a6", "CustomerRef3_a6", "CustomerReference3_a6", "PayRef3_a6", "PayReference3_a6", "PaymentReference3_a6", "Reference3_a6", "BillRef3_a6", "BillingRef3_a6", "BillingReference3_a6" : sPaymentCustRef3_a6 = sValue
			'		Case "PaymentCustRef4_a6", "CustRef4_a6", "PaymentCustomerRef4_a6", "CustomerRef4_a6", "CustomerReference4_a6", "PayRef4_a6", "PayReference4_a6", "PaymentReference4_a6", "Reference4_a6", "BillRef4_a6", "BillingRef4_a6", "BillingReference4_a6" : sPaymentCustRef4_a6 = sValue
			'		Case "PaymentDescription_a6", "PayDescription_a6", "Description_a6" : sPaymentDescription_a6 = sValue


			'					'---------------------------------------------------
			'					' Eightth transaction details
			'					'---------------------------------------------------


			'		Case "PaymentAmount_a7", "Amount_a7", "AmountInPence_a7", "Amt_a7", "AmtInPence_a7", "Pence_a7" : sPaymentAmount_a7 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a7", "Payment_a7", "Pounds_a7", "PoundsPence_a7" : sPaymentAmount_a7 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a7", "PaymentFundCode_a7", "FundCode_a7", "FundID_a7", "FundIdentifier_a7", "FundPaymentCode_a7", "TypeOfPayment_a7", "Fund_a7", "PaymentType_a7"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a7 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a7 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a7 = oFundCode.CostCode
			'				If sPaymentDescription_a7 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a7 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a7 = sValue
			'			End If

			'		Case "PaymentCustRef_a7", "CustRef_a7", "PaymentCustomerRef_a7", "CustomerRef_a7", "CustomerReference_a7", "PayRef_a7", "PayReference_a7", "PaymentReference_a7", "Reference_a7", "ReferenceToPay_a7", "RefToPay_a7", "BillRef_a7", "BillingRef_a7", "BillingReference_a7", "PaymentCustRef1_a7", "CustRef1_a7", "PaymentCustomerRef1_a7", "CustomerRef1_a7", "CustomerReference1_a7", "PayRef1_a7", "PayReference1_a7", "PaymentReference1_a7", "Reference1_a7", "BillRef1_a7", "BillingRef1_a7", "BillingReference1_a7" : sPaymentCustRef1_a7 = sValue
			'		Case "PaymentCustRef2_a7", "CustRef2_a7", "PaymentCustomerRef2_a7", "CustomerRef2_a7", "CustomerReference2_a7", "PayRef2_a7", "PayReference2_a7", "PaymentReference2_a7", "Reference2_a7", "BillRef2_a7", "BillingRef2_a7", "BillingReference2_a7" : sPaymentCustRef2_a7 = sValue
			'		Case "PaymentCustRef3_a7", "CustRef3_a7", "PaymentCustomerRef3_a7", "CustomerRef3_a7", "CustomerReference3_a7", "PayRef3_a7", "PayReference3_a7", "PaymentReference3_a7", "Reference3_a7", "BillRef3_a7", "BillingRef3_a7", "BillingReference3_a7" : sPaymentCustRef3_a7 = sValue
			'		Case "PaymentCustRef4_a7", "CustRef4_a7", "PaymentCustomerRef4_a7", "CustomerRef4_a7", "CustomerReference4_a7", "PayRef4_a7", "PayReference4_a7", "PaymentReference4_a7", "Reference4_a7", "BillRef4_a7", "BillingRef4_a7", "BillingReference4_a7" : sPaymentCustRef4_a7 = sValue
			'		Case "PaymentDescription_a7", "PayDescription_a7", "Description_a7" : sPaymentDescription_a7 = sValue


			'					'---------------------------------------------------
			'					' Ninth transaction details
			'					'---------------------------------------------------


			'		Case "PaymentAmount_a8", "Amount_a8", "AmountInPence_a8", "Amt_a8", "AmtInPence_a8", "Pence_a8" : sPaymentAmount_a8 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a8", "Payment_a8", "Pounds_a8", "PoundsPence_a8" : sPaymentAmount_a8 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a8", "PaymentFundCode_a8", "FundCode_a8", "FundID_a8", "FundIdentifier_a8", "FundPaymentCode_a8", "TypeOfPayment_a8", "Fund_a8", "PaymentType_a8"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a8 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a8 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a8 = oFundCode.CostCode
			'				If sPaymentDescription_a8 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a8 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a8 = sValue
			'			End If

			'		Case "PaymentCustRef_a8", "CustRef_a8", "PaymentCustomerRef_a8", "CustomerRef_a8", "CustomerReference_a8", "PayRef_a8", "PayReference_a8", "PaymentReference_a8", "Reference_a8", "ReferenceToPay_a8", "RefToPay_a8", "BillRef_a8", "BillingRef_a8", "BillingReference_a8", "PaymentCustRef1_a8", "CustRef1_a8", "PaymentCustomerRef1_a8", "CustomerRef1_a8", "CustomerReference1_a8", "PayRef1_a8", "PayReference1_a8", "PaymentReference1_a8", "Reference1_a8", "BillRef1_a8", "BillingRef1_a8", "BillingReference1_a8" : sPaymentCustRef1_a8 = sValue
			'		Case "PaymentCustRef2_a8", "CustRef2_a8", "PaymentCustomerRef2_a8", "CustomerRef2_a8", "CustomerReference2_a8", "PayRef2_a8", "PayReference2_a8", "PaymentReference2_a8", "Reference2_a8", "BillRef2_a8", "BillingRef2_a8", "BillingReference2_a8" : sPaymentCustRef2_a8 = sValue
			'		Case "PaymentCustRef3_a8", "CustRef3_a8", "PaymentCustomerRef3_a8", "CustomerRef3_a8", "CustomerReference3_a8", "PayRef3_a8", "PayReference3_a8", "PaymentReference3_a8", "Reference3_a8", "BillRef3_a8", "BillingRef3_a8", "BillingReference3_a8" : sPaymentCustRef3_a8 = sValue
			'		Case "PaymentCustRef4_a8", "CustRef4_a8", "PaymentCustomerRef4_a8", "CustomerRef4_a8", "CustomerReference4_a8", "PayRef4_a8", "PayReference4_a8", "PaymentReference4_a8", "Reference4_a8", "BillRef4_a8", "BillingRef4_a8", "BillingReference4_a8" : sPaymentCustRef4_a8 = sValue
			'		Case "PaymentDescription_a8", "PayDescription_a8", "Description_a8" : sPaymentDescription_a8 = sValue


			'					'---------------------------------------------------
			'					' Tenth transaction details
			'					'---------------------------------------------------


			'		Case "PaymentAmount_a9", "Amount_a9", "AmountInPence_a9", "Amt_a9", "AmtInPence_a9", "Pence_a9" : sPaymentAmount_a9 = CInt(Math.Floor(Val(sValue.Replace("£", "")))).ToString
			'		Case "PayAmount_a9", "Payment_a9", "Pounds_a9", "PoundsPence_a9" : sPaymentAmount_a9 = CInt(Math.Floor(Val(sValue.Replace("£", "")) * 100)).ToString
			'		Case "PaymentFundPaymentCode_a9", "PaymentFundCode_a9", "FundCode_a9", "FundID_a9", "FundIdentifier_a9", "FundPaymentCode_a9", "TypeOfPayment_a9", "Fund_a9", "PaymentType_a9"
			'			aFundCodes = oFundCodes.FindByFundID(sValue)

			'			If aFundCodes IsNot Nothing AndAlso aFundCodes.Count > 0 Then
			'				oFundCode = aFundCodes(0)
			'				If oFundCode.FundPaymentCode <> "" Then sPaymentFundPaymentCode_a9 = oFundCode.FundPaymentCode
			'				If sPaymentCustRef1_a9 = "" And oFundCode.CostCode <> "" Then sPaymentCustRef1_a9 = oFundCode.CostCode
			'				If sPaymentDescription_a9 = "" And oFundCode.FundName <> "" Then sPaymentDescription_a9 = oFundCode.FundName
			'			Else
			'				sPaymentFundPaymentCode_a9 = sValue
			'			End If

			'		Case "PaymentCustRef_a9", "CustRef_a9", "PaymentCustomerRef_a9", "CustomerRef_a9", "CustomerReference_a9", "PayRef_a9", "PayReference_a9", "PaymentReference_a9", "Reference_a9", "ReferenceToPay_a9", "RefToPay_a9", "BillRef_a9", "BillingRef_a9", "BillingReference_a9", "PaymentCustRef1_a9", "CustRef1_a9", "PaymentCustomerRef1_a9", "CustomerRef1_a9", "CustomerReference1_a9", "PayRef1_a9", "PayReference1_a9", "PaymentReference1_a9", "Reference1_a9", "BillRef1_a9", "BillingRef1_a9", "BillingReference1_a9" : sPaymentCustRef1_a9 = sValue
			'		Case "PaymentCustRef2_a9", "CustRef2_a9", "PaymentCustomerRef2_a9", "CustomerRef2_a9", "CustomerReference2_a9", "PayRef2_a9", "PayReference2_a9", "PaymentReference2_a9", "Reference2_a9", "BillRef2_a9", "BillingRef2_a9", "BillingReference2_a9" : sPaymentCustRef2_a9 = sValue
			'		Case "PaymentCustRef3_a9", "CustRef3_a9", "PaymentCustomerRef3_a9", "CustomerRef3_a9", "CustomerReference3_a9", "PayRef3_a9", "PayReference3_a9", "PaymentReference3_a9", "Reference3_a9", "BillRef3_a9", "BillingRef3_a9", "BillingReference3_a9" : sPaymentCustRef3_a9 = sValue
			'		Case "PaymentCustRef4_a9", "CustRef4_a9", "PaymentCustomerRef4_a9", "CustomerRef4_a9", "CustomerReference4_a9", "PayRef4_a9", "PayReference4_a9", "PaymentReference4_a9", "Reference4_a9", "BillRef4_a9", "BillingRef4_a9", "BillingReference4_a9" : sPaymentCustRef4_a9 = sValue
			'		Case "PaymentDescription_a9", "PayDescription_a9", "Description_a9" : sPaymentDescription_a9 = sValue


			'	End Select

			'Next

			' ==================================================================
			'   Apply values back to field name groups
			' ==================================================================
			' -------------------------------------------------------------------
			' Payment Identity definition
			' -------------------------------------------------------------------
			If sPaymentChannel <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentChannelValLabels", sPaymentChannel, True)
			If sPaymentSessionID <> "" Then oFormSupportCore.ApplyValueIfExists("SessionIDValLabels", sPaymentSessionID, True)
			' -------------------------------------------------------------------
			' Payee name
			' -------------------------------------------------------------------
			If sPaymentTitle <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentTitleValLabels", sPaymentTitle, True)
			If sPaymentCfName <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentForenamesValLabels", sPaymentCfName, True)
			If sPaymentCsName <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentSurnameValLabels", sPaymentCsName, True)
			If sPaymentName <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentNameValLabels", sPaymentName, True)
			' -------------------------------------------------------------------
			' Payee address
			' -------------------------------------------------------------------
			If sPaymentHouse <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentHouseValLabels", sPaymentHouse, True)
			If sPaymentAddr1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAddr1ValLabels", sPaymentAddr1, True)
			If sPaymentAddrTown <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentTownValLabels", sPaymentAddrTown, True)
			If sPaymentCounty <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCountyValLabels", sPaymentCounty, True)
			If sPaymentPostcode <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentPostcodeValLabels", sPaymentPostcode, True)
			If sPaymentAddress <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAddressValLabels", sPaymentAddress, True)
			' -------------------------------------------------------------------
			' Payee contact details
			' -------------------------------------------------------------------
			If sPaymentTelNo <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentTelNoValLabels", sPaymentTelNo, True)
			If sPaymentEMail <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentEMailValLabels", sPaymentEMail, True)
			' -------------------------------------------------------------------
			' Payment processing options
			' -------------------------------------------------------------------
			If sPaymentSendMail <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentSendMailValLabels", sPaymentSendMail, True)
			If sPaymentDisplayReceipt <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDisplayReceiptValLabels", sPaymentDisplayReceipt, True)
			' -------------------------------------------------------------------
			' Process return details
			' -------------------------------------------------------------------
			If sReturnMethod <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentReturnMethodValLabels", sReturnMethod, True)
			If sReturnURL <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentReturnURLValLabels", sReturnURL, True)
			If sPostBackURL <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentPostBackURLValLabels", sPostBackURL, True)
			If sReturnOnFailureURL <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentReturnOnFailureURLValLabels", sReturnOnFailureURL, True)
			If sReturnOnSuccessURL <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentReturnOnSuccessURLValLabels", sReturnOnSuccessURL, True)
			' -------------------------------------------------------------------
			' Transaction detail default line
			' -------------------------------------------------------------------
			If sPaymentAmount <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPenceValLabels", sPaymentAmount, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPoundsValLabels", CDbl(Val(sPaymentAmount) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCodeValLabels", sPaymentFundPaymentCode, True)
			If sPaymentCustRef1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1ValLabels", sPaymentCustRef1, True)
			If sPaymentCustRef2 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2ValLabels", sPaymentCustRef2, True)
			If sPaymentCustRef3 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3ValLabels", sPaymentCustRef3, True)
			If sPaymentCustRef4 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4ValLabels", sPaymentCustRef4, True)
			If sPaymentDescription <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescriptionValLabels", sPaymentDescription, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a1
			' -------------------------------------------------------------------
			If sPaymentAmount_a1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a1ValLabels", sPaymentAmount_a1, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a1ValLabels", CDbl(Val(sPaymentAmount_a1) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a1ValLabels", sPaymentFundPaymentCode_a1, True)
			If sPaymentCustRef1_a1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a1ValLabels", sPaymentCustRef1_a1, True)
			If sPaymentCustRef2_a1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a1ValLabels", sPaymentCustRef2_a1, True)
			If sPaymentCustRef3_a1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a1ValLabels", sPaymentCustRef3_a1, True)
			If sPaymentCustRef4_a1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a1ValLabels", sPaymentCustRef4_a1, True)
			If sPaymentDescription_a1 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a1ValLabels", sPaymentDescription_a1, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a2
			' -------------------------------------------------------------------
			If sPaymentAmount_a2 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a2ValLabels", sPaymentAmount_a2, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a2ValLabels", CDbl(Val(sPaymentAmount_a2) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a2 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a2ValLabels", sPaymentFundPaymentCode_a2, True)
			If sPaymentCustRef1_a2 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a2ValLabels", sPaymentCustRef1_a2, True)
			If sPaymentCustRef2_a2 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a2ValLabels", sPaymentCustRef2_a2, True)
			If sPaymentCustRef3_a2 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a2ValLabels", sPaymentCustRef3_a2, True)
			If sPaymentCustRef4_a2 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a2ValLabels", sPaymentCustRef4_a2, True)
			If sPaymentDescription_a2 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a2ValLabels", sPaymentDescription_a2, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a3
			' -------------------------------------------------------------------
			If sPaymentAmount_a3 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a3ValLabels", sPaymentAmount_a3, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a3ValLabels", CDbl(Val(sPaymentAmount_a3) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a3 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a3ValLabels", sPaymentFundPaymentCode_a3, True)
			If sPaymentCustRef1_a3 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a3ValLabels", sPaymentCustRef1_a3, True)
			If sPaymentCustRef2_a3 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a3ValLabels", sPaymentCustRef2_a3, True)
			If sPaymentCustRef3_a3 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a3ValLabels", sPaymentCustRef3_a3, True)
			If sPaymentCustRef4_a3 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a3ValLabels", sPaymentCustRef4_a3, True)
			If sPaymentDescription_a3 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a3ValLabels", sPaymentDescription_a3, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a4
			' -------------------------------------------------------------------
			If sPaymentAmount_a4 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a4ValLabels", sPaymentAmount_a4, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a4ValLabels", CDbl(Val(sPaymentAmount_a4) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a4 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a4ValLabels", sPaymentFundPaymentCode_a4, True)
			If sPaymentCustRef1_a4 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a4ValLabels", sPaymentCustRef1_a4, True)
			If sPaymentCustRef2_a4 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a4ValLabels", sPaymentCustRef2_a4, True)
			If sPaymentCustRef3_a4 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a4ValLabels", sPaymentCustRef3_a4, True)
			If sPaymentCustRef4_a4 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a4ValLabels", sPaymentCustRef4_a4, True)
			If sPaymentDescription_a4 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a4ValLabels", sPaymentDescription_a4, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a5
			' -------------------------------------------------------------------
			If sPaymentAmount_a5 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a5ValLabels", sPaymentAmount_a5, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a5ValLabels", CDbl(Val(sPaymentAmount_a5) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a5 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a5ValLabels", sPaymentFundPaymentCode_a5, True)
			If sPaymentCustRef1_a5 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a5ValLabels", sPaymentCustRef1_a5, True)
			If sPaymentCustRef2_a5 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a5ValLabels", sPaymentCustRef2_a5, True)
			If sPaymentCustRef3_a5 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a5ValLabels", sPaymentCustRef3_a5, True)
			If sPaymentCustRef4_a5 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a5ValLabels", sPaymentCustRef4_a5, True)
			If sPaymentDescription_a5 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a5ValLabels", sPaymentDescription_a5, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a6
			' -------------------------------------------------------------------
			If sPaymentAmount_a6 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a6ValLabels", sPaymentAmount_a6, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a6ValLabels", CDbl(Val(sPaymentAmount_a6) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a6 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a6ValLabels", sPaymentFundPaymentCode_a6, True)
			If sPaymentCustRef1_a6 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a6ValLabels", sPaymentCustRef1_a6, True)
			If sPaymentCustRef2_a6 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a6ValLabels", sPaymentCustRef2_a6, True)
			If sPaymentCustRef3_a6 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a6ValLabels", sPaymentCustRef3_a6, True)
			If sPaymentCustRef4_a6 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a6ValLabels", sPaymentCustRef4_a6, True)
			If sPaymentDescription_a6 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a6ValLabels", sPaymentDescription_a6, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a7
			' -------------------------------------------------------------------
			If sPaymentAmount_a7 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a7ValLabels", sPaymentAmount_a7, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a7ValLabels", CDbl(Val(sPaymentAmount_a7) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a7 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a7ValLabels", sPaymentFundPaymentCode_a7, True)
			If sPaymentCustRef1_a7 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a7ValLabels", sPaymentCustRef1_a7, True)
			If sPaymentCustRef2_a7 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a7ValLabels", sPaymentCustRef2_a7, True)
			If sPaymentCustRef3_a7 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a7ValLabels", sPaymentCustRef3_a7, True)
			If sPaymentCustRef4_a7 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a7ValLabels", sPaymentCustRef4_a7, True)
			If sPaymentDescription_a7 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a7ValLabels", sPaymentDescription_a7, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a8
			' -------------------------------------------------------------------
			If sPaymentAmount_a8 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a8ValLabels", sPaymentAmount_a8, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a8ValLabels", CDbl(Val(sPaymentAmount_a8) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a8 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a8ValLabels", sPaymentFundPaymentCode_a8, True)
			If sPaymentCustRef1_a8 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a8ValLabels", sPaymentCustRef1_a8, True)
			If sPaymentCustRef2_a8 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a8ValLabels", sPaymentCustRef2_a8, True)
			If sPaymentCustRef3_a8 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a8ValLabels", sPaymentCustRef3_a8, True)
			If sPaymentCustRef4_a8 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a8ValLabels", sPaymentCustRef4_a8, True)
			If sPaymentDescription_a8 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a8ValLabels", sPaymentDescription_a8, True)
			' -------------------------------------------------------------------
			' Transaction detail extra Line a9
			' -------------------------------------------------------------------
			If sPaymentAmount_a9 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentAmountInPence_a9ValLabels", sPaymentAmount_a9, True) : oFormSupportCore.ApplyValueIfExists("PaymentAmountInPounds_a9ValLabels", CDbl(Val(sPaymentAmount_a9) / 100).ToString("F2"), True)
			If sPaymentFundPaymentCode_a9 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentFundCode_a9ValLabels", sPaymentFundPaymentCode_a9, True)
			If sPaymentCustRef1_a9 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef1_a9ValLabels", sPaymentCustRef1_a9, True)
			If sPaymentCustRef2_a9 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef2_a9ValLabels", sPaymentCustRef2_a9, True)
			If sPaymentCustRef3_a9 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef3_a9ValLabels", sPaymentCustRef3_a9, True)
			If sPaymentCustRef4_a9 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentCustRef4_a9ValLabels", sPaymentCustRef4_a9, True)
			If sPaymentDescription_a9 <> "" Then oFormSupportCore.ApplyValueIfExists("PaymentDescription_a9ValLabels", sPaymentDescription_a9, True)



		Catch ex As Exception

		End Try
	End Sub


End Class