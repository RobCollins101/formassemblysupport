﻿Public Class afSupportSubmission
	Inherits System.Web.UI.Page

	Public bShowValues As Boolean = False
	Public sReturnURLToCaller As String = ""
	Public sHTMLPassthroughForm As String = ""
	Public sMessageToUser As String = ""
	Public bPostValues As Boolean = True
	Public sConfigID As String = ""

	Public oFormSupportCore As clsFormSupportCore
	Public bDelayMessage As Boolean = False


	Private Sub afSupportSubmission_Init(sender As Object, e As EventArgs) Handles Me.Init

		If StringIsTrue(GetMySettingString("SystemUnderMaintenance")) Then Response.Redirect(GetMySettingString("MaintenancePage"), True)

	End Sub


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


		If StringIsTrue(GetMySettingString("SystemUnderMaintenance")) Then
			Response.Redirect(GetMySettingString("MaintenancePage"), True)
		Else

			If Not IsPostBack Then
				DoProcessPostedData()

			End If

		End If

	End Sub

	Protected Sub DoProcessPostedData()
		' All the processing is done in the class clsFormSupportCore
		' This will read the configuration and manage the processing of :
		'    Values to be stored in XML file
		'    Files to be stored
		'    Load up the configuration details that dictate
		'       Process events based on files
		'       Create documents
		'       Send Emails
		'    
		'    

		Dim oRow As TableRow, oCell As TableCell
		Dim sTemp As String, bShowMessage As Boolean = False

		Try

			oFormSupportCore = New clsFormSupportCore

			oFormSupportCore.tblPostStringValues = Me.tblPostStringValues
			oFormSupportCore.tblQueryStringValues = Me.tblQueryStringValues
			oFormSupportCore.tblRecoveredStringValues = Me.tblRecoveredStringValues

			oFormSupportCore.ProcessHTTPRequest(My.Request, Page, clsFormSupportCore.StagesToDo.AttachmentsDocsAndEmails)
			If oFormSupportCore.ConfigDetailsFromXML IsNot Nothing Then sConfigID = oFormSupportCore.ConfigDetailsFromXML.XMLConfigKey

			sTemp = oFormSupportCore.GetValueIfExists("ShowMessageToUserValLabels",,, "DefaultShowMessageToUsers")
			If sTemp <> "" Then bShowMessage = StringIsTrue(sTemp)
			If bShowMessage Then
				sMessageToUser = oFormSupportCore.GetValueIfExists("MessageToUserValLabels", True,, "DefaultMessageToUserHTML")
				sMessageToUser = LoadIfFile(sMessageToUser, oFormSupportCore.oFieldDataUsed, oFormSupportCore.oFieldDataUsed)
			End If

			sReturnURLToCaller = oFormSupportCore.GetValueIfExists("URLOnCompletionValLabels")
			bPostValues = StringIsTrue(oFormSupportCore.GetValueIfExists("PostValuesOnReturnValLabels", True))

			For Each oFile In oFormSupportCore.AssociatedFiles.SubmissionFiles.Values
				oRow = New TableRow
				oCell = New TableCell()
				oCell.Text = oFile.FileName
				oRow.Cells.Add(oCell)
				oCell = New TableCell()
				oCell.Text = oFile.FileData.Length.ToString
				oRow.Cells.Add(oCell)
				oCell = New TableCell()
				oCell.Text = oFile.oSavedFileInfo.Extension
				oRow.Cells.Add(oCell)
				Me.tblFilesPosted.Rows.Add(oRow)

			Next

			DoSetFieldsToPost()

		Catch ex As Exception

		End Try

	End Sub



	Private Sub DoSetFieldsToPost()
		Dim sFullSet As String = "", sTemp As String
		Dim sKey As String, sValue As String
		Dim sHTML As String

		'input type="hidden" id="custref1" name="custref1" value="<% Response.Write(sPaymentCustRef1)%>" 
		Try

			If sReturnURLToCaller <> "" Then
				sTemp = oFormSupportCore.GetValueIfExists("PauseURLReturnValLabels", True)
				If sTemp <> "" AndAlso StringIsTrue(sTemp) Then bDelayMessage = True

				If bPostValues Then

					For Each sKey In oFormSupportCore.ValuesFromHTTPRequest.SubmittedDataValues.Keys
						sValue = oFormSupportCore.ValuesFromHTTPRequest.SubmittedDataValues.Item(sKey)
						sHTML = "<input type='hidden' id='" & Server.HtmlEncode(sKey) & "' name='" & Server.HtmlEncode(sKey) & "' value='" & Server.HtmlEncode(sValue) & "' />"
						sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML

					Next

					For Each sKey In oFormSupportCore.ValuesFromXML.aXMLDataValues.Keys
						sValue = oFormSupportCore.ValuesFromXML.aXMLDataValues.Item(sKey)
						sHTML = "<input type='hidden' id='" & Server.HtmlEncode(sKey) & "' name='" & Server.HtmlEncode(sKey) & "' value='" & Server.HtmlEncode(sValue) & "' />"
						sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML

					Next

				End If

				sHTML = "<input type='submit' value='Click here to proceed' />"
				If bDelayMessage Then
					sHTML &= " <script type='text/javascript'>if (frmReturn.action != '' ) frmReturn.submit();</script>"

				End If
				sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML

			End If

			lblHTMLFormOut.Text = sFullSet

		Catch ex As Exception

		End Try

	End Sub

End Class