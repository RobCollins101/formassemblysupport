﻿Public Class afPaymentResponse2
	Inherits System.Web.UI.Page

	Public sSuccessfulText As String = "", sUnsuccessfulText As String = ""

	Dim moCurrentData As New clsCurrentData
	Public bShowValues As Boolean = False
	Public bPaymentSuccessful As Boolean = False
	Public bPaymentError As Boolean = False
	Public bPauseBeforeReturn As Boolean = False
	Public sReturnURLToCaller As String = ""
	Public sURLToGoTo As String = ""
	Public bPostValues As Boolean = True
	Public sConfigID As String = ""
	Public sErrorCode As String = ""
	Public sErrorDescription As String = ""

	Public sMessageToUser As String = ""

	Public oFormSupportCore As clsFormSupportCore


	Private Sub afSupportSubmission_Init(sender As Object, e As EventArgs) Handles Me.Init

		If StringIsTrue(GetMySettingString("SystemUnderMaintenance")) Then Response.Redirect(GetMySettingString("MaintenancePage"), True)

	End Sub


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try

			If StringIsTrue(GetMySettingString("SystemUnderMaintenance")) Then
				Response.Redirect(GetMySettingString("MaintenancePage"), True)
			Else


				If Not IsPostBack Then
					DoProcessPostedData()

				End If


				If Not bPaymentSuccessful Then

					pnlPaymentFailed.Visible = True
					pnlPaymentSuccessful.Visible = False

				Else

				End If

				SetFieldToPost()
			End If


		Catch ex As Exception

		End Try

	End Sub



	Protected Sub DoProcessPostedData()
		Dim oRow As TableRow, oCell As TableCell
		Dim oTemp As Object, sTemp As String, bShowMessage As Boolean

		Dim sValue As String, sKey As String
		Dim oTblRow As TableRow
		Dim oTblCell As TableCell


		Try
			oTemp = GetMySetting("DebugShowFieldVals")
			If oTemp IsNot Nothing Then bShowValues = StringIsTrue(oTemp.ToString)

			oFormSupportCore = New clsFormSupportCore

			oFormSupportCore.tblPostStringValues = Me.tblPostStringValues
			oFormSupportCore.tblQueryStringValues = Me.tblQueryStringValues
			oFormSupportCore.tblRecoveredStringValues = Me.tblRecoveredStringValues

			oFormSupportCore.ProcessHTTPRequest(My.Request, Page, clsFormSupportCore.StagesToDo.FieldsOnly)
			If oFormSupportCore.ConfigDetailsFromXML IsNot Nothing Then sConfigID = oFormSupportCore.ConfigDetailsFromXML.XMLConfigKey



			sTemp = oFormSupportCore.GetValueIfExists("PaymentValidValLabels")
			bPaymentSuccessful = StringIsTrue(sTemp)

			sErrorCode = oFormSupportCore.GetValueIfExists("PaymentErrorValLabels")
			bPaymentError = Not (sErrorCode = "")
			If bPaymentError Then
				bPaymentSuccessful = False
				sErrorDescription = oFormSupportCore.GetValueIfExists("PaymentErrorDescValLabels")
			End If

			pnlPaymentFailed.Visible = False
			pnlPaymentSuccessful.Visible = False

			sMessageToUser = oFormSupportCore.GetValueIfExists("PaymentSuccessMessageValLabels", True,, "DefaultPaymentSuccessMessageHTML")
			sMessageToUser = LoadIfFile(sMessageToUser, oFormSupportCore.oFieldDataUsed, oFormSupportCore.oFieldDataUsed)

			If bPaymentSuccessful Then
				sTemp = oFormSupportCore.GetValueIfExists("PaymentSuccessShowMessageValLabels",,, "DefaultShowMessageToUsers")
				If sTemp <> "" Then bShowMessage = StringIsTrue(sTemp)
				If Not bShowMessage Then sMessageToUser = ""

				oFormSupportCore.ProcessHTTPRequest(My.Request, Page, clsFormSupportCore.StagesToDo.AttachmentsDocsAndEmails)
				sURLToGoTo = oFormSupportCore.GetValueIfExists("PaymentURLOnPaySuccessValLabels", False)
				bPostValues = StringIsTrue(oFormSupportCore.GetValueIfExists("PostValuesOnSuccessValLabels"))
				pnlPaymentSuccessful.Visible = True

			Else
				sMessageToUser = oFormSupportCore.GetValueIfExists("PaymentFailureMessageValLabels", True,, "DefaultPaymentFailureMessageHTML")
				sMessageToUser = LoadIfFile(sMessageToUser, oFormSupportCore.oFieldDataUsed, oFormSupportCore.oFieldDataUsed)

				sTemp = oFormSupportCore.GetValueIfExists("PaymentFailureShowMessageValLabels",,, "DefaultShowPaymentFailureMessage")
				If sTemp <> "" Then bShowMessage = StringIsTrue(sTemp)
				If Not bShowMessage Then sMessageToUser = ""

				sURLToGoTo = oFormSupportCore.GetValueIfExists("PaymentURLOnPayFailValLabels", False)
				bPostValues = StringIsTrue(oFormSupportCore.GetValueIfExists("PostValuesOnFailVallabels"))
				pnlPaymentFailed.Visible = True

			End If

			If oFormSupportCore.bShowDebugValues Or bShowValues Then

				For Each oFile In oFormSupportCore.AssociatedFiles.SubmissionFiles.Values
					oRow = New TableRow
					oCell = New TableCell()
					oCell.Text = oFile.FileName
					oRow.Cells.Add(oCell)
					oCell = New TableCell()
					oCell.Text = oFile.FileData.Length.ToString
					oRow.Cells.Add(oCell)
					oCell = New TableCell()
					oCell.Text = oFile.oSavedFileInfo.Extension
					oRow.Cells.Add(oCell)
					Me.tblFilesPosted.Rows.Add(oRow)

				Next




				For Each oDoc In oFormSupportCore.RequestDocs
					oRow = New TableRow
					oCell = New TableCell()
					oCell.Text = oDoc.sStoredPath
					oRow.Cells.Add(oCell)
					Me.tblDocsSaved.Rows.Add(oRow)

				Next


				oRow = New TableRow
				oCell = New TableCell()
				oCell.Text = "Email to " & oFormSupportCore.RequestEmails.ClientEmailTo
				oRow.Cells.Add(oCell)
				oCell = New TableCell()
				oCell.Text = oFormSupportCore.RequestEmails.ClientSubject
				oRow.Cells.Add(oCell)
				Me.tblEmailsSent.Rows.Add(oRow)

				oRow = New TableRow
				oCell = New TableCell()
				oCell.Text = "Email to " & oFormSupportCore.RequestEmails.AdminEmailTo
				oRow.Cells.Add(oCell)
				oCell = New TableCell()
				oCell.Text = oFormSupportCore.RequestEmails.AdminSubject
				oRow.Cells.Add(oCell)
				Me.tblEmailsSent.Rows.Add(oRow)



				If tblConfigValues IsNot Nothing Then

					If oFormSupportCore.ConfigDetailsFromXML.XMLDataValues.Count > 0 Then
						For Each sKey In oFormSupportCore.ConfigDetailsFromXML.XMLDataValues.Keys
							oTblRow = New TableRow
							sValue = Me.Server.UrlDecode(oFormSupportCore.ConfigDetailsFromXML.XMLDataValues.Item(sKey))
							oTblCell = New TableCell
							oTblCell.Text = HttpUtility.HtmlEncode(sKey)
							oTblRow.Cells.Add(oTblCell)

							oTblCell = New TableCell
							sTemp = sValue
							oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
							oTblRow.Cells.Add(oTblCell)

							tblConfigValues.Rows.Add(oTblRow)
						Next
					End If

				End If

			End If

		Catch ex As Exception

		End Try

	End Sub


	Private Sub SetFieldToPost()
		Dim sFullSet As String = ""
		Dim sKey As String, sValue As String
		Dim sHTML As String

		'input type="hidden" id="custref1" name="custref1" value="<% Response.Write(sPaymentCustRef1)%>" 
		Try

			sHTML = "<div>" & sMessageToUser & "£</div>"

			If sURLToGoTo <> "" Then
				sHTML &= "<form name='frmSubmitReturn' action='" & sURLToGoTo & "' target='_self' "
				If bPostValues Then
					sHTML &= " method='post' "
				Else
					sHTML &= " method='get' "
				End If

				sHTML &= ">"
				sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML

				For Each sKey In oFormSupportCore.ValuesFromHTTPRequest.SubmittedDataValues.Keys
					sValue = oFormSupportCore.ValuesFromHTTPRequest.SubmittedDataValues.Item(sKey)
					sHTML = "<input type='hidden' id='" & Server.HtmlEncode(sKey) & "' name='" & Server.HtmlEncode(sKey) & "' value='" & Server.HtmlEncode(sValue) & "' />"
					sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML

				Next

				For Each sKey In oFormSupportCore.ValuesFromXML.aXMLDataValues.Keys
					sValue = oFormSupportCore.ValuesFromXML.aXMLDataValues.Item(sKey)
					sHTML = "<input type='hidden' id='" & Server.HtmlEncode(sKey) & "' name='" & Server.HtmlEncode(sKey) & "' value='" & Server.HtmlEncode(sValue) & "' />"
					sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML

				Next


				sHTML = "</form>"
				sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML

				If bPauseBeforeReturn Then
					If bPaymentSuccessful Then
						sHTML = "<input type='submit' value='Click here to proceed' />"
						sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML
					Else

						sHTML = "<input type='submit' value='Click here to return' />"
						sFullSet &= IIf(sFullSet <> "", vbCrLf, "").ToString & sHTML
					End If
				Else
					sHTML = "<script type='text/javascript'>if (frmSubmitReturn.action != '' ) frmSubmitReturn.submit();</script>"
				End If
			End If

			lblPostValues.Text = sFullSet

		Catch ex As Exception

		End Try

	End Sub

	'Protected Sub ProcessPostedDataOld()

	'    'Dim oFormSupportParts As New clsFormSupportParts

	'    Dim sSourceHTMLPath As String = "", sDestGeneratedDocFullPath As String = ""
	'    Dim sDestLogPath As String = "", sLogFileEntry As String = ""
	'    Dim sKey As String, sGeneratedDocFilePath As String = ""
	'    Dim sTempURL As String = "", aTemp As ArrayList, oTemp As Object
	'    Dim sTempSessionID As String = ""

	'    Try

	'        ' Process the field values and the passed files

	'        oTemp = GetMySetting("DebugShowFieldVals")
	'        If oTemp IsNot Nothing Then bShowValues = StringIsTrue(oTemp.ToString)


	'        oFormSupportParts.bShowValues = bShowValues
	'        oFormSupportParts.oPage = Page
	'        oFormSupportParts.tblQueryStringValues = tblQueryStringValues
	'        oFormSupportParts.tblPostStringValues = tblPostStringValues
	'        oFormSupportParts.tblRecoveredStringValues = tblRecoveredStringValues

	'        oFormSupportParts.sBaseDestLogFilePathTemplate = GetMySetting("DefaultLogFilePath").ToString
	'        oFormSupportParts.sBaseLogFileEntryTemplate = GetMySetting("DefaultLogFileEntry").ToString
	'        oFormSupportParts.sBaseDestFilePathTemplate = GetMySetting("DefaultStoreFolderDest").ToString
	'        oFormSupportParts.sBaseDestDirectoryTemplate = GetMySetting("DefaultStoreFolderDest").ToString

	'        ' Get all recovered field value and submitted files into array for processing
	'        ' Submitted files are stored

	'        oFormSupportParts.ProcessPostedData(Request, moCurrentData)

	'        oFormSupportParts.LoadValuesFromXML(moCurrentData)

	'        bShowValues = oFormSupportParts.bShowValues

	'        pnlPaymentFailed.Visible = False
	'        bPaymentSuccessful = Not oFormSupportParts.oFieldDataUsed.ContainsKey("ErrorCode")

	'        If Not bPaymentSuccessful Then

	'            pnlPaymentFailed.Visible = True
	'            pnlPaymentSuccessful.Visible = False

	'        Else


	'            ' Prepare field values for the generated document

	'            sKey = "HTMLDocumentFilePath" : If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
	'            sKey = "SourceDocumentFilePath" : If sSourceHTMLPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
	'            sKey = "HTMLFilePath" : If sSourceHTMLPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sSourceHTMLPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString

	'            sDestLogPath = oFormSupportParts.sDestLogFilePathTemplate
	'            sLogFileEntry = oFormSupportParts.sLogFileEntryTemplate

	'            sKey = "DocDestinationPath" : If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
	'            sKey = "DocumentPath" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
	'            sKey = "HTMLDocumentPath" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
	'            sKey = "HTMLDocumentDest" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
	'            sKey = "HTMLDocumentDestination" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
	'            sKey = "HTMLDocumentFilePath" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString
	'            sKey = "HTMLDocumentStorage" : If sDestGeneratedDocFullPath = "" And oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then sDestGeneratedDocFullPath = oFormSupportParts.oFieldDataUsed.Item(sKey).ToString


	'            If sSourceHTMLPath = "" Then sSourceHTMLPath = GetMySetting("DefaultGeneratedHTMLDocument").ToString
	'            If sDestLogPath = "" Then sDestLogPath = GetMySetting("DefaultLogFilePath").ToString
	'            If sLogFileEntry = "" Then sLogFileEntry = GetMySetting("DefaultLogFileEntry").ToString
	'            If sDestGeneratedDocFullPath = "" Then sDestGeneratedDocFullPath = GetMySetting("DefaultGeneratedDocumentName").ToString

	'            ' Generate the actual document of the submission

	'            If bShowValues Then lblProcessing.Text &= "Document source used<br/>" & sSourceHTMLPath & "<br/>" & "<br/>"
	'            If bShowValues Then lblProcessing.Text &= "Document storage template to : " & sDestGeneratedDocFullPath & "<br/>"

	'            sGeneratedDocFilePath = oFormSupportParts.GenerateDocument(sSourceHTMLPath, sDestGeneratedDocFullPath, sDestLogPath, sLogFileEntry)
	'            If bShowValues Then lblProcessing.Text &= "Document stored at : " & sGeneratedDocFilePath & "<br/>"

	'        End If

	'        ' Note emails won't be fired unless the right parts are present and valid

	'        If bPaymentSuccessful Then oFormSupportParts.SendEmailsUsingFieldValues()

	'        bShowValues = oFormSupportParts.bShowValues
	'        sReturnURLToCaller = oFormSupportParts.sReturnURLToCaller

	'        If bShowValues Then lblProcessing.Text &= oFormSupportParts.sProcessing
	'        If bShowValues Then lblFileList.Text &= oFormSupportParts.sFileList
	'        If bShowValues Then lblFilesDetected.Text &= oFormSupportParts.sFilesDetected
	'        If bShowValues Then lblErrors.Text &= oFormSupportParts.sErrors

	'        sTempURL = ""
	'        aTemp = TryCast(GetMySetting("PassToURLFieldNames"), ArrayList)

	'        If aTemp IsNot Nothing Then

	'            For Each sKey In aTemp
	'                If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then
	'                    sTempURL = oFormSupportParts.oFieldDataUsed.Item(sKey)
	'                    Exit For
	'                End If
	'            Next
	'        End If

	'        sTempSessionID = ""
	'        aTemp = TryCast(GetMySetting("PassToURLSessionIDFieldNames"), ArrayList)

	'        If aTemp IsNot Nothing Then

	'            For Each sKey In aTemp
	'                If oFormSupportParts.oFieldDataUsed.ContainsKey(sKey) Then
	'                    sTempSessionID = oFormSupportParts.oFieldDataUsed.Item(sKey)
	'                    Exit For
	'                End If
	'            Next
	'        End If

	'        If bPaymentSuccessful Then

	'            If sTempURL <> "" And sTempSessionID <> "" Then

	'                Session.Add("Fields_" & sTempSessionID, oFormSupportParts.oFieldDataUsed)
	'                lblHTMLFormOut.Text = oFormSupportParts.generateHTMLForm(sTempURL)

	'            Else

	'                oFormSupportParts.SendEmailsUsingFieldValues()

	'            End If

	'        End If

	'        If bShowValues Then lblProcessing.Text &= oFormSupportParts.sProcessing
	'        If bShowValues Then lblFileList.Text &= oFormSupportParts.sFileList
	'        If bShowValues Then lblFilesDetected.Text &= oFormSupportParts.sFilesDetected
	'        If bShowValues Then lblErrors.Text &= oFormSupportParts.sErrors


	'    Catch ex As Exception

	'    End Try
	'End Sub

End Class