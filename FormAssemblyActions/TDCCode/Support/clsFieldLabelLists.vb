﻿Imports System.Xml
Public Class clsFieldLabelLists
	Protected Friend LabelLists As New SortedList(Of String, List(Of String))(StringComparer.CurrentCultureIgnoreCase)

	Protected Friend LabelListXMLPath As String

	Public Sub New()
		Dim AppPath As String = ""

		LabelListXMLPath = GetMySettingString("LabelListXMLPath")
		If Regex.IsMatch(LabelListXMLPath.Substring(0, 2), "\w+") Then

			AppPath = My.Request.PhysicalApplicationPath
			LabelListXMLPath = AppPath & LabelListXMLPath
		End If
		LoadLists()

	End Sub

	Protected Friend Function GetList(LabelListName As String) As List(Of String)
		Dim oReturn As New List(Of String)
		Try

			If LabelLists.Count = 0 Then LoadLists()
			If LabelLists.ContainsKey(LabelListName) Then oReturn = LabelLists(LabelListName)

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			oError.AddDetail("List name : " & LabelListName)
			gsErr = goErrs.ToString

		End Try
		Return oReturn
	End Function

	Private Sub LoadLists(LabelListPath As String)
		Try
			LabelListXMLPath = LabelListPath
			LoadLists()

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			gsErr = goErrs.ToString

		End Try

	End Sub

	Private Sub LoadLists()
		Dim sRegexFile As String, oXMLStream As IO.StreamReader
		Dim oXMLDom As XmlDocument, oNodes As XmlNodeList
		Dim oXMLListNode As XmlNode, oXMLAttr As XmlAttribute
		Dim oListLabelNodes As XmlNodeList, oChildNode As XmlNode
		Dim oXMLListElem As XmlElement, oXMLListValElem As XmlElement
		Dim sListName As String, sListEntry As String, aList As List(Of String)
		Try
			sRegexFile = My.Settings.RegexFilePath
			If LabelListXMLPath <> "" Then
				If Regex.IsMatch(LabelListXMLPath, sRegexFile) Then
					LabelListXMLPath = IO.Path.GetFullPath(LabelListXMLPath)

					If IO.File.Exists(LabelListXMLPath) Then
						oXMLStream = IO.File.OpenText(LabelListXMLPath)
						oXMLDom = New XmlDocument()
						oXMLDom.Load(oXMLStream)
						oXMLStream.Close()

						oNodes = oXMLDom.DocumentElement.SelectNodes("//LabelList[@name]")
						For Each oNode In oNodes
							oXMLListElem = TryCast(oNode, XmlElement)
							If oXMLListElem IsNot Nothing Then
								oXMLAttr = TryCast(oXMLListElem.Attributes.GetNamedItem("name"), XmlAttribute)
								sListName = oXMLAttr.Value
								oListLabelNodes = oXMLListElem.ChildNodes

								If LabelLists.ContainsKey(sListName) Then
									aList = LabelLists(sListName)
								Else
									aList = New List(Of String)
									LabelLists.Add(sListName, aList)
								End If

								For Each oChildNode In oListLabelNodes
									oXMLListValElem = TryCast(oChildNode, XmlElement)
									sListEntry = oXMLListValElem.InnerText
									oXMLListValElem = Nothing
									aList.Add(sListEntry)
								Next
								oListLabelNodes = Nothing
							End If
							oXMLListElem = Nothing
						Next
					End If
				End If
			End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			gsErr = goErrs.ToString

		End Try

	End Sub

End Class
