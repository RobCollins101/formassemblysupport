﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.IO
Imports System.Diagnostics
Imports System.Xml.Serialization
'Imports System.Data.SqlClient

Public Class wRuntimeErrors

	Inherits List(Of wRuntimeError)

	Public Function NewError() As wRuntimeError
		Dim RuntimeError As New wRuntimeError
		Try
			RuntimeError.oRuntimeErrors = Me
			Me.Add(RuntimeError)
		Catch ex As Exception
			Dim oError As wRuntimeError = Me.NewError(ex)

		End Try

		Return RuntimeError
	End Function

	Public Function NewError(ByVal ex As Exception) As wRuntimeError
		Dim RuntimeError As New wRuntimeError(ex)
		Try
			If Not (TypeOf (ex) Is System.Threading.ThreadAbortException) Then
				RuntimeError.oRuntimeErrors = Me
				Me.Add(RuntimeError)
			End If
		Catch ex2 As Exception
			'Dim oError As wRuntimeError = Me.NewError(ex2)
		End Try

		Return RuntimeError
	End Function


	Public Function NewError(ErrorMessage As String) As wRuntimeError
		Dim RuntimeError As New wRuntimeError(ErrorMessage)
		Try
			RuntimeError.oRuntimeErrors = Me
			Me.Add(RuntimeError)
		Catch ex2 As Exception
			Dim oError As wRuntimeError = Me.NewError(ex2)
		End Try

		Return RuntimeError
	End Function


	Public ReadOnly Property ErrorStrings() As String
		Get
			Dim oErr As wRuntimeError, sReturn As String = ""
			Try

				For Each oErr In Me
					sReturn &= IIf(sReturn <> "", vbCrLf, "").ToString & oErr.ToString
				Next

			Catch ex2 As Exception
				Dim oError As wRuntimeError = Me.NewError(ex2)

			End Try

			Return sReturn
		End Get
	End Property

	Public ReadOnly Property ErrorHTML() As String
		Get
			Dim oErr As wRuntimeError, sReturn As String = ""
			Try

				For Each oErr In Me
					sReturn &= IIf(sReturn <> "", vbCrLf, "<br />").ToString & Replace(oErr.ToString, vbCrLf, "<br />")
				Next

			Catch ex2 As Exception
				Dim oError As wRuntimeError = Me.NewError(ex2)
			End Try

			Return sReturn
		End Get
	End Property


	Public ReadOnly Property ResponseErrList As List(Of clsErrorLine)
		Get
			Dim aReturn As New List(Of clsErrorLine), oErr As wRuntimeError
			Try
				For Each oErr In Me
					aReturn.Add(New clsErrorLine(oErr.ToString, oErr.sErrorCode, True))
				Next
			Catch ex As Exception

			End Try

			Return aReturn
		End Get
	End Property

End Class

Public Class wRuntimeError
	Public sErrorCode As String = ""
	Public sErrorMsg As String = ""
	Public sErrorType As String = ""
	Public sModule As String = ""
	Public sFunction As String = ""
	Public sExtraDetail As String = ""
	Public sLineNo As Long = 0
	Public oRuntimeErrors As New wRuntimeErrors
	Public oOrigErr As Exception
	'	Public oSettings As New hscSettingsSet

	Public Sub New(ByVal ex As Exception, ByVal RunErrors As wRuntimeErrors)
		Dim oKey As Object
		Try
			If Not (TypeOf (ex) Is System.Threading.ThreadAbortException) Then
				oOrigErr = ex
				sErrorMsg = ex.Message & vbCrLf & ex.StackTrace
				For Each oKey In ex.Data.Keys
					sErrorMsg = sErrorMsg & vbCrLf & oKey.ToString & " : " & ex.Data.Item(oKey).ToString
				Next
				sErrorType = ex.GetType.ToString
				sModule = Me.GetType.ToString
				sFunction = ex.Source
				oRuntimeErrors = RunErrors
				oRuntimeErrors.Add(Me)

				WriteErrorLog()

			End If

		Catch ex2 As Exception
			'Dim oError As wRuntimeError = goErrs.NewError(ex2)
			'gsErr &= oError.ToString
			'Dim oError As New wRuntimeError(ex2)
		End Try

	End Sub


	Public Sub New(ByVal ex As Exception)
		Dim oKey As Object
		Try
			If Not (TypeOf (ex) Is System.Threading.ThreadAbortException) Then
				oOrigErr = ex

				sErrorMsg = ex.Message & vbCrLf & ex.StackTrace
				For Each oKey In ex.Data.Keys
					sErrorMsg = sErrorMsg & vbCrLf & oKey.ToString & " : " & ex.Data.Item(oKey).ToString
				Next
				sErrorType = ex.GetType.ToString
				sModule = Me.GetType.ToString
				sFunction = ex.Source

				WriteErrorLog()

			End If

		Catch ex2 As Exception
			'Dim oError As wRuntimeError = goErrs.NewError(ex2)
			'gsErr &= oError.ToString
			'Dim oError As New wRuntimeError(ex2)
		End Try

	End Sub


	Public Sub New(ByVal ErrorMsgIn As String)
		Try

			sErrorMsg = ErrorMsgIn
			WriteErrorLog()

		Catch ex2 As Exception
			'Dim oError As wRuntimeError = goErrs.NewError(ex2)
			'gsErr &= oError.ToString
			'Dim oError As New wRuntimeError(ex2)
		End Try

	End Sub

	Public Sub New()

	End Sub


	Public Sub AddDetail(DetailAddition As String, Optional Divider As String = vbCrLf)
		Try

			sExtraDetail &= IIf(sExtraDetail <> "", Divider, "").ToString & DetailAddition

		Catch ex2 As Exception
			'Dim oError As wRuntimeError = goErrs.NewError(ex2)
			'gsErr &= oError.ToString
			'Dim oError As New wRuntimeError(ex2)
		End Try
	End Sub

	Public Sub WriteErrorLog()
		Dim oWriteTo As StreamWriter, sPath As String = "", sError As String = ""
		Try

			sError = Me.ToString
			'sPath = GetMySetting("ErrLogFile
			If sPath <> "" Then
				'If Left(sPath, 1) = "~" Or Left(sPath, 1) = "." Then sPath = gsApplicationPath & Mid(sPath, 2)

				oWriteTo = File.AppendText(sPath)
				oWriteTo.WriteLine("====>  Error at " & Now.ToLongDateString & " " & Now.ToLongTimeString)
				oWriteTo.WriteLine(sError & vbCrLf)
				oWriteTo.Flush() : oWriteTo.Close() : oWriteTo = Nothing
			End If

			WriteToEventLog(sError)

		Catch ex3 As Exception
			'Dim oError As wRuntimeError = goErrs.NewError(ex3)
			'gsErr &= oError.ToString
		End Try

	End Sub

	Public Function WriteToEventLog(ByVal entry As String,
				Optional ByVal appName As String = "",
				Optional ByVal eventType As EventLogEntryType = EventLogEntryType.Information,
				Optional ByVal logName As String = "ProductName") As Boolean

		Dim objEventLog As New EventLog

		Try

			If appName = "" Then appName = My.Request.Url.Segments(My.Request.Url.Segments.Count - 1).ToString
			If logName = "" Then logName = My.Request.Url.Segments(My.Request.Url.Segments.Count - 1).ToString

			'Register the Application as an Event Source

			If Not EventLog.SourceExists(appName) Then EventLog.CreateEventSource(appName, logName)

			'log the entry

			objEventLog.Source = appName
			objEventLog.WriteEntry(entry, eventType)

			Return True

		Catch Ex As Exception

			Return False

		End Try

	End Function

	Public Overrides Function ToString() As String
		Dim sMsg As String = sErrorMsg & vbCrLf & "Module " & sModule & " procedure " & sFunction
		If sExtraDetail <> "" Then sMsg &= vbCrLf & " Extra detail " & sExtraDetail
		Return sMsg
	End Function

End Class





<XmlType(Namespace:="http://tandridge.gov.uk/HTMLToImage/", TypeName:="ErrorItem")>
Public Class clsErrorLine
	<XmlElement(ElementName:="ErrorCode", Namespace:="http://tandridge.gov.uk/HTMLToImage/")>
	Public sErrorCode As String = ""
	<XmlElement(ElementName:="ErrorDescription", Namespace:="http://tandridge.gov.uk/HTMLToImage/")>
	Public sError As String = ""
	<XmlElement(ElementName:="RuntimeError", Namespace:="http://tandridge.gov.uk/HTMLToImage/")>
	Public bRuntimeError As Boolean = False

	Public Sub New(sErrorIn As String, sErrorCodeIn As String)
		sError = sErrorIn
		sErrorCode = sErrorCodeIn
	End Sub

	Public Sub New(sErrorIn As String)
		sError = sErrorIn
	End Sub

	Public Sub New(sErrorIn As String, sErrorCodeIn As String, bRuntimeErrorIn As Boolean)
		sError = sErrorIn
		sErrorCode = sErrorCodeIn
		bRuntimeError = bRuntimeErrorIn
	End Sub

	Public Sub New(sErrorIn As String, bRuntimeErrorIn As Boolean)
		sError = sErrorIn
		bRuntimeError = bRuntimeErrorIn
	End Sub

	Public Sub New()

	End Sub

End Class