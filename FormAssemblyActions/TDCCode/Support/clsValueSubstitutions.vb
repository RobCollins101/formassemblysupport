﻿Option Compare Text

Imports System.Reflection
Imports Microsoft.VisualBasic
Imports System.Text.RegularExpressions
Imports System.Net

Imports System.CodeDom.Compiler
Imports System.Linq

' =====================================================================================================================
' Class to manage substitutions in a string based on key/value pairs
'-----------------------------------------------------------------------
' v2.0.00 : 1 Aug 2014 :    Convert passed key.value data into sortedlist(New CaseInsensitiveComparer())
'                           Also apply a RecordError subroutine to simplify error handling variations
'-----------------------------------------------------------------------
' v2.0.01 : 28 Aug 2014 :   Use #if #end if to ascertain default key value pairs depending on type
'                           of program being developed
'-----------------------------------------------------------------------
' v2.0.02 : 7 Sept 2014 :    Amend OriginalString to always refresh values if PUT is invoked
'-----------------------------------------------------------------------
' v2.0.03 : 17 Sept 2014 :  Fixed error in second and subsequent identification of a replacement in 
'                           a large string. Added variable iPrevMatchEnd
'-----------------------------------------------------------------------
' v2.1.00 : 30 Jan 2015 :  Found out the MSScriptControl only functions if compiled up on Dell4ykt44j.
'						MEasures put in to prevent fatal errors causing crashes
'-----------------------------------------------------------------------
' v2.1.01 : 9 Feb 2015 : Identified calculations being missed off for <> (not equal to)
'-----------------------------------------------------------------------
' v2.2.0.0 : 24 Aug 2015 : Replace using MSScriptControl with embedded class clsEvalExpression
'					Placed the class at the bottom of this file
'-----------------------------------------------------------------------
' v2.2.1.0 : 26 Aug 2015 : Added the modifier extension to a value
'					To use this, add an extension after the value
'-----------------------------------------------------------------------
' v2.2.2.0 : 11 Feb 2016 : Found a bug in the modifier where similar starting substitutions were
'                   presenting the earlier value, eg Commentator and Comment gives Comment twice
'-----------------------------------------------------------------------
' v2.2.3.0 : 19 Apr 2016 : Fix condition statement where field name is only parameter
'                          Also sorted out some errors in the IF condition if there is no space after it
'-----------------------------------------------------------------------
' v2.2.4.0 : 18 Nov 2016 : Added a NOT modifier to the[[IF fieldname]] conditional statement
'                          Also allowed variable names to also have a ~ as part of the name
'-----------------------------------------------------------------------
' v2.2.5.0 : 16 Feb 2017 : Added a list of the fields as an arraylist
'-----------------------------------------------------------------------
' v2.2.6.0 : 23 Feb 2017 : Added list of extra substitutions as a friend sortedlist for reference
'-----------------------------------------------------------------------
' v2.2.7.0 : 24 Feb 2017 : Added modifier to GetSubstitutionForString for targetting
'                          substitution in strings For specific scenarios of File and URL
'-----------------------------------------------------------------------
' v2.3.0.0 : 28 Jul 2017 : Uses SortedList(of string, string)(StringComparer.CurrentCultureIgnoreCase) for collections
'						   Amended ValueNTPathModified to use the PATH invalid characters from Path.GetInvalidFileNameChars()
'-----------------------------------------------------------------------
' v2.3.1.0 :  6 Sep 2017 : amended Assembly and Application presets
'-----------------------------------------------------------------------
' v2.3.2.0 : 20 Sep 2017 : Corrected bug where environmental variables disrupted dynamic population
'-----------------------------------------------------------------------
' v2.3.3.0 :  2 Jan 2018 : Corrected bug where additions were not applying detecting of key correctly
'  =====================================================================================================================



' Examples. Note that line feeds not ok in the substitution brackets, but ok outside them
'	{{TestValue}}-[[TestValue2]]
'	((If "[[TestValue]]" <> "Wot"))stuff{{endif}}
'	((If [[TestValue]]))stuff{{endif}}     Will be false if the field doesn't exists or is blank
'	((If TestValue))stuff{{endif}}         Will be false if the field doesn't exists or is blank
'	((If NOT TestValue))stuff{{endif}}         Will be TRUE if the field is blank or equates to false
'	((If "[[TestValue]]"))stuff{{endif}}   Will be false if the field doesn't exists or is blank
'	((If "TestValue"))stuff{{endif}}   Only if the field exists
'	((If [[TestValue]]="Value"))stuff{{endif}}     Will be false if the field doesn't exists or is not equal to value
'	((If TestValue="Value"))stuff{{endif}}         Will be false if the field doesn't exists or is not equal to value
'	((If "[[TestValue]]"="Value"))stuff{{endif}}   Will be false if the field doesn't exists or is not equal to value
'	((If "TestValue"="Value"))stuff{{endif}}   WILL BE FALSE
'	{{beginif [[fred]]}}Fred{{if [[jones]]}} Jones{{endif}}<<if {{Bloggs}}>> Bloggs((endif))((endif))
'	{{beginif [[fred]]}}Fred{{if "[[jones]]" <> "No" AND "@@Jones@@" <> ""}} Jones{{endif}}<<if {{Bloggs}}>> Bloggs((endif))((endif))
'	{{beginif [[fred]]}}Fred{{if "[[jones]]" <> "No" AND "@@Jones@@" <> ""}} Jones{{endif}}<<if {{Bloggs}}>> Bloggs [[eval 2*len("{{Bloggs}}")]]((endif))((endif))
'	[[if datediff("d", now(),"##date##") < 10]]Fresh[[endif]]
'	
'	On a base value, can use a modifier to the data returned.
'		an example is {{date:format s}}
'	which will give the date in format yyyy-MM-ddTHH:mm:ss
'		or			[[MyMoneys format #,##0.00]]
'	which gives the value of MyMoneys in format of 1,234.56
'	The modifiers are : 
'			Format - uses Vb formats on dates and numbers
'				For dates see https://msdn.microsoft.com/en-us/library/az4se3k1(v=vs.110).aspx 
'				Can use custom formats as well. See https://msdn.microsoft.com/en-us/library/8kb3ddd4%28v=vs.110%29.aspx 
'				For numbers see https://msdn.microsoft.com/en-us/library/dwhawy9k%28v=vs.110%29.aspx
'				can use custom formats as well.See https://msdn.microsoft.com/en-us/library/0c899ak8%28v=vs.110%29.aspx 
'			Case (or VbStrConv) - uses case formats based on vbStrConv
'				Most common formats would be : Uppercase, Lowercase, Propercase
'				See https://msdn.microsoft.com/en-us/library/microsoft.visualbasic.vbstrconv%28v=vs.100%29.aspx
'				
'
'	can use opening brackets for block followed by one of
'			if, beginif, ifbegin, begin 
'		then a value to be not empty or calculation to a positive result to test
'			By a non-empty expression, this means
'				for boolean, true
'				for numbers, non zero (0)
'				for string, not empty
'		then the closing brackets
'	then the data to show if the condition produces a positive result, including line feeds
'	then close block with
'			endif, ifend, end in the opening and closing substitution brackets
'
'	[[eval left("((Test))",((Size)))]]
'	{{eval datediff("d", now(), dateadd("d",5,"##date##"))}}
'	{{eval datediff("d", dateadd("d", 5,now()), "##date format s##")}}
'	{{eval datediff("d", now(), "##date##"}}
'		Uses marker eval, evaluate, compute, calculate, calc
'
'	Can use any VB single result expression for a calculation result

'	Acceptable paired value markers
'		{{value}}
'		[[value]]
'		((value))
'		<<value>>
'		##value##
'		@@value@@
'		%%value%%


Public Class clsValueSubstitutions
	Inherits List(Of SubstituteEntry)

	Protected Friend oFixedSubstituteList As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
	Protected Friend oVariableSubstituteList As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
	Protected Friend oCurrentSubstituteStrings As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

	Protected Friend oAllSubstituteStrings As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

	Protected Friend oExtraSubstituteStrings As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)


	'Protected Friend oFixedSubstituteList As New SortedList(New CaseInsensitiveComparer())
	'Protected Friend oVariableSubstituteList As New SortedList(New CaseInsensitiveComparer())
	'Protected Friend oCurrentSubstituteStrings As New SortedList(New CaseInsensitiveComparer())

	'Protected Friend oAllSubstituteStrings As New SortedList(New CaseInsensitiveComparer())

	'Protected Friend oExtraSubstituteStrings As New SortedList(New CaseInsensitiveComparer())


	Protected Friend OtherValues As New Hashtable

	Private sOriginalString As String = ""
	Protected Friend dPassedDate As Date = Now
	Protected Friend ReplacementModifier As TargetModifier = TargetModifier.NoModify
	Protected Friend sFilePathSpaceSubstitution As String = ""

	Protected Friend StartTokens() As String = {"{{", "[[", "<<", "((", "@@", "##", "%%"}
	Protected Friend EndTokens() As String = {"}}", "]]", ">>", "))", "@@", "##", "%%"}

	Public Enum TargetModifier
		NoModify
		ForFilePath
		ForURI
	End Enum


	Public Function GetSubstitutionForString(TemplateString As String, Optional ReplacementModifier As TargetModifier = TargetModifier.NoModify) As String
		Dim sReturn As String = TemplateString
		Try

			Me.ReplacementModifier = ReplacementModifier
			Me.OriginalString = sReturn
			sReturn = Me.Result

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return sReturn
	End Function

	Public Property OriginalString As String
		Get
			Return sOriginalString
		End Get
		Set(value As String)
			sOriginalString = value
			SplitString(Nothing, True)
		End Set
	End Property


	Public Function ContainsKey(Key As String) As Boolean
		Dim bReturn As Boolean = False
		Try

			bReturn = Me.oAllSubstituteStrings.ContainsKey(Key)

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return bReturn
	End Function


	Public Sub ResetAll()
		Try
			Me.Clear()
			Me.oAllSubstituteStrings.Clear()
			Me.oCurrentSubstituteStrings.Clear()
			Me.oFixedSubstituteList.Clear()
			Me.OtherValues.Clear()
			sOriginalString = ""


		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
	End Sub


	Public Function ValueOf(Key As String) As String
		Dim sReturn As String = ""
		Try

			If Me.oAllSubstituteStrings.ContainsKey(Key) Then sReturn = oAllSubstituteStrings.Item(Key).ToString

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return sReturn
	End Function

	Public ReadOnly Property Result As String
		Get
			Dim sReturn As String = "", oValItem As SubstituteEntry
			Dim iValidSection As Integer = -1
			Try

				For Each oValItem In Me
					If iValidSection > 0 Then
						If oValItem.BlockLevel < iValidSection Then iValidSection = -1
					Else
						If oValItem.TokenType = "if" Or oValItem.TokenType = "ifbegin" Or oValItem.TokenType = "beginif" Or oValItem.TokenType = "begin" Then
							If Not oValItem.EvalIsValid Then iValidSection = oValItem.BlockLevel
						Else
							sReturn &= oValItem.Result
						End If

					End If

				Next

			Catch ex As Exception
				Dim aDetail As New ArrayList
				RecordError(ex, aDetail)

			End Try
			Return sReturn

		End Get
	End Property


	Public Function ListOfFieldNames() As ArrayList
		Dim oReturn As New ArrayList, sKey As String
		Try

			For Each sKey In oFixedSubstituteList.Keys
				If Not oReturn.Contains(sKey) Then oReturn.Add(sKey)
			Next

			For Each sKey In oVariableSubstituteList.Keys
				If Not oReturn.Contains(sKey) Then oReturn.Add(sKey)
			Next

			For Each sKey In oCurrentSubstituteStrings.Keys
				If Not oReturn.Contains(sKey) Then oReturn.Add(sKey)
			Next

			For Each sKey In oAllSubstituteStrings.Keys
				If Not oReturn.Contains(sKey) Then oReturn.Add(sKey)
			Next

			oReturn.Sort()

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)


		End Try
		Return oReturn
	End Function



	Protected Friend Sub PopulateReplacementArray(Optional ByRef aExtraStringsIn As SortedList(Of String, String) = Nothing, Optional ByVal ResetValues As Boolean = False)
		Dim iPosLast As Integer = 1, sKey As String = "", sValue As String = "", sNewValue As String = ""
		Dim sTemp As String = "", sNewKey As String = "", oAssembly As Assembly, oAssemblyName As AssemblyName
		Dim oTempContextMatches As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
		Dim sTemp2 As String = "", oTemp As Object = Nothing
		Dim dtFinYear As Date, iFinYear As Integer, iFinMonth As Integer, iFinDays As Integer, iFinWeek As Integer, iMilliseconds As Integer
		Dim iYear As Integer, iMonth As Integer, iMonthDay As Integer, iYearDay As Integer, iWeek As Integer, iWeekDay As Integer
		Dim iHour As Integer, iMinute As Integer, iSecond As Integer, sDayName As String, lTicks As Long
		Dim aParts() As String, sUserId As String = "", sDomain As String = "", sClientCompName As String = "", sClientDomain As String = ""
		Dim oReturn As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase), oDNSEntry As IPHostEntry
		Dim aExtraStrings As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase), sTempPath As String = "", bInTest As Boolean = False

		Try

			'If aExtraStringsIn IsNot Nothing Then aExtraStrings = New SortedList(aExtraStringsIn, New CaseInsensitiveComparer()) Else aExtraStrings = New SortedList(New CaseInsensitiveComparer())
			If aExtraStringsIn IsNot Nothing Then aExtraStrings = aExtraStringsIn Else aExtraStrings = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

#Region "Reset Fixed Substitution"

			If oFixedSubstituteList.Count = 0 Or ResetValues Or gbResetReplacements Or oAllSubstituteStrings.Count = 0 Then

				gbResetReplacements = False

				oFixedSubstituteList.Clear()


				For Each oEnvKey In Environment.GetEnvironmentVariables.Keys
					sKey = "" & UCase(oEnvKey.ToString) & ""
					sValue = Environment.GetEnvironmentVariables.Item(oEnvKey).ToString
					'If Not oTempContextMatches.ContainsKey(sKey) Then oTempContextMatches.Add(sKey, sValue) Else oTempContextMatches(sKey) = sValue
					TryAdd(oTempContextMatches, sKey, sValue)
				Next


				aParts = Split(My.User.Name, "\")
				If UBound(aParts) > 0 Then
					sDomain = aParts(0)
					sUserId = aParts(UBound(aParts))
				Else
					sUserId = My.User.Name
				End If

				sClientCompName = My.Computer.Name

				oDNSEntry = System.Net.Dns.GetHostEntry(sClientCompName)

				sClientCompName = oDNSEntry.HostName
				aParts = Split(sClientCompName, ".")
				If UBound(aParts) > 0 Then
					sClientCompName = aParts(0)
					aParts(0) = ""
					sClientDomain = Strings.Join(aParts, ".")
				End If
				sUserId = My.User.Name

				oAssembly = Assembly.GetExecutingAssembly
				oAssemblyName = New AssemblyName(oAssembly.FullName)

				TryAdd(oTempContextMatches, "DOMAIN", sDomain)
				TryAdd(oTempContextMatches, "DOMAINID", sDomain)
				TryAdd(oTempContextMatches, "DOMAINNAME", sDomain)
				TryAdd(oTempContextMatches, "NETWORKID", sDomain)
				TryAdd(oTempContextMatches, "NAME", sUserId)
				TryAdd(oTempContextMatches, "USER", sUserId)
				TryAdd(oTempContextMatches, "USERID", sUserId)
				TryAdd(oTempContextMatches, "LOGINNAME", My.User.Name)
				TryAdd(oTempContextMatches, "LOGINUSERID", My.User.Name)
				TryAdd(oTempContextMatches, "LOGIN", My.User.Name)
				TryAdd(oTempContextMatches, "CLIENTCOMPUTERADDRESS", oDNSEntry.AddressList.ToString)
				TryAdd(oTempContextMatches, "CLIENTCOMPUTERNAME", sClientCompName)
				TryAdd(oTempContextMatches, "CLIENTCOMPUTERDOMAIN", sClientDomain)
				TryAdd(oTempContextMatches, "HOSTCOMPUTERNAME", My.Computer.Name)
				TryAdd(oTempContextMatches, "ASSEMBLYNAME", oAssembly.FullName)
				TryAdd(oTempContextMatches, "ASSEMBLY", oAssembly.FullName)
				TryAdd(oTempContextMatches, "ASSEMBLYVERSION", oAssemblyName.Version.ToString)
				TryAdd(oTempContextMatches, "VERSION", oAssemblyName.Version.ToString)
				TryAdd(oTempContextMatches, "APPLICATIONDATE", System.IO.File.GetLastWriteTime(oAssembly.Location).ToString)
				TryAdd(oTempContextMatches, "ASSEMBLYDATE", System.IO.File.GetLastWriteTime(oAssembly.Location).ToString)
				TryAdd(oTempContextMatches, "CURRENTDIRECTORY", My.Computer.FileSystem.CurrentDirectory)
				TryAdd(oTempContextMatches, "TEMP", System.IO.Path.GetTempPath)
				TryAdd(oTempContextMatches, "TMP", System.IO.Path.GetTempPath)
				TryAdd(oTempContextMatches, "TEMPFOLDER", System.IO.Path.GetTempPath)
				TryAdd(oTempContextMatches, "TEMPDIRECTORY", System.IO.Path.GetTempPath)

				'http://msdn.microsoft.com/en-us/library/7ah135z7.aspx
				'http://msdn.microsoft.com/en-us/magazine/cc188706.aspx
				'http://msdn.microsoft.com/en-us/library/ms233781%28v=vs.100%29.aspx
				'http://www.tutorialspoint.com/vb.net/vb.net_directives.htm

				Try

#If _MyType = "Windows" Then
                oTempContextMatches("ASSEMBLYNAME") = My.Application.Info.AssemblyName
				tryadd(oTempContextMatches,"APPLICATIONEXE", My.Application.Info.AssemblyName & ".exe")
                tryadd(oTempContextMatches,"APPLICATIONNAME", My.Application.Info.ProductName)
                tryadd(oTempContextMatches,"APPLICATIONDIRECTORY", My.Application.Info.DirectoryPath)
				tryadd(oTempContextMatches,"APPLICATIONFOLDER", My.Application.Info.DirectoryPath)
				tryadd(oTempContextMatches,"APPLICATIONPATH", My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".exe")
				stemppath = My.Application.Info.DirectoryPath

#ElseIf _MyType = "WindowsForms" Then
                oTempContextMatches("ASSEMBLYNAME") = My.Application.Info.AssemblyName
                tryadd(oTempContextMatches,"APPLICATIONEXE", My.Application.Info.AssemblyName & ".exe")
                tryadd(oTempContextMatches,"APPLICATIONNAME", My.Application.Info.ProductName)
				tryadd(oTempContextMatches,"APPLICATIONDIRECTORY", My.Application.Info.DirectoryPath)
				tryadd(oTempContextMatches,"APPLICATIONFOLDER", My.Application.Info.DirectoryPath)
				tryadd(oTempContextMatches,"APPLICATIONPATH", My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".exe")
				stemppath = My.Application.Info.DirectoryPath
#ElseIf _MyType = "Web" Then
					tryadd(oTempContextMatches,"RequestURL", My.Request.Url.AbsoluteUri)
					tryadd(oTempContextMatches,"RequestFilepath", My.Request.FilePath)
					tryadd(oTempContextMatches,"ApplicationPhysicalFilepath", My.Request.PhysicalApplicationPath)
					tryadd(oTempContextMatches,"APPLICATIONPATH", My.Request.ApplicationPath)
					'tryadd(oTempContextMatches,"APPLICATIONNAME", My.Request.FilePathObject.filename)
					stemppath = My.Request.Url.AbsoluteUri
#Else
					TryAdd(oTempContextMatches, "RequestURL", My.Request.Url.AbsoluteUri)
					TryAdd(oTempContextMatches, "RequestFilepath", My.Request.FilePath)
					TryAdd(oTempContextMatches, "ApplicationPhysicalFilepath", My.Request.PhysicalApplicationPath)
					TryAdd(oTempContextMatches, "APPLICATIONPATH", My.Request.ApplicationPath)
					'tryadd(oTempContextMatches,"APPLICATIONNAME", My.Request.FilePathObject.filename)
					sTempPath = My.Request.Url.AbsoluteUri

#End If

					If sTempPath <> "" Then bInTest = (sTempPath.IndexOf("test", StringComparison.CurrentCultureIgnoreCase) > 0 Or sTempPath.IndexOf("dev", StringComparison.CurrentCultureIgnoreCase) > 0)

				Catch ex As Exception

				End Try

				For Each sKey In oTempContextMatches.Keys
					sValue = oTempContextMatches.Item(sKey).ToString
					TryAdd(oFixedSubstituteList, sKey, sValue)
					'If Not oFixedSubstituteList.ContainsKey(sKey) Then oFixedSubstituteList.Add(sKey, sValue) Else oFixedSubstituteList(sKey) = sValue
				Next

			End If

#End Region

#Region "Reset variable data"

			oReturn = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase) '  New SortedList(Of String, String)
			For Each sKey In oFixedSubstituteList.Keys
				sValue = oFixedSubstituteList.Item(sKey).ToString
				'If Not oReturn.ContainsKey(sKey) Then oReturn.Add(sKey, sValue) Else oReturn(sKey) = sValue
				TryAdd(oReturn, sKey, sValue)
			Next

			oVariableSubstituteList.Clear()

			iMonthDay = DatePart(DateInterval.Day, dPassedDate)
			iYearDay = DatePart(DateInterval.DayOfYear, dPassedDate)
			iMonth = DatePart(DateInterval.Month, dPassedDate)
			iYearDay = DatePart(DateInterval.Year, dPassedDate)
			iYear = DatePart(DateInterval.Year, dPassedDate)
			iWeekDay = DatePart(DateInterval.Weekday, dPassedDate)
			sDayName = WeekdayName(iWeekDay, , Microsoft.VisualBasic.FirstDayOfWeek.Sunday)
			iWeek = DatePart(DateInterval.WeekOfYear, dPassedDate)

			iFinYear = Year(Now) + CInt(IIf(Month(dPassedDate) < 4, -1, 0))
			dtFinYear = New Date(iFinYear, 4, 1)
			iFinMonth = CInt(DateDiff(DateInterval.Month, dtFinYear, dPassedDate) + 1)
			iFinDays = CInt(DateDiff(DateInterval.DayOfYear, dtFinYear, dPassedDate) + 1)
			iFinWeek = CInt(DateDiff(DateInterval.WeekOfYear, dtFinYear, dPassedDate) + 1)

			iHour = DatePart(DateInterval.Hour, dPassedDate)
			iMinute = DatePart(DateInterval.Minute, dPassedDate)
			iSecond = DatePart(DateInterval.Second, dPassedDate)
			iMilliseconds = dPassedDate.Millisecond
			lTicks = dPassedDate.Ticks

			TryAdd(oTempContextMatches, "date", dPassedDate.ToShortDateString)
			TryAdd(oTempContextMatches, "fulldate", dPassedDate.ToString("dd MMMM yyyy"))
			TryAdd(oTempContextMatches, "GenericDate", dPassedDate.ToString("dd-MMM-yyyy"))
			TryAdd(oTempContextMatches, "dateshort", dPassedDate.ToShortDateString)
			TryAdd(oTempContextMatches, "datesort", dPassedDate.ToString("yyyyMMdd"))
			TryAdd(oTempContextMatches, "sortdate", dPassedDate.ToString("yyyyMMdd"))
			TryAdd(oTempContextMatches, "datelong", dPassedDate.ToLongDateString)
			TryAdd(oTempContextMatches, "dateUTC", dPassedDate.ToString("o"))
			TryAdd(oTempContextMatches, "datetimesort", dPassedDate.ToString("yyyyMMddHHmmssffff"))
			TryAdd(oTempContextMatches, "time", dPassedDate.ToShortTimeString)
			TryAdd(oTempContextMatches, "timeshort", dPassedDate.ToShortTimeString)
			TryAdd(oTempContextMatches, "timelong", dPassedDate.ToLongTimeString)
			TryAdd(oTempContextMatches, "timesort", dPassedDate.ToString("HHmmss"))
			TryAdd(oTempContextMatches, "day", iMonthDay.ToString)
			TryAdd(oTempContextMatches, "d", iMonthDay.ToString)
			TryAdd(oTempContextMatches, "dd", iMonthDay.ToString("00"))
			TryAdd(oTempContextMatches, "days", iYearDay.ToString)
			TryAdd(oTempContextMatches, "findays", iFinDays.ToString)
			TryAdd(oTempContextMatches, "ww", iWeek.ToString("00"))
			TryAdd(oTempContextMatches, "week", iWeek.ToString)
			TryAdd(oTempContextMatches, "weekday", iWeekDay.ToString)
			TryAdd(oTempContextMatches, "weekdayname", sDayName)
			TryAdd(oTempContextMatches, "finww", iFinWeek.ToString("00"))
			TryAdd(oTempContextMatches, "finweek", iFinWeek.ToString)
			TryAdd(oTempContextMatches, "mm", iMonth.ToString("00"))
			TryAdd(oTempContextMatches, "mmm", MonthName(iMonth, True))
			TryAdd(oTempContextMatches, "mon", MonthName(iMonth, True))
			TryAdd(oTempContextMatches, "monthshort", MonthName(iMonth, True))
			TryAdd(oTempContextMatches, "month", MonthName(iMonth, False))
			TryAdd(oTempContextMatches, "monthlong", MonthName(iMonth, False))
			TryAdd(oTempContextMatches, "mmmm", MonthName(iMonth, False))
			TryAdd(oTempContextMatches, "finmm", iFinMonth.ToString("00"))
			TryAdd(oTempContextMatches, "year", iYear.ToString("0000"))
			TryAdd(oTempContextMatches, "yyyy", iYear.ToString("0000"))
			TryAdd(oTempContextMatches, "yy", (iYear Mod 100).ToString("00"))
			TryAdd(oTempContextMatches, "finyear", iFinYear.ToString("0000"))
			TryAdd(oTempContextMatches, "finyyyy", iFinYear.ToString("0000"))
			TryAdd(oTempContextMatches, "finyy", (iFinYear Mod 100).ToString("00"))
			TryAdd(oTempContextMatches, "yyyymm", iYear.ToString("0000") & iMonth.ToString("00"))
			TryAdd(oTempContextMatches, "mmyyyy", iMonth.ToString("00") & iYear.ToString("0000"))
			TryAdd(oTempContextMatches, "yyyymmdd", iYear.ToString("0000") & iMonth.ToString("00") & iMonthDay.ToString("00"))
			TryAdd(oTempContextMatches, "ddmmyyyy", iMonthDay.ToString("00") & iMonth.ToString("00") & iYear.ToString("0000"))
			TryAdd(oTempContextMatches, "hour", iHour.ToString)
			TryAdd(oTempContextMatches, "hours", iHour.ToString)
			TryAdd(oTempContextMatches, "hh", iHour.ToString("00"))
			TryAdd(oTempContextMatches, "h", iHour.ToString)
			TryAdd(oTempContextMatches, "hhmm", iHour.ToString("00") & iMinute.ToString("00"))
			TryAdd(oTempContextMatches, "min", iMinute.ToString("00"))
			TryAdd(oTempContextMatches, "minute", iMinute.ToString)
			TryAdd(oTempContextMatches, "minutes", iMinute.ToString)
			TryAdd(oTempContextMatches, "nn", iMinute.ToString("00"))
			TryAdd(oTempContextMatches, "n", iMinute.ToString)
			TryAdd(oTempContextMatches, "hhmmss", iHour.ToString("00") & iMinute.ToString("00") & iSecond.ToString("00"))
			TryAdd(oTempContextMatches, "ss", iSecond.ToString("00"))
			TryAdd(oTempContextMatches, "s", iSecond.ToString)
			TryAdd(oTempContextMatches, "sec", iSecond.ToString)
			TryAdd(oTempContextMatches, "second", iSecond.ToString)
			TryAdd(oTempContextMatches, "seconds", iSecond.ToString)
			TryAdd(oTempContextMatches, "milliseconds", iMilliseconds.ToString)
			TryAdd(oTempContextMatches, "millisecond", iMilliseconds.ToString)
			TryAdd(oTempContextMatches, "mill", iMilliseconds.ToString)
			TryAdd(oTempContextMatches, "tick", lTicks.ToString)
			TryAdd(oTempContextMatches, "ticks", lTicks.ToString)

			TryAdd(oTempContextMatches, "TestOrLive", IIf(bInTest, "Test", "Live").ToString)
			TryAdd(oTempContextMatches, "LIVEOrNothing", IIf(bInTest, "", "LIVE").ToString)
			TryAdd(oTempContextMatches, "TestOrNothing", IIf(bInTest, "Test", "").ToString)
			TryAdd(oTempContextMatches, "TESTOrAPP", IIf(bInTest, "TEST", "APP").ToString)

			For Each sKey In oTempContextMatches.Keys
				sValue = oTempContextMatches.Item(sKey).ToString
				TryAdd(oVariableSubstituteList, sKey, sValue)
				'If Not oVariableSubstituteList.ContainsKey(sKey) Then oVariableSubstituteList.Add(sKey, sValue) Else oVariableSubstituteList.Item(sKey) = sValue

			Next

			For Each sKey In oVariableSubstituteList.Keys
				sValue = oVariableSubstituteList.Item(sKey).ToString
				'If Not oReturn.ContainsKey(sKey) Then oReturn.Add(sKey, sValue) Else oReturn.Item(sKey) = sValue
				TryAdd(oReturn, sKey, sValue)
			Next

#End Region

			'If aExtraStrings.Count > 0 Then
			'    For Each sKey In aExtraStrings.Keys
			'        'If Not aExtraStrings.ContainsKey(sKey) Then sValue = aExtraStrings.Item(sKey).ToString
			'    Next
			'End If



			For Each sKey In aExtraStrings.Keys
				Try

					oTemp = aExtraStrings.Item(sKey)
					If oTemp Is Nothing Then sValue = "" Else sValue = oTemp.ToString
					TryAdd(oExtraSubstituteStrings, sKey, sValue)
					TryAdd(oReturn, sKey, sValue)
					'If Not oExtraSubstituteStrings.ContainsKey(sKey) Then oExtraSubstituteStrings.Add(sKey, sValue) Else oExtraSubstituteStrings.Item(sKey) = sValue
					'If Not oReturn.ContainsKey(sKey) Then oReturn.Add(sKey, sValue) Else oReturn.Item(sKey) = sValue

				Catch ex2 As Exception
					If Not oReturn.ContainsKey(sKey) Then oReturn.Add(sKey, "")
				End Try
			Next

			oAllSubstituteStrings = oReturn

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
	End Sub


	Private Sub TryAdd(ByRef oCollection As SortedList(Of String, String), sKey As String, sValue As String)
		Try
			If oCollection.ContainsKey(sKey) Then oCollection.Item(sKey) = sValue Else oCollection.Add(sKey, sValue)

		Catch ex As Exception
			Try : oCollection.Item(sKey) = sValue : Catch ex2 As Exception : End Try
			'Dim aDetail As New ArrayList
			'aDetail.Add("Key : " & sKey)
			'aDetail.Add("Value : """ & sValue & """")
			'RecordError(ex, aDetail)

		End Try
	End Sub


	Protected Friend Sub SplitString(aExtraStringsIn As SortedList(Of String, String), Optional UseTokenMarks As Boolean = False, Optional ResetValues As Boolean = False)
		Dim sReturn As String = sOriginalString, sUpdateIgnore As String = Space(Len(sReturn))
		Dim iPosLast As Integer = 1, oVal As SubstituteEntry, iMatchlength As Integer
		Dim iPrevMatchEnd As Integer = 0, iPrevToNewLength As Integer = 0
		Dim sRegexKey As String = "", sKeyRaw As String = "", sKeyUsed As String
		Dim sValue As String = "", sNewValue As String = "", sTemp As String = "", sNewKey As String = ""
		Dim oTempContextMatches As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase) ' New SortedList(Of String, String)
		Dim sTemp2 As String = ""
		Dim sUserId As String = "", sDomain As String = ""
		Dim iPosMarker As Integer, oMatches As MatchCollection, oMatch As Match, iCurrMatchStart As Integer
		Dim oReconstructLine As New Hashtable, iCharPos As Integer = 1, iTokenPos As Integer = 0, iAftMatchPos As Integer = 0
		Dim sCurrValString As String, sBefore As String, sAfter As String, sPrevValString As String = ""
		Dim aUsedTokenStart As ArrayList, aUsedTokenEnd As ArrayList, sTokenStart As String, sTokenEnd As String, sEvalString As String
		Dim sTokenType As String = "", sValueModifier As String = "", iNewBlockLevel As Integer, bSplitStringSection As Boolean = False
		Dim iPrevIndex As Integer = 0, iNextIndex As Integer = 0, iCurrMatchEnd As Integer, sPrevToken As String
		Dim iBaseCharPos As Integer, iMatchCharPos As Integer, iBefCharPos As Integer, iAftCharPos As Integer, bForceNewVal As Boolean = False, bForceNextNewValue As Boolean = False
		Dim aExtraStrings As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase), iTemp As Integer
		Dim oCurrSubstitutes As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
		Dim gBefore As Group, gTag As Group, gAfter As Group, sRegexKeyUsed As String, sValueLabel As String

		Try

			Me.Clear()
			Me.AddNew(sReturn, "", iCharPos, False, sReturn)
			'If aExtraStringsIn IsNot Nothing Then aExtraStrings = New SortedList(aExtraStringsIn, New CaseInsensitiveComparer()) Else aExtraStrings = New SortedList(New CaseInsensitiveComparer())
			If aExtraStringsIn IsNot Nothing Then aExtraStrings = aExtraStringsIn Else aExtraStrings = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

			aUsedTokenStart = New ArrayList
			aUsedTokenEnd = New ArrayList

			If UseTokenMarks Then
				aUsedTokenStart = New ArrayList(StartTokens.ToArray)
				aUsedTokenEnd = New ArrayList(EndTokens.ToArray)
				For iTokenPos = aUsedTokenStart.Count - 1 To 0 Step -1
					If sReturn.IndexOf(aUsedTokenStart(iTokenPos).ToString, StringComparison.CurrentCultureIgnoreCase) < 0 Then
						aUsedTokenStart.RemoveAt(iTokenPos)
						aUsedTokenEnd.RemoveAt(iTokenPos)
					End If
				Next
			End If

			If sReturn <> "" And aUsedTokenStart.Count > 0 And aUsedTokenEnd.Count > 0 Then
				If oAllSubstituteStrings Is Nothing Then oAllSubstituteStrings = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
				If ResetValues Or gbResetReplacements Or oAllSubstituteStrings.Count = 0 Then PopulateReplacementArray(aExtraStringsIn, ResetValues) ' Create all the needed values

				iPosMarker = 0
				sRegexKey = ""
				For iTokenPos = 0 To aUsedTokenStart.Count - 1

					sTokenStart = aUsedTokenStart(iTokenPos).ToString
					sTokenEnd = aUsedTokenEnd(iTokenPos).ToString

					' Split the string into block sections based on key words, each with it's own syntax.
					If sTokenStart <> "" Then
						sRegexKey &= IIf(sRegexKey <> "", "|", "").ToString &
							  "(?<TokenStart>" & RegexMetaString(sTokenStart) &
							  "\s*)(?<TokenType>[a-z0-9_.~]+)(?<ValueMods>(?<FirstDelim>\s*[|:;\s]?\s*)?(?<ValueModifier>[^" & RegexMetaString(Left(sTokenEnd, 1)) &
							  "]+))?(?<TokenEnd>\s*" & RegexMetaString(sTokenEnd) & ")"
					End If
				Next ' token delimiter types

				Do While iPosMarker < Me.Count
					oVal = Me.Item(iPosMarker)
					iBaseCharPos = oVal.CharPos
					sCurrValString = oVal.OrigString
					iPosMarker += 1
					iCharPos = iBaseCharPos
					sPrevToken = oVal.TokenType
					sPrevValString = oVal.OrigString

					oMatches = Regex.Matches(sCurrValString, sRegexKey, RegexOptions.IgnoreCase Or RegexOptions.Multiline)
					iPrevIndex = 0

					iBefCharPos = 0
					For Each oMatch In oMatches

						If iBefCharPos = 0 Then iBefCharPos = iBaseCharPos
						iMatchCharPos = iBaseCharPos + oMatch.Index - 1
						If oMatch.NextMatch IsNot Nothing AndAlso oMatch.NextMatch.Success Then
							iNextIndex = oMatch.NextMatch.Index
						Else
							iNextIndex = sCurrValString.Length
						End If

						Try
							gBefore = oMatch.Groups("TokenStart")
							gTag = oMatch.Groups("TokenType")
							gAfter = oMatch.Groups("TokenEnd")

							sTokenType = oMatch.Groups("TokenType").Value
							sValueLabel = sTokenType
							sValueModifier = oMatch.Groups("ValueModifier").Value
							sEvalString = ""
							iNewBlockLevel = oVal.BlockLevel
							bSplitStringSection = False
							bForceNewVal = bForceNextNewValue
							bForceNextNewValue = False
							Select Case sTokenType
								Case "If", "beginif", "ifbegin", "begin"
									iNewBlockLevel += 1
									bSplitStringSection = True
									sEvalString = sValueModifier
									sValueModifier = ""
									bForceNextNewValue = True ' TEst this
								Case "endif", "ifend", "end"
									iNewBlockLevel -= 1
									bSplitStringSection = True
									bForceNewVal = True
								Case "eval", "evaluate", "compute", "calculate", "calc"
									bSplitStringSection = True
									sEvalString = sValueModifier
									sValueModifier = ""
									'bForceNewVal = True
									bForceNextNewValue = True
								Case Else
									If oAllSubstituteStrings.ContainsKey(sTokenType) AndAlso Not oCurrSubstitutes.ContainsKey(sTokenType) Then oCurrSubstitutes.Add(sTokenType, oAllSubstituteStrings.Item(sTokenType))
									If iCharPos <= 1 And oMatch.Index > iCharPos Then iCharPos = oMatch.Index + oMatch.Length
							End Select

							If bSplitStringSection Then

								sBefore = ""
								sAfter = ""
								iMatchlength = oMatch.Index - iPrevIndex
								iCurrMatchStart = gBefore.Index
								iCurrMatchEnd = gAfter.Index + gAfter.Length
								iAftCharPos = iMatchCharPos + oMatch.Length
								If oMatch.Index - iPrevIndex > 0 Then sBefore = sCurrValString.Substring(iPrevIndex, oMatch.Index - iPrevIndex)
								If iCurrMatchEnd < sCurrValString.Length Then
									sAfter = sCurrValString.Substring(iCurrMatchEnd, iNextIndex - iCurrMatchEnd)
								End If
								iPrevIndex = iAftCharPos

								If sBefore <> "" Or bForceNewVal Or sPrevToken <> "" Then
									oVal.OrigString = sBefore
									oVal.Result = sBefore
									iCharPos = iBaseCharPos + sBefore.Length
									oVal = Me.AddNew(sValueModifier, sValueLabel, iMatchCharPos, True, sValueModifier, sEvalString, iNewBlockLevel, sTokenType, sValueModifier)
									iPosMarker += 1
								Else
									oVal.OrigString = sValueModifier
									oVal.Result = sValueModifier
									oVal.HasBeenSubstituted = True
									oVal.BlockLevel = iNewBlockLevel
									oVal.EvalExpression = sEvalString
									oVal.TokenType = sTokenType
								End If

								iCharPos = oVal.CharPos + oMatch.Length

								If sAfter <> "" Or bForceNextNewValue Then
									oVal = Me.AddNew(sAfter, sValueLabel, iAftCharPos, False, sAfter, "", iNewBlockLevel, "", sValueModifier)
									iPosMarker += 1
									iCharPos = oVal.CharPos + sAfter.Length
									iBefCharPos = iAftCharPos

								End If
							End If ' splitting up the tokenised section
						Catch ex2 As Exception

						End Try
						sPrevToken = oVal.TokenType
						sPrevValString = oVal.OrigString
					Next '  matches collection cycle

					If iCharPos < sCurrValString.Length And iPosMarker > 1 Then
						sAfter = sCurrValString.Substring(iCharPos)
						oVal = Me.AddNew(sAfter, "", iMatchCharPos, False, sAfter, "", iNewBlockLevel, "", sValueModifier)
					Else
						sAfter = ""
					End If
				Loop ' loop for string sections



				' Check all strings for substitution using the label and value array


				For Each sKeyRaw In oCurrSubstitutes.Keys

					If sKeyRaw <> "" Then
						sNewValue = oCurrSubstitutes.Item(sKeyRaw).ToString
						iPosLast = 1
						sRegexKey = ""
						sKeyUsed = sKeyRaw
						For iTokenPos = 0 To aUsedTokenStart.Count - 1

							sTokenStart = aUsedTokenStart(iTokenPos).ToString
							sTokenEnd = aUsedTokenEnd(iTokenPos).ToString

							sKeyUsed = sTokenStart & sKeyRaw & sTokenEnd
							sRegexKeyUsed = RegexMetaString(sTokenStart) & "(?<ValueLabel>" & RegexMetaString(sKeyRaw) & ")(?<ValueMods>\s*([|:;\s])\s*(?<ValueModifier>" & IIf(sTokenEnd = "", "", "[^" & RegexMetaString(Left(sTokenEnd, 1))).ToString & "]*))?" & RegexMetaString(sTokenEnd)

							sRegexKey &= IIf(sRegexKey <> "", "|", "").ToString & IIf(sTokenStart = "", "(?<=^|[^a-z0-9_.~])", "").ToString & "(?<FieldName>" & sRegexKeyUsed & ")" & IIf(sTokenEnd = "", "(?=$|[^a-z0-9_.~])", "").ToString
						Next

						iPosMarker = 0
						Do While iPosMarker < Me.Count
							oVal = Me.Item(iPosMarker)
							iBaseCharPos = oVal.CharPos
							sCurrValString = oVal.OrigString
							iPrevIndex = 0
							iNextIndex = oVal.OrigString.Length
							iPosMarker += 1
							iCharPos = iBaseCharPos
							iBefCharPos = 0
							sPrevToken = oVal.TokenType
							sPrevValString = oVal.OrigString


							If Not oVal.HasBeenSubstituted And sCurrValString.Length >= sKeyUsed.Length Then
								sBefore = ""
								sAfter = ""
								oMatches = Regex.Matches(sCurrValString, sRegexKey, RegexOptions.IgnoreCase Or RegexOptions.Multiline)
								If oMatches.Count > 0 Then

									iPrevMatchEnd = 0
									For Each oMatch In oMatches
										If iBefCharPos = 0 Then iBefCharPos = iBaseCharPos
										iMatchCharPos = iBaseCharPos + oMatch.Index - 1

										sValueLabel = oMatch.Groups("ValueLabel").Value
										sValueModifier = oMatch.Groups("ValueModifier").Value

										If oMatch.NextMatch IsNot Nothing AndAlso oMatch.NextMatch.Success Then iNextIndex = oMatch.NextMatch.Index Else iNextIndex = sCurrValString.Length
										sBefore = ""
										sAfter = ""


										iPrevToNewLength = oMatch.Index - iPrevMatchEnd
										iMatchlength = oMatch.Index - iPrevIndex
										iAftCharPos = iMatchCharPos + oMatch.Length
										iAftMatchPos = oMatch.Index + oMatch.Length
										If iPrevToNewLength > 0 Then sBefore = sCurrValString.Substring(iPrevMatchEnd, iPrevToNewLength)
										If oMatch.Index + oMatch.Length < sCurrValString.Length And iAftMatchPos < sCurrValString.Length Then
											iTemp = iNextIndex - iAftMatchPos
											If iTemp >= sCurrValString.Length - iAftMatchPos Then
												sAfter = sCurrValString.Substring(iAftMatchPos)
											Else
												sAfter = sCurrValString.Substring(iAftMatchPos, iTemp)
											End If

										End If
										iPrevIndex = iAftCharPos
										iPrevMatchEnd = iAftMatchPos

										If sBefore <> "" Or sPrevToken <> "" Then
											oVal.OrigString = sBefore
											oVal.Result = sBefore
											iCharPos = oVal.CharPos + sBefore.Length
											oVal = Me.AddNew(oMatch.Value, sValueLabel, iCharPos, True, sNewValue, "", oVal.BlockLevel, "", sValueModifier)
											iPosMarker += 1
										Else
											oVal.OrigString = oMatch.Value
											oVal.ValueLabel = sValueLabel
											oVal.ValueModifier = sValueModifier
											oVal.Result = sNewValue
											oVal.HasBeenSubstituted = True
										End If

										iCharPos = oVal.CharPos + oMatch.Length

										If sAfter <> "" Then
											oVal = Me.AddNew(sAfter, "", iAftCharPos, False, sAfter, "", oVal.BlockLevel, "", sValueModifier)
											iCharPos = oVal.CharPos + sAfter.Length
										End If

										sPrevToken = oVal.TokenType
										sPrevValString = oVal.OrigString
									Next ' Matches count
								End If ' Has matches end block

							End If ' Match value length could be in tokenised string
						Loop ' Loop for string sections

					End If
				Next ' Substitution key value pairs
			End If

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Examining string of """ & sReturn & """")
			If sKeyRaw > "" Then aDetail.Add("Last key of """ & sKeyRaw & """ with value """ & sNewValue & """")
			RecordError(ex, aDetail)

		End Try

	End Sub


	Private Function RegexMetaString(sOrig As String) As String
		Dim sReturn As String = sOrig, sVal As String = ""
		Dim aMetaVals() As String = {"\", "$", "<", ">", "{", "}", "[", "]", "(", ")", "|", ".", "*", "?", "+"}
		Try

			For Each sVal In aMetaVals
				sReturn = sReturn.Replace(sVal, "\" & sVal)
			Next

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Examining string of """ & sReturn & """")
			If sVal > "" Then aDetail.Add("Last Meta of """ & sVal)
			RecordError(ex, aDetail)

		End Try
		Return sReturn
	End Function

	Public Sub New()

	End Sub

	Public Sub New(StringToUpdate As String)
		Try
			PopulateReplacementArray()
			Me.OriginalString = StringToUpdate

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Opening with string of """ & StringToUpdate & """")
			RecordError(ex, aDetail)

		End Try

	End Sub

	Public Sub New(StringToUpdate As String, oVariableSubstitutes As SortedList(Of String, String))
		Try
			PopulateReplacementArray(oVariableSubstitutes)
			Me.OriginalString = StringToUpdate

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Opening with string of """ & StringToUpdate & """")
			RecordError(ex, aDetail)

		End Try

	End Sub

	Public Sub New(StringToUpdate As String, oVariableSubstitutes As SortedList(Of String, String), UseReplacementModifier As TargetModifier)
		Try
			PopulateReplacementArray(oVariableSubstitutes)
			ReplacementModifier = UseReplacementModifier
			Me.OriginalString = StringToUpdate

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Opening with string of """ & StringToUpdate & """")
			RecordError(ex, aDetail)

		End Try

	End Sub

	Public Sub AddNewValues(ValuesList As SortedList(Of String, String))
		Dim aExtraStrings As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
		Dim sValue As String, sKey As String
		Try

			If oAllSubstituteStrings Is Nothing OrElse oAllSubstituteStrings.Count = 0 Then
				PopulateReplacementArray(ValuesList, False)

			Else

				aExtraStrings = ValuesList


				For Each sKey In aExtraStrings.Keys
					Try

						sValue = aExtraStrings.Item(sKey).ToString
						AddNewValue(sKey, sValue)

					Catch ex2 As Exception
						If Not oAllSubstituteStrings.ContainsKey(sKey) Then oAllSubstituteStrings.Add(sKey, "")
					End Try
				Next

			End If

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
	End Sub


	Public Sub AddNewValue(TokenKey As String, Value As String)
		Try

			Try
				If Not oAllSubstituteStrings.ContainsKey(TokenKey) Then
					oAllSubstituteStrings.Add(TokenKey, Value)
				Else
					oAllSubstituteStrings(TokenKey) = Value
				End If

				If Not oExtraSubstituteStrings.ContainsKey(TokenKey) Then
					oExtraSubstituteStrings.Add(TokenKey, Value)
				Else
					oExtraSubstituteStrings(TokenKey) = Value
				End If

			Catch ex2 As Exception
				If Not oAllSubstituteStrings.ContainsKey(TokenKey) Then oAllSubstituteStrings.Add(TokenKey, "")
			End Try

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
	End Sub


	Public Function AddNew(OrigString As String, ValueLabel As String, Position As Integer, HasBeenSustituted As Boolean, NewString As String) As SubstituteEntry
		Dim oNewVal As SubstituteEntry = Nothing
		Try
			oNewVal = Me.AddNew(OrigString, ValueLabel, Position, HasBeenSustituted, NewString, "", 1, "", "")
		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return oNewVal
	End Function


	Public Function AddNew(OrigString As String, ValueLabel As String, Position As Integer, HasBeenSustituted As Boolean, NewString As String, EvalExpression As String, BlockLevel As Integer, TokenType As String, ValueModifier As String) As SubstituteEntry
		Dim oNewVal As SubstituteEntry = Nothing, oVal As SubstituteEntry = Nothing, iPos As Integer
		Try
			oNewVal = New SubstituteEntry(OrigString, ValueLabel, Position, HasBeenSustituted, NewString, EvalExpression, BlockLevel, TokenType, ValueModifier)
			oNewVal.oParentCol = Me

			For iPos = 0 To Me.Count - 1
				oVal = Me.Item(iPos)
				If oVal.CharPos > Position Then Exit For
				If oVal.CharPos = Position Then
					iPos += 1
					Exit For
				End If
				oVal = Nothing
			Next

			If oVal IsNot Nothing Then
				Me.Insert(iPos, oNewVal)
			Else
				Me.Add(oNewVal)
			End If

		Catch ex As Exception

			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return oNewVal
	End Function


	Protected Friend Sub RecordError(exIn As Exception, ExtraDets As ArrayList)
		Try

			Dim oErr As wRuntimeError = goErrs.NewError(exIn)
			If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.sExtraDetail &= String.Join("<br/>", ExtraDets)
			gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")
			'Dim oError As ErrorItem = glErrors.NewError(exIn, ExtraDets)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		Catch ex As Exception

		End Try
	End Sub

End Class




Public Class SubstituteEntry
	Implements IComparable(Of SubstituteEntry)

	Protected Friend CharPos As Integer = 0
	Protected Friend OrigString As String = ""
	Protected Friend ValueLabel As String = ""
	Protected Friend HasBeenSubstituted As Boolean = False
	Dim sResult As String = ""
	Protected Friend BlockLevel As Integer = 1
	Protected Friend EvalExpression As String = ""
	Protected Friend ValueModifier As String = ""
	Protected Friend TokenType As String = ""

	Protected Friend oParentCol As clsValueSubstitutions

	Protected Friend Property Result As String
		Get
			Dim oReturn As Object = sResult, sReturn As String = sResult
			Dim ModType As String = "", ModParameters As String = ""
			Dim oMatches As MatchCollection, oMatch As Match, oGrp As Group

			Try

				If sResult = "" And EvalExpression <> "" Then
					oReturn = Me.EvalResult
					sReturn = oReturn.ToString
				End If
				If ValueModifier <> "" Then
					oMatches = Regex.Matches(ValueModifier, "^(?<ModType>[^:;\s]+)((\s|[:;])(?<ModParameters>.*))?$")

					If oMatches.Count > 0 Then
						oMatch = oMatches.Item(0)
						oGrp = oMatch.Groups.Item("ModType")
						If oGrp IsNot Nothing Then ModType = oGrp.Value
						oGrp = oMatch.Groups.Item("ModParameters")
						If oGrp IsNot Nothing Then ModParameters = oGrp.Value

						Select Case ModType
							Case "Format"
								If IsDate(sReturn) Then
									sReturn = CDate(sReturn).ToString(ModParameters)
								ElseIf IsNumeric(sReturn) Then
									sReturn = CDbl(sReturn).ToString(ModParameters)
								Else
									sReturn = String.Format(sReturn, ModParameters)
								End If

							Case "Case", "StrConv"
								sReturn = StrConv(sReturn, CType([Enum].Parse(GetType(VbStrConv), ModParameters), VbStrConv))

						End Select

					End If
				End If
				If ValueLabel <> "" Then

					Select Case oParentCol.ReplacementModifier
						Case clsValueSubstitutions.TargetModifier.NoModify
						Case clsValueSubstitutions.TargetModifier.ForFilePath
							sReturn = ValueNTPathModified(sReturn)
						Case clsValueSubstitutions.TargetModifier.ForURI
							sReturn = ValueURIModified(sReturn)

					End Select

				End If

			Catch ex As Exception

			End Try

			Return sReturn
		End Get
		Set(value As String)
			sResult = value
		End Set
	End Property



	Public Function CompareTo(other As SubstituteEntry) As Integer Implements IComparable(Of SubstituteEntry).CompareTo
		Return Me.CharPos.CompareTo(other.CharPos)
	End Function

	Public Sub New(OrigString As String, ValueLabel As String, Position As Integer, HasBeenSustituted As Boolean, NewString As String)

		Me.New(OrigString, ValueLabel, Position, HasBeenSustituted, NewString, "", 1, "", "")

	End Sub

	Public Sub New(OrigString As String, ValueLabel As String, Position As Integer, HasBeenSustituted As Boolean, NewString As String, EvalExpression As String, BlockLevel As Integer, TokenType As String, ValueModifier As String)


		Me.OrigString = OrigString
		Me.ValueLabel = ValueLabel
		Me.CharPos = Position
		Me.HasBeenSubstituted = HasBeenSustituted
		Me.Result = NewString
		Me.EvalExpression = EvalExpression
		Me.BlockLevel = BlockLevel
		Me.TokenType = TokenType
		Me.ValueModifier = ValueModifier

	End Sub


	Public Sub New()

	End Sub


	Public Function EvalIsValid() As Boolean
		Dim oChildSubstitution As New clsValueSubstitutions
		Dim bReturn As Boolean = False, sReplacedOrig As String = ""
		Dim oEvalResponse As Object = Nothing
		Try

			If EvalExpression = "" Then
				bReturn = (Result <> "")
			Else
				oEvalResponse = Me.EvalResult

				bReturn = oEvalResponse IsNot Nothing AndAlso (IsNumeric(oEvalResponse) AndAlso oEvalResponse.ToString <> "" AndAlso CDbl(oEvalResponse) <> 0) Or
							 (TypeOf (oEvalResponse) Is Boolean AndAlso CBool(oEvalResponse)) Or
							 (Not (TypeOf (oEvalResponse) Is Boolean Or IsNumeric(oEvalResponse)) And oEvalResponse.ToString <> "")

			End If


		Catch ex As Exception
			Dim aDetail As New ArrayList
			If EvalExpression <> "" Then aDetail.Add("Generating eval string of """ & EvalExpression & """")
			If sReplacedOrig <> "" Then aDetail.Add("Evaluating string of """ & sReplacedOrig & """")
			If oEvalResponse IsNot Nothing Then aDetail.Add("Eval result type is " & oEvalResponse.GetType.ToString & " with value of """ & oEvalResponse.ToString & """")

			RecordError(ex, aDetail)

		End Try
		Return bReturn
	End Function


	Public Function EvalResult() As Object
		Dim oChildSubstitution As clsValueSubstitutions
		Dim bReturn As Boolean = False, sReplacedOrig As String = "", oGrp As Group
		Dim oEvalResponse As Object = Nothing
		Dim sTemp As String, sTempStart As String = "", sTempEnd As String = "", sTempRegex As String
		Dim oMatch As Match, oMatches As MatchCollection, sFieldName As String = "", sFieldModifier As String = ""
		Try

			If EvalExpression = "" Then
				oEvalResponse = ""
			Else

				For iPos = 0 To UBound(oParentCol.StartTokens)
					sTemp = oParentCol.StartTokens(iPos).Substring(0, 1)
					If EvalExpression.IndexOf(sTemp) >= 0 Then
						If sTempStart = "" Then
							sTempStart = """?(["
							sTempEnd = "]{2})?""?"
						End If
						sTempStart &= sTemp
						sTemp = oParentCol.EndTokens(iPos).Substring(0, 1)
						If sTemp = "]" Then sTemp = "\]"
						sTempEnd = sTemp & sTempEnd
					End If
				Next
				If sTempStart = "" Then
					sTempStart = """?"
					sTempEnd = """?"
				Else
					sTempStart &= "]{2})?"
					sTempEnd = "([" & sTempEnd
				End If

				sTempRegex = "^" & sTempStart & "(?<FieldModifier>(!|NOT\s+))?(?<FieldName>\w[a-zA-Z0-9._~]+\w)" & sTempEnd & "$"

				oMatches = Regex.Matches(EvalExpression, sTempRegex, RegexOptions.IgnoreCase)
				If oMatches.Count > 0 Then
					oMatch = oMatches.Item(0)
					sFieldName = oMatch.Groups.Item("FieldName").Value
					oGrp = oMatch.Groups.Item("FieldModifier")
					If oGrp IsNot Nothing AndAlso oGrp.Value <> "" Then
						sFieldModifier = Trim(oGrp.Value)
					End If
					'ElseIf Regex.IsMatch(EvalExpression, "^""?\w[a-zA-Z0-9_.]+\w""?$") Then

					If oParentCol.oAllSubstituteStrings.ContainsKey(sFieldName) Then
						oEvalResponse = oParentCol.oAllSubstituteStrings.Item(sFieldName)
					Else
						If Regex.IsMatch(EvalExpression, "^""\w[a-zA-Z0-9._~]+\w""$") Then
							oEvalResponse = sReplacedOrig
						Else
							oEvalResponse = ""
						End If
					End If
					If sFieldModifier = "!" Or sFieldModifier = "NOT" Then
						If oEvalResponse Is Nothing OrElse oEvalResponse.ToString = "" Then oEvalResponse = "true" Else oEvalResponse = ""
					End If
				Else
					oChildSubstitution = New clsValueSubstitutions
					oChildSubstitution.oAllSubstituteStrings = oParentCol.oAllSubstituteStrings
					oChildSubstitution.OriginalString = EvalExpression

					sReplacedOrig = oChildSubstitution.Result
					If Regex.IsMatch(EvalExpression, "[(+\-*^/=<>!]") Then

						Try

							oEvalResponse = New clsEvalExpression(sReplacedOrig).Result

						Catch ex As Exception
							oEvalResponse = ex.TargetSite.ReflectedType.ToString & vbCrLf &
										 ex.Source & vbCrLf &
										 "Error in evaluating """ & sReplacedOrig & """" & vbCrLf &
										 ex.Message & vbCrLf &
										 ex.StackTrace
						End Try

					Else
						oEvalResponse = sReplacedOrig
					End If
				End If
			End If


			If oEvalResponse.ToString = """""" Then oEvalResponse = ""

		Catch ex As Exception
			Dim aDetail As New ArrayList
			If EvalExpression <> "" Then aDetail.Add("Generating eval String Of """ & EvalExpression & """")
			If sReplacedOrig <> "" Then aDetail.Add("Evaluating String Of """ & sReplacedOrig & """")
			If oEvalResponse IsNot Nothing Then aDetail.Add("Eval result type Is " & oEvalResponse.GetType.ToString & " With value Of """ & oEvalResponse.ToString & """")

			RecordError(ex, aDetail)

		End Try
		Return oEvalResponse

	End Function



	Protected Friend Sub RecordError(exIn As Exception, ExtraDets As ArrayList)
		Try

			Dim oErr As wRuntimeError = goErrs.NewError(exIn)
			If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.sExtraDetail &= String.Join("<br/>", ExtraDets)
			gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")
			'Dim oError As ErrorItem = glErrors.NewError(exIn, ExtraDets)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		Catch ex As Exception

		End Try

	End Sub

	Protected Friend Function ValueNTPathModified(Valuein As String) As String
		Dim sReturn As String = Valuein, aCode As Integer, iPos As Integer, sChar As Char, sNewChars As String
		'Dim sIllegalChars As String, sTemp As String
		Dim sBefore As String, sAfter As String, aIllegalChars As New List(Of Char)
		Try


			aIllegalChars.AddRange(IO.Path.GetInvalidFileNameChars.ToArray)
			aIllegalChars.AddRange(IO.Path.GetInvalidPathChars.ToArray)

			For iPos = Len(sReturn) - 1 To 0 Step -1
				sChar = CChar(sReturn.Substring(iPos, 1))
				aCode = Asc(sChar)
				sNewChars = sChar
				Select Case aCode
					Case 48 To 57, 65 To 90, 97 To 122
							' don't touch alphanumeric chars
					Case 40, 41, 44, 45, 46, 59, 91, 93, 94, 95, 126
								 ' <Space>  (   )   ,   -   .   ;   [   ]   ^   _    ~
										  ' these punctuation chars require no translation
					Case 57, 92
						' \   /
						' Change to an underscore (neutral character)
						sNewChars = "_"

					Case Else
						If aIllegalChars.Contains(sChar) Then
							sNewChars = "~" & Hex(aCode)
							'sReturn = sReturn.Substring(0, iPos - 1) & "~" & Hex(aCode) & sReturn.Substring(iPos + 1)
						ElseIf sChar = " " And oParentCol.sFilePathSpaceSubstitution <> "" Then
							' replace other chars with "~hex"
							sNewChars = oParentCol.sFilePathSpaceSubstitution
							'sReturn = sReturn.Substring(0, iPos - 1) & " _" & sReturn.Substring(iPos + 1)
						End If
				End Select
				If sNewChars <> sChar Then
					sBefore = "" : If iPos > 0 Then sBefore = sReturn.Substring(0, iPos)
					sAfter = "" : If iPos < sReturn.Length - 1 Then sAfter = sReturn.Substring(iPos + 1)
					'sReturn = sReturn.Substring(0, iPos - 1) & sNewChars & sReturn.Substring(iPos + 1)
					sReturn = sBefore & sNewChars & sAfter
				End If
			Next

		Catch ex As Exception
			Dim aDetail As New ArrayList
			If Valuein <> "" Then aDetail.Add("Modifying value """ & Valuein & """ for URI")

			RecordError(ex, aDetail)

		End Try
		Return sReturn
	End Function


	Protected Friend Function ValueURIModified(Valuein As String) As String
		Dim sReturn As String = Valuein, aCode As Integer, iPos As Integer
		Try

			For iPos = Len(sReturn) To 1 Step -1
				aCode = Asc(Mid$(sReturn, iPos, 1))
				Select Case aCode
					Case 48 To 57, 65 To 90, 97 To 122
							' don't touch alphanumeric chars
					Case 33, 35, 36, 38, 42, 43, 45, 46, 47, 58, 63, 64, 95, 126
						' !   #   $   &   *   +   -   .   /   :   ?   @   _    ~
						' these punctuation chars require no translation
					Case Else
						' replace other chars with "%hex"
						sReturn = Left$(sReturn, iPos - 1) & "%" & Hex$(aCode) _
				& Mid$(sReturn, iPos + 1)
				End Select
			Next

		Catch ex As Exception
			Dim aDetail As New ArrayList
			If Valuein <> "" Then aDetail.Add("Modifying value """ & Valuein & """ for URI")

			RecordError(ex, aDetail)

		End Try
		Return sReturn
	End Function


End Class




' ==================================================================================
' Version : 1.0.0
' Author  : R Collins of TDC
' Date    : 24 August 2015
' ==================================================================================

Public Class clsEvalExpression

	Private sExpression As String = ""
	Private sResult As String = ""
	Private oResult As Object = Nothing
	Private sCompileErrors As New ArrayList
	Private sClassTemplate As String
	Public ShowErrorAsResult As Boolean = True
	Protected Friend CompareAsText As Boolean = True

	Public ReadOnly Property Result As Object
		Get
			Dim oResult As Object = Nothing
			Try
				oResult = Evaluate()

			Catch ex As Exception
				Dim aDetail As New ArrayList
				RecordError(ex, aDetail)

			End Try

			Return oResult
		End Get
	End Property


	Public Property ClassTemplate As String
		Get
			Try
				'If sClassTemplate = "" Then
				sClassTemplate = "Option Compare " & IIf(CompareAsText, "Text ", "Binary").ToString & vbCrLf &
					  "Imports System " & vbCrLf &
					  "Imports Microsoft " & vbCrLf &
					  "Imports Microsoft.VisualBasic " & vbCrLf &
					  "Imports Microsoft.VisualBasic.Strings " & vbCrLf &
					  "Imports System.Math " & vbCrLf &
					  "Namespace AnyNamespace " & vbCrLf &
							 "Public Class AnyClass " & vbCrLf &
									 "Public Function AnyMethod() As Object " & vbCrLf &
											"Return {0}" & vbCrLf &
									 "End Function " & vbCrLf &
							 "End Class " & vbCrLf &
					  "End Namespace"
				'End If
			Catch ex As Exception
				Dim aDetail As New ArrayList
				RecordError(ex, aDetail)

			End Try

			Return sClassTemplate
		End Get
		Set(value As String)

			sClassTemplate = value

		End Set
	End Property


	Public Property Expression As String
		Get
			Return sExpression
		End Get
		Set(value As String)
			sExpression = value
			sResult = ""
		End Set
	End Property


	Public ReadOnly Property CompileErrorList As ArrayList
		Get
			Return sCompileErrors
		End Get
	End Property

	Public ReadOnly Property CompileErrorsAsString As String
		Get
			Dim sResult As String = ""
			Try
				If sCompileErrors.Count > 0 Then sResult = Join(sCompileErrors.ToArray, vbCrLf)

			Catch ex As Exception
				Dim aDetail As New ArrayList
				RecordError(ex, aDetail)

			End Try

			Return sResult
		End Get
	End Property


	Public Function Evaluate(Expression As String) As Object
		sExpression = Expression
		sResult = ""

		Return Me.Result
	End Function


	Public Function Evaluate() As Object
		Dim oReturn As Object = Nothing, oReturnClass As MethodInfo
		Dim oCompParams As New CompilerParameters
		Dim oCompResults As CompilerResults
		Dim aAssembly As Assembly, oAssemblyClass As Object
		Dim sClassCode As String = ""
		Dim sNamespace As String = "AnyNamespace", sClassFullName As String = "AnyNamespace.AnyClass"
		Dim sClassName As String = "AnyClass", sFunctionName As String = "AnyMethod"
		Dim aTypes() As Type, oAssembledType As Type, aMethods() As MethodInfo, oMethodTest As MethodInfo

		Try
			sResult = ""

			sClassCode = String.Format(Me.ClassTemplate, sExpression)

			oCompParams.GenerateExecutable = False
			oCompParams.GenerateInMemory = True

			oCompResults = New VBCodeProvider().CompileAssemblyFromSource(oCompParams, sClassCode)

			If oCompResults.Errors.Count > 0 Then
				For Each oErr In oCompResults.Errors
					If TypeOf oErr Is System.CodeDom.Compiler.CompilerError Then
						sCompileErrors.Add("Expression """ & sExpression & """ compilation Error : " & DirectCast(oErr, System.CodeDom.Compiler.CompilerError).ErrorText)
					Else
						sCompileErrors.Add("Expression """ & sExpression & """ compilation error : " & oErr.ToString)
					End If
				Next

			Else

				aAssembly = oCompResults.CompiledAssembly
				aTypes = aAssembly.GetTypes()
				For iPos = aTypes.Count - 1 To 0 Step -1
					oAssembledType = aTypes(iPos)
					If oAssembledType.IsVisible Then
						sNamespace = oAssembledType.Namespace
						sClassFullName = oAssembledType.FullName
						sClassName = oAssembledType.Name
						aMethods = oAssembledType.GetMethods()
						For iPos2 = 0 To aMethods.Count - 1
							oMethodTest = aMethods(iPos2)
							If oMethodTest.DeclaringType.Namespace = sNamespace And oMethodTest.DeclaringType.Name = sClassName Then
								sFunctionName = oMethodTest.Name
								Exit For
							End If

						Next
						Exit For
					End If

				Next

				oAssemblyClass = aAssembly.CreateInstance(sClassFullName)

				oReturnClass = oAssemblyClass.GetType().GetMethod(sFunctionName)
				oReturn = oReturnClass.Invoke(oAssemblyClass, Nothing)

				sResult = oReturn.ToString

			End If

		Catch ex As Exception
			Dim aDetail As New ArrayList

			aDetail.Add("Expression is " & sExpression)
			aDetail.Add("Class generated is  " & sClassCode)

			RecordError(ex, aDetail)

		End Try

		If sCompileErrors.Count > 0 And ShowErrorAsResult Then oReturn = Join(sCompileErrors.ToArray, vbCrLf)

		Return oReturn
	End Function


	Public Sub New(Expression As String)
		sExpression = Expression
	End Sub




	Public Sub New()

	End Sub


	Public Shadows Function ToString() As String
		Dim sReturn As String = "", oReturn As Object = Nothing
		Try

			oReturn = Evaluate()
			If oReturn IsNot Nothing Then sReturn = oReturn.ToString

		Catch ex As Exception

			Dim aDetail As New ArrayList
			aDetail.Add("Return is " & oReturn.ToString)
			RecordError(ex, aDetail)

		End Try

		Return sReturn
	End Function




	Protected Friend Sub RecordError(exIn As Exception, ExtraDets As ArrayList)
		Try

			Dim oErr As wRuntimeError = goErrs.NewError(exIn)
			If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.sExtraDetail &= String.Join("<br/>", ExtraDets)
			gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")
			'Dim oError As ErrorItem = glErrors.NewError(exIn, ExtraDets)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		Catch ex As Exception


		End Try

	End Sub
End Class

