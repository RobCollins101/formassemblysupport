﻿Imports System.Net.Mail
Imports System.Text.RegularExpressions

Public Class clsEMail
	Inherits MailMessage
	Event FileNotAccessible(ByVal FilePath As String)
	Event EMailSent(ByVal EMailSender As clsEMail)
	Event EMailFailed(ByVal ex As SystemException, ByVal EMailSender As clsEMail)
	Event UnknownError(ByVal ex As SystemException)

	Private colAttachments As New SortedList
	Public ErrorMessage As String = ""

	Public SMTPServer As String = ""
	Public SMTPPort As Long = 25
	Public SMTPUserID As String = ""
	Public SMTPPassword As String = ""


	Public Sub AddFile(ByVal sFilePath As String)
		'Dim oFile As System.IO.File

		Try

			If Not colAttachments.ContainsKey(sFilePath) Then
				If IO.File.Exists(sFilePath) Then
					colAttachments.Add(sFilePath, sFilePath)
				Else
					RaiseEvent FileNotAccessible(sFilePath)
				End If
			End If

		Catch ex As Exception
			ErrorMessage = ex.ToString
			Dim ExtraDets As New ArrayList
			RecordError(ex, ExtraDets)
		End Try

	End Sub

	Public Function SendEmail() As Boolean
		Dim oAttachment As Net.Mail.Attachment, oSMTP As SmtpClient
		Dim sKey As String, exMail As New Exception, bResult As Boolean = False
		Dim strEMailMatch As String = "^(((?>[a-zA-Z\d!#$%&'*+\-/=?^_`{|}~]+\x20*|""((?=[\x01-\x7f])[^""\\]|\\[\x01-\x7f])*""\x20*)*(?<angle><))?((?!\.)(?>\.?[a-zA-Z\d!#$%&'*+\-/=?^_`{|}~]+)+|""((?=[\x01-\x7f])[^""\\]|\\[\x01-\x7f])*"")@(((?!-)[a-zA-Z\d\-]+(?<!-)\.)+[a-zA-Z]{2,}|\[(((?(?<!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d)){4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\[\x01-\x7f])+)\])(?(angle)>)[;,]?\s*)+$"

		Dim oICredentials As Net.ICredentialsByHost, oNetCreds As Net.NetworkCredential
		Dim ExtraDets As New ArrayList

		Try

			If Me.SMTPServer = "" Then Me.SMTPServer = GetMySettingString("SMTPServer")
			If Me.SMTPPort = 0 Then Me.SMTPPort = CInt(Math.Floor(Val(GetMySettingString("SMTPPort"))))
			If Me.SMTPPort = 0 Then Me.SMTPPort = 25
			If Me.SMTPUserID = "" Then Me.SMTPUserID = GetMySettingString("SMTPUserID")
			If Me.SMTPPassword = "" Then Me.SMTPPassword = GetMySettingString("SMTPPassword")
			If Me.From.Address = "" Then Me.From = New MailAddress(GetMySettingString("DefaultClientEMailFrom"))

			For Each sKey In colAttachments.Keys
				oAttachment = New Net.Mail.Attachment(sKey)
				Me.Attachments.Add(oAttachment)
			Next

			If Me.To.Count = 0 Then
				ErrorMessage = "No Recipient Email Address"
				exMail = New Exception("No Recipient EMail address")
				ExtraDets.Add("")
				RecordError(exMail, ExtraDets)

			ElseIf Me.From.Address = "" Then
				ErrorMessage = "No sender EMail Address"
				exMail = New Exception("No Sender EMail address")
				ExtraDets.Add("")
				RecordError(exMail, ExtraDets)

			ElseIf Me.Subject = "" Then
				ErrorMessage = "No Subject line"
				exMail = New Exception("No Subject Line")
				ExtraDets.Add("")
				RecordError(exMail, ExtraDets)

			ElseIf Not Regex.IsMatch(Me.To.ToString, strEMailMatch) Then
				ErrorMessage = "Incorrect Recipient EMail Address"
				exMail = New Exception("Incorrect Recipient EMail address")
				ExtraDets.Add("Addresses supplied are " & Me.To.ToString)
				RecordError(exMail, ExtraDets)

			ElseIf Not Regex.IsMatch(Me.From.Address, strEMailMatch) Then
				ErrorMessage = "Incorrect sender EMail Address"
				exMail = New Exception("Incorrect Sender EMail address")
				ExtraDets.Add("Addresses supplied are " & Me.From.ToString)
				RecordError(exMail, ExtraDets)

			ElseIf Me.CC.Count > 0 AndAlso Not Regex.IsMatch(Me.CC.ToString, strEMailMatch) Then
				ErrorMessage = "Incorrect CC EMail Address"
				exMail = New Exception("Incorrect CC EMail address")
				ExtraDets.Add("Addresses supplied are " & Me.From.ToString)
				RecordError(exMail, ExtraDets)

			ElseIf Me.Bcc.Count > 0 AndAlso Not Regex.IsMatch(Me.Bcc.ToString, strEMailMatch) Then
				ErrorMessage = "Incorrect BCC EMail Address"
				exMail = New Exception("Incorrect BCC EMail address")
				ExtraDets.Add("Addresses supplied are " & Me.From.ToString)
				RecordError(exMail, ExtraDets)

			ElseIf Me.SMTPServer = "" Then
				ErrorMessage = "No SMTP Server"
				exMail = New Exception("No SMPT Server")
				ExtraDets.Add("")
				RecordError(exMail, ExtraDets)

			ElseIf Me.SMTPPort = 0 Then
				ErrorMessage = "No valid SMTP Port"
				exMail = New Exception("No valid SMPT Port")
				ExtraDets.Add("")
				RecordError(exMail, ExtraDets)

			ElseIf Me.SMTPUserID <> "" And Me.SMTPPassword = "" Then
				ErrorMessage = "SMTP Server UserID supplied with no password"
				exMail = New Exception("SMTP Server UserID supplied with no password")
				ExtraDets.Add("User ID was " & Me.SMTPUserID)
				RecordError(exMail, ExtraDets)

			Else

				oSMTP = New SmtpClient(SMTPServer, CInt(SMTPPort))

				If SMTPUserID = "" Or oSMTP.UseDefaultCredentials Then
					oSMTP.UseDefaultCredentials = True
				Else

					If oSMTP.Credentials Is Nothing Then oSMTP.Credentials = Net.CredentialCache.DefaultNetworkCredentials.GetCredential(SMTPServer, CInt(SMTPPort), "")
					oICredentials = oSMTP.Credentials
					oNetCreds = oICredentials.GetCredential(SMTPServer, CInt(SMTPPort), "")
					oNetCreds.UserName = SMTPUserID
					oNetCreds.Password = SMTPPassword
				End If

				oSMTP.Send(Me)
				bResult = True
				ErrorMessage = "EMail Sent"
				RaiseEvent EMailSent(Me)
			End If

		Catch ex As Exception
			ErrorMessage = ex.ToString
			RecordError(ex, ExtraDets)
		End Try

		Return bResult
	End Function



	Protected Friend Sub RecordError(exIn As Exception, ExtraDets As ArrayList)
		Try


			Dim oErr As wRuntimeError = goErrs.NewError(exIn)
			If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.sExtraDetail &= String.Join("<br/>", ExtraDets)
			gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")
			'Dim oError As ErrorItem = glErrors.NewError(exIn, ExtraDets)
			'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

		Catch ex As Exception


		End Try

	End Sub

	Overrides Function ToString() As String
		Dim sReturn As String = "", sTemp As String = ""
		Try
			sReturn = "To         : "
			For Each oMailAddr In Me.To
				sTemp &= IIf(sTemp <> "", ";", "").ToString & oMailAddr.Address
			Next
			sReturn &= sTemp
			If Me.CC.Count > 0 Then
				sReturn &= vbCrLf & "CC         : "
				sTemp = ""
				For Each oMailAddr In Me.CC
					sTemp &= IIf(sTemp <> "", ";", "").ToString & oMailAddr.Address
				Next
				sReturn &= sTemp
			End If
			If Me.Bcc.Count > 0 Then
				sReturn &= vbCrLf & "BCC        : "
				sTemp = ""
				For Each oMailAddr In Me.Bcc
					sTemp &= IIf(sTemp <> "", ";", "").ToString & oMailAddr.Address
				Next
				sReturn &= sTemp
			End If


			sReturn &= vbCrLf & "From       : " & Me.From.Address
			sReturn &= vbCrLf & "Subject    : " & Me.Subject
			If Me.Attachments.Count > 0 Then
				sReturn &= vbCrLf & "Attachments :"
				sTemp = ""
				For Each oMailAtt In Me.Attachments
					sTemp &= IIf(sTemp <> "", vbCrLf & "          ", "").ToString & oMailAtt.Name
				Next
				sReturn &= sTemp

			End If
			sReturn &= vbCrLf & vbCrLf & "Body of Email" & vbCrLf & vbCrLf & Me.Body

		Catch ex As Exception

		End Try
		Return sReturn
	End Function

End Class

