Option Compare Text

Imports System.Text.RegularExpressions
Imports System.Data.SqlTypes
Imports System.Collections.Generic

' ==================================================================================
' Version 1.1
' Author : R Collins
' Date of initial creation : 1 Mar 2011
' ----------------------------------------------------------------------------------
' Version 1.1  : Modified by R Collins on 29 Jan 2016
' 		Modified for enhanced company detection by key words
' ==================================================================================

Public Class clsNameParts
	Dim aTitles As New ArrayList, aOrgInd As New ArrayList, aCutOuts As New ArrayList
	Dim sTitle As String = "", sForenames As String = "", sInitials As String = "", sSurname As String = ""
	Dim sFullName As String = "", sOrganisationName As String = ""
	Dim aSurnamePrefixes As New ArrayList, sRegexMatchOrganisation As String


	Public ReadOnly Property Title() As String
		Get
			Return sTitle
		End Get
	End Property

	Public ReadOnly Property Forenames() As String
		Get
			Return sForenames
		End Get
	End Property

	Public ReadOnly Property Forename() As String
		Get
			Dim aSplit As ArrayList, sReturn As String = ""
			Try
				If Trim(sForenames) > "" Then
					aSplit = New ArrayList(Split(sForenames, ","))
					sReturn = aSplit(0).ToString
				End If

			Catch ex As Exception

			End Try
			Return sReturn
		End Get
	End Property

	Public ReadOnly Property MiddleNames() As String
		Get
			Dim aSplit As ArrayList, sReturn As String = ""
			Try
				If Trim(sForenames) > "" Then
					aSplit = New ArrayList(Split(sForenames, ","))
					If aSplit.Count > 1 Then
						aSplit.RemoveAt(0)
						sReturn = Join(CType(aSplit.ToArray(GetType(System.String)), String()), " ")

					End If
				End If

			Catch ex As Exception

			End Try
			Return sReturn
		End Get
	End Property

	Public ReadOnly Property Initials() As String
		Get
			Return sInitials
		End Get
	End Property

	Public ReadOnly Property Surname() As String
		Get
			Return sSurname
		End Get
	End Property

	Public ReadOnly Property OrganisationName() As String
		Get
			Return sOrganisationName
		End Get
	End Property



	Public Property FullName() As String
		Get
			Return sFullName
		End Get
		Set(ByVal value As String)

			Dim aParts() As String, iPartPos As Integer = 0, sTemp As String = "", sTemp2 As String = "", sTempFullName As String = ""
			Dim arrParts As ArrayList

			Try
				If Trim(value) > "" Then

					PrepValues()

					If Regex.IsMatch(value, sRegexMatchOrganisation) Then

						sOrganisationName = StrConv(SuperTrim(value), VbStrConv.ProperCase)
						sFullName = ""
						sSurname = ""
						sForenames = ""
						sTitle = ""

					Else

						sOrganisationName = ""
						sFullName = value

						sTempFullName = Replace(sFullName, "&", "and")

						For Each sTemp In aCutOuts
							If sTempFullName.Contains(sTemp) Then sTempFullName = Trim(" " & sTempFullName & " ").Replace(" " & sTemp & " ", " ")
						Next

						sTempFullName = SuperTrim(sTempFullName)
						aParts = Split(sTempFullName, ",")
						If aParts.Length > 1 Then
							' the surname must be the first part before the comma
							arrParts = New ArrayList(aParts)

							sTempFullName = arrParts(0).ToString ' The surname
							arrParts.RemoveAt(0)
							sTempFullName = SuperTrim(Join(CType(arrParts.ToArray(GetType(System.String)), String()), " ") & " " & sTempFullName)

						End If

						aParts = Split(SuperTrim(sTempFullName), " ")

						sTitle = ""
						sInitials = ""
						sForenames = ""
						sSurname = ""

						If aParts.Length < 2 And Not (aTitles.Contains(sTempFullName)) Then
							sOrganisationName = sTempFullName
							sTempFullName = ""
							sFullName = ""
						Else
							Do While iPartPos < UBound(aParts) And aTitles.Contains(UCase(aParts(iPartPos)))
								sTitle &= IIf(sTitle = "", "", " ").ToString & StrConv(aParts(iPartPos), VbStrConv.ProperCase)
								iPartPos += 1
							Loop

							sTemp = aParts(UBound(aParts))
							If sTitle = "" And aTitles.Contains(UCase(sTemp)) Then
								sTitle = StrConv(sTemp, vbProperCase)
								ReDim Preserve aParts(UBound(aParts) - 1)
								iPartPos = 0
							End If

							Do While iPartPos < UBound(aParts)
								sForenames &= IIf(sForenames = "", "", " ").ToString & StrConv(aParts(iPartPos), VbStrConv.ProperCase)
								sInitials &= IIf(sInitials = "", "", " ").ToString & UCase(Left(aParts(iPartPos), 1))
								iPartPos += 1
							Loop

							sSurname = StrConv(aParts(UBound(aParts)), VbStrConv.ProperCase)
							For Each sTemp In aSurnamePrefixes
								If Left(sSurname, Len(sTemp)) = sTemp Then
									sTemp2 = Left(sSurname, Len(sTemp))
									If Len(sTemp) + 1 <= Len(sSurname) Then sTemp2 &= UCase(Mid(sSurname, Len(sTemp) + 1, 1))
									If Len(sTemp) + 2 <= Len(sSurname) Then sTemp2 &= Mid(sSurname, Len(sTemp) + 2)
									sSurname = sTemp2
									Exit For
								End If
							Next

							If sTitle = "" Or Not (aTitles.Contains(sTitle)) Then

								sTemp = Replace(Replace(Replace(Replace(" " & Trim(sForenames & " " & sSurname) & " ", ",", ""), ".", ""), "'", ""), "`", "")
								For Each sCheck In aOrgInd
									If sTemp.Contains(" " & CStr(sCheck) & " ") Then
										sOrganisationName = Trim(sForenames & " " & sSurname)
										sForenames = ""
										sSurname = ""
										sTitle = ""
										Exit For
									End If
								Next

							End If

							sFullName = SuperTrim(sTitle & " " & sForenames & " " & sSurname)

						End If
					End If
				End If

			Catch ex As Exception

			End Try

		End Set
	End Property



	Public Sub PrepValues()
		aOrgInd = New ArrayList() From {{"Ltd"} _
, {"Lt"} _
, {"Limited"} _
, {"Plc"} _
, {"Council"} _
, {"Cncl"} _
, {"Exec"} _
, {"Executive"} _
, {"Care"} _
, {"Leisure"} _
, {"T/A"} _
, {"The"} _
, {"Co"} _
, {"Company"} _
, {"Corporation"} _
, {"Corp"} _
, {"Corps"} _
, {"Group"} _
, {"Org"} _
, {"Organisation"} _
, {"Agent"} _
, {"Agents"} _
, {"Son"} _
, {"Sons"} _
, {"Partner"} _
, {"Partners"} _
, {"Brothers"} _
, {"Treasurer"} _
, {"Insurance"} _
, {"School"} _
, {"College"} _
, {"University"} _
, {"Church"} _
, {"Unit"} _
, {"Club"} _
, {"Assoc"} _
, {"Association"} _
, {"Associates"} _
, {"Associate"} _
, {"Scout"} _
, {"Scouts"} _
, {"Guide"} _
, {"Guides"} _
, {"Garage"} _
, {"garages"} _
, {"Social"} _
, {"Soc"} _
, {"Society"} _
, {"Inst"} _
, {"Institiute"} _
, {"Trust"} _
, {"Of"} _
, {"Theatre"} _
, {"Parish"} _
, {"Trustee"} _
, {"Trustees"} _
, {"Centre"} _
, {"Center"} _
, {"By"} _
, {"Property"} _
, {"Properties"} _
, {"Holding"} _
, {"Committee"} _
, {"Committees"} _
, {"Institiute"} _
, {"Inst"} _
, {"Village"} _
, {"Museum"} _
, {"School"} _
, {"Band"} _
, {"Zoo"} _
, {"Garden"} _
, {"Gardens"} _
, {"Foundation"} _
, {"Service"} _
, {"Services"} _
, {"Governor"} _
, {"Gov"} _
, {"NHS"} _
, {"Eng"} _
, {"Management"} _
, {"Legion"} _
, {"T/A"} _
, {"Partner"} _
, {"Partneteship"} _
, {"Partners"} _
, {"Commissioner"} _
, {"Legion"} _
, {"Shop"} _
, {"Enterprise"} _
, {"Enterprises"} _
, {"Financial"} _
, {"Department"} _
, {"Section"} _
, {"Region"} _
, {"Underwriting"} _
, {"Underwriters"} _
, {"Hospice"} _
, {"Hostel"} _
, {"Hotel"} _
, {"Motel"} _
, {"Holding"} _
, {"Holdings"} _
, {"Finance"} _
, {"Financial"} _
, {"Chapel"} _
, {"Church"} _
, {"Barber"} _
, {"Barbers"} _
, {"Store"} _
, {"Storea"} _
, {"Warehouse"} _
, {"Accountant"} _
, {"Accountants"} _
, {"Practice"} _
, {"Association"} _
, {"Clinic"} _
, {"Practice"} _
, {"Work"} _
, {"Works"} _
, {"Community"} _
, {"Gallery"} _
, {"Labs"} _
, {"Vineyard"} _
, {"Kitchen"} _
, {"Camp"} _
, {"Camping"} _
, {"Auto"} _
, {"Autos"} _
, {"Motor"} _
, {"Motors"} _
, {"Car"} _
, {"Cars"} _
, {"Farm"} _
, {"Cafe"} _
, {"Restaurant"} _
, {"Bar"} _
, {"Public"} _
, {"Estate"} _
, {"Estates"}}


		aCutOuts = New ArrayList() From {{"FAO"}, {"C/O"}, {"T/A"}}


		aTitles = New ArrayList() From {{"MS"} _
		, {"MISS"} _
		, {"MRS"} _
		, {"MIS"} _
		, {"MIZ"} _
		, {"MZ"} _
		, {"MS"} _
		, {"MR"} _
		, {"MASTER"} _
		, {"REV"} _
		, {"REVEREND"} _
		, {"FR"} _
		, {"FATHER"} _
		, {"DR"} _
		, {"DOCTOR"} _
		, {"ATTY"} _
		, {"ATTORNEY"} _
		, {"PROF"} _
		, {"PROFESSOR"} _
		, {"HON"} _
		, {"HONORABLE"} _
		, {"OFC"} _
		, {"OFFICER"} _
		, {"SIR"} _
		, {"LORD"} _
		, {"LADY"} _
		, {"MSGR"} _
		, {"MONSIGNOR"} _
		, {"SR"} _
		, {"SISTER"} _
		, {"BR"} _
		, {"BROTHER"} _
		, {"SUPT"} _
		, {"SUPERINTENDENT"} _
		, {"REP"} _
		, {"REPRESENTATIVE"} _
		, {"SEN"} _
		, {"SENATOR"} _
		, {"AMB"} _
		, {"AMBASSADOR"} _
		, {"TREAS"} _
		, {"TREASURER"} _
		, {"SEC"} _
		, {"SECRETARY"} _
		, {"PVT"} _
		, {"PRIVATE"} _
		, {"CPL"} _
		, {"CORPORAL"} _
		, {"SGT"} _
		, {"SARGENT"} _
		, {"ADM"} _
		, {"ADMINISTRATIVE"} _
		, {"MAJ"} _
		, {"MAJOR"} _
		, {"CAPT"} _
		, {"CAPTAIN"} _
		, {"CMDR"} _
		, {"COMMANDER"} _
		, {"LT"} _
		, {"LIEUTENANT"} _
		, {"COL"} _
		, {"COLONEL"} _
		, {"GEN"} _
		, {"GENERAL"} _
		, {"BARON"} _
		, {"BARONESS"} _
		, {"MARQUAY"} _
		, {"VISCOUNT"} _
		, {"VISCOUNTESS"} _
		, {"VERY"} _
		, {"VICE"} _
		, {"OF"} _
		, {"MY"} _
		, {"RIGHT"} _
		, {"RT"} _
		, {"ADMIRAL"}}

		sRegexMatchOrganisation = "((\s|^)Hotels?(\s|$|\.))|(\sLtd(\s|$|\.))|(\sLimited(\s|$|\.))|(\sServices?(\s|$|\.))|(\sPLC(\s|$|\.))|(\sGroups?(\s|$|\.))|((\s|^)Solicitors?(\s|$|\.))|((\s|^)Co(mpany|\s|\.))|((\s|^)Organisations?(\s|$|\.))|((\s|^)Councils?(\s|$|\.))|((\s|^)County(\s|$|\.))|((\s|^)Office(s)?(\s|$|\.))|((\s|^)Clubs?(\s|$|\.))|((\s|^)Business((.|')?s)?(\s|$|\.))|((\s|^)School((.|')?s)?(\s|$|\.))|((\s|^)Universit(y|ies)(\s|$|\.))|((\s|^)Units?(\s|$|\.))|((\s|^)Factory(\s|$|\.))|((\s|^)Workshops?(\s|$|\.))|((\s|^)Garages?(\s|$|\.))|((\s|^)CO(\s|$|\.))|((\s|^)The\s)|(\sBrother(s)?(\s|$|\.))|((\s|^)Others?(\s|$|\.))|((\s|^)Public(\s|$|\.))|((\s|^)Assoc(iation|\.)(\s|$|\.))|((\s|^)District(\s|$|\.))|((\s|^)Charit(y|ies)(\s|$|\.))|((\s|^)Shops?(\s|$|\.))|((\s|^)Trust(\s|$|\.))|((\s|^)College((.|')?s)?(\s|$|\.))|((\s|^)Medical(\s|$|\.))|((\s|^)Centre(\s|$|\.))|((\s|^)Societ(y|ies)?(\s|$|\.))|((\s|^)Farms?(\s|$|\.))|((\s|^)Agenc(y|ies)(\s|$|\.))|((\s|^)Studios?(\s|$|\.))|((\s|^)Tele(co|vision|phone))|((\s|^)UK($|\.|\s))|((\s|^)Restaurants?($|\.|\s))|((\s|^)Train(ing)?($|\.|\s))|((\s|^)Memorial($|\.|\s))|((\s|^)Auto)|((\s|^)Energ(y|ies)?($|\.|\s))|((\s|^)Partners(hip)?s?($|\.|\s))|((\s|^)Laborator(y|ies)?($|\.|\s))|((\s|^)Hires?($|\.|\s))|((\s|^)Civil($|\.|\s))|((\s|^)Motor(way)?s?($|\.|\s))|((\s|^)Centres?($|\.|\s))|((\s|^)Research($|\.|\s))|((\s|^)Practice($|\.|\s))"

		aSurnamePrefixes = New ArrayList() From {{"Mc"}, {"Mac"}, {"O"}}

	End Sub

	Public Sub New(ByVal sFullName As String)
		Try

			Me.FullName = sFullName

		Catch ex As Exception

		End Try
	End Sub


	Public Sub New()

	End Sub



	Private Function SuperTrim(ByVal Source As String) As String
		Dim sReturn As String = Trim(Source), Regex As New Text.RegularExpressions.Regex("\s{2,}")
		Try

			Do While Text.RegularExpressions.Regex.IsMatch(sReturn, "(?<BeforeMarks>(.+\s*|^))(?<RemoveMarks>[.<>;:?#~{}_=+*^%$?""!?|\(\)]+)(?<AfterMarks>(\s*.+|$))")
				sReturn = Trim(Text.RegularExpressions.Regex.Replace(sReturn, "(?<BeforeMarks>(.+\s*|^))(?<RemoveMarks>[.<>;:?#~{}_=+*^%$?""!?|\(\)]+)(?<AfterMarks>(\s*.+|$))", "${BeforeMarks} ${AfterMarks}"))
			Loop

			Do While Regex.IsMatch(sReturn)
				sReturn = Text.RegularExpressions.Regex.Replace(sReturn, "\s{2,}", " ")
			Loop

			Do While Text.RegularExpressions.Regex.IsMatch(sReturn, "\s+\-")
				sReturn = Text.RegularExpressions.Regex.Replace(sReturn, "\s+\-", "-")
			Loop

			Do While Text.RegularExpressions.Regex.IsMatch(sReturn, "\w+((['`?][S])|['?]s)(\s|[.,]|$)(\s+|\w+|$)+")
				sReturn = Text.RegularExpressions.Regex.Replace(sReturn, "['`?][Ss]", "`s")
			Loop

			Do While Text.RegularExpressions.Regex.IsMatch(sReturn, "\w+((['`?][S])|['?]s)(\s|[.,]|$)(\s+|\w+|$)+")
				sReturn = Text.RegularExpressions.Regex.Replace(sReturn, "['`?][Ss]", "`s")
			Loop

		Catch ex As Exception

		End Try

		Return sReturn
	End Function

End Class
