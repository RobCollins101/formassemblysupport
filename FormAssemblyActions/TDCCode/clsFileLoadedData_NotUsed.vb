﻿Option Compare Text

Public Class colLoadedFileData
	Inherits SortedList(Of String, clsLoadedFileData)

	Protected Friend sDestinationFileType As String
	Protected Friend sDestinationOlderPath As String
	Protected Friend sDestinationFileNameTemplate As String
	'Protected Friend aFieldValues As New SortedList(Of String, String)
	Protected Friend aFieldValues As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)


	Public Sub ProcessDataSaves()

	End Sub

End Class


Public Class clsLoadedFileData
	Private sFileName As String = ""
	Public Property FileName As String
		Get
			If sFileName = "" And FilePath <> "" Then sFileName = IO.Path.GetFileName(FilePath)
			Return sFileName
		End Get
		Set(value As String)
			sFileName = value
			If IO.Path.GetDirectoryName(sFileName) <> "" Then
				FilePath = IO.Path.GetDirectoryName(sFileName)
				sFileName = IO.Path.GetFileName(sFileName)
			End If
		End Set
	End Property

	Public FilePath As String = ""
	Public FileData As Byte()
	Public SavedFilePath As String
	Public LogRecordLineTemplate As String


	Public Sub New()

	End Sub

	Public Sub New(LoadedFilePath As String, aFileData As Byte())

		FileName = LoadedFilePath
		FileData = aFileData

	End Sub

End Class
