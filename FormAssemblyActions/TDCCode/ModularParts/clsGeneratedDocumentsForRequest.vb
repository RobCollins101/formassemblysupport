﻿Public Class colGeneratedDocumentsForRequest
	Inherits List(Of clsGeneratedDocumentForRequest)

	Public oParentCore As clsFormSupportCore

	Friend bAddNewDocsToLog As Boolean = True

	Public Sub New(oParent As clsFormSupportCore)
		Dim sTemp As String
		Try
			oParentCore = oParent
			sTemp = oParentCore.GetValueIfExists("CreatedDocAddToLogValLabels")
			If sTemp <> "" Then bAddNewDocsToLog = StringIsTrue(sTemp)

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			gsErr = goErrs.ToString

		End Try
	End Sub

	Public Overloads Function Add(oGeneratedDoc As clsGeneratedDocumentForRequest) As clsGeneratedDocumentForRequest
		Try
			oGeneratedDoc.oParentGeneratedDocs = Me
			MyBase.Add(oGeneratedDoc)

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			gsErr = goErrs.ToString

		End Try

		Return oGeneratedDoc
	End Function


	Public Function NewDoc(sTemplate As String) As clsGeneratedDocumentForRequest
		Dim oGeneratedDoc As clsGeneratedDocumentForRequest = Nothing
		Try
			oGeneratedDoc = New clsGeneratedDocumentForRequest
			oGeneratedDoc.oParentGeneratedDocs = Me
			oGeneratedDoc.DocTemplateHTML = sTemplate

			Add(oGeneratedDoc)

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			gsErr = goErrs.ToString

		End Try
		Return oGeneratedDoc
	End Function


	Friend Function AllDocsList() As ArrayList
		Dim aReturn As New ArrayList
		Try

		Catch ex As Exception

		End Try
		Return aReturn
	End Function

	Friend Function AttachableDocsList() As ArrayList
		Dim aReturn As New ArrayList
		Try

		Catch ex As Exception

		End Try
		Return aReturn
	End Function

	Friend Function OtherDocsList() As ArrayList
		Dim aReturn As New ArrayList
		Try

		Catch ex As Exception

		End Try
		Return aReturn
	End Function

	Friend Sub GenerateDocuments()
		'Dim oDocGenerated As clsGeneratedDocumentForRequest
		'Dim sHTMLPassed As String = "", sFileStorePathUsed As String, sTargetExtn As String
		'Dim oItems As Array, bCanConvert As Boolean = False
		'Dim oFileInfo As IO.FileInfo, owsConvert As wsConvertToImage.wsConvertToImage
		'Dim owsConvertibles As wsConvertToImage.FileTypesListed
		'Dim oConvParams As wsConvertToImage.RequestHTMLConvWithFields
		'Dim owrStream As IO.StreamWriter ', oWrStream As IO.StreamWriter
		'Dim oCreatedDoc As wsConvertToImage.ConvertedDoc, owsResponse As wsConvertToImage.clsWebResponseConvertedDocs

		Dim oDoc As clsGeneratedDocumentForRequest, sPath As String = ""

		Dim sDocList As String, aDocs As ArrayList, sPathList As String
		Dim sTemp As String, sFolder As String
		Dim aDocGeneratedList As New ArrayList, sDocNameList As String = ""

		Try

			sDocList = oParentCore.GetValueIfExists("CreatedDocTemplatePathValLabels", True, clsValueSubstitutions.TargetModifier.NoModify, "DefaultGeneratedHTMLDocument")

			If sDocList.IndexOf("<head") >= 0 Then
				Me.NewDoc(sDocList)
			Else
				aDocs = oParentCore.SplitToArray(sDocList)
				For Each sPath In aDocs
					Me.NewDoc(sPath)
				Next
			End If

			For Each oDoc In Me
				sPath = oDoc.GenerateDocument
				aDocGeneratedList.AddRange(oParentCore.SplitToArray(sPath))
			Next

			If aDocGeneratedList.Count > 0 Then
				sTemp = aDocGeneratedList(0).ToString
				sFolder = IO.Path.GetDirectoryName(sTemp)
				If sFolder <> "" Then oParentCore.ApplyValueIfExists("GeneratedDocFolderValLabels", sFolder, True)

				sPathList = Strings.Join(aDocGeneratedList.ToArray, oParentCore.sPathSeparatorToToUse)
				oParentCore.ApplyValueIfExists("GeneratedDocPathValLabels", sPathList, True)
				For Each sTemp In aDocGeneratedList
					sDocNameList &= IIf(sDocNameList <> "", oParentCore.sPathSeparatorToToUse, "").ToString & IO.Path.GetFileName(sTemp)
				Next
				oParentCore.ApplyValueIfExists("GeneratedDocFileNamesValLabels", sDocNameList, True)

			End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			gsErr = goErrs.ToString

		End Try

	End Sub

End Class


Public Class clsGeneratedDocumentForRequest
	Friend oParentGeneratedDocs As colGeneratedDocumentsForRequest

	Friend sTargetDocPath As String
	Private sDocTemplateHTML As String = ""
	Private sDocTemplatePath As String = ""
	Friend sStoredPath As String = ""

	Friend Property DocTemplatePath As String
		Get
			Return sDocTemplatePath
		End Get
		Set(value As String)
			Try
				If value <> sDocTemplatePath Then
					sDocTemplatePath = value
					If Regex.IsMatch(sDocTemplatePath, GetMySettingString("RegexFilePath")) Then
						sDocTemplateHTML = LoadIfFile(sDocTemplatePath, oParentGeneratedDocs.oParentCore.oFieldDataUsed, oParentGeneratedDocs.oParentCore.oFieldDataUsed)
					End If
				End If

			Catch ex As Exception
				Dim oError As wRuntimeError = goErrs.NewError(ex)
				oError.AddDetail("Path : " & sDocTemplatePath)
				gsErr = goErrs.ToString

			End Try

		End Set
	End Property

	Friend Property DocTemplateHTML As String
		Get
			Try
				If Regex.IsMatch(sDocTemplateHTML, GetMySettingString("RegexFilePath")) Then
					sDocTemplateHTML = LoadIfFile(sDocTemplateHTML, oParentGeneratedDocs.oParentCore.oFieldDataUsed, oParentGeneratedDocs.oParentCore.oFieldDataUsed)

				End If

			Catch ex As Exception
				Dim oError As wRuntimeError = goErrs.NewError(ex)
				If sDocTemplateHTML <> "" Then oError.AddDetail("Raw HTML : " & sDocTemplateHTML)
				gsErr = goErrs.ToString

			End Try

			Return sDocTemplateHTML
		End Get
		Set(value As String)
			Try
				If value <> sDocTemplateHTML Then
					sDocTemplateHTML = value
					If Regex.IsMatch(value, GetMySettingString("RegexFilePath")) Then
						DocTemplatePath = value
						sDocTemplateHTML = LoadIfFile(sDocTemplateHTML, oParentGeneratedDocs.oParentCore.oFieldDataUsed, oParentGeneratedDocs.oParentCore.oFieldDataUsed)
					End If

				End If

			Catch ex As Exception
				Dim oError As wRuntimeError = goErrs.NewError(ex)
				If sDocTemplateHTML <> "" Then oError.AddDetail("Raw HTML : " & sDocTemplateHTML)
				gsErr = goErrs.ToString

			End Try
		End Set
	End Property


	Friend Function GenerateDocument() As String
		Dim sHTMLPassed As String = "", sTargetDocPathUsed As String, sTargetExtn As String
		Dim oFileInfo As IO.FileInfo, owsConvert As wsConvToImg.wsConvertToImage
		Dim owsConvertibles As wsConvToImg.FileTypesListed
		Dim oConvParamsWithFields As wsConvToImg.RequestHTMLConvWithFields
		Dim oConvParams As wsConvToImg.RequestHTMLConv
		'Dim sLogFilePathUsed As String, 
		Dim sLogFileEntryUsed As String, sReturn As String = ""
		'Dim aConvExtns As New ArrayList From {"pdf", "bmp", "jpg", "png", "gif", "tif"}
		Dim oItems As Array, bCanConvert As Boolean = False
		Dim owrStream As IO.StreamWriter ', oWrStream As IO.StreamWriter
		Dim oCreatedDoc As wsConvToImg.ConvertedDoc, owsResponse As wsConvToImg.clsWebResponseConvertedDocs
		Dim oFieldVals As New List(Of wsConvToImg.FieldValue), oFieldVal As wsConvToImg.FieldValue
		Dim sKey As String, sValue As String, iFileNo As Integer, oLogFileInfo As IO.FileInfo

		Dim oAttachedFileData As clsAttachedFileData
		Dim sDocName As String = "", sDocFolder As String = ""
		Dim sDefaultDestPath As String = "", sDefaultDestFolder As String = "", sDefaultDestName As String = "", sDefaultDestNameNoExt As String = ""
		Dim bUseFieldsInConv As Boolean = False

		Try

			'oParentGeneratedDocs.oParentCore.AddValuesToFieldData()
			'oSubst.AddNewValues(oFieldDataUsed)
			sHTMLPassed = DocTemplateHTML

			If sTargetDocPath = "" Then
				sTargetDocPath = oParentGeneratedDocs.oParentCore.GetValueIfExists("CreatedDocPathValLabels", True, clsValueSubstitutions.TargetModifier.ForFilePath) ' , "DefaultGeneratedDocumentName"
			End If

			If sTargetDocPath = "" Then
				sDefaultDestPath = GetMySettingString("DefaultGeneratedDocumentName")
				sDefaultDestFolder = GetMySettingString("DefaultStoreFolderDest")
				If sDefaultDestFolder = "" Then sDefaultDestFolder = IO.Path.GetDirectoryName(sDefaultDestPath)
				sDefaultDestName = IO.Path.GetFileName(sDefaultDestPath)
				sDefaultDestNameNoExt = IO.Path.GetFileNameWithoutExtension(sDefaultDestPath)

				' Use given folder or the default folder path

				sDocFolder = oParentGeneratedDocs.oParentCore.GetValueIfExists("CreatedDocFolderValLabels", True, clsValueSubstitutions.TargetModifier.ForFilePath)
				If sDocFolder = "" Then sDocFolder = oParentGeneratedDocs.oParentCore.ApplySubstitution(sDefaultDestFolder, clsValueSubstitutions.TargetModifier.ForFilePath)

				' Use what is default for file name

				'sDocName = oParentGeneratedDocs.oParentCore.ApplySubstitution(sDefaultDestName, clsValueSubstitutions.TargetModifier.ForFilePath)

				If sDefaultDestNameNoExt <> "" And sDocName = "" Then sDocName = oParentGeneratedDocs.oParentCore.ApplySubstitution(sDefaultDestNameNoExt, clsValueSubstitutions.TargetModifier.ForFilePath) & "." & GetMySettingString("DefaultStoreFileType")

				If sDocName = "" Then sDocName = IO.Path.GetFileNameWithoutExtension(sDocTemplatePath) & Now.ToString("yyyyMMdd_HHmmssffff") & "." & GetMySettingString("DefaultStoreFileType")

				If sDocFolder = "" Then
					sTargetDocPath = oParentGeneratedDocs.oParentCore.ApplySubstitution(GetMySettingString("DefaultGeneratedDocumentName"), clsValueSubstitutions.TargetModifier.ForFilePath)
					sDocFolder = IO.Path.GetDirectoryName(sTargetDocPath)
					sDocName = IO.Path.GetFileName(sTargetDocPath)
				Else
					sTargetDocPath = sDocFolder & IIf(sDocFolder.Substring(sDocFolder.Length - 1, 1) <> IO.Path.DirectorySeparatorChar, IO.Path.DirectorySeparatorChar, "").ToString & sDocName
					'If sTargetDocPath <> "" Then sTargetDocPath &= IIf(sTargetDocPath.Substring(-1, 1) <> IO.Path.DirectorySeparatorChar, IO.Path.DirectorySeparatorChar, "").ToString & IO.Path.GetFileNameWithoutExtension(sDocTemplatePath) & Now.ToString("yyyyMMdd_HHmmssffff") & ".htm"

				End If
			Else
				sDocName = IO.Path.GetFileName(sTargetDocPath)
				sDocFolder = IO.Path.GetDirectoryName(sTargetDocPath)
			End If

			If sTargetDocPath <> "" Then
				sTargetDocPathUsed = oParentGeneratedDocs.oParentCore.ApplySubstitution(sTargetDocPath, clsValueSubstitutions.TargetModifier.ForFilePath)

				sDocName = IO.Path.GetFileName(sTargetDocPathUsed)
				sDocFolder = IO.Path.GetDirectoryName(sTargetDocPathUsed)

				oFileInfo = New IO.FileInfo(sTargetDocPathUsed)
				If Not (oFileInfo.Directory.Exists) Then oFileInfo.Directory.Create()

				If oFileInfo.Directory.Exists Then

					sTargetExtn = oFileInfo.Extension.Replace(".", "")
					owsConvert = New wsConvToImg.wsConvertToImage
					owsConvertibles = owsConvert.ConvertibleExtensions("HTM")
					oItems = owsConvertibles.Items.ToArray
					If oItems.Length > 0 Then

						For Each oItem In oItems
							If oItem.ToString = sTargetExtn Then
								bCanConvert = True
								Exit For
							End If
						Next
					End If
					If bCanConvert Then
						If bUseFieldsInConv Then
							oConvParamsWithFields = New wsConvToImg.RequestHTMLConvWithFields
							oConvParamsWithFields.DestinationFileNameTemplate = sTargetDocPathUsed
							oConvParamsWithFields.DestinationFileType = oFileInfo.Extension
							oConvParamsWithFields.CanOverwriteDestination = "True"
							oConvParamsWithFields.HTMLToConvert = sHTMLPassed
							oConvParamsWithFields.FileType = "HTM"

							oFieldVal = New wsConvToImg.FieldValue
							For Each sKey In oParentGeneratedDocs.oParentCore.oFieldDataUsed.Keys
								sValue = oParentGeneratedDocs.oParentCore.oFieldDataUsed.Item(sKey).ToString
								oFieldVal.FieldName = sKey
								oFieldVal.FieldData = sValue
								oFieldVals.Add(oFieldVal)
							Next

							oConvParamsWithFields.FieldData = oFieldVals.ToArray

							owsResponse = owsConvert.HTMLImageConversionWithFields(oConvParamsWithFields)
							iFileNo = 0

						Else
							oConvParams = New wsConvToImg.RequestHTMLConv
							oConvParams.DestinationFileNameTemplate = sTargetDocPathUsed
							oConvParams.DestinationFileType = oFileInfo.Extension
							oConvParams.CanOverwriteDestination = "True"
							oConvParams.HTMLToConvert = sHTMLPassed
							oConvParams.FileType = "HTM"

							owsResponse = owsConvert.HTMLImageConversion(oConvParams)
							iFileNo = 0

						End If

						For Each oCreatedDoc In owsResponse.ConvertedDocuments
								oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocPathValLabels", oCreatedDoc.DestinationPath, True)
								oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocFolderValLabels", IO.Path.GetDirectoryName(oCreatedDoc.DestinationPath), True)


								'oSubst.AddNewValue("DestinationPath", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DestinationDocumentPath", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DocumentPath", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DocPath", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DestDocumentPath", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DestDocPath", oCreatedDoc.DestinationPath)

								'oSubst.AddNewValue("DestinationFolder", IO.Path.GetDirectoryName(oCreatedDoc.DestinationPath))
								'oSubst.AddNewValue("DestinationFilePath", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DestinationFileName", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DestinationFile", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("FilePath", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DestFilePath", oCreatedDoc.DestinationPath)
								'oSubst.AddNewValue("DestFolder", IO.Path.GetDirectoryName(oCreatedDoc.DestinationPath))

								iFileNo += 1
								'oSubst.AddNewValue("DocumentNo", iFileNo.ToString)
								'oSubst.AddNewValue("PageNo", iFileNo.ToString)
								'oSubst.AddNewValue("FileNo", iFileNo.ToString)
								'oSubst.AddNewValue("DocumentNumber", iFileNo.ToString)
								'oSubst.AddNewValue("PageNumber", iFileNo.ToString)
								'oSubst.AddNewValue("FileNumber", iFileNo.ToString)

								oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocFileNoValLabels", iFileNo.ToString, True)
								oAttachedFileData = New clsAttachedFileData(oCreatedDoc.DestinationPath)
								oParentGeneratedDocs.oParentCore.AssociatedFiles.Add(oAttachedFileData)
								If oParentGeneratedDocs.bAddNewDocsToLog Then oAttachedFileData.WriteToLogFile()

								'If sLogFilePath <> "" And sLogFileEntry <> "" Then
								'    sLogFilePathUsed = oSubst.GetSubstitutionForString(sLogFilePath, clsValueSubstitutions.TargetModifier.ForFilePath)
								'    sLogFileEntryUsed = oSubst.GetSubstitutionForString(sLogFileEntry)

								'    oLogFileInfo = New IO.FileInfo(sLogFilePathUsed)
								'    owrStream = oLogFileInfo.AppendText()
								'    owrStream.AutoFlush = True
								'    owrStream.WriteLine(sLogFileEntryUsed)
								'    owrStream.Flush() : owrStream.Close() : owrStream = Nothing

								'End If

								sReturn &= IIf(sReturn <> "", oParentGeneratedDocs.oParentCore.sPathSeparatorToToUse, "").ToString & oCreatedDoc.DestinationPath
								sStoredPath = sReturn

							Next


						Else

							oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocPathValLabels", oFileInfo.FullName, True)
						oParentGeneratedDocs.oParentCore.ApplyValueIfExists("CreatedDocFolderValLabels", oFileInfo.DirectoryName, True)


						'oSubst.AddNewValue("DestinationPath", oFileInfo.FullName)
						'oSubst.AddNewValue("DestinationDocumentPath", oFileInfo.FullName)
						'oSubst.AddNewValue("DocumentPath", oFileInfo.FullName)
						'oSubst.AddNewValue("DocPath", oFileInfo.FullName)
						'oSubst.AddNewValue("DestDocumentPath", oFileInfo.FullName)
						'oSubst.AddNewValue("DestDocPath", oFileInfo.FullName)
						'oSubst.AddNewValue("DestinationFolder", oFileInfo.DirectoryName)
						'oSubst.AddNewValue("DestinationFilePath", oFileInfo.FullName)
						'oSubst.AddNewValue("DestinationFileName", oFileInfo.FullName)
						'oSubst.AddNewValue("DestinationFile", oFileInfo.FullName)
						'oSubst.AddNewValue("FilePath", oFileInfo.FullName)
						'oSubst.AddNewValue("DestFilePath", oFileInfo.FullName)
						'oSubst.AddNewValue("DestFolder", oFileInfo.DirectoryName)

						sHTMLPassed = oParentGeneratedDocs.oParentCore.ApplySubstitution(sHTMLPassed)
						'sHTMLPassed = oSubst.GetSubstitutionForString(sHTMLPassed)

						'oFileInfo.AppendText().WriteLine(sHTMLPassed)
						owrStream = oFileInfo.AppendText()
						owrStream.AutoFlush = True
						owrStream.WriteLine(sHTMLPassed)
						owrStream.Flush() : owrStream.Close() : owrStream = Nothing

						oAttachedFileData = New clsAttachedFileData(oFileInfo.FullName)
						oParentGeneratedDocs.oParentCore.AssociatedFiles.Add(oAttachedFileData)
						If oParentGeneratedDocs.bAddNewDocsToLog Then oAttachedFileData.WriteToLogFile()

						'IO.File.WriteAllText(oFileInfo.FullName, sHTMLPassed)

						'If sLogFilePath <> "" And sLogFileEntry <> "" Then
						'    sLogFilePathUsed = oSubst.GetSubstitutionForString(sLogFilePath, clsValueSubstitutions.TargetModifier.ForFilePath)
						'    sLogFileEntryUsed = oSubst.GetSubstitutionForString(sLogFileEntry)

						'    oLogFileInfo = New IO.FileInfo(sLogFilePathUsed)
						'    owrStream = oLogFileInfo.AppendText()
						'    owrStream.AutoFlush = True
						'    owrStream.WriteLine(sLogFileEntryUsed)
						'    owrStream.Flush() : owrStream.Close() : owrStream = Nothing

						'End If

						sReturn = oFileInfo.FullName
						sStoredPath = sReturn

					End If

					'    If aConvExtns.Contains(oFileInfo.Extension) Then

					'        owrStream = oFileInfo.Create
					'        owrStream.Write()
				End If

			End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If DocTemplateHTML <> "" Then oError.AddDetail("Raw HTML : " & DocTemplateHTML)
			If oAttachedFileData IsNot Nothing Then oError.AddDetail("Attached file : " & oAttachedFileData.FileName)
			gsErr = goErrs.ToString

		End Try

		If owrStream IsNot Nothing Then owrStream.Flush() : owrStream.Close() : owrStream = Nothing
		If oFileInfo IsNot Nothing Then oFileInfo = Nothing
		If owsConvert IsNot Nothing Then owsConvert = Nothing
		If owsConvertibles IsNot Nothing Then owsConvertibles = Nothing
		If oConvParams IsNot Nothing Then oConvParams = Nothing


		Return sReturn
	End Function


End Class

