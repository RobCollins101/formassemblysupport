﻿Imports System.IO

Public Class clsLogFileUpdater

	Protected Friend oParentCore As clsFormSupportCore

	Private sLogLineEntryTemplate As String = ""
	Protected Friend sLastLogLineEntry As String = ""
	Protected Friend sLogFilePathTemplate As String = ""

	Protected Friend sLogFilePath As String = ""
	Protected Friend oLogFile As FileInfo


	Public Sub New(oParent As clsFormSupportCore)
		Try
			oParentCore = oParent

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			gsErr = goErrs.ToString

		End Try
	End Sub


	Friend Property LogLineEntryTemplate As String
		Get
			Dim oTemp As Object
			If sLogLineEntryTemplate = "" Then
				sLogLineEntryTemplate = oParentCore.GetValueIfExists("LogEntryTemplateValLabels", True, clsValueSubstitutions.TargetModifier.NoModify, "DefaultLogFileEntry")
				If sLogLineEntryTemplate = "" Then
					oTemp = GetMySetting("DefaultLogFileEntry")
					If TypeOf (oTemp) Is String Then LoadFile(oTemp.ToString)
				End If
			End If
			Return sLogLineEntryTemplate
		End Get
		Set(value As String)
			sLogLineEntryTemplate = value
		End Set
	End Property

	Friend Property LogLineEntry As String
		Get
			Try
				sLastLogLineEntry = oParentCore.ApplySubstitution(LogLineEntryTemplate)

			Catch ex As Exception

			End Try
			Return sLastLogLineEntry
		End Get
		Set(value As String)
			sLastLogLineEntry = value
		End Set
	End Property

	'Public Function LogFileLine(sLogLineEntryTemplateIn As String) As String
	'    Dim sReturn As String = ""
	'    Try
	'        LogLineEntryTemplate = sLogLineEntryTemplateIn
	'        sReturn = LogLineEntry

	'    Catch ex As Exception

	'    End Try
	'    Return sReturn
	'End Function


	Friend Property LogFilePathTemplate As String
		Get
			Return sLogFilePathTemplate
		End Get
		Set(value As String)
			sLogFilePathTemplate = value
			sLogFilePath = oParentCore.ApplySubstitution(sLogFilePathTemplate)
			oLogFile = Nothing
		End Set
	End Property


	Friend Property LogFilePath As String
		Get
			If oLogFile Is Nothing And sLogFilePath = "" Then sLogFilePath = oParentCore.GetValueIfExists("LogPathValLabels", True, clsValueSubstitutions.TargetModifier.NoModify, "DefaultLogFilePath")
			If oLogFile IsNot Nothing AndAlso oLogFile.FullName <> sLogFilePath Then oLogFile = Nothing

			Return sLogFilePath
		End Get
		Set(value As String)
			sLogFilePath = value
			If sLogFilePath <> "" Then oLogFile = New FileInfo(sLogFilePath)
		End Set
	End Property


	Friend Property LogFileInfo As FileInfo
		Get
			If oLogFile Is Nothing AndAlso StringIsPath(sLogFilePath) Then oLogFile = New FileInfo(sLogFilePath)

			Return oLogFile
		End Get
		Set(value As FileInfo)
			oLogFile = value
			sLogFilePath = ""
			If oLogFile IsNot Nothing Then sLogFilePath = oLogFile.FullName
		End Set
	End Property


	Public Sub SaveLogLine(sLineTemplate As String)
		Try
			LogLineEntryTemplate = sLineTemplate
			SaveLogLine()

		Catch ex As Exception

		End Try

	End Sub


	Public Sub SaveLogLine()
		Dim owrStream As IO.StreamWriter
		Try
			If oLogFile Is Nothing Then oLogFile = New FileInfo(LogFilePath)
			If oLogFile IsNot Nothing Then

				owrStream = oLogFile.AppendText()
				owrStream.AutoFlush = True
				owrStream.WriteLine(LogLineEntry)
				owrStream.Flush() : owrStream.Close() : owrStream = Nothing

				oParentCore.ApplyValueIfExists("LogFilePathValLabels", oLogFile.FullName, True)
				oParentCore.ApplyValueIfExists("LogFileFolderValLabels", oLogFile.DirectoryName, True)
				oParentCore.ApplyValueIfExists("LogFileNameValLabels", oLogFile.Name & oLogFile.Extension, True)

			End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If oLogFile IsNot Nothing Then oError.AddDetail("Log file : " & oLogFile.FullName)
			gsErr = goErrs.ToString

		End Try
	End Sub


End Class
