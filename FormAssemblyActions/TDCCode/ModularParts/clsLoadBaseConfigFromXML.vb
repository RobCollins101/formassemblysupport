﻿Option Compare Text

Imports System.IO
Imports System.Xml

Public Class clsBaseConfigFromXML

	Protected Friend oParentCore As clsFormSupportCore

	Private sXMLConfigKey As String = ""

	Friend sFolderOfXMLFile As String = ""
	Friend sPathOfXMLFile As String = ""
	Friend oFileInfoOfXMLFile As FileInfo

	Friend aXMLDataValues As SortedList(Of String, String)
	Friend aFieldTranslation As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

	Public Sub New()

	End Sub

	Public Sub New(oParent As clsFormSupportCore)
		Me.oParentCore = oParent
	End Sub


	Friend Property XMLConfigKey As String
		Get
			Return sXMLConfigKey
		End Get
		Set(value As String)
			Try
				If value <> sXMLConfigKey Then
					sXMLConfigKey = value
					aXMLDataValues = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
					oFileInfoOfXMLFile = Nothing
					If sFolderOfXMLFile <> "" And sXMLConfigKey <> "" Then
						sPathOfXMLFile = sFolderOfXMLFile & IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1) <> "\", "\", "").ToString & sXMLConfigKey & ".xml"

					End If
				End If

			Catch ex As Exception
				Dim oError As wRuntimeError = goErrs.NewError(ex)
				If value <> "" Then oError.AddDetail("Value : " & value)
				If sPathOfXMLFile <> "" Then oError.AddDetail("PathOfXMLFile : " & sPathOfXMLFile)
				gsErr = goErrs.ToString
			End Try
		End Set
	End Property


	Friend Property XMLDataValues As SortedList(Of String, String)
		Get
			Try
				If aXMLDataValues Is Nothing Then
					aXMLDataValues = New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
					If sPathOfXMLFile <> "" AndAlso sXMLConfigKey <> "" AndAlso FileInfoOfXMLFile IsNot Nothing AndAlso FileInfoOfXMLFile.Exists Then
						LoadConfigFromXML()
					End If
				End If

			Catch ex As Exception
				Dim oError As wRuntimeError = goErrs.NewError(ex)
				If sPathOfXMLFile <> "" Then oError.AddDetail("PathOfXMLFile : " & sPathOfXMLFile)
				gsErr = goErrs.ToString
			End Try
			Return aXMLDataValues
		End Get
		Set(value As SortedList(Of String, String))
			aXMLDataValues = value
		End Set
	End Property


	Friend Property FolderNameOfXMLFile As String
		Get
			If sFolderOfXMLFile = "" And oFileInfoOfXMLFile IsNot Nothing Then sFolderOfXMLFile = oFileInfoOfXMLFile.DirectoryName
			If sFolderOfXMLFile = "" And sPathOfXMLFile <> "" Then sFolderOfXMLFile = IO.Path.GetDirectoryName(sPathOfXMLFile)

			Return sFolderOfXMLFile
		End Get
		Set(value As String)
			If sFolderOfXMLFile <> value Then
				sFolderOfXMLFile = value
				sPathOfXMLFile = ""
				oFileInfoOfXMLFile = Nothing
			End If

		End Set
	End Property


	Friend Property FilePathOfXMLFile As String
		Get
			If sPathOfXMLFile = "" And oFileInfoOfXMLFile IsNot Nothing Then sPathOfXMLFile = oFileInfoOfXMLFile.FullName
			If sPathOfXMLFile = "" And sFolderOfXMLFile <> "" Then
				sPathOfXMLFile = sFolderOfXMLFile & IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1) <> "\", "\", "").ToString & sXMLConfigKey & ".xml"
			End If

			Return sPathOfXMLFile
		End Get
		Set(value As String)
			If value <> sPathOfXMLFile Then
				sPathOfXMLFile = value
				oFileInfoOfXMLFile = Nothing
			End If

		End Set
	End Property

	Friend Property FileInfoOfXMLFile As FileInfo
		Get
			If oFileInfoOfXMLFile Is Nothing And sPathOfXMLFile <> "" Then
				oFileInfoOfXMLFile = New FileInfo(sPathOfXMLFile)
			End If
			Return oFileInfoOfXMLFile
		End Get
		Set(value As FileInfo)
			oFileInfoOfXMLFile = value
			sPathOfXMLFile = ""
			sFolderOfXMLFile = ""
		End Set
	End Property

	Protected Friend Sub LoadConfigFromXML()

		Dim oValues As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
		Dim sKey As String, sValue As String

		Try
			'oValues = LoadValuesFromTempXMLFile(oParentCore.SessionID)
			oValues = LoadValuesFromXMLConfigFile(sXMLConfigKey)
			For Each sKey In oValues.Keys
				sValue = oValues.Item(sKey)

				'If Not oParentCore.ValuesFromHTTPRequest.SubmittedDataValues.ContainsKey(sKey) Then oParentCore.ValuesFromHTTPRequest.SubmittedDataValues.Add(sKey, sValue) Else oParentCore.ValuesFromHTTPRequest.SubmittedDataValues.Item(sKey) = sValue
				UpdStringSortedList(oParentCore.ValuesFromHTTPRequest.SubmittedDataValues, sKey, sValue)

				'If Not oParentCore.oCurrentData.oDataFromPost.aFieldValues.ContainsKey(sKey) Then oParentCore.oCurrentData.oDataFromPost.aFieldValues.Add(sKey, sValue) Else oParentCore.oCurrentData.oDataFromPost.aFieldValues.Item(sKey) = sValue
				'If oParentCore.oFieldDataUsed.ContainsKey(sKey) Then oParentCore.oFieldDataUsed.Item(sKey) = sValue Else oParentCore.oFieldDataUsed.Add(sKey, sValue)
				If oParentCore.oFieldDataUsed.ContainsKey(sKey) Then

				Else
					oParentCore.oFieldDataUsed.Add(sKey, sValue)
				End If

			Next

			ApplyTranslationCodes()

			'ProcessPostedDataValues(oCurrentData)

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKey <> "" Then oError.AddDetail("Key : " & sKey)
			If sValue <> "" Then oError.AddDetail("Value : " & sValue)
			gsErr = goErrs.ToString

		End Try
	End Sub

	Public Sub ApplyTranslationCodes()
		Dim sKey As String, sNewKey As String, sValue As String
		Dim aAdds As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
		Try
			For Each sKey In oParentCore.oFieldDataUsed.Keys
				If aFieldTranslation.ContainsKey(sKey) Then
					sNewKey = aFieldTranslation.Item(sKey)
					If Not (oParentCore.oFieldDataUsed.ContainsKey(sNewKey)) Then
						sValue = oParentCore.oFieldDataUsed.Item(sKey).ToString
						aAdds.Add(sNewKey, sValue)
					End If
				End If
			Next
			For Each sNewKey In aAdds.Keys
				sValue = aAdds.Item(sNewKey)
				oParentCore.oFieldDataUsed.Add(sNewKey, sValue)
			Next

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKey <> "" Then oError.AddDetail("Key : " & sKey)
			If sValue <> "" Then oError.AddDetail("Value : " & sValue)
			gsErr = goErrs.ToString

		End Try
	End Sub


	'Public Function LoadValuesFromTempXMLFile(sSessionID As String) As SortedList(Of String, String)
	Public Function LoadValuesFromXMLConfigFile(sConfigID As String) As SortedList(Of String, String)
		'Dim oReturn As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)
		Dim oDOM As XmlDocument, oRootElement As XmlElement
		Dim oValueSetElement As XmlElement, oValElement As XmlElement, oAttr As XmlAttribute, oAttr2 As XmlAttribute
		Dim sKey As String, sValue As String, oNode As XmlNode
		Dim oTemp As Object, sFileKey As String ', sFolderOfXMLFile As String ', sPathOfXMLFile As String
		Dim aChildren As XmlNodeList, oOneValElement As XmlElement
		Dim oChildTransNode As XmlNode, oChildTransElem As XmlElement
		Dim sOrigFieldName As String, sNewFieldName As String
		Dim oTblRow As TableRow, oTblCell As TableCell, sTemp As String
		Dim aSourceAttrNames As New ArrayList, aTranslatedAttrNames As New ArrayList, sAttrName As String
		Try
			If sFolderOfXMLFile = "" Then
				sFolderOfXMLFile = oParentCore.GetValueIfExists("ConfigLocationValLabels",,, "DefaultConfigStoreLocation")
				If sFolderOfXMLFile = "" And sConfigID.IndexOf("\") >= 0 Then sFolderOfXMLFile = Path.GetDirectoryName(sConfigID)
			End If

			If sFolderOfXMLFile <> "" And sConfigID <> "" Then

				'sFileKey = oParentCore.SessionID & "_Values.xml"
				sFileKey = sConfigID & "_Config.xml"
				FilePathOfXMLFile = sFolderOfXMLFile & IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1, 1) <> "\", "\", "").ToString & sFileKey

				If FileInfoOfXMLFile.Exists Then
					'If IO.File.Exists(sPathOfXMLFile) Then

					oDOM = New XmlDocument()
					oDOM.Load(FilePathOfXMLFile)
					oRootElement = TryCast(oDOM.SelectSingleNode("Configuration"), XmlElement)
					If oRootElement IsNot Nothing Then

						oValElement = TryCast(oRootElement.SelectSingleNode("Values"), XmlElement)
						If oValElement IsNot Nothing Then
							oTemp = GetMySetting("FieldNameTranslationSourceAttrName")
							If TypeOf (oTemp) Is Specialized.StringCollection Then
								aSourceAttrNames = New ArrayList(TryCast(oTemp, Specialized.StringCollection))
							ElseIf TypeOf (oTemp) Is String Then
								aSourceAttrNames = oParentCore.SplitToArray(oTemp.ToString)
							Else
								aSourceAttrNames.Add(oTemp.ToString)
							End If
							oTemp = GetMySetting("FieldNameTranslateNewAttrName")
							If TypeOf (oTemp) Is Specialized.StringCollection Then
								aTranslatedAttrNames = New ArrayList(TryCast(oTemp, Specialized.StringCollection))
							ElseIf TypeOf (oTemp) Is String Then
								aTranslatedAttrNames = oParentCore.SplitToArray(oTemp.ToString)
							Else
								aTranslatedAttrNames.Add(oTemp.ToString)
							End If

							aChildren = oValElement.ChildNodes
							For Each oNode In aChildren
								oOneValElement = TryCast(oNode, XmlElement)
								If oOneValElement IsNot Nothing Then
									sKey = oOneValElement.Name
									If sKey = "FieldTranslations" Then
										For Each oChildTransNode In oOneValElement.ChildNodes
											oChildTransElem = TryCast(oChildTransNode, XmlElement)
											oAttr = Nothing
											oAttr2 = Nothing
											For Each sAttrName In aSourceAttrNames
												Try : oAttr = TryCast(oChildTransNode.Attributes.GetNamedItem(sAttrName), XmlAttribute) : Catch ex As Exception : End Try
												If oAttr IsNot Nothing Then Exit For
											Next
											For Each sAttrName In aTranslatedAttrNames
												Try : oAttr2 = TryCast(oChildTransNode.Attributes.GetNamedItem(sAttrName), XmlAttribute) : Catch ex As Exception : End Try
												If oAttr2 IsNot Nothing Then Exit For
											Next
											If oAttr IsNot Nothing Then
												sOrigFieldName = oAttr.Value
											Else
												sOrigFieldName = oChildTransElem.Name
											End If
											If oAttr2 Is Nothing Then
												sNewFieldName = oChildTransElem.InnerText
											Else
												sNewFieldName = oAttr2.Value
											End If
											UpdStringSortedList(aFieldTranslation, sOrigFieldName, sNewFieldName)
										Next
									Else
										sValue = oOneValElement.InnerText
										'If XMLDataValues.ContainsKey(sKey) Then XMLDataValues.Item(sKey) = sValue Else XMLDataValues.Add(sKey, sValue)
										UpdStringSortedList(XMLDataValues, sKey, sValue)
										'If XMLDataValues.ContainsKey(sKey) Then XMLDataValues.Item(sKey) = sValue Else XMLDataValues.Add(sKey, sValue)

										If oParentCore.tblRecoveredStringValues IsNot Nothing Then
											oTblRow = New TableRow
											oTblCell = New TableCell
											oTblCell.Text = HttpUtility.HtmlEncode(sKey)
											oTblRow.Cells.Add(oTblCell)

											oTblCell = New TableCell
											sTemp = sValue
											oTblCell.Text = HttpUtility.HtmlEncode(sTemp)
											oTblRow.Cells.Add(oTblCell)
											oParentCore.tblRecoveredStringValues.Rows.Add(oTblRow)
										End If
									End If
								End If
							Next

						End If
						oParentCore.AddValuesToFieldData(XMLDataValues)
					End If
				End If
			End If


		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKey <> "" Then oError.AddDetail("Key : " & sKey)
			If sValue <> "" Then oError.AddDetail("Value : " & sValue)
			If oNode IsNot Nothing Then oError.AddDetail("Node Name : " & oNode.Name)
			gsErr = goErrs.ToString

		End Try

		Return XMLDataValues
	End Function


	'Friend Sub SaveBaseConfigValuesToTempXMLFile()
	'    Try
	'        SaveValuesToTempXMLFile(oParentCore.ValuesFromHTTPRequest.SubmittedDataValues)
	'    Catch ex As Exception

	'    End Try
	'End Sub


	'Friend Sub SaveValuesToTempXMLFile(aValues As SortedList(Of String, String))
	'    Dim oDOM As XmlDocument, oRootElement As XmlElement
	'    Dim oElement As XmlElement, oValElement As XmlElement, oAttr As XmlAttribute
	'    Dim sKey As String, sValue As String
	'    Dim oTemp As Object, sFileKey As String ', sFileFolder As String, sFilePath As String

	'    Try

	'        'oTemp = GetMySetting("TempValueStore")
	'        'If oTemp IsNot Nothing Then
	'        '    sFileFolder = oTemp.ToString
	'        'Else
	'        '    sFileFolder = IO.Path.GetTempPath
	'        'End If
	'        'sFileFolder &= IIf(sFileFolder.Substring(sFileFolder.Length - 1, 1) <> "\", "\", "").ToString

	'        If sFolderOfXMLFile = "" Then
	'            sFolderOfXMLFile = oParentCore.GetValueIfExists("TempValueStoreValLabels",,, "TempValueStore")
	'            If sFolderOfXMLFile = "" Then
	'                oTemp = GetMySetting("TempValueStore")
	'                If oTemp IsNot Nothing Then
	'                    sFolderOfXMLFile = oTemp.ToString
	'                Else
	'                    sFolderOfXMLFile = IO.Path.GetTempPath
	'                End If
	'            End If
	'            sFolderOfXMLFile &= IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1, 1) <> "\", "\", "").ToString
	'        End If

	'        sFileKey = oParentCore.SessionID & "_Values.xml"
	'        FilePathOfXMLFile = sFolderOfXMLFile & IIf(sFolderOfXMLFile.Substring(sFolderOfXMLFile.Length - 1, 1) <> "\", "\", "").ToString & sFileKey


	'        oDOM = New XmlDocument
	'        oRootElement = oDOM.CreateElement("Details")
	'        oDOM.AppendChild(oRootElement)

	'        oAttr = oDOM.CreateAttribute("DateCreated")
	'        oAttr.Value = Now.ToString("o")
	'        oRootElement.Attributes.Append(oAttr)

	'        oAttr = oDOM.CreateAttribute("SessionKey")
	'        oAttr.Value = oParentCore.SessionID
	'        oRootElement.Attributes.Append(oAttr)

	'        oElement = oDOM.CreateElement("Values")
	'        oRootElement.AppendChild(oElement)

	'        'For Each sKey In aFieldValues.Keys

	'        'For Each sKey In oParentCore.ValuesFromHTTPRequest.SubmittedDataValues.Keys
	'        For Each sKey In aValues.Keys
	'            sValue = aValues.Item(sKey).ToString
	'            oValElement = oDOM.CreateElement(sKey)
	'            oValElement.InnerText = sValue
	'            oElement.AppendChild(oValElement)
	'        Next

	'        'sFileKey = sPaymentSessionID & "_Values.xml"
	'        'sFilePath = sFileFolder & IIf(sFileFolder.Substring(sFileFolder.Length - 1, 1) <> "\", "\", "").ToString & sFileKey

	'        'oDOM.Save(sFilePath)

	'        oDOM.Save(FilePathOfXMLFile)

	'    Catch ex As Exception

	'    End Try

	'End Sub


End Class
