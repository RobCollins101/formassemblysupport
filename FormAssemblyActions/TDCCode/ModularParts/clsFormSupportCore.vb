﻿Option Compare Text
Imports System.Xml

Public Class clsFormSupportCore
	Friend oFieldDataUsed As New SortedList(Of String, String)(StringComparer.CurrentCultureIgnoreCase)

	Friend oRequest As HttpRequest
	Private sSessionID As String = ""
	Friend sSubmissionTypeCode As String = ""

	Friend oCurrentData As clsCurrentData

	Friend tblRecoveredStringValues As Table
	Friend tblQueryStringValues As Table
	Friend tblPostStringValues As Table
	Friend sReturnURLToCaller As String = ""

	Friend oPage As Page
	Friend bShowDebugValues As Boolean = False

	Private oValuesFromHTTPRequest As clsValuesFromHTTPRequest
	Private oValuesFromXML As clsValuesFromStoredXML
	Private oRequestFiles As clsAttachmentsOfHTTPRequest
	Private oLogFileUpd As clsLogFileUpdater
	Private oRequestEmails As clsGeneratedEmailsForRequest
	Private oRequestDocs As colGeneratedDocumentsForRequest
	Private oAssociatedFiles As colAssociatedFiles
	Private oConfigDetails As clsBaseConfigFromXML

	Friend sPathSeparatorToToUse As String = ";"

	Public Enum StagesToDo
		AttachmentsDocsAndEmails
		AttachmentsDocs
		AttachmentsEmails
		AttachmentsOnly
		FieldsOnly
	End Enum


	Friend Property ValuesFromHTTPRequest As clsValuesFromHTTPRequest
		Get
			If oValuesFromHTTPRequest Is Nothing Then
				oValuesFromHTTPRequest = New clsValuesFromHTTPRequest(Me)
				'oValuesFromHTTPRequest.oParentCore = Me
				oValuesFromHTTPRequest.ProcessFieldValues()
			End If
			oValuesFromHTTPRequest.oParentCore = Me
			Return oValuesFromHTTPRequest
		End Get
		Set(value As clsValuesFromHTTPRequest)
			oValuesFromHTTPRequest = value
			oValuesFromHTTPRequest.oParentCore = Me
		End Set
	End Property


	Friend Property ValuesFromXML As clsValuesFromStoredXML
		Get
			If oValuesFromXML Is Nothing Then
				oValuesFromXML = New clsValuesFromStoredXML(Me)
				'oValuesFromXML.oParentCore = Me
				oValuesFromXML.LoadDataFromXMLFile()
			End If
			oValuesFromXML.oParentCore = Me
			Return oValuesFromXML
		End Get
		Set(value As clsValuesFromStoredXML)
			oValuesFromXML = value
			oValuesFromXML.oParentCore = Me
		End Set
	End Property


	Friend Property ConfigDetailsFromXML As clsBaseConfigFromXML
		Get
			If oConfigDetails Is Nothing Then
				oConfigDetails = New clsBaseConfigFromXML(Me)
				'oConfigDetails.oParentCore = Me
				oConfigDetails.LoadConfigFromXML()
			End If
			oConfigDetails.oParentCore = Me
			Return oConfigDetails
		End Get
		Set(value As clsBaseConfigFromXML)
			oConfigDetails = value
			oConfigDetails.oParentCore = Me
		End Set
	End Property


	Friend Property RequestAttachments As clsAttachmentsOfHTTPRequest
		Get
			If oRequestFiles Is Nothing Then
				oRequestFiles = New clsAttachmentsOfHTTPRequest(Me)
				'oRequestFiles.oParentCore = Me
				'oSaveRequestAttachments.SaveFilesOfSubmission()
			End If
			oRequestFiles.oParentCore = Me
			Return oRequestFiles
		End Get
		Set(value As clsAttachmentsOfHTTPRequest)
			oRequestFiles = oRequestFiles
			oRequestFiles.oParentCore = Me
		End Set
	End Property


	Friend Property LogFileUpd As clsLogFileUpdater
		Get
			If oLogFileUpd Is Nothing Then
				oLogFileUpd = New clsLogFileUpdater(Me)
				'oLogFileUpd.oParentCore = Me
			End If
			oLogFileUpd.oParentCore = Me
			Return oLogFileUpd
		End Get
		Set(value As clsLogFileUpdater)
			oLogFileUpd = value
			oLogFileUpd.oParentCore = Me
		End Set
	End Property


	Friend Property RequestEmails As clsGeneratedEmailsForRequest
		Get
			If oRequestEmails Is Nothing Then
				oRequestEmails = New clsGeneratedEmailsForRequest(Me)
				'oRequestEmails.oParentCore = Me
			End If
			oRequestEmails.oParentCore = Me
			Return oRequestEmails
		End Get
		Set(value As clsGeneratedEmailsForRequest)
			oRequestEmails = value
			oRequestEmails.oParentCore = Me
		End Set
	End Property


	Friend Property RequestDocs As colGeneratedDocumentsForRequest
		Get
			If oRequestDocs Is Nothing Then
				oRequestDocs = New colGeneratedDocumentsForRequest(Me)
				'oRequestDoc.oParentCore = Me
			End If
			oRequestDocs.oParentCore = Me
			Return oRequestDocs
		End Get
		Set(value As colGeneratedDocumentsForRequest)
			oRequestDocs.oParentCore = Me
			oRequestDocs = value
		End Set
	End Property


	Friend Property AssociatedFiles As colAssociatedFiles
		Get
			If oAssociatedFiles Is Nothing Then
				oAssociatedFiles = New colAssociatedFiles
				oAssociatedFiles.oParentCore = Me
			End If
			oAssociatedFiles.oParentCore = Me
			Return oAssociatedFiles
		End Get
		Set(value As colAssociatedFiles)
			oAssociatedFiles.oParentCore = Me
			oAssociatedFiles = value
		End Set
	End Property


	Public Property SessionID As String
		Get
			If sSessionID = "" And oFieldDataUsed.Count > 0 Then
				sSessionID = GetValueIfExists("SessionIDValLabels")
			End If
			Return sSessionID
		End Get
		Set(value As String)
			sSessionID = value
			ApplyValueIfExists("SessionIDValLabels", sSessionID, True)
		End Set
	End Property




	Public Sub ProcessHTTPRequest(oRequest As HttpRequest, oPage As Page, Optional eStages As StagesToDo = StagesToDo.AttachmentsDocsAndEmails, Optional ResetXML As Boolean = False)
		' Parts are:
		'   Load and process field values
		'   load configuration from XML file
		'   Process files
		'   Create documents
		'   Send emails

		Dim bLoadfields As Boolean = True
		Dim bLoadConfig As Boolean = True
		Dim bProcessAttachments As Boolean = False
		Dim bCreateDocs As Boolean = False
		Dim bSendEmails As Boolean = False
		Dim sTempSavePath As String, sTempDebugSavePath As String, sSaveFolder As String

		Try

			Me.oRequest = oRequest
			Me.oPage = oPage

			If ValuesFromHTTPRequest.SubmittedDataValues.Count = 0 Then ValuesFromHTTPRequest.ProcessFieldValues()

			sSubmissionTypeCode = GetValueIfExists("SubmissonConfigIDValLabels")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("SubmissionConfigIDKey")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("SubmissionConfigID")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("FormConfigIDKeyValues")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("FormConfigIDKey")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("FormConfigID")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("ConfigIDKey")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("ConfigID")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("FormIDKey")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("FormID")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("SubmissionConfigurationIDKey")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("SubmissionConfigurationID")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("FormConfigurationIDKeyValues")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("FormConfigurationIDKey")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("FormConfigurationID")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("ConfigurationIDKey")
			If sSubmissionTypeCode = "" Then sSubmissionTypeCode = GetValueIfExists("ConfigurationID")

			' -----------------------------------------
			' Add special codes
			' -----------------------------------------
			ApplyValueIfExists("RequestDateValLabels", Now().ToString())


			Select Case eStages
				Case StagesToDo.AttachmentsDocsAndEmails
					bProcessAttachments = True
					bCreateDocs = True
					bSendEmails = True
					If oValuesFromXML.aXMLDataValues.Count > 0 Then bLoadfields = False
				Case StagesToDo.AttachmentsDocs
					bProcessAttachments = True
					bCreateDocs = True
					If oValuesFromXML.aXMLDataValues.Count > 0 Then bLoadfields = False
				Case StagesToDo.AttachmentsEmails
					bProcessAttachments = True
					bSendEmails = True
					If oValuesFromXML.aXMLDataValues.Count > 0 Then bLoadfields = False
				Case StagesToDo.AttachmentsOnly
					bProcessAttachments = True
					If oValuesFromXML.aXMLDataValues.Count > 0 Then bLoadfields = False
				Case StagesToDo.FieldsOnly

			End Select

			' If config is known, preload config details

			If bLoadfields And sSubmissionTypeCode <> "" Then
				If ConfigDetailsFromXML.XMLConfigKey = "" Or ConfigDetailsFromXML.aXMLDataValues.Count = 0 Then
					ConfigDetailsFromXML.XMLConfigKey = sSubmissionTypeCode
					ConfigDetailsFromXML.LoadConfigFromXML()
				End If
			End If

			'If bLoadConfig And sSubmissionTypeCode <> "" And ConfigDetailsFromXML.aXMLDataValues.Count = 0 Then
			'	ConfigDetailsFromXML.XMLConfigKey = sSubmissionTypeCode
			'	ConfigDetailsFromXML.LoadConfigFromXML()
			'End If

			If bLoadfields And ValuesFromXML.aXMLDataValues.Count = 0 And Not ResetXML Then
				ValuesFromXML.LoadDataFromXMLFile()
				If sSubmissionTypeCode = "" Then
					' If config is known, reload config details
					sSubmissionTypeCode = GetValueIfExists("SubmissonConfigIDValLabels")
					If bLoadfields And sSubmissionTypeCode <> "" Then
						If ConfigDetailsFromXML.XMLConfigKey = "" Or ConfigDetailsFromXML.aXMLDataValues.Count = 0 Then
							ConfigDetailsFromXML.XMLConfigKey = sSubmissionTypeCode
							ConfigDetailsFromXML.LoadConfigFromXML()
						End If
					End If
				End If
			End If

			ReverseApplyCodeValues()

			bShowDebugValues = StringIsTrue(GetValueIfExists("DebugFieldNames"))

			' ============================================
			' The main action part
			' ============================================

			If bProcessAttachments Then RequestAttachments.ProcessFilesOfSubmission()
			If bCreateDocs Then RequestDocs.GenerateDocuments()
			If bSendEmails Then RequestEmails.SendEmails()

			' ============================================
			' End of the main action part
			' ============================================


			If bShowDebugValues Then sTempDebugSavePath = StoreDebugValues()

			If bCreateDocs Then

				sTempSavePath = GetValueIfExists("DestinationDocPath", True, clsValueSubstitutions.TargetModifier.ForFilePath, "")
				If sTempSavePath = "" Then
					sSaveFolder = GetMySettingString("DefaultStoreFolderDest")
				Else
					sSaveFolder = IO.Path.GetDirectoryName(sTempSavePath)
				End If

				sSaveFolder &= IIf(sSaveFolder.Substring(sSaveFolder.Length - 1, 1) = IO.Path.DirectorySeparatorChar, "", IO.Path.DirectorySeparatorChar).ToString()

				sTempSavePath = sSaveFolder & IO.Path.GetFileName(ValuesFromXML.FilePathOfXMLFile)
				sTempSavePath = ApplySubstitution(sTempSavePath, clsValueSubstitutions.TargetModifier.ForFilePath)
				Try : IO.File.Copy(ValuesFromXML.FilePathOfXMLFile, sTempSavePath) : Catch ex2 As Exception : End Try

				If sTempDebugSavePath <> "" Then
					sTempSavePath = sSaveFolder & IO.Path.GetFileName(sTempDebugSavePath)
					sTempSavePath = ApplySubstitution(sTempSavePath, clsValueSubstitutions.TargetModifier.ForFilePath)
					Try : IO.File.Copy(sTempDebugSavePath, sTempSavePath) : Catch ex2 As Exception : End Try
				End If

			End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			gsErr = goErrs.ToString

		End Try
	End Sub




	Public Function ApplySubstitution(ValueToUpdate As String, Optional SubstModifider As clsValueSubstitutions.TargetModifier = clsValueSubstitutions.TargetModifier.NoModify) As String
		Dim sReturn As String = "", oSubst As clsValueSubstitutions
		Try
			sReturn = ValueToUpdate
			If sReturn.Length > 4 Then
				oSubst = New clsValueSubstitutions(sReturn, oFieldDataUsed, SubstModifider)
				sReturn = oSubst.Result
			End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If ValueToUpdate <> "" Then oError.AddDetail("Value to apply substitution : " & ValueToUpdate)
			gsErr = goErrs.ToString

		End Try
		Return sReturn
	End Function


	Public Function GetValueIfExists(sKeyIDOfList As String,
												Optional bUseSubst As Boolean = False,
												Optional SubstModifier As clsValueSubstitutions.TargetModifier = clsValueSubstitutions.TargetModifier.NoModify,
												Optional DefaultValueKey As String = "",
												Optional ApplyValueToAll As Boolean = True) As String

		Dim sReturn As String = "", sKey As String, oTemp As Object, iPos As Integer, oTempKeyList As Object
		Dim aTemp As New ArrayList, aKeyVals As New List(Of String), sVal As String, aCodedKeyVals As New List(Of String)
		Try

			If gaFieldLabelLists Is Nothing Then gaFieldLabelLists = New clsFieldLabelLists

			If gaFieldLabelLists.LabelLists.ContainsKey(sKeyIDOfList) Then

				oTempKeyList = gaFieldLabelLists.GetList(sKeyIDOfList)

				If TypeOf oTempKeyList Is ArrayList Then
					aTemp = DirectCast(oTempKeyList, ArrayList)
					For Each sVal In aTemp
						If Not aKeyVals.Contains(sVal) Then aKeyVals.Add(sVal)
					Next
				ElseIf TypeOf oTempKeyList Is List(Of String) Then
					aKeyVals = DirectCast(oTempKeyList, List(Of String))
				End If
				If aKeyVals.Count > 0 Then aCodedKeyVals.AddRange(aKeyVals)

			Else

				oTemp = GetMySetting(sKeyIDOfList)
				If oTemp.ToString = "" Then
					oTemp = GetMySetting(DefaultValueKey)
					sReturn = ApplySubstitution(oTemp.ToString, SubstModifier)
					aKeyVals.Clear()
				ElseIf oTemp Is Nothing Then
					aTemp = SplitToArray(sKeyIDOfList)
					For Each sVal In aTemp
						If Not aKeyVals.Contains(sVal) Then aKeyVals.Add(sVal)
					Next
				Else
					If TypeOf (oTemp) Is Specialized.StringCollection Then
						aTemp = New ArrayList(TryCast(oTemp, Specialized.StringCollection))
						For Each sVal In aTemp
							If Not aKeyVals.Contains(sVal) Then aKeyVals.Add(sVal)
						Next
					ElseIf TypeOf (oTemp) Is String Then
						aTemp = SplitToArray(oTemp.ToString)
						For Each sVal In aTemp
							If Not aKeyVals.Contains(sVal) Then aKeyVals.Add(sVal)
						Next
					Else
						aKeyVals.Add(oTemp.ToString)
					End If

					For iPos = aKeyVals.Count - 1 To 0 Step -1
						aKeyVals(iPos) = Trim(aKeyVals(iPos).ToString)
						If aKeyVals(iPos).ToString = "" Then aKeyVals.RemoveAt(iPos)
					Next

					If aKeyVals.Count > 0 Then
						If gaFieldLabelLists.LabelLists.ContainsKey(sKeyIDOfList) Then gaFieldLabelLists.LabelLists(sKeyIDOfList) = aKeyVals Else gaFieldLabelLists.LabelLists.Add(sKeyIDOfList, aKeyVals)

					End If

				End If

			End If

			If aKeyVals.Count > 0 Then
				sReturn = GetValueIfExists(aKeyVals, bUseSubst, SubstModifier, DefaultValueKey)

				If ApplyValueToAll And aCodedKeyVals.Count > 0 Then
					'ApplyValueIfExists(sKeyIDOfList, sReturn, True)
					sVal = ""
					For Each sKey In aCodedKeyVals
						If oFieldDataUsed.ContainsKey(sKey) Then
							If oFieldDataUsed.Item(sKey) <> "" Then
								sVal = oFieldDataUsed.Item(sKey)
								Exit For
							End If
						End If
					Next
					'If sVal <> "" Then
					For Each sKey In aCodedKeyVals
						If oFieldDataUsed.ContainsKey(sKey) Then
							If oFieldDataUsed.Item(sKey) = "" And sVal <> "" Then oFieldDataUsed.Item(sKey) = sVal
						Else
							oFieldDataUsed.Add(sKey, sVal)
						End If
					Next
					'End If
				End If
			End If

			'For Each sKey In aVals
			'    If oFieldDataUsed.ContainsKey(sKey) Then
			'        sReturn = oFieldDataUsed.Item(sKey)
			'        If sReturn <> "" Then Exit For
			'    End If
			'Next

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKeyIDOfList <> "" Then oError.AddDetail("Key List : " & sKeyIDOfList)
			gsErr = goErrs.ToString

		End Try
		Return sReturn
	End Function

	'Public Function GetValueIfExists(sKeyList As ArrayList,
	Public Function GetValueIfExists(sKeyList As List(Of String),
												Optional bUseSubst As Boolean = False,
												Optional SubstModifider As clsValueSubstitutions.TargetModifier = clsValueSubstitutions.TargetModifier.NoModify,
												Optional DefaultValueKey As String = "") As String

		Dim sReturn As String = "", sKey As String, aVals As ArrayList, bFound As Boolean = False
		Dim oTemp As Object, sTemp As String
		Try

			aVals = New ArrayList(sKeyList)
			For Each sKey In sKeyList
				If oFieldDataUsed.ContainsKey(sKey) Then
					sReturn = oFieldDataUsed.Item(sKey)
					bFound = True
					If sReturn <> "" Then
						Exit For
					End If
				End If
			Next

			If Not bFound And DefaultValueKey <> "" Then
				oTemp = GetMySetting(DefaultValueKey)
				If oTemp IsNot Nothing Then
					sTemp = oTemp.ToString
					sReturn = ApplySubstitution(sTemp, SubstModifider)
				End If
			Else
				If sReturn <> "" And bUseSubst Then
					sReturn = ApplySubstitution(sReturn, SubstModifider)
				End If
			End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKeyList IsNot Nothing Then oError.AddDetail("Keys : " & Join(sKeyList.ToArray, ","))
			If sKey <> "" Then oError.AddDetail("sKey : " & sKey)
			gsErr = goErrs.ToString


		End Try
		Return sReturn
	End Function


	Protected Sub ReverseApplyCodeValues()
		Dim sCodeListKey As String = "", sValue As String
		Try

			For Each sCodeListKey In gaFieldLabelLists.LabelLists.Keys
				sValue = GetValueIfExists(sCodeListKey)
			Next

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sCodeListKey <> "" Then oError.AddDetail("sCodeListKey : " & sCodeListKey)
			gsErr = goErrs.ToString

		End Try
	End Sub


	Protected Friend Function SplitToArray(ValString As String) As ArrayList
		Dim oReturn As New ArrayList
		Dim oTemp As Object, sTemp As String, aDelims As ArrayList = Nothing, sVal As String
		Dim iMax As Integer, iPos As Integer, sFirstDelim As String
		Try

			oTemp = GetMySetting("ValueDividersToArray")
			If TypeOf (oTemp) Is String Then
				aDelims = New ArrayList(oTemp.ToString.ToCharArray)
			ElseIf TypeOf (oTemp) Is Specialized.StringCollection Then
				aDelims = New ArrayList(TryCast(oTemp, Specialized.StringCollection))
			End If
			sPathSeparatorToToUse = aDelims(0).ToString
			oReturn = SplitToArray(ValString, aDelims)

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If ValString <> "" Then oError.AddDetail("ValString : " & ValString)
			If iPos <> 0 Then oError.AddDetail("iPos : " & iPos.ToString)
			gsErr = goErrs.ToString


		End Try
		Return oReturn
	End Function


	Protected Friend Function SplitToArray(ValString As String, sDelims As String) As ArrayList
		Dim oReturn As New ArrayList
		Dim oTemp As Object, sTemp As String, aDelims As ArrayList = Nothing, sVal As String
		Dim iMax As Integer, iPos As Integer, sFirstDelim As String
		Try
			aDelims = New ArrayList(sDelims.ToCharArray)

			oReturn = SplitToArray(ValString, aDelims)

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If ValString <> "" Then oError.AddDetail("ValString : " & ValString)
			If iPos <> 0 Then oError.AddDetail("iPos : " & iPos.ToString)
			gsErr = goErrs.ToString


		End Try
		Return oReturn
	End Function


	Protected Friend Function SplitToArray(ValString As String, aDelims As ArrayList) As ArrayList
		Dim oReturn As New ArrayList
		Dim oTemp As Object, sTemp As String, sVal As String
		Dim iMax As Integer, iPos As Integer, sFirstDelim As String
		Try
			If aDelims IsNot Nothing Then

				iMax = aDelims.Count - 1

				If iMax > 0 Then
					sFirstDelim = aDelims(0).ToString
					For iPos = 1 To iMax
						sVal = aDelims(iPos).ToString
						If sVal <> "" Then ValString = ValString.Replace(sVal, sFirstDelim)
					Next
					oReturn = New ArrayList(Split(ValString, sFirstDelim))
				End If
			Else
				oReturn.Add(ValString)
			End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If ValString <> "" Then oError.AddDetail("ValString : " & ValString)
			If iPos <> 0 Then oError.AddDetail("iPos : " & iPos.ToString)
			gsErr = goErrs.ToString


		End Try
		Return oReturn
	End Function



	Public Sub ApplyValueIfExists(sKeyIDOfList As String, ValueToApply As String, Optional CreateAll As Boolean = False)
		'Dim oTemp As Object, aKeyVals As New ArrayList
		Dim oKeyList As Object, oTemp As Object, aKeyVals As New List(Of String)
		Dim sReturn As String = "", sKey As String, iPos As Integer
		Dim aTemp As New ArrayList, sVal As String
		Try

			If gaFieldLabelLists Is Nothing Then gaFieldLabelLists = New clsFieldLabelLists
			If gaFieldLabelLists.LabelLists.ContainsKey(sKeyIDOfList) Then

				oKeyList = gaFieldLabelLists.GetList(sKeyIDOfList)

				If TypeOf oKeyList Is ArrayList Then
					aTemp = DirectCast(oKeyList, ArrayList)
					For Each sVal In aTemp
						If Not aKeyVals.Contains(sVal) Then aKeyVals.Add(sVal)
					Next
				ElseIf TypeOf oKeyList Is List(Of String) Then
					aKeyVals = DirectCast(oKeyList, List(Of String))
				End If

			Else

				oTemp = GetMySetting(sKeyIDOfList)
				If oTemp Is Nothing Then
					aTemp = SplitToArray(sKeyIDOfList)
					For Each sVal In aTemp
						If Not aKeyVals.Contains(sVal) Then aKeyVals.Add(sVal)
					Next
					'aVals.Add(sKeyListID)
				Else
					If TypeOf (oTemp) Is Specialized.StringCollection Then
						aTemp = New ArrayList(TryCast(oTemp, Specialized.StringCollection))
						For Each sVal In aTemp
							If Not aKeyVals.Contains(sVal) Then aKeyVals.Add(sVal)
						Next
					ElseIf TypeOf (oTemp) Is String Then
						aTemp = SplitToArray(oTemp.ToString)
						For Each sVal In aTemp
							If Not aKeyVals.Contains(sVal) Then aKeyVals.Add(sVal)
						Next
					Else
						aKeyVals.Add(oTemp.ToString)
					End If
					If gaFieldLabelLists.LabelLists.ContainsKey(sKeyIDOfList) Then gaFieldLabelLists.LabelLists(sKeyIDOfList) = aKeyVals Else gaFieldLabelLists.LabelLists.Add(sKeyIDOfList, aKeyVals)
				End If
			End If

			If aKeyVals.Count > 0 Then ApplyValueIfExists(aKeyVals, ValueToApply, CreateAll)

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKeyIDOfList <> "" Then oError.AddDetail("List Key : " & sKeyIDOfList)
			If ValueToApply <> "" Then oError.AddDetail("Value : " & ValueToApply)
			gsErr = goErrs.ToString


		End Try
	End Sub

	'Public Sub ApplyValueIfExists(sKeyList As ArrayList, ValueToApply As String, Optional CreateAll As Boolean = False)
	Public Sub ApplyValueIfExists(sKeyList As List(Of String), ValueToApply As String, Optional CreateAll As Boolean = False)
		Dim sKey As String ', bFound As Boolean
		Try

			For Each sKey In sKeyList
				If sKey <> "" Then
					'If oFieldDataUsed.ContainsKey(sKey) Then
					'	oFieldDataUsed.Item(sKey) = ValueToApply
					'	bFound = True
					'ElseIf CreateAll Then
					'	oFieldDataUsed.Add(sKey, ValueToApply)
					'End If
					UpdStringSortedList(oFieldDataUsed, sKey, ValueToApply)

				End If
			Next

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKey <> "" Then oError.AddDetail("Field Key : " & sKey)
			gsErr = goErrs.ToString


		End Try

	End Sub

	Public Sub AddValuesToFieldData(aNewValues As SortedList(Of String, String))
		Dim sKey As String, sKeyUsed As String, sValue As String
		Try

			For Each sKey In aNewValues.Keys
				sKeyUsed = sKey
				If ConfigDetailsFromXML.aFieldTranslation.Count > 0 Then
					'If oConfigDetails IsNot Nothing AndAlso oConfigDetails.aFieldTranslation.Count > 0 Then
					If oConfigDetails.aFieldTranslation.ContainsKey(sKeyUsed) Then sKeyUsed = oConfigDetails.aFieldTranslation.Item(sKeyUsed)
				End If

				sValue = aNewValues.Item(sKey)
				UpdStringSortedList(oFieldDataUsed, sKey, sValue)
				If sKey <> sKeyUsed Then UpdStringSortedList(oFieldDataUsed, sKeyUsed, sValue)

				'If oFieldDataUsed.ContainsKey(sKeyUsed) Then
				'	oFieldDataUsed.Item(sKeyUsed) = sValue
				'Else
				'	oFieldDataUsed.Add(sKeyUsed, sValue)
				'End If
			Next

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKey <> "" Then oError.AddDetail("Field Key : " & sKey)
			gsErr = goErrs.ToString


		End Try
	End Sub


	Protected Friend Function StoreDebugValues() As String
		Dim sReturn As String, sKey As String, sValue As String
		Dim sSavePath As String, sSaveFolder As String = "", sSaveFileName As String = ""
		Dim oOut As XmlDocument, oNode As XmlNode
		Dim oParentElem As XmlElement, oSectionElem As XmlElement, oChildElem As XmlElement
		Dim oAssocFile As clsAttachedFileData, oDoc As clsGeneratedDocumentForRequest
		Try

			If oValuesFromXML IsNot Nothing Then
				sSaveFileName = IO.Path.GetFileNameWithoutExtension(oValuesFromXML.FilePathOfXMLFile) & "_DEBUG.xml"

				sSavePath = GetValueIfExists("DestinationDocPath", True, clsValueSubstitutions.TargetModifier.ForFilePath, "")
				If sSavePath = "" Then
					sSaveFolder = ApplySubstitution(GetMySettingString("DefaultStoreFolderDest"), clsValueSubstitutions.TargetModifier.ForFilePath)
				Else
					sSaveFolder = IO.Path.GetDirectoryName(sSavePath)
				End If

				sSavePath = sSaveFolder & IIf(sSaveFolder.Substring(sSaveFolder.Length - 1, 1) = IO.Path.DirectorySeparatorChar, "", IO.Path.DirectorySeparatorChar).ToString() & sSaveFileName

				sReturn = sSavePath

				oOut = New XmlDocument()
				oParentElem = oOut.CreateElement("DebugValues")
				oOut.AppendChild(oParentElem)

				oSectionElem = oOut.CreateElement("PostedValues")
				For Each sKey In oValuesFromHTTPRequest.SubmittedDataValues.Keys
					sValue = oValuesFromHTTPRequest.SubmittedDataValues.Item(sKey)
					oChildElem = oOut.CreateElement(sKey)
					oChildElem.InnerText = sValue
					oSectionElem.AppendChild(oChildElem)
				Next
				oParentElem.AppendChild(oSectionElem)

				oSectionElem = oOut.CreateElement("ConfigValues")
				For Each sKey In ConfigDetailsFromXML.aXMLDataValues.Keys
					sValue = ConfigDetailsFromXML.aXMLDataValues.Item(sKey)
					oChildElem = oOut.CreateElement(sKey)
					oChildElem.InnerText = sValue
					oSectionElem.AppendChild(oChildElem)
				Next
				oParentElem.AppendChild(oSectionElem)

				oSectionElem = oOut.CreateElement("StoredValues")
				For Each sKey In oValuesFromXML.aXMLDataValues.Keys
					sValue = oValuesFromXML.aXMLDataValues.Item(sKey)
					oChildElem = oOut.CreateElement(sKey)
					oChildElem.InnerText = sValue
					oSectionElem.AppendChild(oChildElem)
				Next
				oParentElem.AppendChild(oSectionElem)

				If oAssociatedFiles IsNot Nothing Then
					oSectionElem = oOut.CreateElement("StoredFiles")
					For Each sKey In oAssociatedFiles.Keys
						oAssocFile = oAssociatedFiles(sKey)
						sValue = oAssocFile.SavedFilePath
						oChildElem = oOut.CreateElement(IO.Path.GetFileName(oAssocFile.SavedFilePath))
						oChildElem.InnerText = sValue
						oSectionElem.AppendChild(oChildElem)
					Next
					oParentElem.AppendChild(oSectionElem)
				End If

				If oRequestDocs IsNot Nothing Then
					oSectionElem = oOut.CreateElement("StoredDocuments")
					For Each oDoc In oRequestDocs
						sValue = oDoc.sStoredPath
						oChildElem = oOut.CreateElement(IO.Path.GetFileName(oDoc.sStoredPath))
						oChildElem.InnerText = sValue
						oSectionElem.AppendChild(oChildElem)
					Next
					oParentElem.AppendChild(oSectionElem)
				End If

				If oAssociatedFiles IsNot Nothing Then
					oSectionElem = oOut.CreateElement("AssociatedFiles")
					For Each sKey In oAssociatedFiles.Keys
						oAssocFile = oAssociatedFiles.Item(sKey)
						sValue = oAssocFile.SavedFilePath
						oChildElem = oOut.CreateElement(IO.Path.GetFileName(oAssocFile.FileName))
						oChildElem.InnerText = sValue
						oSectionElem.AppendChild(oChildElem)
					Next
					oParentElem.AppendChild(oSectionElem)
				End If


				oSectionElem = oOut.CreateElement("AllDataValues")
				For Each sKey In oFieldDataUsed.Keys
					If sKey <> "" Then
						sValue = oFieldDataUsed.Item(sKey)
						oChildElem = oOut.CreateElement(sKey)
						oChildElem.InnerText = sValue
						oSectionElem.AppendChild(oChildElem)
					End If
				Next
				oParentElem.AppendChild(oSectionElem)
				If Not (IO.Directory.Exists(sSaveFolder)) Then IO.Directory.CreateDirectory(sSaveFolder)
				oOut.Save(sSavePath)
				End If

		Catch ex As Exception
			Dim oError As wRuntimeError = goErrs.NewError(ex)
			If sKey <> "" Then oError.AddDetail("Field Key : " & sKey)
			If sValue <> "" Then oError.AddDetail("Value : " & sValue)
			gsErr = goErrs.ToString

		End Try
		Return sReturn
	End Function


End Class
