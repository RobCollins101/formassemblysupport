﻿Module basGlobalVars
	Public gsErr As String = ""
	Public gsApplicationPath As String = ""
	Public gsURLParentPath As String = ""

	Public gbInTest As Boolean = False
	Public gbResetReplacements As Boolean = False

	Private sErr As String
	Public goErrs As New wRuntimeErrors
	Public gsNextPage As String = ""

	'Public oCurrentData As clsCurrentData  ' a carrier of all data passed between session pages for quick reference

	Public MySession As HttpSessionState = HttpContext.Current.Session

	Public gaFieldLabelLists As clsFieldLabelLists

End Module
