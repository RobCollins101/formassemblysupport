﻿<html><head><title>Tandridge District Council forms submission</title><style>body {font-family: Calibri, Helvetica, sans-serif; font-weight: normal; vertical-align: top;  }  .Label {padding: 3px; border-style: solid; border-width: thin; border-color: #C0C0C0; font-size: smaller; width: 200px; overflow: hidden; display: inline-block;} .Data {padding: 3px; border-style: solid; border-width: thin; border-color: #C0C0C0; font-size: medium; text-align: justify; vertical-align: top; white-space: normal; width: 400px; height: 20px; overflow: hidden; display: inline-block;} </style></head><body><div id="TDCInfo"><h3>TDC Internal notification of {{FormDescription}} with submission ref [[SubmissionUID]]</h3>

[[if {{CustomerName}}]]Form filled by : [[CustomerName]]

[[end]][[if {{CustomerEMail}}]]email address : [[CustomerEMail]]
[[end]][[if {{CustomerPhone}}]]Telephone : [[CustomerPhone]]
[[end]]
Documents are stored at <a href='((DestDocDictionary))'>((DestDocDictionary))</a>
</body>
</html>
