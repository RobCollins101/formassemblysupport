﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace FormAssemblySupport
{
	public class Global : System.Web.HttpApplication
	{

		protected void Application_Start(object sender, EventArgs e)
		{

		}

		protected void Session_Start(object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{
			bool bChangeToHTTPS = false;
			string sProtocol = "http";
			string sHost = "";
			try
			{
				System.Configuration.Configuration rootWebConfig1 = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
				//ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
				SecurityProtocolType oTempProtocol;
				string sTempProtocol = (GetMySetting("TransferSecurityProtocol").ToString() == "" ? "Tls12" : GetMySetting("TransferSecurityProtocol").ToString());
				if (!Enum.TryParse(sTempProtocol, out oTempProtocol)) oTempProtocol = SecurityProtocolType.Tls12;
				ServicePointManager.SecurityProtocol = oTempProtocol;


				string fullOrigionalpath = Request.Url.ToString();
				string sLastPart = Request.Url.Segments.Last();
				int iLastNumber;
				string sNewURL;
				string sAllParams = Request.Url.Query;
				string currentWebSiteName = System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();
				string ApplicationAlias = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
				string ApplicationName = ApplicationAlias.Substring(1);

				bool bDoNotChange = false;
				string sLastOfPath = Request.Url.Segments.ElementAt<string>(Request.Url.Segments.Length - 1);

				if (Request.Url.Query.Length < 1 && sLastOfPath.Contains(".") && sLastOfPath.LastIndexOf(".") > sLastOfPath.Length - 8) bDoNotChange = true;

				if (!bDoNotChange)
				{

					sHost = Request.Url.Authority;
					sProtocol = Request.Url.Scheme;
					System.Collections.Specialized.StringCollection aForceHTTPS = (System.Collections.Specialized.StringCollection) GetMySetting("HostsForHTTPS");
					bChangeToHTTPS = (bool)GetMySetting("UseRedirectInGlobal");
					bChangeToHTTPS = (bChangeToHTTPS && sProtocol == "http" );

					string sToNewHost = sHost.ToLower();
					string sFromOldPath = sHost.ToLower();
					char sSplitChar = '|';
					if (bChangeToHTTPS)
					{
						foreach (string sTest in aForceHTTPS) {
							string[] aTest = sTest.Split(sSplitChar);
							string sTestHost = aTest[0];
							bChangeToHTTPS = (sTestHost.ToLower() == sHost.ToLower());

							if (bChangeToHTTPS) {
								sFromOldPath = aTest[1].ToLower();
								sToNewHost = aTest[2].ToLower();
								break;
							}
						}
					}

					var sNewPath = "";
					for (int iPos = 0; iPos < Request.Url.Segments.Length + (int.TryParse(sLastPart, out iLastNumber) ? -1 : 0); iPos++)
					{

						sNewPath += Request.Url.Segments[iPos];
					}
					if (sNewPath != "" && sNewPath.LastIndexOf("/") == sNewPath.Length - 1) sNewPath = sNewPath.Substring(0, sNewPath.Length - 1);

					sNewURL = sNewPath;

					if (!(sNewURL.Contains("/tfaBranded")) && sNewURL.Contains("/TDCWebAppsPublic/"))
					{
						sNewURL = sNewURL.Replace("/TDCWebAppsPublic/", "/TDCWebAppsPublic/" + currentWebSiteName + "/");
					}

					if (!sNewURL.Contains(".aspx")) sNewURL = sNewURL + (sNewURL != "" && sNewURL.Substring(sNewURL.Length - 1, 1) == "/" ? "" : "/") + "tfaBrandedCodeBehind.aspx";

					if (bChangeToHTTPS)
					{
						sNewURL = Request.Url.AbsoluteUri.ToLower().Replace("http://" + sFromOldPath, "https://" + sToNewHost);
						Response.Redirect(sNewURL);
						//Server.Transfer(sNewURL,true);
					}
					else if (int.TryParse(sLastPart, out iLastNumber))
					{
						//var sNewPath = "";
						//for (int iPos = 0; iPos < Request.Url.Segments.Length - 1; iPos++)
						//{
						//    sNewPath += Request.Url.Segments[iPos];
						//}
						//if (sNewPath != "" && sNewPath.LastIndexOf("/") == sNewPath.Length - 1) sNewPath = sNewPath.Substring(0, sNewPath.Length - 1);

						//sNewURL = sNewPath;
						//if (!sNewURL.Contains(".aspx")) sNewURL = sNewURL + "/tfaBrandedCodeBehind.aspx";
						//if (!(sNewURL.Contains("/tfaBranded")) && sNewURL.Contains("/TDCWebAppsPublic/"))
						//{
						//    sNewURL = sNewURL.Replace("/TDCWebAppsPublic/", "/TDCWebAppsPublic/" + currentWebSiteName + "/");
						//}
						sNewURL = sNewURL + "?FormID=" + sLastPart;
						if (sAllParams != "") sNewURL += "&" + sAllParams.Substring(1);
						//Context.RewritePath(sNewURL);

						HttpContext.Current.RewritePath(sNewURL);
					}
					else if (!fullOrigionalpath.Contains(".aspx"))
					{
						//var sNewPath = "";
						//for (int iPos = 0; iPos < Request.Url.Segments.Length - 1; iPos++)
						//{
						//    sNewPath += Request.Url.Segments[iPos];
						//}
						//if (sNewPath != "" && sNewPath.LastIndexOf("/") == sNewPath.Length - 1) sNewPath = sNewPath.Substring(0, sNewPath.Length - 1);

						//sNewURL = sNewPath;
						//sNewURL = sNewURL + "/tfaBrandedCodeBehind.aspx";
						if (sAllParams != "") sNewURL += "?" + sAllParams.Substring(1);
						//Context.RewritePath(sNewURL);
						HttpContext.Current.RewritePath(sNewURL);

					}
				}
			}
			catch (Exception ex)
			{

			}


		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{

		}

		protected void Application_Error(object sender, EventArgs e)
		{

		}

		protected void Session_End(object sender, EventArgs e)
		{

		}

		protected void Application_End(object sender, EventArgs e)
		{

		}


		protected object GetMySetting(string SettingName)
		{
			object oReturn = "";
			System.Configuration.Configuration rootWebConfig1 =
				 System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
			try
			{
				// See http://msdn.microsoft.com/en-us/library/ms178688%28v=vs.100%29.aspx
				// and http://forums.asp.net/t/1124253.aspx
				// These relate to the web.config section 	<configuration><appSettings>

				oReturn = Properties.Settings.Default[SettingName] ?? null;
				if (oReturn == null && rootWebConfig1.AppSettings.Settings.AllKeys.Contains(SettingName)) oReturn = rootWebConfig1.AppSettings.Settings[SettingName].Value;
				if (oReturn == null) oReturn = "";

			}
			catch
			{

			}
			return oReturn;
		}

	}
}