﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FormAssemblySupport
{
    public partial class TestForm20MultipageBranded : System.Web.UI.Page
    {
        public String sHTMLFormHeadPart = "";
        public String sHTMLFormBodyPart = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            {
                WebRequest request;
                string TempForm = "";
                string InBodyPart = "";
                string InHeadPart = "";
                Regex oRegex;
                Match oMatch;
                GroupCollection oMatchGroups;
                Group oMatchGroup;
                if (Request.QueryString["tfa_next"] == null)
                {
                    request = WebRequest.Create("https://tandridge.tfaforms.net/rest/forms/view/20");
                }
                else
                {
                    request = WebRequest.Create("https://tandridge.tfaforms.net/rest/forms/view/20" + Request.QueryString["tfa_next"]);
                }
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream, Encoding.UTF8);
                TempForm = reader.ReadToEnd();
                reader.Close();
                response.Close();

                //oRegex = new Regex("^.*(?<HTMLTag><HTML[^>]*>)?(<head[^>]*>(?<HeadPart>.*)</head>)?(<Body[^>]*>)?(?<BodyPart>.*)(</Body>)?$");

                oRegex = new Regex("<head[^>]*>(?<HeadContent>.*)</head>", RegexOptions.IgnoreCase);
                oMatch = oRegex.Match(TempForm);
                oMatchGroups = oMatch.Groups;
                oMatchGroup = oMatchGroups["HeadContent"];
                if (oMatchGroup.Success) InHeadPart = oMatchGroup.Value;
                else if(TempForm.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase) > 0)
                {

                    InHeadPart = TempForm;
                    if (InHeadPart.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase) > 0)
                    {
                        InHeadPart = InHeadPart.Substring(InHeadPart.IndexOf(">", InHeadPart.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase)) + 1);
                        if (InHeadPart.IndexOf("</head", StringComparison.InvariantCultureIgnoreCase) > 0)
                        {
                            InHeadPart = InHeadPart.Substring(0, InHeadPart.IndexOf("</head", StringComparison.InvariantCultureIgnoreCase));
                        }
                    }

                };

                oRegex = new Regex("(<body[^>]*>)(?<BodyContent>.*)(</body>)", RegexOptions.IgnoreCase);
                oMatchGroup = oMatchGroups["BodyContent"];
                if (oMatchGroup.Success) InBodyPart = oMatchGroup.Value;
                else
                {
                    InBodyPart = TempForm;
                    if (InBodyPart.IndexOf("<body", StringComparison.InvariantCultureIgnoreCase) > 0)
                    {
                        InBodyPart = InBodyPart.Substring(InBodyPart.IndexOf(">", InBodyPart.IndexOf("<body", StringComparison.InvariantCultureIgnoreCase))+1);
                        if (InBodyPart.IndexOf("</body", StringComparison.InvariantCultureIgnoreCase) > 0)
                        {
                            InBodyPart = InBodyPart.Substring(0, InBodyPart.IndexOf("</body", StringComparison.InvariantCultureIgnoreCase));
                        }
                    }
                }

                sHTMLFormHeadPart = InHeadPart;
                sHTMLFormBodyPart = InBodyPart;
                //FAForm.Text = InBodyPart;
                //FAHeadForm.Text = InHeadPart;

            }
        }
    }
}