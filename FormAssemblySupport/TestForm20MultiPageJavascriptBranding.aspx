﻿<%@ Import Namespace='System' %>
<%@ Import Namespace='System.IO' %>
<%@ Import Namespace='System.Net' %>
<%@ Import Namespace='System.Text' %>
<!DOCTYPE html>
<html>
<head>
    <script language='C#' runat='server'>
    void Page_Load(Object Src, EventArgs E)
    {
        WebRequest request;
        if (Request.QueryString["tfa_next"] == null)
        {
            request = WebRequest.Create("https://tandridge.tfaforms.net/rest/forms/view/20");
        }
        else
        {
            request = WebRequest.Create("https://tandridge.tfaforms.net/rest/forms/view/20" + Request.QueryString["tfa_next"]);
        }
        WebResponse response = request.GetResponse();
        Stream dataStream = response.GetResponseStream();
        StreamReader reader = new StreamReader(dataStream, Encoding.UTF8);
        FAForm.InnerHtml = reader.ReadToEnd();
        reader.Close();
        response.Close();
    }
    </script>

    <!-- ==========================================================================
        HTML HEAD component to manage TDC functions import
   ========================================================================== -->

    <script type="text/javascript">
        //<![CDATA[
        if (typeof loadJsLibrary === 'undefined') {
            var script = document.createElement('script');
            script.src = '//tdcws01.tandridge.gov.uk/PublicFiles/TDCJsModules/LLPGAddressSearch/TDCLoadJsLibraryFunction.js'; // Location of the >LoadJsLibrary script
            document.write(script.outerHTML);
        }
        //]]>
    </script>

    <!-- ==========================================================================
        Javascript components for JQuery
========================================================================== -->

    <!--<script type="text/javascript">if (typeof jQuery === 'undefined') loadJsLibrary('//tdcws01.tandridge.gov.uk/PublicFiles/JQueryVersions/Versions1.x/jquery-1.12.4.js', null);</script>-->
    <script type="text/javascript">if (typeof jQuery === 'undefined') loadJsLibrary('//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js', null);</script>

    <!-- ==========================================================================
        Javascript components for JQuery UI. See http://jqueryui.com/
========================================================================== -->

    <link rel="stylesheet" href="//tdcws01.tandridge.gov.uk/PublicFiles/JQueryUIVersions/jquery-ui-themes-1.12.0/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript">if (typeof jQuery.ui === 'undefined') loadJsLibrary('//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js', null);</script>


    <!-- ==========================================================================
        Javascript components for TDC Branding placement
        This assumes JQuery has been loaded up
========================================================================== -->

    <script src="//tdcws01.tandridge.gov.uk/PublicFiles/TDCJsModules/TDCBrandingPlaced/TDCLoadBranding.js" type="text/javascript"></script>
    <!-- ==========================================================================
        Javascript components for TDC postcode search
        This assumes JQuery has been loaded up
========================================================================== -->

    <link rel="stylesheet" type="text/css" href="//tdcws01.tandridge.gov.uk/PublicFiles/TDCJsModules/LLPGAddressSearch/TDCAddrSearchItems.css" media="all" />
    <script src="//tdcws01.tandridge.gov.uk/PublicFiles/TDCJsModules/LLPGAddressSearch/TDCPostcodeAddrSearch.js" type="text/javascript"></script>

    <!-- ==========================================================================
          End of HTML Import component to manage JQuery, JQuery.UI and TDC functions import
   ========================================================================== -->

</head>
<body>
    <h2>TestForm1SupportForm.aspx</h2>
    <h2>Title on page</h2>
    <p>
        Description of page<br />
    </p>

    <span id='FAForm' runat='server' />
</body>
</html>


