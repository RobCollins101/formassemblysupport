﻿<%@ Import Namespace='System' %>
<%@ Import Namespace='System.IO' %>
<%@ Import Namespace='System.Net' %>
<%@ Import Namespace='System.Text' %>
<!DOCTYPE html>
<html>
<script language='C#' runat='server'>
    void Page_Load(Object Src, EventArgs E)
    {
        WebRequest request;
        if (Request.QueryString["tfa_next"] == null)
        {
            request = WebRequest.Create("https://tandridge.tfaforms.net/rest/forms/view/20");
        }
        else
        {
            request = WebRequest.Create("https://tandridge.tfaforms.net/rest/forms/view/20" + Request.QueryString["tfa_next"]);
        }
        WebResponse response = request.GetResponse();
        Stream dataStream = response.GetResponseStream();
        StreamReader reader = new StreamReader(dataStream, Encoding.UTF8);
        FAForm.InnerHtml = reader.ReadToEnd();
        reader.Close();
        response.Close();
    }
</script>
<body>
    <h2>TestForm1SupportForm.aspx</h2>
		<h2>Title on page</h2>
		<p>
			Description of page<br />
		</p>

    <span id='FAForm' runat='server' />
</body>
</html>


