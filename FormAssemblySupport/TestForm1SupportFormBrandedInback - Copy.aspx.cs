﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FormAssemblySupport
{
    public partial class TestForm1SupportFormBrandedInback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            {
                string TempForm = "";
                string InBodyPart = "";
                string InHeadPart = "";
                Regex oRegex;
                Match oMatch;
                GroupCollection oMatchGroups;
                Group oMatchGroup;
                TempForm = GetDataFromURL("http://tandridge.tfaforms.net", true);

                //oRegex = new Regex("^.*(?<HTMLTag><HTML[^>]*>)?(<head[^>]*>(?<HeadPart>.*)</head>)?(<Body[^>]*>)?(?<BodyPart>.*)(</Body>)?$");

                oRegex = new Regex("<head[^>]*>(?<HeadContent>.*)</head>", RegexOptions.IgnoreCase);
                oMatch = oRegex.Match(TempForm);
                oMatchGroups = oMatch.Groups;
                oMatchGroup = oMatchGroups["HeadContent"];
                if (oMatchGroup.Success) InHeadPart = oMatchGroup.Value;
                else if(TempForm.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase) > 0)
                {

                    InHeadPart = TempForm;
                    if (InHeadPart.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase) > 0)
                    {
                        InHeadPart = InHeadPart.Substring(InHeadPart.IndexOf(">", InHeadPart.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase)) + 1);
                        if (InHeadPart.IndexOf("</head", StringComparison.InvariantCultureIgnoreCase) > 0)
                        {
                            InHeadPart = InHeadPart.Substring(0, InHeadPart.IndexOf("</head", StringComparison.InvariantCultureIgnoreCase));
                        }
                    }

                };

                oRegex = new Regex("(<body[^>]*>)(?<BodyContent>.*)(</body>)", RegexOptions.IgnoreCase);
                oMatchGroup = oMatchGroups["BodyContent"];
                if (oMatchGroup.Success) InBodyPart = oMatchGroup.Value;
                else
                {
                    InBodyPart = TempForm;
                    if (InBodyPart.IndexOf("<body", StringComparison.InvariantCultureIgnoreCase) > 0)
                    {
                        InBodyPart = InBodyPart.Substring(InBodyPart.IndexOf(">", InBodyPart.IndexOf("<body", StringComparison.InvariantCultureIgnoreCase))+1);
                        if (InBodyPart.IndexOf("</body", StringComparison.InvariantCultureIgnoreCase) > 0)
                        {
                            InBodyPart = InBodyPart.Substring(0, InBodyPart.IndexOf("</body", StringComparison.InvariantCultureIgnoreCase));
                        }
                    }
                }

                litAchieveFormHead.Text = InHeadPart;
                litAchieveFormBody.Text = InBodyPart;
                //FAForm.Text = InBodyPart;
                //FAHeadForm.Text = InHeadPart;

                string brandingPath = "https://tdcws01.tandridge.gov.uk/TDCWebAppsPublic/BrandingSplit2015/";
                string brandingPathAlt = brandingPath + "StaticBranding/";


                litBrandInHeader.Text = GetDataFromURL(brandingPath + "Branding1HeadTagPartSansScripts.aspx", false);
                if (litBrandInHeader.Text == "") litBranding2BodyTag.Text = GetDataFromURL(brandingPathAlt + "Branding1HeadTagPartSansScripts.htm", false);

                litBranding2BodyTag.Text = GetDataFromURL(brandingPath + "Branding2BodyTagStartSansScripts.aspx", false);
                if (litBranding2BodyTag.Text == "" ) litBranding2BodyTag.Text = GetDataFromURL(brandingPathAlt + "Branding2BodyTagStartSansScripts.htm", false);

                litBranding3PreMenu.Text = GetDataFromURL(brandingPath + "Branding3_AllBodyPreMenuSansScripts.aspx", false);
                if (litBranding3PreMenu.Text == "") litBranding3PreMenu.Text = GetDataFromURL(brandingPathAlt + "Branding3_AllBodyPreMenuSansScripts.htm", false);

                litBranding4LHMenu.Text = GetDataFromURL(brandingPath + "Branding4BodyLHMenuPart.aspx", false);
                if (litBranding4LHMenu.Text == "") litBranding4LHMenu.Text = GetDataFromURL(brandingPathAlt + "Branding4BodyLHMenuPart.aspx", false);

                litBranding5Footer.Text = GetDataFromURL(brandingPath + "Branding5FootPartSansScripts.aspx", false);
                if (litBranding5Footer.Text == "") litBranding5Footer.Text = GetDataFromURL(brandingPathAlt + "Branding5FootPartSansScripts.htm", false);

            }
        }
        protected string GetDataFromURL(string sURLToLoad, bool bApplyQueryParams)
        {
            string sReturn = "";
            string sURLToUse = sURLToLoad;
            string sQueryParams = "", sFormID = "";
            WebRequest requestFromURL;
            
            if (bApplyQueryParams && Request.QueryString.Count > 0)
            {
                foreach (string sPos in Request.QueryString.Keys)
                {
                    if (sPos == "tfaFormID" || sPos == "FormID")
                    {
                        sFormID = "/" + Request.QueryString[sPos];
                    }
                    else
                    {
                        sQueryParams += (sQueryParams == "" ? "?" : "&") + sPos + "=" + Request.QueryString[sPos];
                    }
                }
            }

            sURLToUse += sFormID + sQueryParams;
            requestFromURL = WebRequest.Create(sURLToUse);

            WebResponse responseFromURL = requestFromURL.GetResponse();
            Stream dataStreamOfContent = responseFromURL.GetResponseStream();
            StreamReader readerOfContent = new StreamReader(dataStreamOfContent, Encoding.UTF8);

            sReturn = readerOfContent.ReadToEnd();
            readerOfContent.Close();
            responseFromURL.Close();

            return sReturn;
        }
    }
}