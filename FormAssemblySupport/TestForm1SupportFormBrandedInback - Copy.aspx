<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestForm1SupportFormBrandedInback.aspx.cs" Inherits="FormAssemblySupport.TestForm1SupportFormBrandedInback" %>
<!DOCTYPE html>
<html>
<head>
	<title>Tandridge District Council Base JQuery forms page</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- see http://msdn.microsoft.com/en-us/library/ff955275%28v=vs.85%29.aspx -->
	<!-- VERY IMPORTANT for IE mode -->
	<!-- http://www.metatags.info/all_meta_tags -->
	<meta name="KEYWORDS" content="Tandridge District Council Content branding page services" />
	<meta name="description" content="Tandridge District Council content in a branding page." />
	<meta name="revisit-after" content="5 days" />
	<meta name="distribution" content="GLOBAL" />
	<meta name="author" content="Rob Collins ICT" />
	<meta name="copyright" content="Tandridge District Council 2015" />
	<meta name="googlebot" content="noodp" />
	<meta name="language" content="english" />
	<meta name="reply-to" content="CustomerService@Tandridge.gov.uk" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1 " />
 	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta name="ROBOTS" content="index, nofollow" />
	<meta name="rating" content="General" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
	</script>

	<script type="text/javascript">
	var g_sFormName = "SomeFormName v1";
	var g_thisDate = new Date();
	var sDate = g_thisDate.toDateString;
	var g_sTypeService = "";
	if (/(dev|localhost)/i.test(location.pathname)) g_sTypeService = "Test"; //"Dev";
	if (g_sTypeService == "" && /test/i.test(location.pathname)) g_sTypeService = "Test";
	//if (g_sTypeService == "" ) g_sTypeService = "Live";

	</script>

    <asp:Literal ID="litBrandInHeader" runat="server"></asp:Literal>
 
	<script type="text/javascript">
		//<![CDATA[
		//document.write(fnExtractFromURL(sURLBrandingLoc + "Branding1HeadTagPartSansScripts.aspx",sAlternativeRoot + "Branding1HeadTagPartSansScripts.htm"));
		//]]>
	</script>
    
	<%--<link rel="stylesheet" type="text/css" href="./TDCCSS/TDCCSSStyle.css" media="all"/>--%>

    <%--<asp:Label id="FAHeadForm" runat="server" Text=""></asp:Label>--%>
    <asp:Literal ID="litAchieveFormHead" runat="server"></asp:Literal>

</head>
<body class="wrapper">
	<div id="AfterBranding2Body"></div>
    <asp:Literal ID="litBranding2BodyTag" runat="server"></asp:Literal>

	<script type="text/javascript">
		//<![CDATA[
		//fnExtractFromURL(sURLBrandingLoc + "Branding2BodyTagStartSansScripts.aspx",sAlternativeRoot + "Branding2BodyTagStartSansScripts.htm", "");
		//]]>
	</script>
	<div id="AfterBranding3PreMenu"></div>
        <asp:Literal ID="litBranding3PreMenu" runat="server"></asp:Literal>

	<script type="text/javascript">
		//<![CDATA[
		//fnExtractFromURL(sURLBrandingLoc + "Branding3_AllBodyPreMenuSansScripts.aspx",sAlternativeRoot + "Branding3_AllBodyPreMenuSansScripts.htm", "");
		//]]>
	</script>
	<div id="AfterBranding4BodyLHMenu"></div>
        <asp:Literal ID="litBranding4LHMenu" runat="server"></asp:Literal>

	<script type="text/javascript">
		//<![CDATA[
		//fnExtractFromURL(sURLBrandingLoc + "Branding4BodyLHMenuPart.aspx",sAlternativeRoot + "Branding4BodyLHMenuPart.htm", "AfterBranding4BodyLHMenu");
		//]]>
	</script>


	<!-- ==========================================================================-->
	<!-- Here is where a designer would place the body of their content -->
	<!-- ==========================================================================-->

	<div id="TDCInfo">
		<h2>TestForm1SupportFormBranded.aspx</h2>
		<h2>Title on page</h2>
		<p>
			Description of page<br />
		</p>


        
        <div style="clear:both;">Stuff before</div>

        <%--<asp:label id="FAForm"  runat="server" text="Label" style="clear:both;"></asp:label>--%>
        <asp:Literal ID="litAchieveFormBody" runat="server"></asp:Literal>
        

        <div style="clear:both;">Stuff After</div>


	</div>


	<!-- ==========================================================================-->
	<!-- End of body content -->
	<!-- ==========================================================================-->


	<div id="AfterBranding5"></div>
    <asp:Literal ID="litBranding5Footer" runat="server"></asp:Literal>

	<!--<script type="text/javascript">
		//<![CDATA[
		fnExtractFromURL(sURLBrandingLoc + "Branding5FootPartSansScripts.aspx",sAlternativeRoot + "Branding5FootPartSansScripts.htm", "", "");
		//]]>
	</script> -->

</body>
</html>
