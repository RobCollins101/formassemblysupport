﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FormAssemblySupport
{
	public partial class tfaBrandedCodeBehind : System.Web.UI.Page
	{
		bool bFailedStream = false;
		string sTargetFullURL;
		protected void Page_Load(object sender, EventArgs e)
		{
			{
				System.Configuration.Configuration rootWebConfig1 = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
				//ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
				//ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
				SecurityProtocolType oTempProtocol;
				string sTempProtocol = (GetMySetting("TransferSecurityProtocol").ToString() == "" ? "Tls12" : GetMySetting("TransferSecurityProtocol").ToString());
				if (!Enum.TryParse(sTempProtocol, out oTempProtocol)) oTempProtocol = SecurityProtocolType.Tls12;
				ServicePointManager.SecurityProtocol = oTempProtocol;

				object oTemp;
				Uri oTargetURI;

				string TempForm = "";
				string InBodyPart = "";
				string InHeadPart = "";
				string sTempURL = "https://tandridge.tfaforms.net";
				bool bAllowRedirect = bool.Parse(GetMySetting("AllowEmergencyRedirect").ToString());
				List<string> aHosts = new List<string>();
				aHosts.Add("tandridge\\.tfaforms\\.net");
				aHosts.Add(".*\\.tandridge\\.gov\\.uk");

				oTemp = GetMySetting("tfaFormURL"); if (oTemp != null) sTempURL = (string)oTemp;
				oTemp = GetMySetting("URLHostWhitelist"); if (oTemp != null)
				{
					System.Collections.Specialized.StringCollection oArr = (System.Collections.Specialized.StringCollection)oTemp;
					aHosts = oArr.Cast<string>().ToList();
				}
				//if (rootWebConfig1.AppSettings.Settings.AllKeys.Contains("tfaFormURL")) sTempURL = rootWebConfig1.AppSettings.Settings["tfaFormURL"].Value;
				//if (rootWebConfig1.AppSettings.Settings.AllKeys.Contains("URLHostWhitelist")) aHosts = rootWebConfig1.AppSettings.Settings["URLHostWhitelist"].Value.Cast<string>().ToList<string>();

				Regex oRegex;
				Match oMatch;
				GroupCollection oMatchGroups;
				Group oMatchGroup;
				if (Request.QueryString["GetURL"] != null || Request.QueryString["GETURL"] != null || Request.QueryString["geturl"] != null) sTempURL = Request.QueryString["GetURL"];
				if (Request.QueryString["TargetURL"] != null || Request.QueryString["TARGETURL"] != null || Request.QueryString["targeturl"] != null) sTempURL = Request.QueryString["TargetURL"];
				if (Request.QueryString["FormURL"] != null || Request.QueryString["FORMURL"] != null || Request.QueryString["formurl"] != null) sTempURL = Request.QueryString["FormURL"];
				if (Request.QueryString["SourceURL"] != null || Request.QueryString["SOURCEURL"] != null || Request.QueryString["sourceurl"] != null) sTempURL = Request.QueryString["SourceURL"];
				if (Request.QueryString["Url"] != null || Request.QueryString["url"] != null) sTempURL = Request.QueryString["URL"];
				if (Request.QueryString["test"] != null || Request.QueryString["Test"] != null || Request.QueryString["TEST"] != null) bAllowRedirect = false;
				if (Request.QueryString["NoRedirect"] != null || Request.QueryString["noredirect"] != null || Request.QueryString["NOREDIRECT"] != null) bAllowRedirect = false;

				bool bValidHost = false;
				if (sTempURL != "")
				{
					if (sTempURL.Substring(0, 1) == "/")
					{
						sTempURL = Request.Url.Scheme + ":" + sTempURL;
					}
					if (sTempURL.Substring(0, 4).ToLower() != "http")
					{
						if (sTempURL.IndexOf("/") < 0 || sTempURL.IndexOf("/") > 4) sTempURL = "//" + sTempURL;
						sTempURL = Request.Url.Scheme + ":" + sTempURL;
					}
					oTargetURI = new Uri(sTempURL);
					foreach (string sRegex in aHosts)
					{
						if (Regex.IsMatch(oTargetURI.Host, sRegex, RegexOptions.IgnoreCase))
						{
							bValidHost = true;
							break;
						}
					}
				}
				if (bValidHost && Request.QueryString.Count > 0)
				{

					TempForm = GetDataFromURL(sTempURL, true);

					oRegex = new Regex("<head[^>]*>(?<HeadContent>.*)</head>", RegexOptions.IgnoreCase);
					oMatch = oRegex.Match(TempForm);
					oMatchGroups = oMatch.Groups;
					oMatchGroup = oMatchGroups["HeadContent"];
					if (oMatchGroup.Success) InHeadPart = oMatchGroup.Value;
					else if (TempForm.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase) > 0)
					{

						InHeadPart = TempForm;
						if (InHeadPart.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase) > 0)
						{
							InHeadPart = InHeadPart.Substring(InHeadPart.IndexOf(">", InHeadPart.IndexOf("<head", StringComparison.InvariantCultureIgnoreCase)) + 1);
							if (InHeadPart.IndexOf("</head", StringComparison.InvariantCultureIgnoreCase) > 0)
							{
								InHeadPart = InHeadPart.Substring(0, InHeadPart.IndexOf("</head", StringComparison.InvariantCultureIgnoreCase));
							}
						}

					};

					oRegex = new Regex("(<body[^>]*>)(?<BodyContent>.*)(</body>)", RegexOptions.IgnoreCase);
					oMatchGroup = oMatchGroups["BodyContent"];
					if (oMatchGroup.Success) InBodyPart = oMatchGroup.Value;
					else
					{
						InBodyPart = TempForm;
						if (InBodyPart.IndexOf("<body", StringComparison.InvariantCultureIgnoreCase) > 0)
						{
							InBodyPart = InBodyPart.Substring(InBodyPart.IndexOf(">", InBodyPart.IndexOf("<body", StringComparison.InvariantCultureIgnoreCase)) + 1);
							if (InBodyPart.IndexOf("</body", StringComparison.InvariantCultureIgnoreCase) > 0)
							{
								InBodyPart = InBodyPart.Substring(0, InBodyPart.IndexOf("</body", StringComparison.InvariantCultureIgnoreCase));
							}
						}
					}

					litAchieveFormHead.Text = InHeadPart;
					litAchieveFormBody.Text = InBodyPart;
					//FAForm.Text = InBodyPart;
					//FAHeadForm.Text = InHeadPart;

					if (bFailedStream && bAllowRedirect)
					{
						Response.Redirect(sTargetFullURL);
					}

				}
				string brandingPath = GetMySetting("BrandingURL").ToString(); //  "https://tdcws01.tandridge.gov.uk/TDCWebAppsPublic/BrandingSplit2015/";
				if ((Request.Url.AbsoluteUri.ToString().ToLower().Contains("test") || Request.Url.AbsoluteUri.ToString().ToLower().Contains("localhost")) && StringIsTrue(GetMySetting("InTest")))
				{
					brandingPath = brandingPath.LastIndexOf("/") + 1 == brandingPath.Length ? brandingPath.Substring(0, brandingPath.Length - 1) + "Test/" : brandingPath + "Test";
					//brandingPath += "Test/";

				}
				string brandingPathAlt = brandingPath + "StaticBranding/";

				litBrandInHeader.Text = GetDataFromURL(brandingPath + "Branding1HeadTagPartSansScripts.aspx", false);
				if (litBrandInHeader.Text == "") litBranding2BodyTag.Text = GetDataFromURL(brandingPathAlt + "Branding1HeadTagPartSansScripts.htm", false);

				litBranding2BodyTag.Text = GetDataFromURL(brandingPath + "Branding2BodyTagStartSansScripts.aspx", false);
				if (litBranding2BodyTag.Text == "") litBranding2BodyTag.Text = GetDataFromURL(brandingPathAlt + "Branding2BodyTagStartSansScripts.htm", false);

				litBranding3PreMenu.Text = GetDataFromURL(brandingPath + "Branding3_AllBodyPreMenuSansScripts.aspx", false);
				if (litBranding3PreMenu.Text == "") litBranding3PreMenu.Text = GetDataFromURL(brandingPathAlt + "Branding3_AllBodyPreMenuSansScripts.htm", false);

				litBranding4LHMenu.Text = GetDataFromURL(brandingPath + "Branding4BodyLHMenuPart.aspx", false);
				if (litBranding4LHMenu.Text == "") litBranding4LHMenu.Text = GetDataFromURL(brandingPathAlt + "Branding4BodyLHMenuPart.aspx", false);

				litBranding5Footer.Text = GetDataFromURL(brandingPath + "Branding5FootPartSansScripts.aspx", false);
				if (litBranding5Footer.Text == "") litBranding5Footer.Text = GetDataFromURL(brandingPathAlt + "Branding5FootPartSansScripts.htm", false);

			}
		}
		protected string GetDataFromURL(string sURLToLoad, bool bApplyQueryParams)
		{
			string sReturn = "";
			string sURLToUse = sURLToLoad;
			string sQueryParams = "", sFormID = "";
			WebRequest requestFromURL;

			if (bApplyQueryParams && Request.QueryString.Count > 0)
			{
				string sPos = "";
				string sVal = "";
				int iVal;
				//foreach (string sPos in Request.QueryString.Keys)
				for (int iPos = 0; iPos < Request.QueryString.Keys.Count; iPos++)
				{
					sPos = Request.QueryString.Keys[iPos];
					sVal = Request.QueryString[sPos];
					{
						if (sPos == null && sVal != null && sVal != "" && int.TryParse(sVal, out iVal)) sPos = "formid";
						if (sPos.ToUpper() != "GETURL" && sPos.ToUpper() != "TARGETURL" && sPos.ToUpper() != "FORMURL" && sPos.ToUpper() != "SOURCEURL" && sPos.ToUpper() != "URL")
						{
							if (sPos.ToLower() == "tfaformid" || sPos.ToLower() == "formid")
							{
								sFormID = "/" + sVal;
							}
							else
							{
								if (sVal == null)
								{
									sQueryParams += (sQueryParams == "" ? "?" : "&") + sPos;

								}
								else
								{
									sQueryParams += (sQueryParams == "" ? "?" : "&") + sPos + "=" + sVal;
								}
							}
						}
					}
				}
			}


			sURLToUse += sFormID + sQueryParams;
			requestFromURL = WebRequest.Create(sURLToUse);
			Uri oPath = new Uri(sURLToLoad);

			sTargetFullURL = sURLToUse;

			if (bApplyQueryParams && Request.Form.Count > 0)
			{
				string postFormData = "";
				foreach (string sPos in Request.Form.Keys)
				{
					postFormData += (sQueryParams == "" ? "" : "&") + sPos + "=" + Request.Form[sPos];
				}

				requestFromURL.Method = "POST";
				requestFromURL.ContentLength = postFormData.Length;
				using (var dataStream = requestFromURL.GetRequestStream())
				{
					byte[] postData = Encoding.ASCII.GetBytes(postFormData);
					dataStream.Write(postData, 0, postData.Length);
				}
			}
			try
			{

				WebResponse responseFromURL = requestFromURL.GetResponse();
				Stream dataStreamOfContent = responseFromURL.GetResponseStream();
				StreamReader readerOfContent = new StreamReader(dataStreamOfContent, Encoding.UTF8);

				sReturn = readerOfContent.ReadToEnd();
				readerOfContent.Close();
				responseFromURL.Close();

				//string sRegexMatch = @"(?<PreLocal>\s(href|src|srcset)\s?=\s?[""'])(?<RelativeLocal>\/?)(?<PostGroup>[^/])";
				string sRegexMatch = @"(?<PreLocal>\s(href|src|srcset)\s?=\s?[""'])(?<RelativeLocal>\/)(?<PostGroup>(?!data:|http|ftp|mail|irc|\/))";

				string sReplacement = oPath.Host; // + oPath.AbsolutePath;
				int iPos = sReplacement.LastIndexOf("/");
				if (iPos > 7 && iPos < sReplacement.Length) sReplacement = sReplacement.Substring(0, sReplacement.LastIndexOf("/"));

				sReplacement = "${PreLocal}//" + sReplacement + "${RelativeLocal}${PostGroup}";
				string sReturn2 = Regex.Replace(sReturn, sRegexMatch, sReplacement, RegexOptions.IgnoreCase);

				sRegexMatch = @"(?<PreLocal>\s(href|src|srcset)\s?=\s?[""'])(?<PostGroup>(?!data:|http|ftp|mail|irc|\/))";
				sReplacement = oPath.Host + oPath.AbsolutePath;
				iPos = sReplacement.LastIndexOf("/");
				if (iPos > 7 && iPos < sReplacement.Length - 1) sReplacement = sReplacement.Substring(0, sReplacement.LastIndexOf("/"));

				sReplacement = "${PreLocal}//" + sReplacement + "${PostGroup}";
				sReturn = Regex.Replace(sReturn2, sRegexMatch, sReplacement, RegexOptions.IgnoreCase);
			}
			catch (Exception e)
			{
				bFailedStream = true;
			}

			return sReturn;
		}


		protected object GetMySetting(string SettingName)
		{
			object oReturn = "";
			System.Configuration.Configuration rootWebConfig1 =
				 System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
			try
			{
				// See http://msdn.microsoft.com/en-us/library/ms178688%28v=vs.100%29.aspx
				// and http://forums.asp.net/t/1124253.aspx
				// These relate to the web.config section 	<configuration><appSettings>

				oReturn = Properties.Settings.Default[SettingName] ?? null;
				if (oReturn == null && rootWebConfig1.AppSettings.Settings.AllKeys.Contains(SettingName)) oReturn = rootWebConfig1.AppSettings.Settings[SettingName].Value;
				if (oReturn == null) oReturn = "";

			}
			catch
			{

			}
			return oReturn;
		}


		protected bool StringIsTrue(object TestValue)
		{
			bool bReturn = false;
			double NumVal = 0;
			string strVal = "";

			if (!(TestValue == null))
			{
				if (double.TryParse(TestValue.ToString(), out NumVal))
				{
					bReturn = (NumVal != 0);
				}
				else
				{
					strVal = TestValue.ToString();
					bReturn = (strVal == "True" || strVal == "T" || strVal == "Yes" || strVal == "Y");
				}
			}
			return bReturn;
		}



	}
}