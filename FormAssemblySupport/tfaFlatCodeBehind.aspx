<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tfaFlatCodeBehind.aspx.cs" Inherits="FormAssemblySupport.tfaFlatCodeBehind" %>

<!DOCTYPE html>
<html>
<head>
    <title>Tandridge District Council Forms</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- see http://msdn.microsoft.com/en-us/library/ff955275%28v=vs.85%29.aspx -->
    <!-- VERY IMPORTANT for IE mode -->
    <!-- http://www.metatags.info/all_meta_tags -->
    <meta name="KEYWORDS" content="Tandridge District Council branded page" />
    <meta name="description" content="Tandridge District Council content in a branded page." />
    <meta name="revisit-after" content="5 days" />
    <meta name="distribution" content="GLOBAL" />
    <meta name="author" content="Rob Collins ICT" />
    <meta name="copyright" content="Tandridge District Council 2015" />
    <meta name="googlebot" content="noodp" />
    <meta name="language" content="english" />
    <meta name="reply-to" content="CustomerService@Tandridge.gov.uk" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1 " />
 	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="ROBOTS" content="index, nofollow" />
    <meta name="rating" content="General" />

    <asp:Literal ID="litBrandInHeader" runat="server"></asp:Literal>
    <asp:Literal ID="litImportedFormHead" runat="server"></asp:Literal>

    <style type="text/css">
        .container_12 .grid_9,
        .container_16 .grid_12 { max-width: inherit; width: inherit; }
        .maincontent { overflow: auto; }
    </style>

</head>
<%--<body class="wrapper">--%>
<%--   <asp:Literal ID="litBranding2BodyTag" runat="server"></asp:Literal>
    <asp:Literal ID="litBranding3PreMenu" runat="server"></asp:Literal>
    <asp:Literal ID="litBranding4LHMenu" runat="server"></asp:Literal>--%>


<!-- ==========================================================================-->
<!-- Here is where a designer would place the body of their content -->
<!-- ==========================================================================-->

<%--	<div id="TDCInfo">--%>


<asp:literal id="litImportedFormBody" runat="server"></asp:literal>


<%--	</div>--%>


<!-- ==========================================================================-->
<!-- End of body content -->
<!-- ==========================================================================-->


<%--	<div id="AfterBranding5"></div>
    <asp:Literal ID="litBranding5Footer" runat="server"></asp:Literal>

</body>--%>
</html>
