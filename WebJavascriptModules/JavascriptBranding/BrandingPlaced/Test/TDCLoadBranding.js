﻿
var g_sTypeService = "";
if (/(dev|localhost)/i.test(location.pathname)) g_sTypeService = "Test"; //"Dev";
if (g_sTypeService == "" && /test/i.test(location.pathname)) g_sTypeService = "Test";

var bShowErrors = true

var sURLBrandingLoc = "//tdcws01.tandridge.gov.uk/TDCWebAppsPublic/BrandingSplit2015" + g_sTypeService + "/";
var sAlternativeRoot = sURLBrandingLoc + "StaticBranding/";

function fnTDCExtractFromURL(sURLLocation, sAlternative, TagIDToInsertBefore, HeaderTagNameToInsertBefore) {
	var xmlCORSHttp; // See http://www.html5rocks.com/en/tutorials/cors/#toc-adding-cors-support-to-the-server
	var xmlHttp;
	var theURL = "";
	var sHTTPResponse = "";
	var sProtocol;
	var sResponseText = "";
	var sReturn;
	var oBeforeElement;
	var bUsedAlternative = false;

	if (typeof TagIDToInsertBefore === "undefined" || TagIDToInsertBefore == "") {
		if (typeof HeaderTagNameToInsertBefore === "undefined" || HeaderTagNameToInsertBefore == "") {

		} else {
			try { oBeforeElement = document.getElementsByName(HeaderTagNameToInsertBefore)[0]; } catch (e) { };
		}
	} else {
		try { oBeforeElement = document.getElementById(TagIDToInsertBefore); } catch (e) { };
	};

	sProtocol = document.location.protocol;
	theURL = sURLLocation;
	do {

		if (sProtocol != "http:" && sProtocol != "https:") {
			try { sProtocol = (("https:" == document.location.protocol) ? "https:" : "http:"); } catch (e) { sProtocol = "http:"; }
			theURL = sProtocol + theURL;
		}

		/* running locally on IE5.5, IE6, IE7 */
		if (location.protocol == "file:") {
			if (typeof xmlHttp === "undefined") try { xmlHttp = new ActiveXObject("MSXML2.XMLHTTP"); } catch (e) { }
			if (typeof xmlHttp === "undefined") try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }
		};
		if (typeof xmlHttp === "undefined" && !(typeof XMLHttpRequest === "undefined")) try { xmlHttp = new XMLHttpRequest(); } catch (e) { }
		/* IE7, Firefox, Safari, Opera...  */
		if (typeof xmlHttp === "undefined" && !(typeof XMLHttpRequest2 === "undefined")) try { xmlHttp = new XMLHttpRequest2(); } catch (e) { }
		/* IE6 */
		if (typeof xmlHttp === "undefined" && !(typeof ActiveXObject === "undefined")) try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }

		try {
			//if (!(typeof XDomainRequest === "undefined")) try { xmlCORSHttp = new XDomainRequest(); } catch (e) { }
			try {
				if (!(typeof XDomainRequest === "undefined")) {
					xmlCORSHttp = new XDomainRequest();
					try {
						if (!(typeof xmlCORSHttp === "undefined")) {

							xmlCORSHttp.open("GET", theURL);
							xmlCORSHttp.send(null);
							if (xmlCORSHttp.status == 200) {
								sResponseText = xmlCORSHttp.responseText;
								sReturn = sResponseText;
							}
							//if (oBeforeElement != "") document.write(sReturn);
						}
					} catch (e) { };
				}
			} catch (e) { };

			if (typeof sResponseText === "undefined" || sResponseText == "") {
				sReturn = "";
				if (!(typeof xmlHttp === "undefined")) {
					xmlHttp.open("GET", theURL, false);
					xmlHttp.send(null);
					if (xmlHttp.status == 200 || xmlHttp.status == 0) {
						sResponseText = xmlHttp.responseText;
						sReturn = sResponseText;
						//if (oBeforeElement != "") document.write(sReturn);
					}
				}
			}
		} catch (err) {
			txt = "There was an error " + (err.number & 0xFFFF) + " " + err.name + " on this page.\n\n";
			txt += "Error message: " + err.message + "\n\n";
			if (err.description != undefined) { txt += "Error description: " + err.description + "\n\n"; }
			sReturn = "<!--" + txt + "-->";
		} finally {
			if (sReturn === "undefined") sReturn = '';
			if (sReturn.search(/Tandridge District Council Error response/i) > 0) sReturn = '';
			if (document.location.protocol == "http:") {
				sReturn = sReturn.replace(/https:/ig, "http:");
			} else if (document.location.protocol == "https:") {
				sReturn = sReturn.replace(/http:/ig, "https:");
			} else {
				sReturn = sReturn.replace(/"\/\//ig, '"http://').replace(/'\/\//ig, "'http://").replace(/="www./ig, '="http://www.').replace(/='www./ig, "='http://www.")
			};
		}
		if (bUsedAlternative) {
			theURL = "";
		} else {
			if ((typeof sResponseText === "undefined" || sResponseText == "")) {
				theURL = sAlternative;
				bUsedAlternative = true;
				sReturn = "";
			}
		}
	}
	while (theURL != "" && (typeof sResponseText === "undefined" || sResponseText == "") && (typeof TagIDToInsertBefore === "undefined" || TagIDToInsertBefore == ""))

	return sReturn;
}



function fnTDCInsertResponseBeforeTag(HTMLToInsert, oBeforeElement) {
	var sParentHTML = "";
	var oParentNode = oBeforeElement.parentNode;

	try {
		if (typeof oBeforeElement === "undefined") {
			document.write(HTMLToInsert);
		} else {
			if (typeof oParentNode === "undefined" || oParentNode == null) {
				try { oParentNode = document.body; } catch (e) { oParentNode = void 0; };
			}

			if (typeof oParentNode === "undefined" || oParentNode == null) {
				oBeforeElement.insertAdjacentHTML("beforebegin", HTMLToInsert);
			} else {
				sParentHTML = oParentNode.innerHTML;
				sParentHTML = sParentHTML.substring(0, sParentHTML.indexOf(oBeforeElement.outerHTML)) + HTMLToInsert + sParentHTML.substring(sParentHTML.indexOf(oBeforeElement.outerHTML));

				oParentNode.innerHTML = sParentHTML;
			}
			// This is needed as sometimes the entire HTML DOM is regenerated on HTML changes and triggers need to be reattached
			if (!(typeof fnSetJQuerytriggers === "undefined")) fnSetJQuerytriggers();
		}


	} catch (err) {
		txt = "There was an error " + (err.number & 0xFFFF) + " " + err.name + " on this page.\n\n";
		txt += "Error message: " + err.message + "\n\n";
		if (err.description != undefined) { txt += "Error description: " + err.description + "\n\n"; }
		sReturn = "<!--" + txt + "-->";
	};
}



function fnPrepTDCJQueryFunctionsBranding() {
	$(window).load(function () {
		fnTDCLoadAllBranding();
	})
}



function fnPlaceHEADDataTags(HeadHTML) {
	var head = document.getElementsByTagName('head')[0]
	var oDomHTML = $.parseHTML(HeadHTML);

	for (var iTag in oDomHTML) {
		var oTag = oDomHTML[iTag];
		if (typeof oTag.tagName !== "undefined") {
			try {
				$(head).append(oTag);
			} catch (ex) { };
		}
	}
}



function fnTDCLoadAllBranding() {
	//var oElementBefore
	var sHTMLToInsertBefore, sHTMLToInsertAfter, sCurrentInnerHTML;

	sHTMLToInsert = fnTDCExtractFromURL(sURLBrandingLoc + "Branding1HeadTagPartSansScripts.aspx", "", "");
	fnPlaceHEADDataTags(sHTMLToInsert);

	oBody = $("body").get(0);
	//oElementAfter = $(oBody).children().first().get(0);
	//if (!(typeof oElementAfter === "undefined")) {
	sCurrentInnerHTML = oBody.innerHTML;
	//sHTMLToInsert = fnTDCExtractFromURL(sURLBrandingLoc + "Branding2BodyTagStartSansScripts.aspx", sAlternativeRoot + "Branding2BodyTagStartSansScripts.htm","","");
	sHTMLToInsertBefore = fnTDCExtractFromURL(sURLBrandingLoc + "Branding3_AllBodyPreMenuSansScripts.aspx", sAlternativeRoot + "Branding3_AllBodyPreMenuSansScripts.htm", "", "");
	//$(sHTMLToInsert).insertBefore(oElementAfter);
	sHTMLToInsertBefore = sHTMLToInsertBefore + fnTDCExtractFromURL(sURLBrandingLoc + "Branding4BodyLHMenuPart.aspx", sAlternativeRoot + "Branding4BodyLHMenuPart.htm", "AfterBranding4BodyLHMenu", "", "");
	//$(sHTMLToInsert).insertBefore(oElementAfter);
	sHTMLToInsertAfter = fnTDCExtractFromURL(sURLBrandingLoc + "Branding5FootPartSansScripts.aspx", sAlternativeRoot + "Branding5FootPartSansScripts.htm", "", "");
	//$(sHTMLToInsert).insertAfter(oElementBefore);
	oBody.innerHTML = sHTMLToInsertBefore + sCurrentInnerHTML + sHTMLToInsertAfter;

	//}

	//oElementBefore = $("body").children().last().get(0);
	//if (!(typeof oElementBefore === "undefined")) {
	//    sHTMLToInsert = fnTDCExtractFromURL(sURLBrandingLoc + "Branding5FootPartSansScripts.aspx", sAlternativeRoot + "Branding5FootPartSansScripts.htm", "", "");
	//    $(sHTMLToInsert).insertAfter(oElementBefore);
	//}


}


fnPrepTDCJQueryFunctionsBranding();
