﻿
var bShowErrors = true

if (typeof g_sLocalHost === "undefined") var g_sLocalHost = "//" + location.host;
if (typeof g_sLocalPath === "undefined") var g_sLocalPath = g_sLocalHost + (location.pathname.substr(-1, 1) == "/") ? location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1) : location.pathname;
if (typeof g_sTargetHost === "undefined") var g_sTargetHost = g_sLocalHost;
if (typeof g_sTestTargetHost === "undefined") {
	var g_sTestTargetHost = g_sTargetHost.toLowerCase();
	if (g_sTestTargetHost.indexOf("localhost") >= 0 || g_sTestTargetHost.indexOf("tfaforms") >= 0 ||  g_sTestTargetHost.indexOf("dev01") >= 0 || g_sTestTargetHost.indexOf("netint02") >= 0) g_sTargetHost = "//TDCWebApps.tandridge.gov.uk";
}
if (typeof g_sPublicHostPath === "undefined") var g_sPublicHostPath = g_sTargetHost + (location.hostname.toUpperCase().indexOf("TDCWS01") >= 0 ? "/TDCWebAppsPublic/" : "/");

function fnPrepTDCJQueryFunctionsLLPGSearch() {
    $(document).ready(
        function () {
            $.extend($.expr[':'], {
                'containsi': function (elem, i, match, array) {
                    return (elem.textContent || elem.innerText || '').toLowerCase()
                    .indexOf((match[3] || "").toLowerCase()) >= 0;
                }
            });

            $("label:containsi('Postcode'),label:containsi('Post code')").map(function () {
                var oParent = $(this).parent().get(0);
                var oInputOfPostcode = $(oParent).find("input:text").get(0);
                if (typeof oInputOfPostcode !== "undefined") {
                    var oLastElement = $(oParent).children().last().get(0);
                    fnTDCPrepareElements(oInputOfPostcode, oLastElement);
                }
            })


            $("label:containsi('Postcode'),label:containsi('Post code')").parent().find("input:text:first").keyup(function () {

                fnTDCCheckForPostcodeText(this);

            })


            $("button[id*='cmdSearch'][id$='Postcode']").click(function () {

                fnTDCClickForPostcodeList(this);

            });



            $("*").mouseup(function (e) {
                var oPopOutContainer = $(".TDCPopoutLargePanel:visible");
                var oOfAddressPanel = $(this).parents(".TDCPopoutLargePanel").get(0);
                var oVisiblePanel = oPopOutContainer.get(0);
                if (!oPopOutContainer.is(e.target) // if the target of the click isn't the container...
            && oPopOutContainer.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    if (typeof oOfAddressPanel === "undefined" && !(typeof oVisiblePanel === "undefined")) {
                        $(oPopOutContainer).map(function () {
                            $(this).hide();
                            $(this).parents("div[id$='Searching']").hide();
                        });
                        //fnValidateFieldOfSubmission(this)
                    }
                }
            })


            $("*").keydown(function () {
                var oOfAddressPanel = $(this).parents(".TDCPopoutLargePanel").get(0);
                var oVisiblePanel = $(".TDCPopoutLargePanel:visible").get(0);
                if (typeof oOfAddressPanel === "undefined" && !(typeof oVisiblePanel === "undefined")) {
                    $(".TDCPopoutLargePanel:visible").map(function () {
                        $(this).hide();
                        $(this).parents("div[id$='Searching']").hide();
                    });
                    //fnValidateFieldOfSubmission(this)
                }
            })

        })
}



/* 
==========================================================================
  Function to call on entering text to use as a postcode
  Will show the button associated to the text area if the text is
  a regex match to a postcode
==========================================================================
*/

function fnTDCCheckForPostcodeText(oTextField) {
    var oRegPostcode = new RegExp(/(((^[BEGLMNS][1-9]\d?)|(^W[2-9])|(^(A[BL]|B[ABDHLNRST]|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]|F[KY]|G[LUY]|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]|M[EKL]|N[EGNPRW]|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKL-PRSTWY]|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)\d\d?)|(^W1[A-HJKSTUW0-9])|(((^WC[1-2])|(^EC[1-4])|(^SW1))[ABEHMNPRVWXY]))(\s*)?([0-9][ABD-HJLNP-UW-Z]{2}))|(^GIR\s?0AA)/i);
    var sValueOfField = $(oTextField).val();

    var oParentContainer = $(oTextField).parents('div:has(":button[id^=\'cmdSearch\'][id*=\'Postcode\']"),fieldset:has(":button[id^=\'cmdSearch\'][id*=\'Postcode\']") ').get(0);
    var oSearchButton = $(oParentContainer).find("[id^='cmdSearch'][id*='Postcode']").get(0);

    if (typeof oSearchButton !== "undefined") {
        if (oRegPostcode.test(sValueOfField)) {
            $(oSearchButton).show();
        } else {
            $(oSearchButton).hide();
        }
    }

}


/* 
==========================================================================
  Function to call on clicking on the button to search for a postcode
==========================================================================
*/

function fnTDCClickForPostcodeList(oButton) {

    var oParent = $(oButton).parents('div:has("div[id^=\'div\'][id$=\'AddrPicklist\']"),fieldset:has("div[id^=\'div\'][id$=\'AddrPicklist\']")').get(0);
    var oDivAddressListContainer = $(oParent).find("div[id^='div'][id$='AddrPicklist']").get(0);
    if (typeof oDivAddressListContainer === "undefined") {
        oParent = $(oParent).parent().get(0);
        oDivAddressListContainer = $(oParent).find("div[id^='div'][id$='AddrPicklist']").get(0);
        if (typeof oDivAddressListContainer === "undefined") {
            oParent = $(oParent).parent().get(0);
        }
    }
    var oPCodeTextBox = $(oParent).find("input:text").get(0);
    var oTxtAddress = $(oParent).find("textarea").get(0);
    if (typeof oTxtAddress === 'undefined') {
        var oParentOfTextArea = $(oParent).parents('div:has(textarea),fieldset:has(textarea)').get(0);
        var oParentTextArea = $(oParentOfTextArea).find("div:has(textarea),fieldset:has(textarea)").get(0);
        oTxtAddress = $(oParentTextArea).find("textarea").get(0);
    }
    var sPCode = $(oPCodeTextBox).val();
    if (typeof sPCode === "undefined") sPCode = "";
    sPCode = sPCode.trim();
    $(oTxtAddress).val("");
    if (sPCode != "") fnTDCCallServiceForPostcode(sPCode, oParent, oTxtAddress);

}

/* 
==========================================================================
   Add HTML to give a button and a displayed list after the
   designated element, also defining the element for
   the postcode and  for populating with the address
==========================================================================
*/

function fnTDCPrepareElements(oPostcodeControl, oPositionElem) {
    var sIdentifier = "";
    if (typeof oPositionElem === "undefined") oPositionElem = oPostcodeControl;

    sIdentifier = oPostcodeControl.id;
    if (typeof sIdentifier === "undefined" || sIdentifier == "") {
        sIdentifier = $(oPostcodeControl).parent().find(":input").get(0).id;
    }

    var oExtraDetails = '<button type="button" id="cmdSearch' + sIdentifier + 'Postcode" class="TDCRounded" name="cmdSearch' + sIdentifier + 'Postcode" style="display: none;">Show Postcode Addresses</button><br/>\
<div id="div' + sIdentifier + 'AddressOfPostcode" class="TDCFieldInfo" style="display:none;"></div>\
<div id="div' + sIdentifier + 'SearchingMessage" style="display: none;" class="TDCFieldBlock">\
<div id="div' + sIdentifier + 'ShowSearching" style="color: DarkRed; padding: 10px 10px 10px 10px; display: inline;"><h2>Searching for addresses of postcode</h2></div>\
</div>\
<div id="div' + sIdentifier + 'AddrPicklist" style="display: none;" class="TDCFieldBlock">\
<h3>Pick your address from these addresses</h3>\
<div id="divSelect' + sIdentifier + 'Address" name="divSelect' + sIdentifier + 'Address" style="background-color: wheat;">\
</div>\
</div>';

    $(oExtraDetails).insertAfter(oPositionElem);

}


/*
==========================================================================
            Validate a post code and return the XML of the TDC NLPG entries
    See : http://www.virtualsecrets.com/jquery-send-receive-xml-json.html
        where it mentions about Soap requests
    READ SOAP-XML AND SEND SOAP-XML WITH ASMX WEB SERVICE
==========================================================================
==========================================================================
Validate a post code and return the XML of the TDC NLPG entries
==========================================================================
*/

function fnTDCCallServiceForPostcode(sPostcode, oParentDiv, oAddressTextArea) {
    var oRegPostcode = new RegExp(/(((^[BEGLMNS][1-9]\d?)|(^W[2-9])|(^(A[BL]|B[ABDHLNRST]|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]|F[KY]|G[LUY]|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]|M[EKL]|N[EGNPRW]|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKL-PRSTWY]|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)\d\d?)|(^W1[A-HJKSTUW0-9])|(((^WC[1-2])|(^EC[1-4])|(^SW1))[ABEHMNPRVWXY]))(\s*)?([0-9][ABD-HJLNP-UW-Z]{2}))|(^GIR\s?0AA)/i)
    var oRegPostcodePostSpace = new RegExp(/((\s*)?([0-9][ABD-HJLNP-UW-Z]{2}))|(\s?0AA)/i)

    try {
        var sTemp = sPostcode;
        if (oRegPostcode.test(sTemp)) {
            var lTemp = sTemp.search(oRegPostcodePostSpace);
            if (lTemp > 0) sTemp = sTemp.replace(oRegPostcodePostSpace, " $&").replace("  ", " ");

            var oDivAddressListContainer = $(oParentDiv).find("div[id^='div'][id$='AddrPicklist']").get(0);

            var oDivAddressList = $(oDivAddressListContainer).find("div[id^='divSelect'][id$='Address']").get(0);
            var oDivPCodeSearchMsg = $(oParentDiv).find("div[id^='div'][id$='PostCodeSearching']").get(0);
            var oDivSearchingMsg = $(oParentDiv).find("div[id^='div'][id$='SearchingMessage']").get(0);

            $(oDivPCodeSearchMsg).show();
            $(oDivSearchingMsg).show();
            $(oDivAddressListContainer).hide();

            var xmlRequest = '<?xml version="1.0" encoding="utf-8"?>\
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">\
  <soap12:Body>\
    <Search_x0020_by_x0020_Post_x0020_Code xmlns="http://www.tandridge.gov.uk/Webservices/">\
      <SearchPostCode>' + sTemp + '</SearchPostCode>\
      <RefTypes></RefTypes>\
      <ReturnRefTypes></ReturnRefTypes>\
      <sUseDate></sUseDate>\
      <sSearchAlternatives></sSearchAlternatives>\
    </Search_x0020_by_x0020_Post_x0020_Code>\
  </soap12:Body>\
</soap12:Envelope>';

            /* Define AJAX Settings */

            //var WebSvcURL = "//TDCWS01.tandridge.gov.uk/WebServices/TDCLLPGSearch/LLPGQuery.asmx";
			var WebSvcURL = g_sPublicHostPath + "WebServices/TDCLLPGSearch/LLPGQuery.asmx";
            var TestHost = location.hostname.toLowerCase();
            //if (TestHost.indexOf("localhost") >= 0 || TestHost.indexOf("dev01") >= 0 || TestHost.indexOf("netint02") >= 0) {
            //    WebSvcURL = "//netint02.tandridge.gov.uk/WebServices/TDCLLPGSearch/LLPGQuery.asmx";
            //}

            var ajaxURL = WebSvcURL + "?op=Search_x0020_by_x0020_Post_x0020_Code";
            var ajaxType = "POST";
            var ajaxAcceptedResponseFormat = "text/xml";
            var ajaxAcceptedResponseTxt = "text/plain";
            var ajaxResponseParseMethod = "xml";
            var ajaxContentType = "text/xml; charset=\"utf-8\"";
            var ajaxAsynchronous = true;
            var ajaxCache = false;
            var ajaxCrossDomain = true;
            var ajaxDataToTarget = xmlRequest; /* This is the XML encoded message*/

            var ajaxUseGlobalHandlers = true;
            var ajaxUsername = "";
            var ajaxPassword = "";
            var ajaxTimeout = 5000;

            /* Handle XML Response (in this example just getting back the XML that was sent to the server) */
            var haveRootNodeTag = "Info";
            var haveChildNodeTag = "Item";

            var haveExpectedFormat = 0;
            var haveExpectedResponse = 0;
            var haveNonExpected = "";
            var haveRawResponse = 0;
            var haveRawResponseData = "";

            $.ajax({
                accepts: { xml: ajaxAcceptedResponseFormat, text: ajaxAcceptedResponseTxt }	/* Accepted response data from the target */
                , global: false															/* Do not use global callback functions */
                , type: ajaxType														/* HEAD/GET/POST/DELETE */
                , contentType: ajaxContentType							/* Default content type */
                , url: ajaxURL															/* Target URL */
                , async: ajaxAsynchronous									/* if false, may lock browser until response is received */
                , cache: ajaxCache												/* true - will cache URL data returned; false - will not cache URL data but only for HEAD AND GET requests */
                , converters: { "text xml": jQuery.parseXML }		/* Formats response - {"* text": window.String, "text html": true, "text json": jQuery.parseJSON, "text xml": jQuery.parseXML} */
                , crossDomain: ajaxCrossDomain					/* Setting to true allows cross domain requests on the same domain and server-side redirection to another domain */
                , data: ajaxDataToTarget				/* Data to send to the target which is appended to the URL for GET requests as name/value pairs like { name: "John", location: "Boston" } or XML like var xmlDocument = [create xml document here]; declared outside the block of code and data: having a value of xmlDocument */
                , dataType: ajaxResponseParseMethod			/* "xml": Returns a XML document that can be processed via jQuery, "html": Returns HTML as plain text; included script tags are evaluated when inserted in the DOM, "script": Evaluates the response as JavaScript and returns it as plain text, "json": Evaluates the response as JSON and returns a JavaScript object, "jsonp": Loads in a JSON block using JSONP, "text": A plain text string */
                , global: ajaxUseGlobalHandlers						/* true - allows global ajax event handlers to be used for request, false - no...we are handling it in the .ajax below */
                , ifModified: false													/* true - allow request success if response has changed since last request, false - ignore */
                , username: ajaxUsername								/* username to be used in response to a http authentication request */
                , password: ajaxPassword									/* password to be used in response to a http authentication request */
                , timeout: ajaxTimeout
                ,
                dataType: "xml",
                success: function (response) {
                    fnTDCProcessAjaxPostcodeResult("Ajax Success ", response, oParentDiv, oAddressTextArea);
                },
                failure: function (response) {
                    $(oParentDiv).find("div[id^='div'][id$='AddressOfPostcode']").html("No addresses found");
                    fnTDCProcessFailPostcodeResult("Ajax Failure ", response, oParentDiv);
                },
                error: function (response) {
                    $(oParentDiv).find("div[id^='div'][id$='AddressOfPostcode']").html("No addresses found");
                    fnTDCProcessFailPostcodeResult("Ajax Error ", response, oParentDiv);
                }
            });
        }
    } catch (err) {
        if (bShowErrors) alert("fnTDCCallServiceForPostcode : Error " + err.name + "\n" + err.message + "\n" + err.description + "\n" + Message + "\n");
    }
}



function fnTDCProcessFailPostcodeResult(Message, response, oParentDiv) {
    try {
        alert(Message + "\n" + response);
    } catch (err) {
        if (bShowErrors) alert("ProcessFailResult : Error " + err.name + "\n" + err.message + Message + "\n" + responseXML + "\n" + err.stack);
    }
}

/*
==========================================================================
   Process the return of TDC NLPG entries, or the failure of the return
==========================================================================
*/

function fnTDCProcessAjaxPostcodeResult(Message, responseXML, oParentDiv, oAddressTextArea) {
    try {
        xmlAddresses = responseXML;

        var oDivAddressListContainer = $(oParentDiv).find("div[id^='div'][id$='AddrPicklist']").get(0);
        if (typeof oDivAddressListContainer === "undefined") {
            oParentDiv = $(oParentDiv).parent;
            oDivAddressListContainer = $(oParentDiv).find("div[id^='div'][id$='AddrPicklist']").get(0);
        }
        var oPCodeTextBos = $(oParentDiv).find("input[id*='txt'][id$='Postcode']").get(0);
        var oDivAddressList = $(oDivAddressListContainer).find("div[id^='divSelect'][id$='Address']").get(0);
        var oTxtAddress = oAddressTextArea;
        if (typeof oTxtAddress === 'undefined') oTxtAddress = $(oParentDiv).find("textarea").get(0);
        if (typeof oTxtAddress === 'undefined') {
            var oParentOfParent = $(oParent).parent().get(0);
            var oParentTextArea = $(oParentOf).nextUntil(":has(textarea)").get(0);
            oTxtAddress = oParentTextArea.find("textarea").get(0);
        }

        $(oParentDiv).find("div[id^='div'][id$='SearchingMessage']").hide();
        $(oDivAddressList).text("");
        $(oDivAddressListContainer).hide();

        $(oParentDiv).find("div[id^='div'][id$='AddressOfPostcode']").html("No addresses found");

        if ($(responseXML).find("LLPGRecord:first").find("Town:first").text() != "" || $(responseXML).find("LLPGRecord:first").find("PostTown:first").text() != "") {
            var sLocality = $(responseXML).find("LLPGRecord:first").find("Locality:first").text();
            var sPostTown = $(responseXML).find("LLPGRecord:first").find("PostTown:first").text();
            var sTown = $(responseXML).find("LLPGRecord:first").find("Town:first").text();

            $(oParentDiv).find("div[id^='div'][id$='AddressOfPostcode']").html(sLocality + "<br />" + sTown + "<br />" + (sTown != sPostTown) ? sPostTown : "");

            if ($(responseXML).find("LLPGRecord").length > 0) {
                $(responseXML).find("LLPGRecord").each(function () {
                    var sUPRN = $(this).find("UPRN:first").text();
                    var sAddress = $(this).find("HouseAddress:first").text();
                    $(oDivAddressList).append('<label><input type="radio" id="rdoAddress" name="rdoAddress" value="' + sUPRN + '" />' + sAddress + '</label><br />');
                });    // End of 'each'

                var aRadios = $(oDivAddressList).find(":radio");
                aRadios.on('click', function () {
                    fnTDCProcessUPRNSelected(this);
                });

                $(oDivAddressListContainer).addClass("TDCPopoutLargePanel")
                $(oDivAddressListContainer).show();
            }
        }

    } catch (err) {
        if (bShowErrors) alert("fnTDCProcessAjaxPostcodeResult : Error " + err.name + "\n" + err.message + "\n" + Message + "\n" + fnXmlToString(responseXML) + "\n" + err.stack);
    }
}

/*
==========================================================================
        Process selecting of a single TDC NLPG entry for address
 ==========================================================================
 */

function fnTDCProcessUPRNSelected(oRdoUPRN) {
    try {
        var sUPRN = oRdoUPRN.value;

        var oParent = $(oRdoUPRN).parents('div:has("div[id^=\'div\'][id$=\'AddrPicklist\']"),fieldset:has("div[id^=\'div\'][id$=\'AddrPicklist\']")').get(0);
        var oDivAddressListContainer = $(oParent).find("div[id^='div'][id$='AddrPicklist']").get(0);
        if (typeof oDivAddressListContainer === "undefined") {
            oParent = $(oParent).parent().get(0);
            oDivAddressListContainer = $(oParent).find("div[id^='div'][id$='AddrPicklist']").get(0);
            if (typeof oDivAddressListContainer === "undefined") {
                oParent = $(oParent).parent().get(0);
            }
        }

        var oParentOfTextArea = $(oParent).parents('div:has(textarea),fieldset:has(textarea)').get(0);
        var oParentTextArea = $(oParentOfTextArea).find("div:has(textarea),fieldset:has(textarea)").get(0);
        var oTxtAddress = $(oParent).find("textarea").get(0);
        if (typeof oTxtAddress === 'undefined') {
            oTxtAddress = $(oParentTextArea).find("textarea").get(0);
        }

        var oLblUPRN = $(oParent).find("label:containsi('uprn')").get(0);
        if (typeof oLblUPRN === 'undefined') { oLblUPRN = $(oParentTextArea).find("label:containsi('uprn')").get(0); }
        if (typeof oLblUPRN === 'undefined') { oLblUPRN = $(oParentOfTextArea).find("label:containsi('uprn')").get(0); }
        var oTxtUPRN;
        var oUPRNParent;

        if (typeof oLblUPRN !== 'undefined') {

            oUPRNParent = $(oParent).parents("div:has(:text),fieldset:has(:text),div:has(input[type='hidden']),fieldset:has(input[type='hidden'])").get(0);
            oTxtUPRN = $(oLblUPRN).next(":text").get(0);
            if (typeof oTxtUPRN === "undefined") oTxtUPRN = $(oLblUPRN).next("[type='hidden'][class*='UPRN'],[type='hidden'][class*='uprn'],[type='hidden'][id*='UPRN'],[type='hidden'][id*='uprn']").get(0);
            if (typeof oTxtUPRN === "undefined") oTxtUPRN = $(oLblUPRN).parent().find(":text,[type='hidden'][class*='UPRN'],[type='hidden'][class*='uprn'],[type='hidden'][id*='UPRN'],[type='hidden'][id*='uprn']").get(0);
            if (typeof oTxtUPRN === "undefined") oTxtUPRN = $(oLblUPRN).parent().parent().find(":text,[type='hidden'][class*='UPRN'],[type='hidden'][class*='uprn'],[type='hidden'][id*='UPRN'],[type='hidden'][id*='uprn']").get(0);
            if (typeof oTxtUPRN === "undefined") oTxtUPRN = $(oLblUPRN).parent().parent().parent().find(":text[id*='UPRN'],:text[id*='uprn'],[type='hidden'][class*='UPRN'],[type='hidden'][class*='uprn'],[type='hidden'][id*='UPRN'],[type='hidden'][id*='uprn']").get(0);
        }
        if (typeof oTxtUPRN === "undefined") {

            if (typeof oUPRNParent === "undefined") { oUPRNParent = $(oTxtAddress).parents("div:has(:text),fieldset:has(:text),div:has(input[type='hidden']),fieldset:has(input[type='hidden'])").get(0); }
            if (typeof oUPRNParent !== "undefined" && typeof oLblUPRN !== "undefined") oTxtUPRN = $(oUPRNParent).find(":text[id*='UPRN']:text[id*='uprn'],[type='hidden'][class*='UPRN'],[type='hidden'][class*='uprn'],[type='hidden'][id*='UPRN'],[type='hidden'][id*='uprn']").get(0);
            if (typeof oUPRNParent !== "undefined" && typeof oLblUPRN === "undefined") oTxtUPRN = $(oUPRNParent).find("[type='hidden'][class*='UPRN'],[type='hidden'][class*='uprn'],[type='hidden'][id*='UPRN'],[type='hidden'][id*='uprn']").get(0);
        }

        $(xmlAddresses).find("LLPGRecord").has("UPRN:contains('" + sUPRN + "')").map(function () {
            var sAddress = $(this).find("FullAddrLine1:first").text();
            var sTemp = $(this).find("FullAddrLine2:first").text();
            if (sTemp != "") sAddress = sAddress + ", \r" + sTemp;
            sTemp = $(this).find("FullAddrLine3:first").text();
            if (sTemp != "") sAddress = sAddress + ", \r" + sTemp;
            sTemp = $(this).find("Locality:first").text();
            if (sTemp != "") sAddress = sAddress + ", \r" + sTemp;
            sTemp = $(this).find("Town:first").text();
            if (sTemp != "") sAddress = sAddress + ", \r" + sTemp;
            sTemp = $(this).find("PostTown:first").text() == $(this).find("Town:first").text() ? "" : $(this).find("PostTown:first").text();
            if (sTemp != "") sAddress = sAddress + ", \r" + sTemp;
            sTemp = $(this).find("PostCode:first").text();
            if (sTemp != "") sAddress = sAddress + ", \r" + sTemp;
            $(oTxtAddress).val(sAddress);
            $(oDivAddressListContainer).hide();
            if (typeof oTxtUPRN !== "undefined") $(oTxtUPRN).val(sUPRN);
        });
    } catch (err) {
        if (bShowErrors) alert("fnTDCProcessUPRNSelected : Error " + err.name + "\n" + err.message + "\nUPRN: " + sUPRN + "\n" + fnXmlToString(xmlAddresses) + "\n" + err.stack);
    }
}

fnPrepTDCJQueryFunctionsLLPGSearch();
