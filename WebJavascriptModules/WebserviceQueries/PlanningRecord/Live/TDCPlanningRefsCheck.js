﻿
var bShowErrors = false;

function fnPrepTDCJQueryFunctionsLLPGSearch() {
    $(document).ready(
        function () {
            $.extend($.expr[':'], {
                'containsi': function (elem, i, match, array) {
                    return (elem.textContent || elem.innerText || '').toLowerCase()
                    .indexOf((match[3] || "").toLowerCase()) >= 0;
                }
            });

            $("label:containsi('Planning Ref'),label:containsi('Plan Ref')").map(function () {
                var oParent = $(this).parent().get(0);
                var oInputOfReference = $(oParent).find("input:text").get(0);
                if (typeof oInputOfReference !== "undefined") {
                    var oLastElement = $(oParent).children().last().get(0);
                    fnTDCPrepareElements(oInputOfReference, oLastElement);
                }
            })


            $("label:containsi('Planning Ref'),label:containsi('Plan Ref')").parent().find("input:text:first").focusout(function () {

                fnTDCCheckForRefDetails(this);

            })

            $("label:containsi('Planning Ref'),label:containsi('Plan Ref')").parent().find("input:text:first").map(function () {

                if ($(this).text != "") fnTDCCheckForRefDetails(this);

            })
        })
}



/* 
==========================================================================
  Function to call on entering text to use as a postcode
  Will show the button associated to the text area if the text is
  a regex match to a postcode
==========================================================================
*/

function fnTDCCheckForRefDetails(oTextField) {
    var oRegRef = new RegExp(/\w+(\/\w+)+/i);
    var sValueOfField = $(oTextField).val();

    var oParentContainer = $(oTextField).parents('div:has(div[id*="DetailsOfReference"]) ').get(0);
    var oDetailsDiv = $(oParentContainer).find("div[id*='DetailsOfReference']").get(0);

    if (typeof oDetailsDiv !== "undefined") {
        if (oRegRef.test(sValueOfField)) {
            fnTDCCallServiceForReference(sValueOfField, oParentContainer, oDetailsDiv);
        } else {
            $(oDetailsDiv).html("")
        }
    }

}


/* 
==========================================================================
   Add HTML to give a details area after the designated element
==========================================================================
*/

function fnTDCPrepareElements(oReferenceControl, oPositionElem) {
    var sIdentifier = "";
    if (typeof oPositionElem === "undefined") oPositionElem = oReferenceControl;

    sIdentifier = oReferenceControl.id;
    if (typeof sIdentifier === "undefined" || sIdentifier == "") {
        sIdentifier = $(oReferenceControl).parent().find(":input").get(0).id;
    }

    var oExtraDetails = '<div id="div' + sIdentifier + 'DetailsOfReference" class="TDCFieldInfo" style="display:none;"></div>\
<div id="div' + sIdentifier + 'SearchingMessage" style="display: none;" class="TDCFieldBlock">\
<div id="div' + sIdentifier + 'ShowSearching" class="TDCMessage"><h2>Searching for details</h2></div>\
</div>';

    $(oExtraDetails).insertAfter(oPositionElem);

}


/*
==========================================================================
  Validate a reference and return the XML of the TDC reference details
==========================================================================
*/

function fnTDCCallServiceForReference(sReference, oParentDiv, oReferenceDetailsArea) {
    var oRegRef = new RegExp(/\w+(\/\w+)+/i);

    try {
        var sTemp = sReference;
        if (oRegRef.test(sTemp)) {

            var oDivRefDetails = $(oParentDiv).find("div[id^='div'][id$='DetailsOfReference']").get(0);
            var oDivSearchingMsg = $(oParentDiv).find("div[id^='div'][id$='SearchingMessage']").get(0);

            $(oDivSearchingMsg).show();
            $(oDivRefDetails).hide();

            var xmlRequestBody = '<GetApplicationDetails xmlns="http://tandridge.gov.uk/PlanArcusData/">\
      <PlanningApplicationRef>' + sTemp + '</PlanningApplicationRef>\
    </GetApplicationDetails >';

            var xmlRequest = '<?xml version="1.0" encoding="utf-8"?>\
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">\
  <soap12:Body>' + xmlRequestBody + '</soap12:Body>\
</soap12:Envelope>';

            /* Define AJAX Settings */

            var WebSvcURL = "//tdcws01.tandridge.gov.uk/WebServices/wsArcusPlanning201504/TDCPlanArcusServices.asmx";
            var TestHost = location.hostname.toLowerCase();
            if (TestHost.indexOf("localhost") >= 0 || TestHost.indexOf("dev01") >= 0 || TestHost.indexOf("netint02") >= 0) {
                WebSvcURL = "//netint02.tandridge.gov.uk/wsArcusPlanning201504/TDCPlanArcusServices.asmx";
            }

            var ajaxURL = WebSvcURL + "?op=GetApplicationDetails";
            var ajaxType = "POST";
            var ajaxAcceptedResponseFormat = "text/xml";
            var ajaxAcceptedResponseTxt = "text/plain";
            var ajaxResponseParseMethod = "xml";
            var ajaxContentType = "text/xml; charset=\"utf-8\"";
            var ajaxAsynchronous = true;
            var ajaxCache = false;
            var ajaxCrossDomain = true;
            var ajaxDataToTarget = xmlRequest; /* This is the XML encoded message*/

            var ajaxUseGlobalHandlers = true;
            var ajaxUsername = "";
            var ajaxPassword = "";
            var ajaxTimeout = 5000;

            /* Handle XML Response (in this example just getting back the XML that was sent to the server) */
            var haveRootNodeTag = "Info";
            var haveChildNodeTag = "Item";

            var haveExpectedFormat = 0;
            var haveExpectedResponse = 0;
            var haveNonExpected = "";
            var haveRawResponse = 0;
            var haveRawResponseData = "";

            $.ajax({
                accepts: { xml: ajaxAcceptedResponseFormat, text: ajaxAcceptedResponseTxt }	/* Accepted response data from the target */
                , global: false															/* Do not use global callback functions */
                , type: ajaxType														/* HEAD/GET/POST/DELETE */
                , contentType: ajaxContentType							/* Default content type */
                , url: ajaxURL															/* Target URL */
                , async: ajaxAsynchronous									/* if false, may lock browser until response is received */
                , cache: ajaxCache												/* true - will cache URL data returned; false - will not cache URL data but only for HEAD AND GET requests */
                , converters: { "text xml": jQuery.parseXML }		/* Formats response - {"* text": window.String, "text html": true, "text json": jQuery.parseJSON, "text xml": jQuery.parseXML} */
                , crossDomain: ajaxCrossDomain					/* Setting to true allows cross domain requests on the same domain and server-side redirection to another domain */
                , data: ajaxDataToTarget				/* Data to send to the target which is appended to the URL for GET requests as name/value pairs like { name: "John", location: "Boston" } or XML like var xmlDocument = [create xml document here]; declared outside the block of code and data: having a value of xmlDocument */
                , dataType: ajaxResponseParseMethod			/* "xml": Returns a XML document that can be processed via jQuery, "html": Returns HTML as plain text; included script tags are evaluated when inserted in the DOM, "script": Evaluates the response as JavaScript and returns it as plain text, "json": Evaluates the response as JSON and returns a JavaScript object, "jsonp": Loads in a JSON block using JSONP, "text": A plain text string */
                , global: ajaxUseGlobalHandlers						/* true - allows global ajax event handlers to be used for request, false - no...we are handling it in the .ajax below */
                , ifModified: false													/* true - allow request success if response has changed since last request, false - ignore */
                , username: ajaxUsername								/* username to be used in response to a http authentication request */
                , password: ajaxPassword									/* password to be used in response to a http authentication request */
                , timeout: ajaxTimeout
                ,
                dataType: "xml",
                success: function (response) {
                    fnTDCProcessAjaxReferenceResult("Ajax Success ", response, oParentDiv, oDivRefDetails);
                },
                failure: function (response) {
                    $(oDivRefDetails).html("No details found");
                    fnTDCProcessFailReferenceResult("Ajax Failure ", response, oParentDiv);
                },
                error: function (response) {
                    $(oDivRefDetails).html("No details found");
                    fnTDCProcessFailReferenceResult("Ajax Error ", response, oParentDiv);
                }
            });
        }
    } catch (err) {
        if (bShowErrors) alert("fnTDCCallServiceForPostcode : Error " + err.name + "\n" + err.message + "\n" + err.description + "\n" + Message + "\n");
    }
}



function fnTDCProcessFailReferenceResult(Message, response, oParentDiv) {
    try {
        var oDivSearchingMsg = $(oParentDiv).find("div[id^='div'][id$='SearchingMessage']").get(0);
        var oDivRefDetails = $(oParentDiv).find("div[id^='div'][id$='DetailsOfReference']").get(0);
        var sMsgOut = "No Reference found";
        if (bShowErrors) sMsgOut += "<br/>" + Message + "<br/>" + response.responseText;
        $(oDivSearchingMsg).html(sMsgOut);
    } catch (err) {
        if (bShowErrors) alert("ProcessFailResult : Error " + err.name + "\n" + err.message + Message + "\n" + response.responseText + "\n" + err.stack);
    }
}

/*
==========================================================================
   Process the return of TDC Reference details, or the failure of the return
==========================================================================
*/

function fnTDCProcessAjaxReferenceResult(Message, responseXML, oParentDiv, oDivRefDetails) {
    try {
        var xmlRefDetails = responseXML;
        var sResult = "";
        var sTemp;
        $(oParentDiv).find("div[id^='div'][id$='SearchingMessage']").hide();
        $(oDivRefDetails).text("");

        $(oDivRefDetails).html("No details found");

        $(responseXML).find("PlanningApplicationDetails:first").map(function () {
            sTemp = $(this).attr("ApplicationNumber"); if (typeof sTemp !== "undefined") sResult += "Application No : " + sTemp;
            sTemp = $(this).attr("DateApplicationReceived"); if (typeof sTemp !== "undefined") sResult += (sResult != "" ? "<br/>" : "") + "Received : " + sTemp;
            sTemp = $(this).find("PrintAddressofSite:first").text(); if (typeof sTemp !== "undefined") sResult += (sResult != "" ? "<br/>" : "") + "Location : " + sTemp;
            $(oDivRefDetails).html(sResult);
        });
        $(oDivRefDetails).show();

    } catch (err) {
        if (bShowErrors) alert("fnTDCProcessAjaxPostcodeResult : Error " + err.name + "\n" + err.message + "\n" + Message + "\n" + fnXmlToString(responseXML) + "\n" + err.stack);
    }
}


fnPrepTDCJQueryFunctionsLLPGSearch();
