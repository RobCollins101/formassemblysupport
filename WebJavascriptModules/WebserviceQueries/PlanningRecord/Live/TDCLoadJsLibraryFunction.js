﻿
function loadJsLibrary(url, success) {
    var script = document.createElement('script');
    script.src = url;

    if (typeof success !== "undefined") {
        document.write(script.outerHTML);
    } else {

        var head = document.getElementsByTagName('head')[0], done = false;
        head.appendChild(script);
        // Attach handlers for all browsers

        script.onload = script.onreadystatechange = function () {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            };
        }
    }
    /*if (typeof jQuery === 'undefined') {
        loadJsLibrary('//tdcws01.tandridge.gov.uk/PublicFiles/JQueryVersions/Versions1.x/jquery-1.12.4.js', null);*/

    /*  loadJsLibrary('http://code.jquery.com/jquery-1.10.2.min.js', function () {
        // Write your jQuery Code
    });
    } else {
    // jQuery was already loaded
    // Write your jQuery Code */
}

